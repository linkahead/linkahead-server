# Set the timezone of the server
# e.g. TIMEZONE=Europe/Berlin or TIMEZONE=UTC.
# Leaving this empty means that the server assumes the timezone of the host.
TIMEZONE=
# Set the name of the server owner
# e.g: SERVER_OWNER=XY Department
SERVER_OWNER=
# Name of this CaosDB Server
SERVER_NAME=CaosDB Server

# --------------------------------------------------
# The following paths are relative to the working directory of the server.
# --------------------------------------------------

# The location(s) of the server side scripting binaries.
# Put your executable python scripts here, if they need to be called from the scripting API.
# The value is a comma or space separated list or a single directory
SERVER_SIDE_SCRIPTING_BIN_DIRS=./scripting/bin/

# Working directory of the server side scripting API.
# On execution of binaries and scripts the server will create a corresponding working directory in this folder.
SERVER_SIDE_SCRIPTING_WORKING_DIR=./scripting/working/

# Home directories of the server side scripting API.
# Specific config files, pip packages or other prerequisites for running a script or binary
# can go into a specific home directory for the respective script within this folder.
SERVER_SIDE_SCRIPTING_HOME_DIR=./scripting/home/

# The CaosDB file system root.
# The file hierarchy of CaosDB's internal file system starts at this folder.
# An absolute file path of File objects within CaosDB is relative to this folder.
FILE_SYSTEM_ROOT=./CaosDBFileSystem/FileSystemRoot/

# Path to the drop off box.
# This is were users can place files that should be picked up by the CaosDB drop off box program.
DROP_OFF_BOX=./CaosDBFileSystem/DropOffBox/

# Location of temporary files
# All temporary files with the exception of files created by the scripting API will go into this folder.
TMP_FILES=./CaosDBFileSystem/TMP/

# Shared folder
# Additional folder for longer term storage of scripting API output.
# In contrast to the script's working directory, these subdirectories are publicly accessible.
SHARED_FOLDER=./CaosDBFileSystem/Shared/

# Path to the chown script which is needed by the drop off box in order to change permissions of files.
CHOWN_SCRIPT=./misc/chown_script/caosdb_chown_dropoffbox

# This file is responsible for setting individual user and group permissions.
USER_SOURCES_INI_FILE=./conf/ext/usersources.ini
# The default state of users which are added to the internal user source.
NEW_USER_DEFAULT_ACTIVITY=INACTIVE
# If set to true, unauthenticated access to the database is possible with an anonymous user.
AUTH_OPTIONAL=FALSE
#AUTH_OPTIONAL=TRUE

# --------------------------------------------------
# MySQL settings
# --------------------------------------------------
# Hostname of the mysql instance used by CaosDB
MYSQL_HOST=localhost
# Port of the mysql instance
MYSQL_PORT=3306
# Database name of the mysql database
MYSQL_DATABASE_NAME=caosdb
# User name for connecting to mysql
MYSQL_USER_NAME=caosdb
# Password for the user
MYSQL_USER_PASSWORD=random1234
# Schema of mysql procedures and tables which is required by this CaosDB instance
MYSQL_SCHEMA_VERSION=v8.0


# --------------------------------------------------
# Server options
# --------------------------------------------------
# The context root is a prefix which allows running multiple instances of CaosDB using the same
# hostname and port. Must start with "/".
CONTEXT_ROOT=
#CONTEXT_ROOT=/caosdb

# Server bind/host address, which is the address to listen to. Set to blank, or
# 0.0.0.0 in IPv4, to listen to all. Set to 127.0.0.1 to make it available to
# localhost only.
SERVER_BIND_ADDRESS=
# HTTPS port of this server instance.
SERVER_PORT_HTTPS=443
# HTTP port of this server instance.
SERVER_PORT_HTTP=80

# Initial number of HTTPConnection objects in the pool.
INITIAL_CONNECTIONS=1
# Maximum number of parallel HTTPConnections of the server
MAX_CONNECTIONS=10


# HTTPS port of the grpc end-point
GRPC_SERVER_PORT_HTTPS=8443
# HTTP port of the grpc end-point
GRPC_SERVER_PORT_HTTP=

# --------------------------------------------------
# Response Log formatting (this cannot be configured by the logging frame work
# and thus has to be configured here).
# --------------------------------------------------

# Logging format of the GRPC API.
# Known keys: user-agent, local-address, remote-address, method.
# 'OFF' turns off the logging.
GRPC_RESPONSE_LOG_FORMAT={method} {local-address} {remote-address} {user-agent}
# Logging format of the REST API.
# Known keys: see column "Variable name" at https://javadocs.restlet.talend.com/2.4/jse/api/index.html?org/restlet/util/Resolver.html
# 'OFF' turns off the logging.
# Leaving this empty means using restlet's default settings.
REST_RESPONSE_LOG_FORMAT=

# --------------------------------------------------
# HTTPS options
# --------------------------------------------------
# Allowed TLS versions
HTTPS_ENABLED_PROTOCOLS=TLSv1.3 TLSv1.2
# Forbidden TLS versions
HTTPS_DISABLED_PROTOCOLS=SSLv3 SSLv2Hello TLSv1.1 TLSv1.0
# Allowed cipher suites which are used for the encryption of the HTTP payload.
HTTPS_ENABLED_CIPHER_SUITES=TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 TLS_AES_128_GCM_SHA256 TLS_AES_256_GCM_SHA384 TLS_CHACHA20_POLY1305_SHA256 TLS_AES_128_CCM_SHA256 TLS_AES_128_CCM_8_SHA256 TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256 TLS_DHE_RSA_WITH_AES_256_GCM_SHA384 TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384
# Forbidden cipher suites which are used for the encryption of the HTTP payload.
HTTPS_DISABLED_CIPHER_SUITES=TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA TLS_RSA_WITH_AES_256_CBC_SHA TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA TLS_ECDH_RSA_WITH_AES_256_CBC_SHA TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA TLS_RSA_WITH_AES_128_CBC_SHA TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA TLS_ECDH_RSA_WITH_AES_128_CBC_SHA TLS_ECDHE_ECDSA_WITH_RC4_128_SHA TLS_ECDHE_RSA_WITH_RC4_128_SHA TLS_ECDH_ECDSA_WITH_RC4_128_SHA TLS_ECDH_RSA_WITH_RC4_128_SHA TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHASSL_RSA_WITH_RC4_128_MD5 SSL_RSA_WITH_3DES_EDE_CBC_SHA SSL_RSA_WITH_RC4_128_SHA TLS_DHE_RSA_WITH_AES_256_CBC_SHA TLS_DHE_DSS_WITH_AES_256_CBC_SHA TLS_DHE_RSA_WITH_AES_128_CBC_SHA TLS_DHE_DSS_WITH_AES_128_CBC_SHA SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA

# Password for the private key for the HTTPS server.
CERTIFICATES_KEY_PASSWORD=
# Path to the keystore which typically ends in jks.
CERTIFICATES_KEY_STORE_PATH=
# Password of the keystore which stores the private key.
CERTIFICATES_KEY_STORE_PASSWORD=

# --------------------------------------------------
# Timeout settings
# --------------------------------------------------

# The session timeout after which the cookie expires.
# 60 min
SESSION_TIMEOUT_MS=3600000

# Time after which one-time tokens expire.
# This is only a default value. The actual timeout of tokens can be
# configured otherwise, for example in authtoken.yml.
# 7days
ONE_TIME_TOKEN_EXPIRES_MS=604800000

# Path to config file for one time tokens, see authtoken.example.yml.
AUTHTOKEN_CONFIG=

# Path to config file for the job rules (e.g. which job is loaded for which kind of entity)
JOB_RULES_CONFIG=conf/core/jobs.csv

# Timeout after which a one-time token expires once it has been first consumed,
# regardless of the maximum of replays that are allowed for that token. This is
# only a default value. The actual timeout of tokens can be configured
# otherwise.
# 30 s
ONE_TIME_TOKEN_REPLAYS_TIMEOUT_MS=30000

# The value for the HTTP cache directive "max-age"
WEBUI_HTTP_HEADER_CACHE_MAX_AGE=28800

# --------------------------------------------------
# Mail settings
# --------------------------------------------------
# The handler that treats sent mails.
# The default handler pipes mails to a file.
MAIL_HANDLER_CLASS=org.caosdb.server.utils.mail.ToFileHandler
# The file were the ToFileHanlder pipes messages to.
MAIL_TO_FILE_HANDLER_LOC=./

# --------------------------------------------------
# Admin settings
# --------------------------------------------------
# Name of the administrator of this instance
ADMIN_NAME=CaosDB Admin
# Email of the administrator of this instance
ADMIN_EMAIL=
# An URL to the bugtracker for managing instance related bugs.
BUGTRACKER_URI=

# If set to true MySQL stores transaction benchmarks for all SQL queries. Used for benchmarking and debugging.
TRANSACTION_BENCHMARK_ENABLED=FALSE
#TRANSACTION_BENCHMARK_ENABLED=TRUE
# Location of the configuration file for the CaosDB cache.
CACHE_CONF_LOC=./conf/core/cache.ccf
# Set this option to true to lobally disable caching. Used for debugging.
CACHE_DISABLE=false

# The server is allowed to create symlinks to files and folders within this whitelist of directories.
INSERT_FILES_IN_DIR_ALLOWED_DIRS=
#INSERT_FILES_IN_DIR_ALLOWED_DIRS=/data/caosdb,/fileserver01/caosdb

# Sudo password of the system.
# Needed by the drop off box to set file permissions.
SUDO_PASSWORD=

# If set to false ACL checks are circumvented during querying. This may leak information but is a lot faster.
QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS=TRUE

# When checking the ACL of an entity roles which are unknown to the server
# raise an error (when set to MUST) or a warning (when set to SHOULD).
# Unknown roles occur when a user or group is removed or when entities are
# loaded from other instances of the CaosDB Server where different users are
# present.
# CHECK_ENTITY_ACL_ROLES_MODE=[MUST,SHOULD]
CHECK_ENTITY_ACL_ROLES_MODE=MUST

# Location of the global ACL file for entities. The global ACL is implicitly
# part of any Entity ACL.
GLOBAL_ENTITY_PERMISSIONS_FILE=./conf/core/global_entity_permissions.xml

# --------------------------------------------------
# User Account Settings
# --------------------------------------------------

# Requirements for user names. The default is POSIX compliant, see
# https://pubs.opengroup.org/onlinepubs/000095399/basedefs/xbd_chap03.html#tag_03_426
USER_NAME_VALID_REGEX=^[\\w\\.][\\w\\.-]*{1,32}$
USER_NAME_INVALID_MESSAGE=User names must have a length from 1 to 32 characters. They must contain only latin letters a-z (upper case or lower case), number 0-9, dots (.), underscores (_), or hyphens (-). They must no start with a hyphen.

PASSWORD_VALID_REGEX=^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\p{Punct}]).{8,128})$
PASSWORD_INVALID_MESSAGE=Passwords must have a length from 8 to 128 characters. THe must contain at least one [A-Z], one [a-z], one [0-9] and a special character e.g. [!§$%&/()=?.;,:#+*+~].

# --------------------------------------------------
# Query Settings
# --------------------------------------------------

# FIND blablabla is short for FIND $FIND_QUERY_DEFAULT_ROLE blablabla. Should be
# either ENTITY or RECORD. RECORDTYPE, FILE, and PROPERTY will work as well but
# are rather unexpected for human users.
FIND_QUERY_DEFAULT_ROLE=RECORD

# --------------------------------------------------
# Extensions
# --------------------------------------------------

# If set to true, versioning of entities' history is enabled.
ENTITY_VERSIONING_ENABLED=true


# Enabling the state machine extension
# EXT_STATE_ENTITY=ENABLE
