# Server Configuration

The server configuration is a list of key-value pairs. A configuration file may contain empty lines, comment lines, and key-value lines.
Comment lines begin with a hash (`#`). Key-value lines must have the format `KEY_NAME=VALUE` or `KEY_NAME = VALUE`.

The server default configuration is located at `./conf/core/server.conf`.

The default configuration can be overriden by

1. the file ./conf/ext/server.conf
2. any file in ./conf/ext/server.conf.d/ in (approximately?) alphabetical order
3. environment variables with the prefix `CAOSDB_CONFIG_`

in this order.

# One-time Authentication Tokens

One-time Authentication Tokens can be configure to be issued for special purposes (e.g. a call of a server-side script) or to be written to a file on a regular basis.

An example of a configuration is located at `./conf/core/authtoken.example.yaml`.

