/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.jdom2.Element;

public class Date extends SemiCompleteDateTime {

  public Date(final Integer year, final Integer month, final Integer dom, final TimeZone tz) {
    super(year, month, dom, null, null, null, null, tz);
  }

  public static Date today() {
    final GregorianCalendar now = new GregorianCalendar();
    now.setTimeZone(TimeZone.getTimeZone("UTC"));
    return new Date(
        now.get(Calendar.YEAR),
        now.get(Calendar.MONTH) + 1,
        now.get(Calendar.DAY_OF_MONTH),
        TimeZone.getTimeZone("UTC"));
  }

  @Override
  public Table getTable() {
    return Table.date_data;
  }

  @Override
  public String toDatabaseString() {
    return toDotNotation();
  }

  private String toDotNotation() {
    final StringBuilder sb = new StringBuilder();
    final Integer date =
        this.year * 10000
            + (this.month == null ? 0 : this.month * 100)
            + (this.dom == null ? 0 : this.dom);
    sb.append(date);
    sb.append(".NULL.NULL");
    return sb.toString();
  }

  @Override
  public String getILB_NF2() {
    return toDotNotation();
  }

  @Override
  public String getEUB_NF2() {
    final StringBuilder sb = new StringBuilder();
    final Integer date =
        this.year * 10000
            + (this.month == null ? 10000 : this.month * 100 + (this.dom == null ? 100 : this.dom));
    sb.append(date);
    sb.append(".NULL.NULL");
    return sb.toString();
  }

  @Override
  public void addToElement(final Element e) {
    e.addContent(toDateTimeString(this.timeZone));
  }
}
