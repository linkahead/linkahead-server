/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import java.util.Arrays;
import java.util.Calendar;
import java.util.TimeZone;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.caosdb.server.query.CQLParsingErrorListener;
import org.caosdb.server.query.CQLParsingErrorListener.ParsingError;

/**
 * Factory which parses string into CaosDB's DATETIME values.
 *
 * @author tf
 */
public class DateTimeFactory2 implements DateTimeFactoryInterface {

  private Long millis = null;
  private Integer date = null;

  @SuppressWarnings("unused")
  private Integer time = null;

  @SuppressWarnings("unused")
  private Integer timeNS = null;

  private Integer year = null;
  private Integer month = null;
  private Integer dom = null;
  private Integer hour = null;
  private Integer minute = null;
  private Integer second = null;
  private Long utcseconds = null;
  private Integer nanosecond = null;
  private TimeZone timeZone = TimeZone.getDefault();

  public DateTimeFactory2(TimeZone timeZone) {
    this.timeZone = timeZone;
  }

  public DateTimeFactory2() {
    this(TimeZone.getDefault());
  }

  @Override
  public void setDate(final String string) {
    this.date = Integer.valueOf(string);
  }

  @Override
  public void setTime(final String string) {
    this.time = Integer.valueOf(string);
  }

  @Override
  public void setTimeNS(final String string) {
    this.timeNS = Integer.valueOf(string);
  }

  @Override
  public void setYear(final int year) {
    this.year = year;
  }

  @Override
  public void setMonth(final int month) {
    this.month = month;
  }

  @Override
  public void setDom(final int dom) {
    this.dom = dom;
  }

  @Override
  public void setHour(final int hour) {
    this.hour = hour;
  }

  @Override
  public void setMinute(final int minute) {
    this.minute = minute;
  }

  @Override
  public void setSecond(final int second) {
    this.second = second;
  }

  @Override
  public void setNanoSecondFromFracionalString(final String str) {
    if (str.length() < 9) {
      final StringBuilder sb = new StringBuilder(str);
      while (sb.length() < 9) {
        sb.append('0');
      }
      setNanosecond(Integer.parseInt(sb.toString()));
    } else if (str.length() > 9) {
      // TODO round instead of truncate
      setNanosecond(Integer.parseInt(str.substring(0, 9)));
    } else {
      setNanosecond(Integer.parseInt(str));
    }
  }

  @Override
  public void setTimeZoneOffset(final int sign, final int h, final int m) {
    this.timeZone = new UTCTimeZoneShift(sign, h, m);
  }

  @Override
  public void setTimeZone(final String str) {
    if (Arrays.asList(TimeZone.getAvailableIDs()).contains(str)) {
      this.timeZone = TimeZone.getTimeZone(str);
    } else {
      throw new IllegalArgumentException("No such timezone");
    }
  }

  @Override
  public void setNanosecond(final int nanosecond) {
    this.nanosecond = nanosecond;
  }

  @Override
  public DateTimeInterface getDateTime() {
    if (this.millis != null) {
      return UTCDateTime.SystemMillisToUTCDateTime(this.millis);
    } else if (fromDatabase()) {
      return UTCDateTime.UTCSeconds(this.utcseconds, this.nanosecond);
      // return new UTCDateTime(date, time, timeNS);
    } else if (isComplete()) {
      return new UTCDateTime(
          this.year,
          this.month,
          this.dom,
          this.hour,
          this.minute,
          this.second,
          this.nanosecond,
          this.timeZone);
    } else if (isDate()) {
      return new Date(this.year, this.month, this.dom, this.timeZone);
    } else if (isSemiInstance()) {
      return new SemiCompleteDateTime(
          this.year,
          this.month,
          this.dom,
          this.hour,
          this.minute,
          this.second,
          this.nanosecond,
          this.timeZone);
    } else if (this.date != null) {
      if (this.date > 0) {
        final Integer dom = this.date % 100;
        final Integer month = ((this.date - dom) % 10000) / 100;
        final Integer year = (this.date - dom - month * 100) / 10000;
        return new Date(year, (month == 0 ? null : month), (dom == 0 ? null : dom), this.timeZone);
      } else {
        Integer dom = this.date % 100;
        dom = (dom == 0 ? dom : dom + 100);
        Integer month = ((this.date - dom) % 10000) / 100;
        month = (month == 0 ? month : month + 100);
        final Integer year = (this.date - dom - month * 100) / 10000;
        return new Date(year, (month == 0 ? null : month), (dom == 0 ? null : dom), this.timeZone);
      }
    } else {
      return null;
    }
  }

  private boolean isDate() {
    return this.year != null
        && this.hour == null
        && this.minute == null
        && this.second == null
        && this.nanosecond == null
        && (this.dom == null || this.month != null);
  }

  private boolean isSemiInstance() {
    return this.year != null
        && this.second == null
        && this.nanosecond == null
        && (this.minute == null || this.hour != null)
        && (this.hour == null || this.dom != null)
        && (this.dom == null || this.month != null);
  }

  private boolean fromDatabase() {
    return this.utcseconds != null;
  }

  private boolean isComplete() {
    return this.year != null
        && this.month != null
        && this.dom != null
        && this.hour != null
        && this.minute != null
        && this.second != null;
  }

  @Override
  public void setSystemDate(final java.util.Date d) {
    this.millis = d.getTime();
  }

  public static DateTimeInterface valueOf(final Calendar c) {
    return UTCDateTime.SystemMillisToUTCDateTime(c.getTimeInMillis());
  }

  public static DateTimeInterface valueOf(final String str) {
    return valueOf(str, TimeZone.getDefault());
  }

  public static DateTimeInterface valueOf(final String str, TimeZone timeZone) {
    final DateTimeFactory2 dtf = new DateTimeFactory2(timeZone);

    return dtf.parse(str);
  }

  public DateTimeInterface parse(String str) {
    final DateTimeLexer lexer = new DateTimeLexer(CharStreams.fromString(str));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final DateTimeParser parser = new DateTimeParser(tokens);

    parser.removeErrorListeners();

    final CQLParsingErrorListener el = new CQLParsingErrorListener(DateTimeLexer.UNKNOWN_CHAR);
    parser.addErrorListener(el);

    parser.datetime(this);

    if (el.hasErrors()) {
      final StringBuilder sb = new StringBuilder();
      for (final ParsingError p : el.getErrors()) {
        sb.append(p.toString());
      }
      throw new IllegalArgumentException(
          "Parsing DateTime finished with errors. \n" + sb.toString());
    }

    return getDateTime();
  }

  @Override
  public void setUTCSeconds(final String string) {
    this.utcseconds = Long.parseLong(string);
  }

  @Override
  public void setNanoseconds(final String string) {
    this.nanosecond = Integer.parseInt(string);
  }
}
