/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * @review Daniel Hornung 2022-03-04
 */
package org.caosdb.datetime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;
import java.util.TimeZone;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.caosdb.server.datatype.Value;
import org.jdom2.Element;

public class UTCDateTime implements Interval {

  public static final boolean SYSTEM_SUPPORTS_LEAP_SECONDS = false;

  private long systemSeconds;
  private final Integer nanoseconds;
  private int leapSeconds = 0;

  /**
   * @param seconds : Seconds since 1970-01-01T00:00:00.
   * @param nanoseconds : additional nanoseconds.
   */
  private UTCDateTime(final long systemSeconds, final int nanoseconds) {
    this.systemSeconds = systemSeconds;
    this.leapSeconds = getLeapCorrection(this.systemSeconds);
    this.nanoseconds = nanoseconds;

    this.dateTimeStringStrategy = new GregorianCalendarDateTimeStringStrategy(this.systemSeconds);
  }

  private UTCDateTime(
      final long systemSeconds,
      final int leapSeconds,
      final Integer nanoseconds,
      final DateTimeStringStrategy dateTimeStringStrategy) {
    this.systemSeconds = systemSeconds;
    this.leapSeconds = leapSeconds;
    this.nanoseconds = nanoseconds;
    this.dateTimeStringStrategy = dateTimeStringStrategy;
  }

  private static int getLeapCorrection(final long systemSeconds) {
    int ret = 0;
    if (!SYSTEM_SUPPORTS_LEAP_SECONDS) {
      if (LEAP_SECONDS.isEmpty()) {
        initLeapSeconds();
      }
      for (final Long l : LEAP_SECONDS) {
        if (systemSeconds >= l) {
          ret++;
        } else {
          break;
        }
      }
    }
    return ret;
  }

  public UTCDateTime(
      final Integer year,
      final Integer month,
      final Integer dom,
      final Integer hour,
      final Integer minute,
      final Integer second,
      final Integer nanosecond,
      final TimeZone timeZone) {
    this(year, month, dom, hour, minute, second, nanosecond, timeZone, false);
  }

  public UTCDateTime(
      final Integer year,
      final Integer month,
      final Integer dom,
      final Integer hour,
      final Integer minute,
      final Integer second,
      final Integer nanosecond,
      final TimeZone timeZone,
      final boolean lenient) {
    final GregorianCalendar gc = new GregorianCalendar();
    gc.clear();
    gc.setLenient(lenient);
    gc.setTimeZone(timeZone);
    if (year < 0) {
      gc.set(GregorianCalendar.ERA, GregorianCalendar.BC);
    }
    gc.set(Math.abs(year), month - 1, dom, hour, minute, second);
    this.nanoseconds = nanosecond;
    this.dateTimeStringStrategy = new GregorianCalendarDateTimeStringStrategy(gc);
    try {
      // test whether this UTCDateTime is valid in the GregorianCalendar
      // sense.
      this.systemSeconds = gc.getTimeInMillis() / 1000;
      this.leapSeconds = getLeapCorrection(this.systemSeconds);
    } catch (final IllegalArgumentException e) {
      if (!SYSTEM_SUPPORTS_LEAP_SECONDS) {
        gc.setLenient(true);
        if (LEAP_SECONDS.isEmpty()) {
          initLeapSeconds();
        }
        this.systemSeconds = gc.getTimeInMillis() / 1000;
        if (LEAP_SECONDS.contains(this.systemSeconds)) {
          // test whether this UTCDateTime is a known LeapSecond
          gc.add(Calendar.SECOND, -1);
          this.dateTimeStringStrategy = new LeapSecondDateTimeStringStrategy(gc, 1);
          this.leapSeconds = getLeapCorrection(this.systemSeconds) - 1;
        } else {
          throw e;
        }
      } else {
        throw e;
      }
    }
  }

  private static final ArrayList<Long> LEAP_SECONDS = new ArrayList<Long>();

  public static UTCDateTime SystemMillisToUTCDateTime(final Long millis) {
    final int fractionalMilliseconds = (int) (millis % 1000);
    final long TAISeconds = (millis - fractionalMilliseconds) / 1000;
    return new UTCDateTime(TAISeconds, fractionalMilliseconds * 1000000);
  }

  /*
   * Add a leap second. Attention: they MUST be added in a chronological
   * order!
   */
  private static void addLeapSecond(final Long second) {
    if (!LEAP_SECONDS.contains(second)) {
      LEAP_SECONDS.add(second);
    }
  }

  private static void addLeapSecond(final int year, final int month, final int dom) {
    final GregorianCalendar gc = new GregorianCalendar();
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(year, month - 1, dom, 23, 59, 60);
    addLeapSecond(gc.getTimeInMillis() / 1000);
  }

  /**
   * Initialize the container with known leap seconds. This container has to be updated according to
   * future changes of leap seconds.
   *
   * <p>Currently all leap seconds between 1972 and 2019 are registered here, with the most recent
   * leap second being added for December 2016.
   */
  private static void initLeapSeconds() {

    // june 1972
    addLeapSecond(1972, 6, 30);

    // dec 1972 - dec 1979
    for (int y = 1972; y <= 1979; y++) {
      addLeapSecond(y, 12, 31);
    }

    // june 1981 - june 1983
    for (int y = 1981; y <= 1983; y++) {
      addLeapSecond(y, 06, 30);
    }

    // june 1985
    addLeapSecond(1985, 6, 30);

    // dec 1987
    addLeapSecond(1987, 12, 31);

    // dec 1989 - dec 1990
    for (int y = 1989; y <= 1990; y++) {
      addLeapSecond(y, 12, 31);
    }

    // june 1992 - june 1994
    for (int y = 1992; y <= 1994; y++) {
      addLeapSecond(y, 06, 30);
    }

    // dec 1995
    addLeapSecond(1995, 12, 31);

    // june 1997
    addLeapSecond(1997, 06, 30);

    // dec 1998
    addLeapSecond(1998, 12, 31);

    // dec 2005
    addLeapSecond(2005, 12, 31);

    // dec 2008
    addLeapSecond(2008, 12, 31);

    // june 2012
    addLeapSecond(2012, 06, 30);

    // june 2015
    addLeapSecond(2015, 06, 30);

    // dec 2016
    addLeapSecond(2016, 12, 31);
  }

  public long getUTCSeconds() {
    return this.systemSeconds + this.leapSeconds;
  }

  public Integer getNanoseconds() {
    return this.nanoseconds;
  }

  private DateTimeStringStrategy dateTimeStringStrategy = null;

  @Override
  public String toDateTimeString(final TimeZone tz) {
    StringBuilder ret;
    if (this.nanoseconds != null) {
      ret = new StringBuilder();
      ret.append('.');
      if (this.nanoseconds == 0) {
        ret.append('0');
      } else {
        final char[] n = this.nanoseconds.toString().toCharArray();
        int l = n.length;
        // zeros at the beginning
        while (l < 9) {
          l++;
          ret.append('0');
        }
        for (l = n.length - 1; l >= 0; l--) {
          if (n[l] == '0') {
            n[l] = 'x';
          } else {
            break;
          }
        }
        for (final char c : n) {
          if (c != 'x') {
            ret.append(c);
          } else {
            break;
          }
        }
      }
      return this.dateTimeStringStrategy.toDateTimeString(tz).replace("<NS>", ret.toString());
    }
    return this.dateTimeStringStrategy.toDateTimeString(tz).replace("<NS>", "");
  }

  @Override
  public void addToElement(final Element e) {
    e.addContent(toDateTimeString(TimeZone.getDefault()));
  }

  @Override
  public Table getTable() {
    return Table.datetime_data;
  }

  @Override
  public String toDatabaseString() {
    final StringBuilder sb = new StringBuilder();
    sb.append(getUTCSeconds());
    sb.append("UTC");
    if (getNanoseconds() != null) {
      sb.append(getNanoseconds());
    }
    return sb.toString();
  }

  @Override
  public String toString() {
    throw new NullPointerException("toString method!!!");
  }

  public static UTCDateTime UTCSeconds(final Long utcseconds, final Integer nanoseconds) {
    if (LEAP_SECONDS.isEmpty()) {
      initLeapSeconds();
    }
    final int leapSeconds2 = getLeapCorrection(utcseconds);
    final int leapSeconds = getLeapCorrection(utcseconds - leapSeconds2);
    final long systemSeconds = utcseconds - leapSeconds;
    final GregorianCalendar gc = new GregorianCalendar();
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.setTimeInMillis(systemSeconds * 1000);
    if (leapSeconds2 != leapSeconds && LEAP_SECONDS.contains(systemSeconds)) {
      gc.add(Calendar.SECOND, -1);
      return new UTCDateTime(
          systemSeconds, leapSeconds, nanoseconds, new LeapSecondDateTimeStringStrategy(gc, 1));
    } else {
      return new UTCDateTime(
          systemSeconds, leapSeconds, nanoseconds, new GregorianCalendarDateTimeStringStrategy(gc));
    }
  }

  public static UTCDateTime valueOf(final String str) {
    final DateTimeInterface dt = DateTimeFactory2.valueOf(str);
    if (dt instanceof UTCDateTime) {
      return (UTCDateTime) dt;
    } else {
      throw new IllegalArgumentException("This is no UTCDateTime: " + str);
    }
  }

  @Override
  public String getILB_NF1() {
    return toDatabaseString();
  }

  @Override
  public String getEUB_NF1() {
    return null;
  }

  @Override
  public String getILB_NF2() {
    final String ns = (this.nanoseconds == null ? "NULL" : this.nanoseconds.toString());
    return this.dateTimeStringStrategy.toDotNotation(TimeZone.getDefault()).replace("<NS>", ns);
  }

  @Override
  public String getEUB_NF2() {
    return null;
  }

  @Override
  public UTCDateTime getInclusiveLowerBound() {
    return this;
  }

  @Override
  public UTCDateTime getExclusiveUpperBound() {
    return null;
  }

  public boolean hasNanoseconds() {
    return this.nanoseconds != null;
  }

  @Override
  public boolean equals(Value val) {
    if (val instanceof UTCDateTime) {
      UTCDateTime that = (UTCDateTime) val;
      return Objects.equals(that.toDatabaseString(), this.toDatabaseString());
    }
    return false;
  }
}
