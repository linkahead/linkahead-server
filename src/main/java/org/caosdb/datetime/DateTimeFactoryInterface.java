/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import java.util.Date;

public interface DateTimeFactoryInterface {

  public void setSystemDate(Date d);

  public void setDate(String str);

  public void setTime(String str);

  public void setTimeNS(String str);

  public void setYear(int year);

  public void setMonth(int month);

  public void setDom(int dom);

  public void setHour(int hour);

  public void setMinute(int minute);

  public void setSecond(int second);

  public void setNanoSecondFromFracionalString(String text);

  public void setNanosecond(int nanoSecond);

  public void setTimeZoneOffset(int sign, int h, int m);

  public void setTimeZone(String id);

  public DateTimeInterface getDateTime();

  public void setUTCSeconds(String string);

  public void setNanoseconds(String string);
}
