/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import java.util.Date;
import java.util.TimeZone;

/**
 * Represents the shift, i.e. the time difference between a time zone 'X' and the UTC+0 time zone.
 *
 * <p>This is used for (de-)serialization of ISO8601-compliant representations of UTC-relative time
 * zones (e.g. +0100 for CEST)
 *
 * @author tf
 */
public class UTCTimeZoneShift extends TimeZone {
  private static final long serialVersionUID = 6962096078347188058L;
  public static final int POSITIVE = 1;
  public static final int NEGATIVE = -1;

  private final int rawOffset;
  private final String str;

  public UTCTimeZoneShift(final int sign, final int h, final int m) {
    if (59 < m || m < 0) {
      throw new IllegalArgumentException("59<m<0 is not allowed. m was " + Integer.toString(m));
    }
    if (14 < h || h < 0) {
      throw new IllegalArgumentException("14<h<0 is not allowed. h was " + Integer.toString(h));
    }

    rawOffset = sign * (3600 * h + 60 * m) * 1000;
    str = generateString(sign, h, m);
  }

  private static String generateString(final int sign, final int h, final int m) {
    final StringBuilder sb = new StringBuilder();
    sb.append(sign == NEGATIVE ? '-' : '+');
    if (59 < m || m < 0) {
      throw new IllegalArgumentException("59<m<0 is not allowed. m was " + Integer.toString(m));
    }
    if (14 < h || h < 0) {
      throw new IllegalArgumentException("14<h<0 is not allowed. h was " + Integer.toString(h));
    }
    if (h < 10) {
      sb.append('0');
    }
    sb.append(h);
    if (m < 10) {
      sb.append('0');
    }
    sb.append(m);
    return sb.toString();
  }

  /** ISO 8601 conform +/-hhmm. */
  @Override
  public String toString() {
    return str;
  }

  @Override
  public int getOffset(
      final int era,
      final int year,
      final int month,
      final int day,
      final int dayOfWeek,
      final int milliseconds) {
    return getRawOffset();
  }

  @Override
  public int getRawOffset() {
    return rawOffset;
  }

  @Override
  public boolean inDaylightTime(final Date date) {
    return false;
  }

  @Override
  public void setRawOffset(final int offsetMillis) {
    throw new UnsupportedOperationException("RawOffset is final.");
  }

  @Override
  public boolean useDaylightTime() {
    return false;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof TimeZone) {
      final TimeZone tz = (TimeZone) obj;
      if (hasSameRules(tz)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public String getID() {
    return str;
  }

  /**
   * Generate an ISO 8601 time zone offset string (e.g. +0200) for the given time zone at the given
   * date.
   *
   * <p>The date is necessary to decide whether the time zone (e.g. Europe/Berlin) is in the
   * daylight saving regime or not. In winter Europe/Berlin has a +0100 offset, in summer it is
   * +0200.
   *
   * @param timezone - the time zone in question.
   * @param date - the date for which the offset is to be calculated (unix time stamp).
   * @return ISO 8601 time zone offset.
   */
  public static String getISO8601Offset(TimeZone timezone, long date) {
    int divisor = 1000 * 60 * 60;
    int offset = timezone.getOffset(date);
    // convert the ms to the ISO format
    int sign = Integer.signum(offset);
    int h = Integer.divideUnsigned(sign * offset, divisor);
    int m = Integer.remainderUnsigned(sign * offset, divisor);
    return generateString(sign, h, m);
  }
}
