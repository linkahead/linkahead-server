package org.caosdb.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import org.caosdb.server.utils.Serializer;
import org.jdom2.Element;

public class ServerPropertiesSerializer implements Serializer<Properties, Element> {

  private boolean sort;

  public ServerPropertiesSerializer(boolean sort) {
    this.sort = sort;
  }

  public ServerPropertiesSerializer() {
    this(true);
  }

  @Override
  public Element serialize(Properties object) {
    Element root = new Element("Properties");
    List<String> names = new ArrayList<>(object.stringPropertyNames());
    if (sort) {
      Collections.sort(names);
    }
    for (String prop : names) {
      Element propElem = new Element(prop);
      propElem.addContent(object.getProperty(prop));
      root.addContent(propElem);
    }
    return root;
  }
}
