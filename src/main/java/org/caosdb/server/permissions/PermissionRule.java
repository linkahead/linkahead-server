/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.permissions;

import java.util.HashMap;
import java.util.Map;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.Principal;
import org.jdom2.Element;

public class PermissionRule {

  private final String permission;
  private final boolean priority;
  private final boolean grant;

  public PermissionRule(final String grant, final String priority, final String permission) {
    this(Boolean.parseBoolean(grant), Boolean.parseBoolean(priority), permission);
  }

  public PermissionRule(final boolean grant, final boolean priority, final String permission) {
    this.grant = grant;
    this.priority = priority;
    if (permission == null) {
      throw new NullPointerException("permission cannot be null");
    }
    this.permission = permission;
  }

  public boolean isGrant() {
    return this.grant;
  }

  public boolean isPriority() {
    return this.priority;
  }

  public String getPermission() {
    return permission;
  }

  public Permission getPermission(String realm, String username) {
    return new WildcardPermission(
        permission.replaceAll("\\?REALM\\?", realm).replaceAll("\\?USERNAME\\?", username));
  }

  public static PermissionRule parse(final Map<String, String> rule) {
    return new PermissionRule(rule.get("grant"), rule.get("priority"), rule.get("permission"));
  }

  public Element toElement() {
    final Element ret = new Element((isGrant() ? "Grant" : "Deny"));
    if (isPriority()) {
      ret.setAttribute("priority", Boolean.toString(true));
    }
    ret.setAttribute("permission", permission);
    return ret;
  }

  @Override
  public String toString() {
    return "PermissionRule("
        + (priority ? "[P]" : "")
        + (grant ? "GRANT" : "DENY")
        + permission
        + ")";
  }

  @Override
  public int hashCode() {
    return permission.hashCode() + (grant ? 3 : 0) - (priority ? 5 : 0);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof PermissionRule) {
      PermissionRule that = (PermissionRule) obj;
      return this.grant == that.grant
          && this.priority == that.priority
          && this.permission.equals(that.permission);
    }
    return false;
  }

  public static PermissionRule parse(final Element e) {
    return new PermissionRule(
        e.getName().equalsIgnoreCase("Grant"),
        e.getAttribute("priority") != null && Boolean.parseBoolean(e.getAttributeValue("priority")),
        e.getAttributeValue("permission"));
  }

  public Map<String, String> getMap() {
    final HashMap<String, String> ret = new HashMap<String, String>();
    ret.put("priority", Boolean.toString(isPriority()));
    ret.put("grant", Boolean.toString(isGrant()));
    ret.put("permission", permission);
    return ret;
  }

  public Permission getPermission(Subject subject) {
    Principal principal = (Principal) subject.getPrincipal();
    return getPermission(principal.getRealm(), principal.getUsername());
  }
}
