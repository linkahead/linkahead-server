package org.caosdb.server.logging.log4j;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.AbstractConfiguration;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.ConfigurationFactory;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Order;
import org.apache.logging.log4j.core.config.composite.CompositeConfiguration;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.properties.PropertiesConfigurationFactory;
import org.apache.logging.log4j.core.util.NetUtils;

@Plugin(name = "CustomConfigurationFactory", category = ConfigurationFactory.CATEGORY)
@Order(100)
public class CustomConfigurationFactory extends PropertiesConfigurationFactory {

  private File core_default = new File("conf/core/log4j2-default.properties");
  private File core_debug = new File("conf/core/log4j2-debug.properties");
  private File ext_file = new File("conf/ext/log4j2.properties");
  private File ext_dir = new File("conf/ext/log4j2.properties.d");

  private List<String> getConfigFiles() {
    List<String> configFiles = new ArrayList<>(10);
    configFiles.add(core_default.getAbsolutePath());
    if (Boolean.getBoolean("caosdb.debug")) {
      configFiles.add(core_debug.getAbsolutePath());
    }
    if (ext_file.exists()) {
      configFiles.add(ext_file.getAbsolutePath());
    }
    if (ext_dir.exists() && ext_dir.isDirectory()) {
      String[] confFiles = ext_dir.list();
      Arrays.sort(confFiles, Comparator.naturalOrder());
      for (String fileName : confFiles) {
        if (fileName.endsWith(".properties"))
          configFiles.add(ext_dir.toPath().toAbsolutePath().resolve(fileName).toString());
      }
    }
    LOGGER.info("Found {} configuration files: {}", configFiles.size(), configFiles);
    return configFiles;
  }

  @Override
  public Configuration getConfiguration(
      LoggerContext loggerContext, String name, URI configLocation) {
    LOGGER.debug("Reconfiguration is done by {}", getClass().toString());

    List<String> sources = getConfigFiles();
    if (sources.size() > 0) {
      final List<AbstractConfiguration> configs = new ArrayList<>();
      for (final String sourceLocation : sources) {
        LOGGER.debug("Reconfigure with {}", sourceLocation);
        ConfigurationSource source = null;
        try {
          source = ConfigurationSource.fromUri(NetUtils.toURI(sourceLocation));
        } catch (final Exception ex) {
          // Ignore the error and try as a String.
          LOGGER.catching(Level.DEBUG, ex);
        }
        final Configuration config = super.getConfiguration(loggerContext, source);
        if (config != null && config instanceof AbstractConfiguration) {
          configs.add((AbstractConfiguration) config);
        } else {
          LOGGER.error("Failed to created configuration at {}", sourceLocation);
          return null;
        }
      }
      return new CompositeConfiguration(configs);
    }
    return super.getConfiguration(loggerContext, name, configLocation);
  }

  @Override
  protected String[] getSupportedTypes() {
    return new String[] {"*"};
  }
}
