/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020-2021,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020-2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import org.caosdb.server.entity.EntityInterface;

/**
 * Keeps track of Jobs.
 *
 * <p>The Schedule class orders jobs by {@link TransactionStage} and also assures that jobs are
 * skipped when appropriate and to prevent that jobs run more than once (because sometimes they
 * trigger each other).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class Schedule {

  private final Map<Integer, List<ScheduledJob>> jobLists = new HashMap<>();
  private ScheduledJob running = null;

  public List<ScheduledJob> addAll(final Collection<Job> jobs) {
    final List<ScheduledJob> result = new ArrayList<ScheduledJob>(jobs.size());
    for (final Job j : jobs) {
      ScheduledJob scheduledJob = add(j);
      if (scheduledJob != null) {
        result.add(scheduledJob);
      }
    }
    return result;
  }

  public ScheduledJob add(final Job j) {
    final ScheduledJob scheduled = new ScheduledJob(j);
    List<ScheduledJob> jobs = jobLists.get(scheduled.getTransactionStage().ordinal());
    if (jobs == null) {
      jobs = new CopyOnWriteArrayList<ScheduledJob>();
      jobLists.put(scheduled.getTransactionStage().ordinal(), jobs);
    }
    if (!jobs.contains(scheduled)) {
      jobs.add(scheduled);
      return scheduled;
    }
    return null;
  }

  /** Run all Jobs from the specified {@link TransactionStage}. */
  public void runJobs(final TransactionStage stage) {
    final List<ScheduledJob> jobs = this.jobLists.get(stage.ordinal());
    if (jobs != null) {
      for (final ScheduledJob scheduledJob : jobs) {
        runJob(scheduledJob);
      }
    }
  }

  public void runJob(final ScheduledJob scheduledJob) {
    if (scheduledJob == null || scheduledJob.skip()) {
      return;
    }
    final ScheduledJob parent = this.running;
    if (parent != null) {
      parent.pause();
    }

    this.running = scheduledJob;
    scheduledJob.run();

    if (parent != null) {
      this.running = parent;
      parent.unpause();
    }
  }

  /** Run all scheduled Jobs of a given class for the given entity. */
  public void runJob(final EntityInterface entity, final Class<? extends Job> jobclass) {

    // the jobs of this class are in the jobList for the TransactionStage of the jobClass.
    final List<ScheduledJob> jobs =
        jobclass.isAnnotationPresent(JobAnnotation.class)
            ? this.jobLists.get(jobclass.getAnnotation(JobAnnotation.class).stage().ordinal())
            : this.jobLists.get(TransactionStage.CHECK.ordinal());
    for (final ScheduledJob scheduledJob : jobs) {
      if (jobclass.isInstance(scheduledJob.getJob())) {
        if (scheduledJob.getJob().getEntity() == entity) {
          runJob(scheduledJob);
        }
      }
    }
  }
}
