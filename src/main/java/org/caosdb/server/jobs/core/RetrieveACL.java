/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

@JobAnnotation(
    stage = TransactionStage.POST_TRANSACTION,
    flag = "ACL",
    description = "Adds an XML representation of the EntityACL to the output of this entity.")
public class RetrieveACL extends FlagJob {

  @Override
  protected void job(final String value) {

    for (final EntityInterface entity : getContainer()) {
      if (entity.getId() != null
          && !entity.getId().isTemporary()
          && entity.getEntityACL() != null) {
        try {
          if (getTransaction() instanceof Retrieve) {
            entity.checkPermission(EntityPermission.RETRIEVE_ACL);
          }
          entity.addMessage(ret -> ret.addContent(entity.getEntityACL().toElement()));
        } catch (final AuthorizationException e) {
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          entity.addError(ServerMessages.AUTHORIZATION_ERROR);
          entity.addInfo(e.getMessage());
        }
      }
    }
  }
}
