/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import com.google.common.base.Objects;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.entity.Affiliation;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether all parents of an entity are valid/qualified.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(stage = TransactionStage.PRE_CHECK)
public class CheckParValid extends EntityJob {
  @Override
  public final void run() {
    if (getEntity() instanceof Property || getEntity() instanceof Parent) {
      return;
    }

    // loop over all parents of the entity
    if (getEntity().hasParents()) {
      for (final Parent parent : getEntity().getParents()) {
        // test by id if an id is present or by name otherwise,
        try {
          if (!parent.hasId() && !parent.hasName()) {
            // The parent has neither an id nor a name.
            // Therefore it cannot be identified.
            parent.addError(ServerMessages.ENTITY_HAS_NO_NAME_OR_ID);
          }
          try {
            EntityInterface foreign = resolve(parent.getId(), parent.getName(), (String) null);
            assertAllowedToUse(foreign);
            parent.linkIdToEntity(foreign);
            parent.setAffiliation(getAffiliation(getEntity().getRole(), foreign.getRole()));
          } catch (EntityDoesNotExistException e) {
            parent.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
          }
        } catch (final Message m) {
          parent.addError(m);
        } catch (AuthorizationException e) {
          parent.addError(ServerMessages.AUTHORIZATION_ERROR);
          parent.addInfo(e.getMessage());
        } catch (final EntityWasNotUniqueException exc) {
          parent.addError(ServerMessages.ENTITY_NAME_DUPLICATES);
        }
      }
    }

    if (getEntity().getEntityStatus() != EntityStatus.UNQUALIFIED) {
      removeDuplicates();
    }
  }

  private void removeDuplicates() {
    for (final Parent par : getEntity().getParents()) {
      if (par.getEntityStatus() != EntityStatus.IGNORE) {
        for (final Parent par2 : getEntity().getParents()) {
          if (par != par2 && par2.getEntityStatus() != EntityStatus.IGNORE) {
            if (par.hasId() && par2.hasId() && par.getId().equals(par2.getId())
                || par.hasName() && par2.hasName() && par.getName().equals(par2.getName())) {
              if (!Objects.equal(par.getFlag("inheritance"), par2.getFlag("inheritance"))) {
                getEntity().addError(ServerMessages.PARENT_DUPLICATES_ERROR);
                getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
                return;
              } else {
                par.setEntityStatus(EntityStatus.IGNORE);
                getEntity().addWarning(ServerMessages.PARENT_DUPLICATES_WARNING);
              }
            }
          }
        }
      }
    }
  }

  private Affiliation getAffiliation(final Role childRole, final Role parentRole) throws Message {
    switch (childRole) {
      case File:
      case Record:
        switch (parentRole) {
          case Record:
            return Affiliation.PARTHOOD;
          case RecordType:
            return Affiliation.INSTANTIATION;
          default:
            break;
        }
        break;
      case RecordType:
        switch (parentRole) {
          case RecordType:
            return Affiliation.SUBTYPING;
          default:
            break;
        }
        break;
      case Property:
        switch (parentRole) {
          case Property:
            return Affiliation.SUBTYPING;
          default:
            break;
        }
        break;
      default:
        break;
    }
    throw ServerMessages.AFFILIATION_ERROR;
  }

  private void assertAllowedToUse(final EntityInterface entity) {
    entity.checkPermission(EntityPermission.USE_AS_PARENT);
  }
}
