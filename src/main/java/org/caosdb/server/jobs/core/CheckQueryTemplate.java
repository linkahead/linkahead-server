/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.Role;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.query.Query;
import org.caosdb.server.query.Query.ParsingException;
import org.caosdb.server.utils.ServerMessages;

public class CheckQueryTemplate extends EntityJob {

  @Override
  protected void run() {
    if (getEntity().getRole() == Role.QueryTemplate) {
      if (getEntity().getQueryTemplateDefinition() != null
          && !getEntity().getQueryTemplateDefinition().isEmpty()) {
        final Query q = new Query(getEntity().getQueryTemplateDefinition());
        try {
          q.parse();
          switch (q.getType()) {
            case COUNT:
              getEntity().addError(ServerMessages.QUERY_TEMPLATE_WITH_COUNT);
              break;

            case SELECT:
              getEntity().addError(ServerMessages.QUERY_TEMPLATE_WITH_SELECT);
              break;

            default:
              break;
          }
        } catch (final ParsingException e) {
          getEntity().addError(ServerMessages.QUERY_PARSING_ERROR);
        }
      } else {
        getEntity().addError(ServerMessages.QUERY_TEMPLATE_HAS_NO_QUERY_DEFINITION);
      }
    }
  }
}
