/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.List;
import java.util.Objects;
import org.caosdb.server.database.backend.transaction.GetDependentEntities;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether an entity is referenced by other entities, is being used as a data type by other
 * entities or has direct children which are not to be deleted along with this entity and add an
 * appropriate error if so.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class CheckDependenciesBeforeDeletion extends EntityJob {
  @Override
  public final void run() {
    if (getEntity().getDomain() == null
        || Objects.equals(getEntity().getDomain(), EntityID.DEFAULT_DOMAIN)) {
      // if (getContainer().contains(getEntity())) {

      // retrieve dependent entities
      final List<EntityID> depends =
          execute(new GetDependentEntities(getEntity().getId())).getList();

      // loop:
      for (final EntityID id : depends) {
        final EntityInterface foreign = getEntityById(id);
        if (foreign == null) {
          // dependent entity is not in the container and will not be
          // deleted. Therefore, this entity cannot be deleted either.
          getEntity().addError(ServerMessages.REQUIRED_BY_PERSISTENT_ENTITY);
          getEntity().addInfo("Required by entity " + id + ".");
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
        }
      }
    }
  }
}
