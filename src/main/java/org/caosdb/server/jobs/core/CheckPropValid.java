/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import static org.caosdb.server.utils.ServerMessages.ENTITY_DOES_NOT_EXIST;

import com.google.common.base.Objects;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether all properties of an entity are valid or qualified.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(stage = TransactionStage.PRE_CHECK)
public class CheckPropValid extends EntityJob {
  @Override
  public final void run() {

    // loop over all properties of the entity
    for (final Property property : getEntity().getProperties()) {
      try {
        if (property.getEntityStatus() == EntityStatus.QUALIFIED) {
          // this property is to be tested.

          if (!property.hasName() && !property.hasId()) {
            // The property has neither an id nor a name.
            // Thus it cannot be identified.

            throw ServerMessages.ENTITY_HAS_NO_NAME_OR_ID;
          }
          EntityInterface abstractProperty =
              resolve(property.getId(), property.getName(), (String) null);
          if (abstractProperty == null) {
            property.addError(ENTITY_DOES_NOT_EXIST);
            continue;
          }
          assertAllowedToUse(abstractProperty);
          deriveOverrideStatus(property, abstractProperty);
          property.linkIdToEntity(abstractProperty);
        }
      } catch (final Message m) {
        property.addError(m);
      } catch (AuthorizationException e) {
        property.addError(ServerMessages.AUTHORIZATION_ERROR);
        property.addInfo(e.getMessage());
      } catch (final EntityDoesNotExistException e) {
        property.addError(ENTITY_DOES_NOT_EXIST);
      } catch (final EntityWasNotUniqueException e) {
        property.addError(ServerMessages.ENTITY_NAME_DUPLICATES);
      }
    }

    // process names
    appendJob(ProcessNameProperties.class);
  }

  private void assertAllowedToUse(final EntityInterface property) {
    property.checkPermission(EntityPermission.USE_AS_PROPERTY);
  }

  private static void deriveOverrideStatus(final Property child, final EntityInterface parent) {
    if (!Objects.equal(child.getName(), parent.getName())) {
      if (child.hasName()) {
        child.setNameOverride(true);
      } else {
        child.setName(parent.getName());
      }
    }
    if (!Objects.equal(child.getDescription(), parent.getDescription())) {
      if (child.hasDescription()) {
        child.setDescOverride(true);
      } else {
        child.setDescription(parent.getDescription());
      }
    }
    if (!Objects.equal(child.getDatatype(), parent.getDatatype())) {
      if (child.hasDatatype()
          // FIXME why this?
          && (!child.getDatatype().toString().equals("REFERENCE") || parent.hasDatatype())) {
        child.setDatatypeOverride(true);
      } else {
        child.setDatatype(parent.getDatatype());
      }
    }
  }
}
