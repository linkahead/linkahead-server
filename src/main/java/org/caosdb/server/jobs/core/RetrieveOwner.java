/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import java.util.List;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.permissions.ResponsibleAgent;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

@JobAnnotation(
    stage = TransactionStage.POST_TRANSACTION,
    flag = "owner",
    description = "Adds an XML representation of the owner(s) to the output of this entity.")
public class RetrieveOwner extends FlagJob {

  @Override
  protected void job(final String value) {

    for (final EntityInterface entity : getContainer()) {
      if (entity.getId() != null && !entity.getId().isTemporary()) {
        try {
          if (getTransaction() instanceof Retrieve) {
            entity.checkPermission(EntityPermission.RETRIEVE_OWNER);
          }
          entity.addMessage(
              ret -> {
                if (entity.getEntityACL() != null) {
                  final List<ResponsibleAgent> owners = entity.getEntityACL().getOwners();
                  for (final ResponsibleAgent o : owners) {
                    final Element e = new Element("Owner");
                    o.addToElement(e);
                    ret.addContent(e);
                  }
                }
              });
        } catch (final AuthorizationException e) {
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          entity.addError(ServerMessages.AUTHORIZATION_ERROR);
          entity.addInfo(e.getMessage());
        }
      }
    }
  }
}
