/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether at least one property is present.
 *
 * @author tf
 */
public class CheckPropPresent extends EntityJob {
  @Override
  public final void run() {
    if (getEntity().getProperties().isEmpty()) {
      switch (getFailureSeverity()) {
        case ERROR:
          getEntity().addError(ServerMessages.ENTITY_HAS_NO_PROPERTIES);
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
          break;
        case WARN:
          getEntity().addWarning(ServerMessages.ENTITY_HAS_NO_PROPERTIES);
        default:
          break;
      }
    }
  }
}
