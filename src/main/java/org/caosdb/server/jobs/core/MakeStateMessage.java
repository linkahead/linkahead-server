/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.List;
import java.util.Map;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.Retrieve;
import org.jdom2.Element;

/**
 * Remove the state property from the entity and, iff necessary, convert it into a State instance
 * which is then being appended to the entity's messages.
 *
 * <p>If this job belongs to a Write transaction there is already a State instance present and the
 * conversion is not necessary.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
@JobAnnotation(
    loadAlways = true,
    transaction = Retrieve.class,
    stage = TransactionStage.POST_TRANSACTION)
public class MakeStateMessage extends EntityStateJob {

  public static final String SPARSE_FLAG = "sparseState";

  @Override
  protected void run() {

    if ("ENABLED".equals(CaosDBServer.getServerProperty(SERVER_PROPERTY_EXT_ENTITY_STATE))) {
      try {
        // fetch all state properties and remove them from the entity (indicated by "true")
        List<Property> stateProperties = getStateProperties(true);
        State stateMessage = getState(false);

        if (stateMessage != null) {
          // trigger retrieval of state model because when the XML Writer calls the addToElement
          // method, it is to late.
          stateMessage.getStateModel();
        } else if (stateProperties != null && stateProperties.size() > 0) {
          for (Property s : stateProperties) {
            getEntity().addMessage(getMessage(s, isSparse()));
          }
        }
      } catch (Message e) {
        getEntity().addError(e);
      }
    }
  }

  private ToElementable getMessage(Property s, boolean sparse) throws Message {
    if (sparse) {
      return getSparseStateMessage(s);
    }
    State stateMessage = createState(s);

    // trigger retrieval of state model because when the XML Writer calls the addToElement method,
    // it is to late.
    stateMessage.getStateModel();
    return stateMessage;
  }

  private ToElementable getSparseStateMessage(Property s) {
    return new ToElementable() {
      @Override
      public void addToElement(Element ret) {
        Element state = new Element(STATE_XML_TAG);
        state.setAttribute(STATE_ATTRIBUTE_ID, s.getValue().toString());
        ret.addContent(state);
      }
    };
  }

  private boolean isSparse() {
    Map<String, String> flags = getTransaction().getContainer().getFlags();
    if (flags != null) {
      return "true".equals(flags.getOrDefault(SPARSE_FLAG, "false"));
    }
    return false;
  }
}
