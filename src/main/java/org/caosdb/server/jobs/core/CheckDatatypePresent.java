/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.List;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.database.exceptions.EntityWasNotUniqueException;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.ReferenceDatatype2;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Role;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.Job;
import org.caosdb.server.jobs.ScheduledJob;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether the entity has a data type.
 *
 * <p>Assign the data type of the abstract property if necessary.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public final class CheckDatatypePresent extends EntityJob {

  @Override
  public final void run() {
    try {

      // inherit datatype
      if (!getEntity().hasDatatype()) {
        if (!(getEntity() instanceof InsertEntity)) {
          resolveId(getEntity());
        }

        inheritDatatypeFromAbstractEntity();

        // still no data type ??? try to get it from parent...
        if (!getEntity().hasDatatype() && getEntity().hasParents()) {
          getDataTypeFromParent();
        }

      } else {

        // check if this data type is an overridden data type
        checkIfOverride();
      }

      // resolve reference data types, run jobs
      if (getEntity().hasDatatype()) {

        if (getEntity().getDatatype() instanceof ReferenceDatatype2) {
          checkReference2((ReferenceDatatype2) getEntity().getDatatype());
        } else if (getEntity().getDatatype() instanceof AbstractCollectionDatatype) {
          final AbstractCollectionDatatype datatype =
              (AbstractCollectionDatatype) getEntity().getDatatype();
          if (datatype.getDatatype() instanceof ReferenceDatatype2) {
            checkReference2((ReferenceDatatype2) datatype.getDatatype());
          }
        }

        // run jobsreturn this.entities;
        final List<Job> datatypeJobs = loadDataTypeSpecificJobs();
        if (datatypeJobs != null) {
          final List<ScheduledJob> scheduledJobs =
              getTransaction().getSchedule().addAll(datatypeJobs);
          for (final ScheduledJob job : scheduledJobs) {
            getTransaction().getSchedule().runJob(job);
          }
        }
      } else {

        // finally, no data type
        throw ServerMessages.PROPERTY_HAS_NO_DATATYPE;
      }
    } catch (final Message m) {
      if (m == ServerMessages.ENTITY_DOES_NOT_EXIST) {
        getEntity().addError(ServerMessages.UNKNOWN_DATATYPE);
      } else {
        getEntity().addError(m);
      }
    } catch (AuthorizationException exc) {
      getEntity().addError(ServerMessages.AUTHORIZATION_ERROR);
      getEntity().addInfo(exc.getMessage());
    } catch (final EntityDoesNotExistException exc) {
      getEntity().addError(ServerMessages.UNKNOWN_DATATYPE);
    } catch (final EntityWasNotUniqueException exc) {
      getEntity().addError(ServerMessages.DATA_TYPE_NAME_DUPLICATES);
    }
  }

  private void checkReference2(final ReferenceDatatype2 datatype) throws Message {
    EntityID datatypeId = datatype.getId();
    if (datatypeId == null) {
      datatypeId = new EntityID(datatype.getName());
    }
    EntityInterface datatypeEntity = resolve(datatypeId, datatype.getName(), (String) null);

    if (datatypeEntity == null) {
      throw ServerMessages.UNKNOWN_DATATYPE;
    }

    // do the actual checking
    assertAllowedToUse(datatypeEntity);
    datatype.setEntity(datatypeEntity);
  }

  private void assertAllowedToUse(final EntityInterface datatype) {
    datatype.checkPermission(EntityPermission.USE_AS_DATA_TYPE);
  }

  private void checkIfOverride() throws Message {
    EntityInterface abstractProperty = resolve(getEntity());
    if (abstractProperty != null
        && abstractProperty.hasDatatype()
        && !abstractProperty.getDatatype().equals(getEntity().getDatatype())) {
      // is override!
      getEntity().setDatatypeOverride(true);
    }
  }

  /**
   * If this is a record type property or a concrete property, assign the data type of the
   * corresponding abstract property.
   */
  private void inheritDatatypeFromAbstractEntity() throws Message {
    EntityInterface abstractProperty = resolve(getEntity());
    if (abstractProperty != null && abstractProperty.hasDatatype()) {
      getEntity().setDatatype(abstractProperty.getDatatype());
    } else if (abstractProperty != null
        && abstractProperty != getEntity()
        && abstractProperty.getRole() == Role.RecordType) {
      getEntity().setDatatype(ReferenceDatatype2.datatypeFactory(abstractProperty.getId()));
    }
  }

  private void resolveId(final EntityInterface entity) {
    if (!entity.hasId() && entity.hasName()) {
      try {
        EntityInterface resolved = resolve(entity);
        if (resolved == null) {
          entity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
        } else {
          entity.setId(resolved.getId());
        }
      } catch (final EntityWasNotUniqueException exc) {
        entity.addError(ServerMessages.ENTITY_NAME_DUPLICATES);
      }
    }
  }

  private void getDataTypeFromParent() throws Message {
    runJobFromSchedule(getEntity(), CheckParValid.class);

    AbstractDatatype datatype = null;
    for (final EntityInterface parent : getEntity().getParents()) {
      EntityInterface parentEntity = resolve(parent);
      if (!parentEntity.hasDatatype() && parentEntity.getEntityStatus() == EntityStatus.QUALIFIED) {
        runJobFromSchedule(parentEntity, CheckDatatypePresent.class);
      }
      if (parentEntity.hasDatatype()
          && parentEntity.getEntityStatus() != EntityStatus.UNQUALIFIED) {
        if (datatype != null && !parentEntity.getDatatype().equals(datatype)) {
          getEntity().addError(ServerMessages.DATATYPE_INHERITANCE_AMBIGUOUS);
          return;
        } else {
          datatype = parentEntity.getDatatype();
        }
      }
    }
    if (datatype != null) {
      getEntity().setDatatype(datatype);
    }
  }
}
