/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.jobs.core;

import java.util.Map;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.WriteTransaction;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check if the attempted state transition is allowed.
 *
 * <p>This job checks if the attempted state transition is in compliance with the state model. This
 * job runs during the CHECK phase and should do all necessary consistency and permission checks.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(stage = TransactionStage.POST_CHECK, transaction = WriteTransaction.class)
public class CheckStateTransition extends EntityStateJob {

  public static final class StateModelPermission extends ACMPermissions {

    public static final String STATE_MODEL_PARAMETER = "?STATE_MODEL?";

    public StateModelPermission(String permission, String description) {
      super(permission, description);
    }

    public final String toString(String state_model) {
      return toString().replace(STATE_MODEL_PARAMETER, state_model);
    }

    public static String init() {
      return StateModelPermission.class.getSimpleName();
    }
  }

  public static final StateModelPermission PERMISSION_STATE_FORCE_FINAL =
      new StateModelPermission(
          "STATE:FORCE:FINAL",
          "Permission to force to leave a state models specified life-cycle even though the currrent state isn't a final state in the that model.");
  public static final StateModelPermission PERMISSION_STATE_UNASSIGN =
      new StateModelPermission(
          "STATE:UNASSIGN:" + StateModelPermission.STATE_MODEL_PARAMETER,
          "Permission to unassign a state model.");
  public static final StateModelPermission PERMISSION_STATE_ASSIGN =
      new StateModelPermission(
          "STATE:ASSIGN:" + StateModelPermission.STATE_MODEL_PARAMETER,
          "Permission to assign a state model.");
  private static final Message TRANSITION_NOT_ALLOWED =
      new Message(MessageType.Error, "Transition not allowed.");
  private static final Message INITIAL_STATE_NOT_ALLOWED =
      new Message(MessageType.Error, "Initial state not allowed.");
  private static final Message FINAL_STATE_NOT_ALLOWED =
      new Message(MessageType.Error, "Final state not allowed.");

  /**
   * The forceFinalState flag is especially useful if you want to delete entities in the middle of
   * the state machine's usual state cycle.
   */
  private static final String FLAG_FORCE_FINAL_STATE = "forceFinalState";

  @Override
  protected void run() {
    try {
      State newState = getState();
      if (newState != null) {
        checkStateValid(newState);
      }
      if (getEntity() instanceof UpdateEntity) {
        State oldState = getState(((UpdateEntity) getEntity()).getOriginal());
        checkStateTransition(oldState, newState);
      } else if (getEntity() instanceof DeleteEntity) {
        if (newState != null) checkFinalState(newState);
      } else { // fresh Entity
        if (newState != null) checkInitialState(newState);
      }
    } catch (Message m) {
      getEntity().addError(m);
    } catch (AuthorizationException e) {
      getEntity().addError(ServerMessages.AUTHORIZATION_ERROR);
      getEntity().addInfo(e.getMessage());
    }
  }

  /**
   * Check if the state belongs to the state model.
   *
   * <p>In practical terms, throw a Message if the state is invalid.
   *
   * @param state
   * @throws Message
   */
  private void checkStateValid(State state) throws Message {
    if (state.isFinal() || state.isInitial() || state.getStateModel().getStates().contains(state)) {
      return;
    }
    throw STATE_NOT_IN_STATE_MODEL;
  }

  /**
   * Check if state is valid and transition is allowed.
   *
   * <p>Especially, transitions between {@code null} states are allowed, non-trivial transitions
   * from or to {@code null} must be initial or final states, respectively ({@link
   * FORCE_FINAL_STATE} exception applies).
   *
   * @param oldState
   * @param newState
   * @throws Message if not
   */
  private void checkStateTransition(State oldState, State newState) throws Message {
    if (oldState == null && newState == null) {
      return;
    } else if (oldState == null && newState != null) {
      checkInitialState(newState);
      return;
    } else if (newState == null && oldState != null) {
      checkFinalState(oldState);
      return;
    }

    StateModel stateModel = findMatchingStateModel(oldState, newState);
    if (stateModel == null) {
      // change from one stateModel to another
      checkInitialState(newState);
      checkFinalState(oldState);
      return;
    }

    boolean transition_defined = false;
    for (Transition t : stateModel.getTransitions()) {
      if (t.isFromState(oldState) && t.isToState(newState)) {
        transition_defined = true;
        if (t.isPermitted(getUser())) {
          return;
        }
      }
    }
    if (transition_defined) {
      throw new AuthorizationException(
          getUser().getPrincipal().toString()
              + " doesn't have permission to perform this transition.");
    }
    throw TRANSITION_NOT_ALLOWED;
  }

  /**
   * Return the two State's common StateModel, or {@code null} if they don't have one in common.
   *
   * @param oldState
   * @param newState
   * @return the state model which contains both of the states.
   * @throws Message if the state model of one of the states cannot be constructed.
   */
  private StateModel findMatchingStateModel(State oldState, State newState) throws Message {
    if (oldState.getStateModel().equals(newState.getStateModel())) {
      return oldState.getStateModel();
    }
    return null;
  }

  /**
   * Check if the old state is final or if the {@link FORCE_FINAL_STATE} flag is true.
   *
   * @param oldState
   * @throws Message if the state is not final.
   */
  private void checkFinalState(State oldState) throws Message {
    if (!oldState.isFinal()) {
      if (isForceFinal()) {
        getUser().checkPermission(PERMISSION_STATE_FORCE_FINAL.toString());
      } else {
        throw FINAL_STATE_NOT_ALLOWED;
      }
    }
    getUser().checkPermission(PERMISSION_STATE_UNASSIGN.toString(oldState.getStateModelName()));
  }

  /**
   * Check if the new state is an initial state.
   *
   * @param newState
   * @throws Message if not
   */
  private void checkInitialState(State newState) throws Message {
    if (!newState.isInitial()) {
      throw INITIAL_STATE_NOT_ALLOWED;
    }
    getUser().checkPermission(PERMISSION_STATE_ASSIGN.toString(newState.getStateModelName()));
  }

  private boolean isForceFinal() {
    Map<String, String> containerFlags = getTransaction().getContainer().getFlags();
    return (containerFlags != null
            && "true".equalsIgnoreCase(containerFlags.get(FLAG_FORCE_FINAL_STATE)))
        || "true".equalsIgnoreCase(getEntity().getFlag(FLAG_FORCE_FINAL_STATE));
  }
}
