/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.database.backend.transaction.RetrieveAll;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.utils.EntityStatus;

@JobAnnotation(flag = "all", stage = TransactionStage.INIT, transaction = Retrieve.class)
public class RetrieveAllJob extends FlagJob {

  @Override
  protected void job(String value) {
    if (getContainer().isEmpty()) {
      // run paging job first
      getTransaction().getSchedule().runJob(null, Paging.class);

      if (value == null) {
        value = "ENTITY";
      }

      execute(new RetrieveAll(getContainer(), value));

      int startIndex = 0;
      int endIndex = getContainer().size();

      if (((Retrieve) getTransaction()).hasPaging()) {
        startIndex =
            Math.min(getContainer().size(), ((Retrieve) getTransaction()).getPaging().startIndex);
        endIndex =
            Math.min(getContainer().size(), ((Retrieve) getTransaction()).getPaging().endIndex);
      }

      // only add jobs for those which are on this page
      int ii = 0;
      for (EntityInterface entity : getContainer()) {
        if (ii >= startIndex && ii < endIndex) {
          getTransaction().getSchedule().addAll(loadJobs(entity, getTransaction()));
        } else {
          entity.setEntityStatus(EntityStatus.IGNORE);
        }
        ii++;
      }
    }
  }
}
