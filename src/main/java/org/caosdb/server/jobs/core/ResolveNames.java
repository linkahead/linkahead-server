/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.database.backend.transaction.GetIDByName;
import org.caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import org.caosdb.server.database.exceptions.EntityDoesNotExistException;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.jobs.ContainerJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Resolve the names to ids for all entities in the current retrieval.
 *
 * <p>If an entity has a name which looks like an id: Check whether that is an existing id first.
 *
 * <p>Otherwise, search via the name.
 *
 * <p>It there are multiple matching entities, add all of them to the current container. The clients
 * may handle the ambiguity.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class ResolveNames extends ContainerJob {

  @Override
  protected void run() {
    resolve(getContainer());
  }

  public void resolve(final TransactionContainer container) {
    final ArrayList<Entity> add = new ArrayList<Entity>();
    for (final EntityInterface e : container) {
      if (!e.hasId() && e.hasName() && getTransaction().matchIdPattern(e.getName())) {
        try {
          EntityInterface valid =
              execute(
                      new RetrieveSparseEntity(
                          new EntityID(e.getName()),
                          e.hasVersion() ? e.getVersion().getId() : null))
                  .getEntity();
          e.setId(valid.getId());
        } catch (final EntityDoesNotExistException exc) {
          // Definitely not an existing id.
        }
      }

      if (e.hasName() && !e.hasId()) {
        try {
          final List<EntityID> c = execute(new GetIDByName(e.getName(), false)).getList();

          e.setId(c.get(0));
          e.setEntityStatus(EntityStatus.QUALIFIED);
          for (int i = 1; i < c.size(); i++) {
            final Entity e2 = new RetrieveEntity(c.get(i), e.getRole());
            e2.setEntityStatus(EntityStatus.QUALIFIED);
            add.add(e2);
          }
        } catch (final EntityDoesNotExistException exc) {
          e.setEntityStatus(EntityStatus.NONEXISTENT);
          e.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
        }
      }
    }
    container.addAll(add);
  }
}
