/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.jobs.FlagJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.jobs.TransactionStage;

/**
 * Turn off caching, just for the current transaction.
 *
 * <p>If the caching is disabled globally this has no effect.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@JobAnnotation(
    flag = "cache",
    defaultValue = "true",
    description =
        "Depending on the configuraion of the server some transactions with the backend database might be cached internally for performance reasons. Disable the internal caching for this transaction with 'false'. Note: You cannot enable the caching with 'true', if the server is not configured to use caching.",
    values = {"true", "false"},
    stage = TransactionStage.INIT)
public class NoCache extends FlagJob {

  @Override
  protected void job(final String value) {
    getTransaction().setUseCache(Boolean.valueOf(value));
  }
}
