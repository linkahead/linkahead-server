package org.caosdb.server.jobs.core;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.jobs.JobAnnotation;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

@JobAnnotation(
    flag = "noAdditionalProperties",
    description =
        "Fail if this entity has any property which has not been defined by the parents of this entity.")
public class CheckNoAdditionalPropertiesPresent extends EntityJob {

  private Set<EntityInterface> used = new HashSet<>();

  @Override
  protected void run() {

    // validate all parents and properties
    runJobFromSchedule(getEntity(), CheckParOblPropPresent.class);

    if (getEntity().getEntityStatus() == EntityStatus.QUALIFIED && getEntity().hasProperties()) {
      for (EntityInterface property : getEntity().getProperties()) {
        checkProperty(property);
      }
    }
  }

  public void checkProperty(EntityInterface property) {
    for (EntityInterface parent : getEntity().getParents()) {
      for (EntityInterface parentProperty : parent.getProperties()) {
        if (sameProperty(property, parentProperty) && !used.contains(parentProperty)) {
          used.add(parentProperty);
          return;
        }
      }
      // not found!
      addMessage(property, ServerMessages.ADDITIONAL_PROPERTY);
    }
  }

  private void addMessage(EntityInterface property, Message message) {
    if (getFailureSeverity() == JobFailureSeverity.ERROR) {
      property.addError(message);
    } else {
      property.addWarning(message);
    }
  }

  private boolean sameProperty(EntityInterface property, EntityInterface parentProperty) {
    return property.getId() != null && Objects.equals(property.getId(), parentProperty.getId());
  }
}
