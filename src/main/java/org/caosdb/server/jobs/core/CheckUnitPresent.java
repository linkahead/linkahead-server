/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.jobs.core;

import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.MagicTypes;
import org.caosdb.server.jobs.EntityJob;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * Check whether this entity has a unit property.
 *
 * @author tf
 */
public class CheckUnitPresent extends EntityJob {

  @Override
  protected void run() {
    runJobFromSchedule(getEntity(), Inheritance.class);

    if (!hasUnit(getEntity())) {
      switch (getFailureSeverity()) {
        case ERROR:
          getEntity().addError(ServerMessages.PROPERTY_HAS_NO_UNIT);
          getEntity().setEntityStatus(EntityStatus.UNQUALIFIED);
          break;
        case WARN:
          getEntity().addWarning(ServerMessages.PROPERTY_HAS_NO_UNIT);
        default:
          break;
      }
    }
  }

  private boolean hasUnit(final EntityInterface entity) {
    for (final EntityInterface property : entity.getProperties()) {
      if (MagicTypes.getType(property.getId()) == MagicTypes.UNIT) {
        return true;
      }
    }
    return false;
  }
}
