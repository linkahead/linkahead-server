/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.jobs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.wrapper.EntityWrapper;
import org.caosdb.server.jobs.core.JobFailureSeverity;
import org.caosdb.server.transaction.Transaction;
import org.caosdb.server.utils.ConfigurationException;

/**
 * Configuration of the jobs which have to run during all entity transactions with "job rules".
 *
 * <p>A job rule contains of a quintuple (domain, entity, transaction type, job name, job failure
 * severity).
 *
 * <p>The domain and entity define for which entities the job (of the given name) should be loaded.
 * The transaction type (e.g. Insert, Update, Delete) defines the transaction where this rule
 * applies. The job failure severity sets the related property of a job.
 *
 * <p>The configuration is being read by default from a config file {@link
 * ServerProperties#KEY_JOB_RULES_CONFIG}
 *
 * <p>This class works as a singleton, most of the time. It is possible to instanciate it otherwise,
 * though.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class JobConfig {

  private static JobConfig instance;

  static {
    try {
      instance = new JobConfig();
    } catch (final Exception e) {
      e.printStackTrace();
      // CaosDB Server cannot work without a proper job configuration.
      System.exit(1);
    }
  }

  /**
   * The keys of these rules consist of the domain, entity and transaction. The value is a pair (job
   * name, job failure severity).
   */
  private final Map<String, List<Object[]>> rules;

  public JobConfig() throws FileNotFoundException, IOException {
    this(new File(CaosDBServer.getServerProperty(ServerProperties.KEY_JOB_RULES_CONFIG)));
  }

  public JobConfig(final File config) throws FileNotFoundException, IOException {
    this.rules = getRulesFromCSV(config);
  }

  /**
   * Read the rules from the CSV file.
   *
   * @param file
   * @return A map with all the rules.
   * @throws FileNotFoundException
   * @throws IOException
   */
  private Map<String, List<Object[]>> getRulesFromCSV(final File file)
      throws FileNotFoundException, IOException {
    try (final BufferedReader reader = new BufferedReader(new FileReader(file))) {
      final Map<String, List<Object[]>> result = new HashMap<>();
      reader
          .lines()
          .forEach(
              line -> {
                // cut away comments, trim, then split into columns
                final String[] row = line.replaceFirst("#.*$", "").strip().split("\\s*,\\s*");
                if (row.length == 1 && row[0].isBlank()) {
                  // comment or empty line
                  // ignore
                  return;
                }
                addRule(result, row);
              });
      return result;
    }
  }

  /** Generate a unique key (for class-internal use only). */
  private String getKey(final EntityID domain, final EntityID entity, final String transaction) {
    final StringBuilder sb = new StringBuilder();
    sb.append("<");
    sb.append(domain);
    sb.append(",");
    sb.append(transaction.toLowerCase());
    sb.append(",");
    sb.append(entity);
    sb.append(">");
    return sb.toString();
  }

  /**
   * Read a rule from the csv-row and put it into the 'result' map
   *
   * @param result map with all the rules.
   * @param row represents a single rule as a comma-separated quintuple.
   */
  private void addRule(final Map<String, List<Object[]>> result, final String[] row) {
    if (row.length != 5) {
      throw new ConfigurationException(
          "Could not parse the job rules. Lines of five comma-separated values expected");
    }
    try {
      final String domain = row[0];
      final String entity = row[1];
      final String transaction = row[2];
      final String job = row[3];
      final JobFailureSeverity severity = JobFailureSeverity.valueOf(row[4]);
      final String key = getKey(new EntityID(domain), new EntityID(entity), transaction);
      if (!result.containsKey(key)) {
        result.put(key, new ArrayList<>());
      }
      result.get(key).add(new Object[] {job, severity});

    } catch (final Exception e) {
      throw new ConfigurationException("Could not parse the job rules.", e);
    }
  }

  /**
   * Factory method for the instantiation and configuration of jobs for a specific entity.
   *
   * @param domain the domain of the rule
   * @param entity the entity of the rule (this might be a particular datatype or a role, like
   *     "RecordType")
   * @param target the entity for which the job will run.
   * @param transaction the transaction for which the job will run.
   * @return Fresh job instances, one for each matching job rule.
   */
  public List<Job> getConfiguredJobs(
      final EntityID domain,
      final EntityID entity,
      final EntityInterface target,
      final Transaction<? extends TransactionContainer> transaction) {
    String transactionType;
    if (target instanceof EntityWrapper) {
      transactionType = getTransactionType(((EntityWrapper) target).getWrapped());
    } else {
      transactionType = getTransactionType(target);
    }
    final String key = getKey(domain, entity, transactionType);
    final List<Object[]> jobRules = rules.get(key);
    if (jobRules != null) {
      final List<Job> result = new LinkedList<>();
      jobRules.forEach(
          config -> {
            Job j =
                Job.getJob((String) config[0], (JobFailureSeverity) config[1], target, transaction);
            if (j == null) {
              throw new NullPointerException("Could not load job: " + config[0]);
            } else {
              result.add(j);
            }
          });
      return result;
    }
    return null;
  }

  public static JobConfig getInstance() {
    return instance;
  }

  /** Return the type of transaction of an entity, e.g. "Retrieve" for a {@link RetrieveEntity}. */
  String getTransactionType(final EntityInterface e) {
    return e.getClass().getSimpleName().replace("Entity", "");
  }
}
