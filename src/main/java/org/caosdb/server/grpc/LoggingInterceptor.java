package org.caosdb.server.grpc;

import io.grpc.Attributes.Key;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCall.Listener;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import java.net.SocketAddress;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.restlet.routing.Template;
import org.restlet.util.Resolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class CallResolver<ReqT, RespT> extends Resolver<String> {

  private ServerCall<ReqT, RespT> call;
  private Metadata headers;

  public static final Metadata.Key<String> KEY_USER_AGENT =
      Metadata.Key.of("user-agent", Metadata.ASCII_STRING_MARSHALLER);
  public static final Key<SocketAddress> KEY_LOCAL_ADDRESS = io.grpc.Grpc.TRANSPORT_ATTR_LOCAL_ADDR;
  public static final Key<SocketAddress> KEY_REMOTE_ADDRESS =
      io.grpc.Grpc.TRANSPORT_ATTR_REMOTE_ADDR;

  public CallResolver(
      ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
    this.call = call;
    this.headers = headers;
  }

  @Override
  public String resolve(String name) {
    switch (name) {
      case "user-agent":
        return headers.get(KEY_USER_AGENT);
      case "remote-address":
        return call.getAttributes().get(KEY_REMOTE_ADDRESS).toString();
      case "local-address":
        return call.getAttributes().get(KEY_LOCAL_ADDRESS).toString();
      case "method":
        return call.getMethodDescriptor().getFullMethodName();
      default:
        break;
    }
    return null;
  }
}

public class LoggingInterceptor implements ServerInterceptor {

  private Template template;

  public LoggingInterceptor() {
    String format = CaosDBServer.getServerProperty(ServerProperties.KEY_GRPC_RESPONSE_LOG_FORMAT);
    if ("OFF".equalsIgnoreCase(format)) {
      this.template = null;
    } else if (format != null) {
      this.template = new Template(format);
    }
  }

  private static final Logger logger = LoggerFactory.getLogger(LoggingInterceptor.class.getName());

  @Override
  public <ReqT, RespT> Listener<ReqT> interceptCall(
      ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
    if (template != null) {
      logger.info(template.format(new CallResolver<ReqT, RespT>(call, headers, next)));
    }
    return Contexts.interceptCall(Context.current(), call, headers, next);
  }
}
