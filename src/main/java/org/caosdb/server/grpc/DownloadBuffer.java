package org.caosdb.server.grpc;

import com.google.protobuf.ByteString;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import org.caosdb.api.entity.v1.FileChunk;
import org.caosdb.api.entity.v1.FileDownloadResponse;
import org.caosdb.api.entity.v1.TransmissionStatus;
import org.caosdb.server.entity.FileProperties;

public class DownloadBuffer {

  private final FileProperties file_properties;
  private FileInputStream fileInputStream;
  private FileChannel fileChannel;

  public DownloadBuffer(final FileProperties file_properties) {
    this.file_properties = file_properties;
    this.fileInputStream = null;
  }

  public FileProperties getFileProperties() {
    return file_properties;
  }

  public FileDownloadResponse getNextChunk() throws FileNotFoundException, IOException {
    if (fileChannel == null) {
      fileInputStream = new FileInputStream(file_properties.getFile());
      fileChannel = fileInputStream.getChannel();
    }
    final long position = fileChannel.position();
    final long unread_bytes = fileChannel.size() - position;
    final long next_chunk_size = Math.min(unread_bytes, getChunkSize());

    final MappedByteBuffer map = fileChannel.map(MapMode.READ_ONLY, position, next_chunk_size);
    fileChannel.position(position + next_chunk_size);

    final FileChunk.Builder builder = FileChunk.newBuilder();
    builder.setData(ByteString.copyFrom(map));

    final TransmissionStatus status;
    if (fileInputStream.available() > 0) {
      status = TransmissionStatus.TRANSMISSION_STATUS_GO_ON;
    } else {
      status = TransmissionStatus.TRANSMISSION_STATUS_SUCCESS;
      cleanUp();
    }
    return FileDownloadResponse.newBuilder().setChunk(builder).setStatus(status).build();
  }

  public void cleanUp() {
    try {
      if (fileChannel != null && fileChannel.isOpen()) {
        fileChannel.close();
      }
      if (fileInputStream != null) {
        fileInputStream.close();
      }
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private int getChunkSize() {
    // 16kB
    return 16384;
  }
}
