/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.grpc;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.TimeZone;
import org.caosdb.api.entity.v1.AtomicDataType;
import org.caosdb.api.entity.v1.CollectionValues;
import org.caosdb.api.entity.v1.DataType;
import org.caosdb.api.entity.v1.Entity;
import org.caosdb.api.entity.v1.Entity.Builder;
import org.caosdb.api.entity.v1.EntityACL;
import org.caosdb.api.entity.v1.EntityAclPermission;
import org.caosdb.api.entity.v1.EntityPermissionRule;
import org.caosdb.api.entity.v1.EntityPermissionRuleCapability;
import org.caosdb.api.entity.v1.EntityResponse;
import org.caosdb.api.entity.v1.EntityRole;
import org.caosdb.api.entity.v1.FileDescriptor;
import org.caosdb.api.entity.v1.Importance;
import org.caosdb.api.entity.v1.ListDataType;
import org.caosdb.api.entity.v1.MessageCode;
import org.caosdb.api.entity.v1.Parent;
import org.caosdb.api.entity.v1.ReferenceDataType;
import org.caosdb.api.entity.v1.ScalarValue;
import org.caosdb.api.entity.v1.SelectQueryColumn;
import org.caosdb.api.entity.v1.SelectQueryHeader;
import org.caosdb.api.entity.v1.SelectQueryResult;
import org.caosdb.api.entity.v1.SelectQueryRow;
import org.caosdb.api.entity.v1.SpecialValue;
import org.caosdb.api.entity.v1.Version;
import org.caosdb.datetime.DateTimeInterface;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.BooleanDatatype;
import org.caosdb.server.datatype.BooleanValue;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.DateTimeDatatype;
import org.caosdb.server.datatype.DoubleDatatype;
import org.caosdb.server.datatype.FileDatatype;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.IndexedSingleValue;
import org.caosdb.server.datatype.IntegerDatatype;
import org.caosdb.server.datatype.ReferenceDatatype;
import org.caosdb.server.datatype.ReferenceDatatype2;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.datatype.TextDatatype;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.MagicTypes;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.StatementStatusInterface;
import org.caosdb.server.entity.container.ParentContainer;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.permissions.EntityACI;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.query.Query;
import org.caosdb.server.query.Query.Selection;

public class CaosDBToGrpcConverters {

  private TimeZone timeZone;

  public CaosDBToGrpcConverters(TimeZone timeZone) {
    this.timeZone = timeZone;
  }

  /** Get the unit as string. */
  public String getStringUnit(final EntityInterface entity) {
    final Iterator<Property> iterator = entity.getProperties().iterator();
    while (iterator.hasNext()) {
      final Property p = iterator.next();
      if (Objects.equals(MagicTypes.UNIT.getId(), p.getId())) {
        iterator.remove();
        return p.getValue().toString();
      }
    }
    return null;
  }

  public EntityResponse.Builder convert(final EntityInterface from) {

    // @review Florian Spreckelsen 2022-03-22

    SerializeFieldStrategy s = from.getSerializeFieldStrategy();
    final Builder entityBuilder = Entity.newBuilder();

    if (from.hasId() && s.isToBeSet("id")) {
      entityBuilder.setId(from.getId().toString());
    }
    if (from.getRole() != null && s.isToBeSet("role")) {
      entityBuilder.setRole(convert(from.getRole()));
    }
    if (from.hasName() && s.isToBeSet("name")) {
      entityBuilder.setName(from.getName());
    }
    if (from.hasDescription() && s.isToBeSet("description")) {
      entityBuilder.setDescription(from.getDescription());
    }
    if (from.hasDatatype() && s.isToBeSet("datatype")) {
      entityBuilder.setDataType(convert(from.getDatatype()));
    }
    if (from.hasValue() && s.isToBeSet("value")) {
      try {
        from.parseValue();
      } catch (final Message e) {
        // ignore. This problem should be handled elsewhere because this is
        // only for the serialization of the data and not for the validation.
        // In any case, the string representation can be used.
      }
      entityBuilder.setValue(convert(from.getValue()));
    }
    final String unit = getStringUnit(from);
    if (unit != null && s.isToBeSet("unit")) {
      entityBuilder.setUnit(unit);
    }
    if (from.hasProperties()) {
      entityBuilder.addAllProperties(convertProperties(from));
    }
    if (from.hasParents() && s.isToBeSet("parent")) {
      entityBuilder.addAllParents(convert(from.getParents()));
    }
    if (from.hasFileProperties()) {
      FileDescriptor.Builder fileDescriptor = convert(s, from.getFileProperties());
      if (fileDescriptor != null) {
        entityBuilder.setFileDescriptor(fileDescriptor);
      }
    }

    final EntityResponse.Builder responseBuilder = EntityResponse.newBuilder();
    responseBuilder.setEntity(entityBuilder);

    appendMessages(from, responseBuilder);

    return responseBuilder;
  }

  private FileDescriptor.Builder convert(SerializeFieldStrategy s, FileProperties fileProperties) {
    // @review Florian Spreckelsen 2022-03-22
    FileDescriptor.Builder result = null;
    if (s.isToBeSet("path")) {
      result = FileDescriptor.newBuilder();
      result.setPath(fileProperties.getPath());
    }
    if (s.isToBeSet("size")) {
      if (result == null) {
        result = FileDescriptor.newBuilder();
      }
      result.setSize(fileProperties.getSize());
    }
    return result;
  }

  private EntityRole convert(final Role role) {
    switch (role) {
      case RecordType:
        return EntityRole.ENTITY_ROLE_RECORD_TYPE;
      case Record:
        return EntityRole.ENTITY_ROLE_RECORD;
      case Property:
        return EntityRole.ENTITY_ROLE_PROPERTY;
      case File:
        return EntityRole.ENTITY_ROLE_FILE;
      default:
        return EntityRole.ENTITY_ROLE_UNSPECIFIED;
    }
  }

  public Iterable<? extends org.caosdb.api.entity.v1.Message> convert(
      final List<Message> messages) {
    final List<org.caosdb.api.entity.v1.Message> result = new LinkedList<>();
    for (final Message m : messages) {
      result.add(convert(m));
    }
    return result;
  }

  public org.caosdb.api.entity.v1.Message convert(final Message m) {
    final org.caosdb.api.entity.v1.Message.Builder builder =
        org.caosdb.api.entity.v1.Message.newBuilder();
    final MessageCode code = getMessageCode(m);
    builder.setCode(code.getNumber());
    builder.setDescription(m.getDescription());
    return builder.build();
  }

  public static MessageCode getMessageCode(final Message m) {
    return m.getCode();
  }

  public Version convert(final org.caosdb.server.entity.Version from) {
    final org.caosdb.api.entity.v1.Version.Builder builder = Version.newBuilder();

    builder.setId(from.getId());
    return builder.build();
  }

  public Parent convert(final org.caosdb.server.entity.wrapper.Parent from) {
    final org.caosdb.api.entity.v1.Parent.Builder builder = Parent.newBuilder();
    if (from.hasId()) {
      builder.setId(from.getId().toString());
    }
    if (from.hasName()) {
      builder.setName(from.getName());
    }
    if (from.hasDescription()) {
      builder.setDescription(from.getDescription());
    }

    return builder.build();
  }

  public org.caosdb.api.entity.v1.Property convert(final Property from) {
    // @review Florian Spreckelsen 2022-03-22
    final org.caosdb.api.entity.v1.Property.Builder builder =
        org.caosdb.api.entity.v1.Property.newBuilder();

    SerializeFieldStrategy s = from.getSerializeFieldStrategy();
    if (from.hasId() && s.isToBeSet("id")) {
      builder.setId(from.getId().toString());
    }
    if (from.hasName() && s.isToBeSet("name")) {
      builder.setName(from.getName());
    }
    if (from.hasDescription() && s.isToBeSet("description")) {
      builder.setDescription(from.getDescription());
    }
    if (from.hasDatatype() && s.isToBeSet("datatype")) {
      builder.setDataType(convert(from.getDatatype()));
    }
    final String unit = getStringUnit(from);
    if (unit != null && s.isToBeSet("unit")) {
      builder.setUnit(unit);
    }
    if (from.hasValue() && s.isToBeSet("value")) {
      try {
        from.parseValue();
      } catch (final Message e) {
        // ignore. This problem should be handled elsewhere because this is
        // only for the serialization of the data and not for the validation.
        // In any case, the string representation can be used.
      }
      builder.setValue(convert(from.getValue()));
    }
    if (s.isToBeSet("importance")) {
      builder.setImportance(convert(from.getStatementStatus()));
    }
    return builder.build();
  }

  private org.caosdb.api.entity.v1.Value.Builder convert(final Value value) {
    if (value instanceof CollectionValue) {
      return convertCollectionValue((CollectionValue) value);
    }
    final org.caosdb.api.entity.v1.Value.Builder builder =
        org.caosdb.api.entity.v1.Value.newBuilder();
    builder.setScalarValue(convertScalarValue(value));
    return builder;
  }

  protected ScalarValue.Builder convertScalarValue(final Value value) {

    if (value instanceof BooleanValue) {
      return convertBooleanValue((BooleanValue) value);

    } else if (value instanceof ReferenceValue) {
      return convertReferenceValue((ReferenceValue) value);

    } else if (value instanceof DateTimeInterface) {
      return convertDateTimeInterface((DateTimeInterface) value);

    } else if (value instanceof GenericValue) {
      return convertGenericValue((GenericValue) value);
    } else if (value == null) {
      return ScalarValue.newBuilder().setSpecialValue(SpecialValue.SPECIAL_VALUE_UNSPECIFIED);
    }
    return null;
  }

  private ScalarValue.Builder convertGenericValue(final GenericValue value) {
    final Object wrappedValue = value.getValue();
    if (wrappedValue instanceof Double) {
      return ScalarValue.newBuilder().setDoubleValue((Double) wrappedValue);
    } else if (wrappedValue instanceof Integer) {
      return ScalarValue.newBuilder().setIntegerValue((Integer) wrappedValue);
    } else {
      return convertStringValue(value.toString());
    }
  }

  private org.caosdb.api.entity.v1.ScalarValue.Builder convertStringValue(final String value) {
    if (value.isEmpty()) {
      return ScalarValue.newBuilder().setSpecialValue(SpecialValue.SPECIAL_VALUE_EMPTY_STRING);
    }
    return ScalarValue.newBuilder().setStringValue(value);
  }

  private org.caosdb.api.entity.v1.ScalarValue.Builder convertDateTimeInterface(
      final DateTimeInterface value) {
    return convertStringValue(value.toDateTimeString(timeZone));
  }

  private org.caosdb.api.entity.v1.ScalarValue.Builder convertBooleanValue(
      final BooleanValue value) {
    return ScalarValue.newBuilder().setBooleanValue(value.getValue());
  }

  private ScalarValue.Builder convertReferenceValue(final ReferenceValue value) {
    return convertStringValue(value.toString());
  }

  private org.caosdb.api.entity.v1.Value.Builder convertCollectionValue(
      final CollectionValue value) {

    final org.caosdb.api.entity.v1.Value.Builder builder =
        org.caosdb.api.entity.v1.Value.newBuilder();
    final List<ScalarValue> values = new LinkedList<>();
    value.forEach(
        (v) -> {
          values.add(convertScalarValue(v));
        });
    builder.setListValues(CollectionValues.newBuilder().addAllValues(values));
    return builder;
  }

  private ScalarValue convertScalarValue(final IndexedSingleValue v) {
    if (v == null || v.getWrapped() == null) {
      return ScalarValue.newBuilder()
          .setSpecialValue(SpecialValue.SPECIAL_VALUE_UNSPECIFIED)
          .build();
    }
    return convertScalarValue(v.getWrapped()).build();
  }

  private Importance convert(final StatementStatusInterface statementStatus) {
    switch ((StatementStatus) statementStatus) {
      case FIX:
        return Importance.IMPORTANCE_FIX;
      case OBLIGATORY:
        return Importance.IMPORTANCE_OBLIGATORY;
      case RECOMMENDED:
        return Importance.IMPORTANCE_RECOMMENDED;
      case SUGGESTED:
        return Importance.IMPORTANCE_SUGGESTED;
      default:
        return null;
    }
  }

  private org.caosdb.api.entity.v1.DataType.Builder convert(final AbstractDatatype datatype) {
    if (datatype instanceof ReferenceDatatype2) {
      return DataType.newBuilder()
          .setReferenceDataType(convertReferenceDatatype((ReferenceDatatype2) datatype));
    } else if (datatype instanceof FileDatatype) {
      return DataType.newBuilder()
          .setReferenceDataType(convertReferenceDatatype((FileDatatype) datatype));
    } else if (datatype instanceof ReferenceDatatype) {
      return DataType.newBuilder()
          .setReferenceDataType(convertReferenceDatatype((ReferenceDatatype) datatype));
    } else if (datatype instanceof AbstractCollectionDatatype) {
      return DataType.newBuilder()
          .setListDataType(
              convertAbstractCollectionDatatype((AbstractCollectionDatatype) datatype));
    } else if (datatype instanceof BooleanDatatype) {
      return DataType.newBuilder()
          .setAtomicDataType(convertBooleanDatatype((BooleanDatatype) datatype));

    } else if (datatype instanceof DateTimeDatatype) {
      return DataType.newBuilder()
          .setAtomicDataType(convertDateTimeDatatype((DateTimeDatatype) datatype));

    } else if (datatype instanceof DoubleDatatype) {
      return DataType.newBuilder()
          .setAtomicDataType(convertDoubleDatatype((DoubleDatatype) datatype));

    } else if (datatype instanceof IntegerDatatype) {
      return DataType.newBuilder()
          .setAtomicDataType(convertIntegerDatatype((IntegerDatatype) datatype));

    } else if (datatype instanceof TextDatatype) {
      return DataType.newBuilder().setAtomicDataType(convertTextDatatype((TextDatatype) datatype));
    }
    return null;
  }

  private AtomicDataType convertTextDatatype(final TextDatatype datatype) {
    return AtomicDataType.ATOMIC_DATA_TYPE_TEXT;
  }

  private AtomicDataType convertIntegerDatatype(final IntegerDatatype datatype) {
    return AtomicDataType.ATOMIC_DATA_TYPE_INTEGER;
  }

  private AtomicDataType convertDoubleDatatype(final DoubleDatatype datatype) {
    return AtomicDataType.ATOMIC_DATA_TYPE_DOUBLE;
  }

  private AtomicDataType convertDateTimeDatatype(final DateTimeDatatype datatype) {
    return AtomicDataType.ATOMIC_DATA_TYPE_DATETIME;
  }

  private AtomicDataType convertBooleanDatatype(final BooleanDatatype datatype) {
    return AtomicDataType.ATOMIC_DATA_TYPE_BOOLEAN;
  }

  private org.caosdb.api.entity.v1.ListDataType.Builder convertAbstractCollectionDatatype(
      final AbstractCollectionDatatype collectionDatatype) {

    final org.caosdb.api.entity.v1.ListDataType.Builder listBuilder = ListDataType.newBuilder();
    final AbstractDatatype datatype = collectionDatatype.getDatatype();
    if (datatype instanceof ReferenceDatatype) {
      listBuilder.setReferenceDataType(convertReferenceDatatype((ReferenceDatatype) datatype));
    } else if (datatype instanceof BooleanDatatype) {
      return listBuilder.setAtomicDataType(convertBooleanDatatype((BooleanDatatype) datatype));

    } else if (datatype instanceof DateTimeDatatype) {
      return listBuilder.setAtomicDataType(convertDateTimeDatatype((DateTimeDatatype) datatype));

    } else if (datatype instanceof DoubleDatatype) {
      return listBuilder.setAtomicDataType(convertDoubleDatatype((DoubleDatatype) datatype));

    } else if (datatype instanceof IntegerDatatype) {
      return listBuilder.setAtomicDataType(convertIntegerDatatype((IntegerDatatype) datatype));

    } else if (datatype instanceof TextDatatype) {
      return listBuilder.setAtomicDataType(convertTextDatatype((TextDatatype) datatype));
    }
    return listBuilder;
  }

  private org.caosdb.api.entity.v1.ReferenceDataType.Builder convertReferenceDatatype(
      final ReferenceDatatype datatype) {
    return ReferenceDataType.newBuilder().setName(datatype.getName());
  }

  public Iterable<? extends org.caosdb.api.entity.v1.Property> convertProperties(
      final EntityInterface from) {
    // @review Florian Spreckelsen 2022-03-22
    final Iterator<org.caosdb.server.entity.wrapper.Property> iterator =
        from.getProperties().iterator();
    return () ->
        new Iterator<>() {

          private Property property;

          @Override
          public boolean hasNext() {
            while (iterator.hasNext()) {
              this.property = iterator.next();
              if (from.getSerializeFieldStrategy().isToBeSet(this.property.getName())) {
                this.property.setSerializeFieldStrategy(
                    from.getSerializeFieldStrategy().forProperty(this.property));
                return true;
              }
            }
            return false;
          }

          @Override
          public org.caosdb.api.entity.v1.Property next() {
            if (this.property == null) {
              // trigger this.property to be non-null
              if (!hasNext()) {
                throw new NoSuchElementException("The iterator has no more elements.");
              }
            }

            Property next_property = this.property;
            this.property = null;

            return convert(next_property);
          }
        };
  }

  public Iterable<? extends Parent> convert(final ParentContainer from) {
    final Iterator<org.caosdb.server.entity.wrapper.Parent> iterator = from.iterator();
    return () ->
        new Iterator<>() {

          @Override
          public boolean hasNext() {
            return iterator.hasNext();
          }

          @Override
          public Parent next() {
            return convert(iterator.next());
          }
        };
  }

  public void appendMessages(
      final EntityInterface from, final org.caosdb.api.entity.v1.EntityResponse.Builder builder) {
    // @review Florian Spreckelsen 2022-03-22
    SerializeFieldStrategy s = from.getSerializeFieldStrategy();
    if (from.hasMessage(Message.MessageType.Error.toString()) && s.isToBeSet("error")) {
      builder.addAllErrors(convert(from.getMessages(Message.MessageType.Error.toString())));
    }
    if (from.hasMessage(Message.MessageType.Warning.toString()) && s.isToBeSet("warning")) {
      builder.addAllWarnings(convert(from.getMessages(Message.MessageType.Warning.toString())));
    }
    if (from.hasMessage(Message.MessageType.Info.toString()) && s.isToBeSet("info")) {
      builder.addAllInfos(convert(from.getMessages(Message.MessageType.Info.toString())));
    }
  }

  public void appendMessages(
      final EntityInterface from, final org.caosdb.api.entity.v1.IdResponse.Builder builder) {
    // @review Florian Spreckelsen 2022-03-22
    SerializeFieldStrategy s = from.getSerializeFieldStrategy();
    if (from.hasMessage(Message.MessageType.Error.toString()) && s.isToBeSet("error")) {
      builder.addAllErrors(convert(from.getMessages(Message.MessageType.Error.toString())));
    }
    if (from.hasMessage(Message.MessageType.Warning.toString()) && s.isToBeSet("warning")) {
      builder.addAllWarnings(convert(from.getMessages(Message.MessageType.Warning.toString())));
    }
    if (from.hasMessage(Message.MessageType.Info.toString()) && s.isToBeSet("info")) {
      builder.addAllInfos(convert(from.getMessages(Message.MessageType.Info.toString())));
    }
  }

  public EntityACL convertACL(EntityInterface e) {
    EntityACL.Builder builder = EntityACL.newBuilder();
    builder.setId(e.getId().toString());
    if (e.hasEntityACL()) {
      builder.addAllRules(convert(e.getEntityACL(), true));
    }
    builder.addAllRules(convert(org.caosdb.server.permissions.EntityACL.GLOBAL_PERMISSIONS, false));
    EntityAclPermission entityAclPermission = getCurrentACLPermission(e);
    if (entityAclPermission != null) {
      builder.setPermission(entityAclPermission);
    }
    return builder.build();
  }

  private org.caosdb.api.entity.v1.EntityAclPermission getCurrentACLPermission(EntityInterface e) {
    if (e.hasPermission(EntityPermission.EDIT_PRIORITY_ACL)) {
      return EntityAclPermission.ENTITY_ACL_PERMISSION_EDIT_PRIORITY_ACL;
    }
    if (e.hasPermission(EntityPermission.EDIT_ACL)) {
      return EntityAclPermission.ENTITY_ACL_PERMISSION_EDIT_ACL;
    }
    return null;
  }

  private Iterable<? extends EntityPermissionRule> convert(
      org.caosdb.server.permissions.EntityACL entityACL, boolean deletable) {
    List<EntityPermissionRule> result = new LinkedList<>();
    for (EntityACI aci : entityACL.getRules()) {
      EntityPermissionRule.Builder builder =
          EntityPermissionRule.newBuilder()
              .setGrant(aci.isGrant())
              .setPriority(aci.isPriority())
              .setRole(aci.getResponsibleAgent().toString())
              .addAllPermissions(convert(aci));
      if (deletable) {
        builder.addCapabilities(
            EntityPermissionRuleCapability.ENTITY_PERMISSION_RULE_CAPABILITY_DELETE);
      }
      result.add(builder.build());
    }
    return result;
  }

  private Iterable<org.caosdb.api.entity.v1.EntityPermission> convert(EntityACI aci) {
    List<org.caosdb.api.entity.v1.EntityPermission> result = new LinkedList<>();

    for (EntityPermission p : aci.getPermission()) {
      result.add(p.getMapping());
    }
    return result;
  }

  public org.caosdb.api.entity.v1.SelectQueryResult.Builder convertSelectResult(
      RetrieveContainer container) {
    SelectQueryResult.Builder result = SelectQueryResult.newBuilder();
    result.setHeader(convertSelectQueryHeader(container));
    for (EntityInterface e : container) {
      result.addDataRows(convert(container.getQuery().getSelections(), e));
    }
    return result;
  }

  private SelectQueryRow.Builder convert(List<Selection> selections, EntityInterface e) {

    SelectQueryRow.Builder result = SelectQueryRow.newBuilder();
    for (Selection s : selections) {
      org.caosdb.api.entity.v1.Value.Builder value = getSelectedValue(s, e);
      result.addCells(value);
    }
    return result;
  }

  /**
   * Handle special selectors like "id", "name", "value", "parent",...
   *
   * @return a {@link org.caosdb.api.entity.v1.Value.Builder}, if the selector has been handled.
   *     Return null when the caller must handle the selector otherwise.
   */
  private org.caosdb.api.entity.v1.Value.Builder handleSpecialSelectors(
      String selector, EntityInterface e) {
    org.caosdb.api.entity.v1.Value.Builder result = null;
    switch (selector) {
      case "value":
        try {
          e.parseValue();
        } catch (Message m) {
          throw new TransactionException(m);
        }
        if (e.hasValue()) {
          return convert(e.getValue());
        }
        break;
      case "version":
        if (e.hasVersion()) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          result.setScalarValue(convertStringValue(e.getVersion().getId()));
          return result;
        }
        break;
      case "name":
        if (e.hasName()) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          result.setScalarValue(convertStringValue(e.getName()));
          return result;
        }
        break;

      case "id":
        result = org.caosdb.api.entity.v1.Value.newBuilder();
        result.setScalarValue(convertStringValue(e.getId().toString()));
        return result;

      case "description":
        if (e.hasDescription()) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          result.setScalarValue(convertStringValue(e.getDescription()));
          return result;
        }
        break;

      case "unit":
        final String unit = getStringUnit(e);
        if (unit != null) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          result.setScalarValue(convertStringValue(unit));
          return result;
        }
        break;
      case "datatype":
        if (e.hasDatatype()) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          result.setScalarValue(convertStringValue(e.getDatatype().toString()));
          return result;
        }
        break;
      case "path":
        if (e.hasFileProperties() && e.getFileProperties().hasPath()) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          result.setScalarValue(convertStringValue(e.getFileProperties().getPath()));
          return result;
        }
        break;
      case "parent":
        if (e.hasParents()) {
          result = org.caosdb.api.entity.v1.Value.newBuilder();
          CollectionValues.Builder parents = CollectionValues.newBuilder();
          for (org.caosdb.server.entity.wrapper.Parent p : e.getParents()) {
            parents.addValues(ScalarValue.newBuilder().setStringValue(p.getName()));
          }
          result.setListValues(parents);
          return result;
        }
        break;
      default:
        break;
    }
    return null;
  }

  org.caosdb.api.entity.v1.Value.Builder getSelectedValue(Selection s, EntityInterface e) {
    org.caosdb.api.entity.v1.Value.Builder result = null;
    String selector = s.getSelector();
    result = handleSpecialSelectors(selector, e);
    if (result == null) {
      // selector for a normal property
      List<org.caosdb.api.entity.v1.Value.Builder> results = new LinkedList<>();
      for (Property p : e.getProperties()) {
        if (p.getName() != null && p.getName().equals(selector)) {
          if (s.getSubselection() == null) {
            // no subselection -> just return the actual value
            try {
              p.parseValue();
            } catch (Message m) {
              throw new TransactionException(m);
            }

            results.add(convert(p.getValue()));
          } else {
            // with subselection, e.g. p1.unit, site.geolocation.longitude
            String subselection = s.getSubselection().getSelector();
            result = handleSpecialSelectors(subselection, p);
            if (result == null) {
              // normal property
              Value v = p.getValue();

              if (v instanceof ReferenceValue) {
                EntityInterface referenced_entity = ((ReferenceValue) v).getEntity();
                result = getSelectedValue(s.getSubselection(), referenced_entity);
              } else if (v instanceof CollectionValue) {
                for (Value i : (CollectionValue) v) {
                  Value wrapped = ((IndexedSingleValue) i).getWrapped();
                  if (wrapped instanceof ReferenceValue) {
                    EntityInterface referenced_entity = ((ReferenceValue) wrapped).getEntity();
                    result = getSelectedValue(s.getSubselection(), referenced_entity);
                    results.add(result);
                  } else {
                    // Non-reference scalar value
                    result = getSelectedValue(s.getSubselection(), p);
                    results.add(result);
                  }
                }
              } else {
                // Actual sub-property
                result = getSelectedValue(s.getSubselection(), p);
                results.add(result);
              }

            } else {
              results.add(result);
            }
          }
        }
      }

      if (results.size() > 1) {
        // There have been multiple matching properties
        CollectionValues.Builder values = CollectionValues.newBuilder();
        for (org.caosdb.api.entity.v1.Value.Builder v : results) {
          // Concatenate all found values to a list
          if (v.hasScalarValue()) {
            values.addValues(v.getScalarValueBuilder());
          } else {
            values.addAllValues(v.getListValuesBuilder().getValuesList());
          }
        }
        result = org.caosdb.api.entity.v1.Value.newBuilder();
        result.setListValues(values);
      } else if (results.size() == 1) {
        // There has been exactly one matching property
        result = results.get(0);
      }
    }

    if (result == null) {
      // no matching property found
      result = org.caosdb.api.entity.v1.Value.newBuilder();
    }
    return result;
  }

  private SelectQueryHeader.Builder convertSelectQueryHeader(RetrieveContainer container) {
    SelectQueryHeader.Builder result = SelectQueryHeader.newBuilder();
    Query query = container.getQuery();
    for (Selection s : query.getSelections()) {
      result.addColumns(SelectQueryColumn.newBuilder().setName(s.toString()));
    }
    return result;
  }
}
