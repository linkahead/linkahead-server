package org.caosdb.server.grpc;

import com.google.protobuf.ByteString;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.caosdb.api.entity.v1.FileChunk;
import org.caosdb.api.entity.v1.FileTransmissionSettings;
import org.caosdb.api.entity.v1.FileTransmissionSettings.Builder;
import org.caosdb.api.entity.v1.TransmissionStatus;
import org.caosdb.server.FileSystem;
import org.caosdb.server.entity.FileProperties;

public class FileUpload extends FileTransmission {

  Map<String, UploadBuffer> buffers = new HashMap<>();

  File tmpDir;

  public FileUpload(final String id) {
    super(id);
    this.tmpDir = null;
  }

  @Override
  public void cleanUp() {
    if (tmpDir != null) {
      org.apache.commons.io.FileUtils.deleteQuietly(tmpDir);
    }
  }

  File getTmpDir() {
    if (tmpDir == null) {
      tmpDir = new File(FileSystem.getTmp() + id + "/");
      tmpDir.mkdirs();
    }
    return tmpDir;
  }

  public TransmissionStatus upload(final String fileId, final ByteString data) throws IOException {
    synchronized (lock) {
      touch();
      return getFileBuffer(fileId).write(data);
    }
  }

  protected UploadBuffer getFileBuffer(final String fileId) {
    touch();
    if (!buffers.containsKey(fileId)) {
      buffers.put(fileId, new UploadBuffer(getTmpDir().toPath().resolve(fileId).toFile()));
    }
    return buffers.get(fileId);
  }

  @Override
  public FileProperties getFile(final String fileId) {
    synchronized (lock) {
      touch();
      if (buffers.containsKey(fileId)) {
        return buffers.get(fileId).toFileProperties(fileId);
      }
      return null;
    }
  }

  @Override
  public FileTransmissionSettings getTransmissionSettings() {
    final Builder builder = FileTransmissionSettings.newBuilder();
    builder.setMaxChunkSize(getMaxChunkSize());
    builder.setMaxFileSize(getMaxFileSize());
    return builder.build();
  }

  public TransmissionStatus uploadChunk(final String fileId, final FileChunk chunk)
      throws IOException {
    return upload(fileId, chunk.getData());
  }
}
