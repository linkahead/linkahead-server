/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package org.caosdb.server.grpc;

import io.grpc.stub.StreamObserver;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.UUID;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.caosdb.api.entity.v1.DeleteRequest;
import org.caosdb.api.entity.v1.DeleteResponse;
import org.caosdb.api.entity.v1.Entity;
import org.caosdb.api.entity.v1.EntityRequest;
import org.caosdb.api.entity.v1.EntityResponse;
import org.caosdb.api.entity.v1.EntityTransactionServiceGrpc.EntityTransactionServiceImplBase;
import org.caosdb.api.entity.v1.FindQueryResult;
import org.caosdb.api.entity.v1.IdResponse;
import org.caosdb.api.entity.v1.InsertRequest;
import org.caosdb.api.entity.v1.InsertResponse;
import org.caosdb.api.entity.v1.MultiRetrieveEntityACLRequest;
import org.caosdb.api.entity.v1.MultiRetrieveEntityACLResponse;
import org.caosdb.api.entity.v1.MultiTransactionRequest;
import org.caosdb.api.entity.v1.MultiTransactionResponse;
import org.caosdb.api.entity.v1.MultiUpdateEntityACLRequest;
import org.caosdb.api.entity.v1.MultiUpdateEntityACLResponse;
import org.caosdb.api.entity.v1.RetrieveResponse;
import org.caosdb.api.entity.v1.SelectQueryResult;
import org.caosdb.api.entity.v1.TransactionRequest;
import org.caosdb.api.entity.v1.TransactionRequest.WrappedRequestsCase;
import org.caosdb.api.entity.v1.UpdateRequest;
import org.caosdb.api.entity.v1.UpdateResponse;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.entity.container.WritableContainer;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.query.Query;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.transaction.RetrieveACL;
import org.caosdb.server.transaction.UpdateACL;
import org.caosdb.server.transaction.WriteTransaction;
import org.caosdb.server.utils.ServerMessages;

/**
 * Main entry point for the entity transaction service of the servers GRPC API.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class EntityTransactionServiceImpl extends EntityTransactionServiceImplBase {

  // TODO(tf) let the clients define the time zone of the date time values which are being returned.
  private final CaosDBToGrpcConverters caosdbToGrpc =
      new CaosDBToGrpcConverters(TimeZone.getDefault());
  private final GrpcToCaosDBConverters grpcToCaosdb = new GrpcToCaosDBConverters();
  private final FileTransmissionServiceImpl fileTransmissionService;

  public EntityTransactionServiceImpl(final FileTransmissionServiceImpl fileTransmissionService) {
    this.fileTransmissionService = fileTransmissionService;
  }

  private void prepareDownload(
      EntityResponse.Builder entityResponse, EntityInterface entity, FileDownload fileDownload)
      throws Exception {
    try {
      entity.checkPermission(EntityPermission.RETRIEVE_FILE);
      if (fileDownload == null) {
        fileDownload = fileTransmissionService.registerFileDownload(null);
      }
      entity.getFileProperties().retrieveFromFileSystem();
      entityResponse.setDownloadId(
          fileTransmissionService.registerFileDownload(
              fileDownload.getId(), entity.getFileProperties()));
    } catch (AuthenticationException exc) {
      entityResponse.addErrors(caosdbToGrpc.convert(ServerMessages.AUTHORIZATION_ERROR));
      entityResponse.addInfos(caosdbToGrpc.convert(new Message(exc.getMessage())));
    }
  }

  /**
   * Handle read-only transactions. Of these only one may be a query at the moment, the others must
   * be ID retrieves.
   *
   * @param request
   * @return
   * @throws Exception
   */
  public MultiTransactionResponse retrieve(final MultiTransactionRequest request) throws Exception {
    // @review Florian Spreckelsen 2022-03-22
    final MultiTransactionResponse.Builder builder = MultiTransactionResponse.newBuilder();
    final RetrieveContainer container =
        new RetrieveContainer(
            SecurityUtils.getSubject(), getTimestamp(), getSRID(), new HashMap<>());
    FileDownload fileDownload = null;

    for (final TransactionRequest sub_request : request.getRequestsList()) {
      if (sub_request.getWrappedRequestsCase() != WrappedRequestsCase.RETRIEVE_REQUEST) {
        throw new CaosDBException(
            "Cannot process a "
                + sub_request.getWrappedRequestsCase().name()
                + " in a read-only request.");
      }
      final boolean isFileDownload = sub_request.getRetrieveRequest().getRegisterFileDownload();
      if (sub_request.getRetrieveRequest().hasQuery() // Retrieves are either queries...
          && !sub_request.getRetrieveRequest().getQuery().getQuery().isBlank()) {
        final String query = sub_request.getRetrieveRequest().getQuery().getQuery();
        if (container.getFlags().containsKey("query")) { // Check for more than one query request.
          throw new CaosDBException("Cannot process more than one query request.");
        }
        container.getFlags().put("query", query);
        if (isFileDownload) {
          container.getFlags().put("download_files", "true");
        }
      } else { // or ID retrieves.
        final String id = sub_request.getRetrieveRequest().getId();
        if (!id.isBlank()) {
          final RetrieveEntity entity = new RetrieveEntity(new EntityID(id));
          if (isFileDownload) {
            entity.setFlag("download_files", "true");
          }
          container.add(entity);
        }
      }
    }

    final Retrieve transaction = new Retrieve(container);
    transaction.execute();
    if (container.getQuery() != null && container.getQuery().getType() == Query.Type.COUNT) {
      // this was a count query
      final int count = container.getQuery().getCount();
      builder
          .addResponsesBuilder()
          .setRetrieveResponse(RetrieveResponse.newBuilder().setCountResult(count));
    } else if (container.getQuery() != null
        && container.getQuery().getType() == Query.Type.SELECT) {
      // this was a select query
      SelectQueryResult.Builder selectResult = caosdbToGrpc.convertSelectResult(container);
      builder
          .addResponsesBuilder()
          .setRetrieveResponse(RetrieveResponse.newBuilder().setSelectResult(selectResult));
    } else if (container.getQuery() != null && container.getQuery().getType() == Query.Type.FIND) {
      // this was a find query
      final boolean download_files_container = container.getFlags().containsKey("download_files");
      FindQueryResult.Builder findResult = FindQueryResult.newBuilder();
      for (final EntityInterface entity : container) {
        final EntityResponse.Builder entityResponse = caosdbToGrpc.convert(entity);
        if ((download_files_container || entity.getFlags().containsKey("download_files"))
            && entity.hasFileProperties()) {
          prepareDownload(entityResponse, entity, fileDownload);
        }
        findResult.addResultSet(entityResponse);
      }
      builder
          .addResponsesBuilder()
          .setRetrieveResponse(RetrieveResponse.newBuilder().setFindResult(findResult));
    } else {
      // normal retrieval via id
      final boolean download_files_container = container.getFlags().containsKey("download_files");
      for (final EntityInterface entity : container) {
        final EntityResponse.Builder entityResponse = caosdbToGrpc.convert(entity);
        if ((download_files_container || entity.getFlags().containsKey("download_files"))
            && entity.hasFileProperties()) {
          prepareDownload(entityResponse, entity, fileDownload);
        }
        builder
            .addResponsesBuilder()
            .setRetrieveResponse(RetrieveResponse.newBuilder().setEntityResponse(entityResponse));
      }
    }

    return builder.build();
  }

  private String getSRID() {
    return UUID.randomUUID().toString();
  }

  private Long getTimestamp() {
    return System.currentTimeMillis();
  }

  /**
   * Handle all entity transactions.
   *
   * <p>Currently either all requests must be read-only/retrieve requests, or none of the requests.
   *
   * @param request
   * @return
   * @throws Exception
   */
  public MultiTransactionResponse transaction(final MultiTransactionRequest request)
      throws Exception {
    if (request.getRequestsCount() > 0) {
      // We only test the first request and raise errors when subsequent sub-transactions do not fit
      // into the retrieve context.  Currently this means that either all or none of the requests
      // must be retrieve requests.
      final WrappedRequestsCase requestCase = request.getRequests(0).getWrappedRequestsCase();
      switch (requestCase) {
        case RETRIEVE_REQUEST:
          // Handle read-only transactions.
          return retrieve(request);
        default:
          // Handle mixed-writed transactions.
          return write(request);
      }
    } else {
      // empty request, empty response.
      return MultiTransactionResponse.newBuilder().build();
    }
  }

  /**
   * Handle mixed-write transactions.
   *
   * <p>The current implementation fails fast, without attempts to execute a single request, if
   * there are requests with non-integer IDs. This will change in the near future, once string IDs
   * are supported by the server.
   *
   * @param requests
   * @return
   * @throws Exception
   */
  private MultiTransactionResponse write(final MultiTransactionRequest requests) throws Exception {
    final MultiTransactionResponse.Builder builder = MultiTransactionResponse.newBuilder();
    final WritableContainer container =
        new WritableContainer(
            SecurityUtils.getSubject(), getTimestamp(), getSRID(), new HashMap<String, String>());

    // put entities into the transaction object
    for (final TransactionRequest subRequest : requests.getRequestsList()) {
      switch (subRequest.getWrappedRequestsCase()) {
        case INSERT_REQUEST:
          {
            final InsertRequest insertRequest = subRequest.getInsertRequest();
            final Entity insertEntity = insertRequest.getEntityRequest().getEntity();

            final InsertEntity entity =
                new InsertEntity(
                    insertEntity.getName().isEmpty() ? null : insertEntity.getName(),
                    grpcToCaosdb.convert(insertEntity.getRole()));
            grpcToCaosdb.convert(insertEntity, entity);
            addFileUpload(container, entity, insertRequest.getEntityRequest());
            container.add(entity);
          }
          break;
        case UPDATE_REQUEST:
          final UpdateRequest updateRequest = subRequest.getUpdateRequest();
          final Entity updateEntity = updateRequest.getEntityRequest().getEntity();

          final UpdateEntity entity =
              new UpdateEntity(
                  new EntityID(updateEntity.getId()), grpcToCaosdb.convert(updateEntity.getRole()));
          grpcToCaosdb.convert(updateEntity, entity);
          addFileUpload(container, entity, updateRequest.getEntityRequest());
          container.add(entity);
          break;
        case DELETE_REQUEST:
          final DeleteRequest deleteRequest = subRequest.getDeleteRequest();
          container.add(new DeleteEntity(new EntityID(deleteRequest.getId())));
          break;
        default:
          throw new CaosDBException(
              "Cannot process a "
                  + subRequest.getWrappedRequestsCase().name()
                  + " in a write request.");
      }
    }

    // execute the transaction
    final WriteTransaction transaction = new WriteTransaction(container);
    transaction.setNoIdIsError(false);
    transaction.execute();

    // put inserted/updated/deleted entities back into the response
    for (final EntityInterface entity : container) {
      final IdResponse.Builder idResponse = IdResponse.newBuilder();
      if (entity.hasId()) {
        idResponse.setId(entity.getId().toString());
      }
      caosdbToGrpc.appendMessages(entity, idResponse);

      if (entity instanceof InsertEntity) {
        builder
            .addResponsesBuilder()
            .setInsertResponse(InsertResponse.newBuilder().setIdResponse(idResponse));
      } else if (entity instanceof UpdateEntity) {
        builder
            .addResponsesBuilder()
            .setUpdateResponse(UpdateResponse.newBuilder().setIdResponse(idResponse));
      } else {
        builder
            .addResponsesBuilder()
            .setDeleteResponse(DeleteResponse.newBuilder().setIdResponse(idResponse));
      }
    }
    return builder.build();
  }

  private void addFileUpload(
      final WritableContainer container,
      final EntityInterface entity,
      final EntityRequest entityRequest) {
    if (entityRequest.hasUploadId()) {
      final FileProperties uploadFile =
          fileTransmissionService.getUploadFile(entityRequest.getUploadId());
      if (uploadFile == null) {
        entity.addError(ServerMessages.FILE_HAS_NOT_BEEN_UPLOAED);
      } else {
        container.addFile(uploadFile.getTmpIdentifier(), uploadFile);
        entity.getFileProperties().setTmpIdentifier(uploadFile.getTmpIdentifier());
      }
    }
  }

  private MultiRetrieveEntityACLResponse multiRetrieveEntityACL(
      MultiRetrieveEntityACLRequest request) throws Exception {
    MultiRetrieveEntityACLResponse.Builder builder = MultiRetrieveEntityACLResponse.newBuilder();
    RetrieveACL transaction = new RetrieveACL(request.getIdList());
    transaction.execute();
    for (EntityInterface e : transaction.getContainer()) {
      builder.addAcls(caosdbToGrpc.convertACL(e));
    }
    return builder.build();
  }

  private MultiUpdateEntityACLResponse multiUpdateEntityACL(MultiUpdateEntityACLRequest request)
      throws Exception {
    MultiUpdateEntityACLResponse.Builder builder = MultiUpdateEntityACLResponse.newBuilder();
    UpdateACL transaction = new UpdateACL(grpcToCaosdb.convertAcls(request.getAclsList()));
    transaction.execute();
    return builder.build();
  }

  @Override
  public void multiTransaction(
      final MultiTransactionRequest request,
      final StreamObserver<MultiTransactionResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final MultiTransactionResponse response = transaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      AccessControlManagementServiceImpl.handleException(responseObserver, e);
    }
  }

  @Override
  public void multiRetrieveEntityACL(
      MultiRetrieveEntityACLRequest request,
      StreamObserver<MultiRetrieveEntityACLResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final MultiRetrieveEntityACLResponse response = multiRetrieveEntityACL(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      AccessControlManagementServiceImpl.handleException(responseObserver, e);
    }
  }

  @Override
  public void multiUpdateEntityACL(
      MultiUpdateEntityACLRequest request,
      StreamObserver<MultiUpdateEntityACLResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final MultiUpdateEntityACLResponse response = multiUpdateEntityACL(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      AccessControlManagementServiceImpl.handleException(responseObserver, e);
    }
  }
}
