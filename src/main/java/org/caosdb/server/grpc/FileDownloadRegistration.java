package org.caosdb.server.grpc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.caosdb.api.entity.v1.FileDownloadResponse;
import org.caosdb.api.entity.v1.FileTransmissionId;
import org.caosdb.api.entity.v1.FileTransmissionSettings;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.utils.Utils;

public class FileDownloadRegistration {

  private final Map<String, FileDownload> registeredDownloads = new HashMap<>();

  public FileTransmissionId registerFileDownload(
      final String registration_id, final FileProperties fp) {
    synchronized (registeredDownloads) {
      final String file_id = registeredDownloads.get(registration_id).append(fp);
      return FileTransmissionId.newBuilder()
          .setRegistrationId(registration_id)
          .setFileId(file_id)
          .build();
    }
  }

  public FileDownload registerFileDownload(final FileTransmissionSettings settings)
      throws Exception {
    final FileDownload result = new FileDownload(settings, Utils.getUID());
    register(result);
    return result;
  }

  private void register(final FileDownload fileTransmission) {
    synchronized (registeredDownloads) {
      registeredDownloads.put(fileTransmission.getId(), fileTransmission);
    }
  }

  public FileDownloadResponse downloadNextChunk(final FileTransmissionId fileTransmissionId)
      throws FileNotFoundException, IOException {
    FileDownload next;
    synchronized (registeredDownloads) {
      next = registeredDownloads.get(fileTransmissionId.getRegistrationId());
    }
    return next.getNextChunk(fileTransmissionId.getFileId());
  }

  public void cleanUp(final boolean all) {
    synchronized (registeredDownloads) {
      final List<String> cleanUp = new LinkedList<>();
      for (final Entry<String, FileDownload> entry : registeredDownloads.entrySet()) {
        if (all || entry.getValue().isExpired()) {
          cleanUp.add(entry.getKey());
        }
      }
      for (final String key : cleanUp) {
        registeredDownloads.get(key).cleanUp();
        registeredDownloads.remove(key);
      }
    }
  }

  public void cleanUp() {
    cleanUp(false);
  }
}
