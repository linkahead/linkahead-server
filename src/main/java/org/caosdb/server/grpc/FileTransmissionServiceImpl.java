package org.caosdb.server.grpc;

import io.grpc.stub.StreamObserver;
import java.io.IOException;
import org.caosdb.api.entity.v1.FileChunk;
import org.caosdb.api.entity.v1.FileDownloadRequest;
import org.caosdb.api.entity.v1.FileDownloadResponse;
import org.caosdb.api.entity.v1.FileTransmissionId;
import org.caosdb.api.entity.v1.FileTransmissionServiceGrpc.FileTransmissionServiceImplBase;
import org.caosdb.api.entity.v1.FileTransmissionSettings;
import org.caosdb.api.entity.v1.FileUploadRequest;
import org.caosdb.api.entity.v1.FileUploadResponse;
import org.caosdb.api.entity.v1.RegisterFileUploadRequest;
import org.caosdb.api.entity.v1.RegisterFileUploadResponse;
import org.caosdb.api.entity.v1.RegisterFileUploadResponse.Builder;
import org.caosdb.api.entity.v1.RegistrationStatus;
import org.caosdb.api.entity.v1.TransmissionStatus;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.utils.CronJob;
import org.caosdb.server.utils.Utils;

public class FileTransmissionServiceImpl extends FileTransmissionServiceImplBase {

  public class FileUploadStreamObserver implements StreamObserver<FileUploadRequest> {

    private FileUpload fileUpload = null;
    private FileTransmissionId fileTransmissionId = null;
    private final StreamObserver<FileUploadResponse> outputStreamObserver;
    private TransmissionStatus status;

    public FileUploadStreamObserver(final StreamObserver<FileUploadResponse> outputStreamObserver) {
      this.outputStreamObserver = outputStreamObserver;
    }

    @Override
    public void onError(final Throwable throwable) {}

    @Override
    public void onCompleted() {
      final FileUploadResponse response = FileUploadResponse.newBuilder().setStatus(status).build();
      outputStreamObserver.onNext(response);
      outputStreamObserver.onCompleted();
    }

    @Override
    public void onNext(final FileUploadRequest request) {
      AuthInterceptor.bindSubject();
      final FileChunk chunk = request.getChunk();
      if (chunk.hasFileTransmissionId()) {
        fileUpload =
            fileUploadRegistration.getFileUpload(chunk.getFileTransmissionId().getRegistrationId());
        fileTransmissionId = chunk.getFileTransmissionId();
        if (fileTransmissionId.getFileId().isBlank()) {
          fileTransmissionId =
              FileTransmissionId.newBuilder(fileTransmissionId).setFileId(Utils.getUID()).build();
        }
      } else {

        try {
          status = fileUpload.uploadChunk(fileTransmissionId.getFileId(), chunk);
        } catch (final IOException e) {
          status = TransmissionStatus.TRANSMISSION_STATUS_ERROR;
          e.printStackTrace();
        }
      }
    }
  }

  public FileTransmissionServiceImpl() {
    CaosDBServer.addPreShutdownHook(
        () -> {
          fileDownloadRegistration.cleanUp(true);
          fileUploadRegistration.cleanUp(true);
        });
  }

  FileUploadRegistration fileUploadRegistration = new FileUploadRegistration();
  FileDownloadRegistration fileDownloadRegistration = new FileDownloadRegistration();
  CronJob cleanUp =
      new CronJob(
          "FileTransmissionCleanUp",
          () -> {
            fileUploadRegistration.cleanUp();
            fileDownloadRegistration.cleanUp();
          },
          60,
          false);

  FileTransmissionId registerFileDownload(final String registration_id, final FileProperties fp)
      throws Exception {
    return fileDownloadRegistration.registerFileDownload(registration_id, fp);
  }

  FileDownload registerFileDownload(final FileTransmissionSettings settings) throws Exception {
    return fileDownloadRegistration.registerFileDownload(settings);
  }

  public FileProperties getUploadFile(final FileTransmissionId uploadId) {
    return fileUploadRegistration.getUploadFile(uploadId);
  }

  @Override
  public void registerFileUpload(
      final RegisterFileUploadRequest request,
      final StreamObserver<RegisterFileUploadResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final FileTransmission result = fileUploadRegistration.registerFileUpload();
      final Builder builder = RegisterFileUploadResponse.newBuilder();
      builder.setStatus(result.getRegistrationStatus());
      if (result.getRegistrationStatus() == RegistrationStatus.REGISTRATION_STATUS_ACCEPTED) {
        builder.setRegistrationId(result.getId());
        builder.setUploadSettings(result.getTransmissionSettings());
      }

      final RegisterFileUploadResponse response = builder.build();
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }

  @Override
  public StreamObserver<FileUploadRequest> fileUpload(
      final StreamObserver<FileUploadResponse> responseObserver) {
    return new FileUploadStreamObserver(responseObserver);
  }

  @Override
  public void fileDownload(
      final FileDownloadRequest request,
      final StreamObserver<FileDownloadResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      FileDownloadResponse response =
          fileDownloadRegistration.downloadNextChunk(request.getFileTransmissionId());
      responseObserver.onNext(response);

      while (response.getStatus() == TransmissionStatus.TRANSMISSION_STATUS_GO_ON) {
        response = fileDownloadRegistration.downloadNextChunk(request.getFileTransmissionId());
        responseObserver.onNext(response);
      }
      responseObserver.onCompleted();
    } catch (final Exception e) {
      e.printStackTrace();
      responseObserver.onError(e);
    }
  }
}
