/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.grpc;

import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.stub.StreamObserver;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.subject.Subject;
import org.caosdb.api.acm.v1alpha1.AccessControlManagementServiceGrpc.AccessControlManagementServiceImplBase;
import org.caosdb.api.acm.v1alpha1.CreateSingleRoleRequest;
import org.caosdb.api.acm.v1alpha1.CreateSingleRoleResponse;
import org.caosdb.api.acm.v1alpha1.CreateSingleUserRequest;
import org.caosdb.api.acm.v1alpha1.CreateSingleUserResponse;
import org.caosdb.api.acm.v1alpha1.DeleteSingleRoleRequest;
import org.caosdb.api.acm.v1alpha1.DeleteSingleRoleResponse;
import org.caosdb.api.acm.v1alpha1.DeleteSingleUserRequest;
import org.caosdb.api.acm.v1alpha1.DeleteSingleUserResponse;
import org.caosdb.api.acm.v1alpha1.EmailSetting;
import org.caosdb.api.acm.v1alpha1.EntitySetting;
import org.caosdb.api.acm.v1alpha1.ListKnownPermissionsRequest;
import org.caosdb.api.acm.v1alpha1.ListKnownPermissionsResponse;
import org.caosdb.api.acm.v1alpha1.ListRoleItem;
import org.caosdb.api.acm.v1alpha1.ListRolesRequest;
import org.caosdb.api.acm.v1alpha1.ListRolesResponse;
import org.caosdb.api.acm.v1alpha1.ListUsersRequest;
import org.caosdb.api.acm.v1alpha1.ListUsersResponse;
import org.caosdb.api.acm.v1alpha1.PermissionDescription;
import org.caosdb.api.acm.v1alpha1.PermissionRule;
import org.caosdb.api.acm.v1alpha1.RetrieveSingleRoleRequest;
import org.caosdb.api.acm.v1alpha1.RetrieveSingleRoleResponse;
import org.caosdb.api.acm.v1alpha1.RetrieveSingleUserRequest;
import org.caosdb.api.acm.v1alpha1.RetrieveSingleUserResponse;
import org.caosdb.api.acm.v1alpha1.RoleCapabilities;
import org.caosdb.api.acm.v1alpha1.RolePermissions;
import org.caosdb.api.acm.v1alpha1.UpdateSingleRoleRequest;
import org.caosdb.api.acm.v1alpha1.UpdateSingleRoleResponse;
import org.caosdb.api.acm.v1alpha1.UpdateSingleUserRequest;
import org.caosdb.api.acm.v1alpha1.UpdateSingleUserResponse;
import org.caosdb.api.acm.v1alpha1.User;
import org.caosdb.api.acm.v1alpha1.UserCapabilities;
import org.caosdb.api.acm.v1alpha1.UserPermissions;
import org.caosdb.api.acm.v1alpha1.UserStatus;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.transaction.DeleteRoleTransaction;
import org.caosdb.server.transaction.DeleteUserTransaction;
import org.caosdb.server.transaction.InsertRoleTransaction;
import org.caosdb.server.transaction.InsertUserTransaction;
import org.caosdb.server.transaction.ListRolesTransaction;
import org.caosdb.server.transaction.ListUsersTransaction;
import org.caosdb.server.transaction.RetrieveRoleTransaction;
import org.caosdb.server.transaction.RetrieveUserTransaction;
import org.caosdb.server.transaction.UpdateRoleTransaction;
import org.caosdb.server.transaction.UpdateUserTransaction;
import org.caosdb.server.utils.ServerMessages;

public class AccessControlManagementServiceImpl extends AccessControlManagementServiceImplBase {

  /////////////////////////////////// CONVERTERS

  private ProtoUser convert(User user) {
    ProtoUser result = new ProtoUser();
    result.realm = user.getRealm();
    result.name = user.getName();
    result.email = user.hasEmailSetting() ? user.getEmailSetting().getEmail() : null;
    result.status = convert(user.getStatus());
    result.roles = new HashSet<String>();
    if (user.getRolesCount() >= 0) {
      user.getRolesList().forEach(result.roles::add);
    }
    return result;
  }

  private org.caosdb.server.accessControl.UserStatus convert(UserStatus status) {
    switch (status) {
      case USER_STATUS_ACTIVE:
        return org.caosdb.server.accessControl.UserStatus.ACTIVE;
      case USER_STATUS_INACTIVE:
        return org.caosdb.server.accessControl.UserStatus.INACTIVE;
      default:
        break;
    }
    return org.caosdb.server.accessControl.UserStatus.INACTIVE;
  }

  private ListUsersResponse convertUsers(List<ProtoUser> users) {
    ListUsersResponse.Builder response = ListUsersResponse.newBuilder();
    users.forEach(
        user -> {
          response.addUsers(convert(user));
        });

    return response.build();
  }

  private User.Builder convert(ProtoUser user) {
    User.Builder result = User.newBuilder();
    result.setRealm(user.realm);
    result.setName(user.name);
    if (user.status != null) result.setStatus(convert(user.status));
    if (user.email != null) {
      result.setEmailSetting(EmailSetting.newBuilder().setEmail(user.email));
    }
    if (user.entity != null) {
      result.setEntitySetting(EntitySetting.newBuilder().setEntityId(user.entity));
    }
    if (user.roles != null && !user.roles.isEmpty()) {
      result.addAllRoles(user.roles);
    }
    return result;
  }

  private UserStatus convert(org.caosdb.server.accessControl.UserStatus status) {
    switch (status) {
      case ACTIVE:
        return UserStatus.USER_STATUS_ACTIVE;
      case INACTIVE:
        return UserStatus.USER_STATUS_INACTIVE;
      default:
        return UserStatus.USER_STATUS_UNSPECIFIED;
    }
  }

  private Role convert(org.caosdb.api.acm.v1alpha1.Role role) {
    Role result = new Role();

    result.name = role.getName();
    result.description = role.getDescription();
    result.permission_rules = convertPermissionRules(role.getPermissionRulesList());
    return result;
  }

  private LinkedList<org.caosdb.server.permissions.PermissionRule> convertPermissionRules(
      List<PermissionRule> permissionRulesList) {
    LinkedList<org.caosdb.server.permissions.PermissionRule> result = new LinkedList<>();
    permissionRulesList.forEach((r) -> result.add(convert(r)));
    return result;
  }

  private org.caosdb.server.permissions.PermissionRule convert(PermissionRule r) {
    boolean grant = r.getGrant();
    boolean priority = r.getPriority();
    String permission = r.getPermission();
    return new org.caosdb.server.permissions.PermissionRule(grant, priority, permission);
  }

  private ListRolesResponse convert(List<Role> roles) {
    ListRolesResponse.Builder response = ListRolesResponse.newBuilder();
    roles.forEach(
        role -> {
          response.addRoles(convert(role, getRolePermissions(role), getRoleCapabilities(role)));
        });

    return response.build();
  }

  private ListRoleItem convert(
      Role role,
      Iterable<? extends RolePermissions> rolePermissions,
      Iterable<? extends RoleCapabilities> roleCapabilities) {
    return ListRoleItem.newBuilder()
        .setRole(convert(role))
        .addAllCapabilities(roleCapabilities)
        .addAllPermissions(rolePermissions)
        .build();
  }

  private org.caosdb.api.acm.v1alpha1.Role.Builder convert(Role role) {
    org.caosdb.api.acm.v1alpha1.Role.Builder result = org.caosdb.api.acm.v1alpha1.Role.newBuilder();
    result.setDescription(role.description);
    result.setName(role.name);
    if (role.permission_rules != null) result.addAllPermissionRules(convert(role.permission_rules));
    return result;
  }

  private PermissionRule convert(org.caosdb.server.permissions.PermissionRule rule) {
    PermissionRule.Builder result = PermissionRule.newBuilder();
    result.setGrant(rule.isGrant());
    result.setPriority(rule.isPriority());
    result.setPermission(rule.getPermission());
    return result.build();
  }

  private Iterable<PermissionRule> convert(
      LinkedList<org.caosdb.server.permissions.PermissionRule> permission_rules) {

    List<PermissionRule> result = new LinkedList<>();
    permission_rules.forEach(
        (rule) -> {
          result.add(convert(rule));
        });
    return result;
  }

  ////////////////////////////////////// RPC Methods (Implementation)

  private ListKnownPermissionsResponse listKnownPermissions(ListKnownPermissionsRequest request) {
    ListKnownPermissionsResponse.Builder builder = ListKnownPermissionsResponse.newBuilder();
    builder.addAllPermissions(listKnownPermissions());
    return builder.build();
  }

  private Iterable<PermissionDescription> listKnownPermissions() {
    List<PermissionDescription> result = new LinkedList<>();
    for (ACMPermissions p : ACMPermissions.getAll()) {
      result.add(
          PermissionDescription.newBuilder()
              .setPermission(p.toString())
              .setDescription(p.getDescription())
              .build());
    }
    return result;
  }

  ////////////////// ... for roles

  private ListRolesResponse listRolesTransaction(ListRolesRequest request) throws Exception {
    ListRolesTransaction transaction = new ListRolesTransaction();
    transaction.execute();
    List<Role> roles = transaction.getRoles();

    return convert(roles);
  }

  private CreateSingleRoleResponse createSingleRoleTransaction(CreateSingleRoleRequest request)
      throws Exception {
    Role role = convert(request.getRole());
    InsertRoleTransaction transaction = new InsertRoleTransaction(role);
    transaction.execute();

    return CreateSingleRoleResponse.newBuilder().build();
  }

  private RetrieveSingleRoleResponse retrieveSingleRoleTransaction(
      RetrieveSingleRoleRequest request) throws Exception {
    RetrieveRoleTransaction transaction = new RetrieveRoleTransaction(request.getName());
    transaction.execute();

    Role role = transaction.getRole();
    RetrieveSingleRoleResponse.Builder builder =
        RetrieveSingleRoleResponse.newBuilder().setRole(convert(transaction.getRole()));
    if (role.users != null && !role.users.isEmpty())
      role.users.forEach(
          (u) -> {
            builder.addUsers(convert(u));
          });
    return builder
        .addAllPermissions(getRolePermissions(role))
        .addAllCapabilities(getRoleCapabilities(role))
        .build();
  }

  /** What can be done with this role. */
  private Iterable<? extends RoleCapabilities> getRoleCapabilities(Role role) {
    List<RoleCapabilities> result = new LinkedList<>();
    if (org.caosdb.server.permissions.Role.ADMINISTRATION.toString().equals(role.name)) {
      // administration cannot be deleted and the permissions cannot be changed (from *)
      result.add(RoleCapabilities.ROLE_CAPABILITIES_ASSIGN);
    } else if (org.caosdb.server.permissions.Role.ANONYMOUS_ROLE.toString().equals(role.name)) {
      // anonymous cannot be deleted or assigned to any user
      result.add(RoleCapabilities.ROLE_CAPABILITIES_UPDATE_PERMISSION_RULES);
    } else {
      result.add(RoleCapabilities.ROLE_CAPABILITIES_ASSIGN);
      result.add(RoleCapabilities.ROLE_CAPABILITIES_DELETE);
      result.add(RoleCapabilities.ROLE_CAPABILITIES_UPDATE_PERMISSION_RULES);
    }
    return result;
  }

  /** The permissions of the current user w.r.t. this role. */
  private Iterable<? extends RolePermissions> getRolePermissions(Role role) {
    List<RolePermissions> result = new LinkedList<>();
    Subject current_user = SecurityUtils.getSubject();
    if (current_user.isPermitted(ACMPermissions.PERMISSION_DELETE_ROLE(role.name))) {
      result.add(RolePermissions.ROLE_PERMISSIONS_DELETE);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_UPDATE_ROLE_DESCRIPTION(role.name))) {
      result.add(RolePermissions.ROLE_PERMISSIONS_UPDATE_DESCRIPTION);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_UPDATE_ROLE_PERMISSIONS(role.name))) {
      result.add(RolePermissions.ROLE_PERMISSIONS_UPDATE_PERMISSION_RULES);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_ASSIGN_ROLE(role.name))) {
      result.add(RolePermissions.ROLE_PERMISSIONS_ASSIGN);
    }
    return result;
  }

  private DeleteSingleRoleResponse deleteSingleRoleTransaction(DeleteSingleRoleRequest request)
      throws Exception {
    DeleteRoleTransaction transaction = new DeleteRoleTransaction(request.getName());
    transaction.execute();

    return DeleteSingleRoleResponse.newBuilder().build();
  }

  private UpdateSingleRoleResponse updateSingleRoleTransaction(UpdateSingleRoleRequest request)
      throws Exception {
    Role role = convert(request.getRole());
    UpdateRoleTransaction transaction = new UpdateRoleTransaction(role);
    transaction.execute();
    return UpdateSingleRoleResponse.newBuilder().build();
  }

  ////////////////// ... for users

  private ListUsersResponse listUsersTransaction(ListUsersRequest request) throws Exception {
    ListUsersTransaction transaction = new ListUsersTransaction();
    transaction.execute();
    List<ProtoUser> users = transaction.getUsers();
    return convertUsers(users);
  }

  private CreateSingleUserResponse createSingleUserTransaction(CreateSingleUserRequest request)
      throws Exception {
    ProtoUser user = convert(request.getUser());
    InsertUserTransaction transaction =
        new InsertUserTransaction(
            user, request.hasPasswordSetting() ? request.getPasswordSetting().getPassword() : null);
    transaction.execute();

    return CreateSingleUserResponse.newBuilder().build();
  }

  private UpdateSingleUserResponse updateSingleUserTransaction(UpdateSingleUserRequest request)
      throws Exception {
    ProtoUser user = convert(request.getUser());
    UpdateUserTransaction transaction =
        new UpdateUserTransaction(
            user, request.hasPasswordSetting() ? request.getPasswordSetting().getPassword() : null);
    transaction.execute();

    return UpdateSingleUserResponse.newBuilder().build();
  }

  private RetrieveSingleUserResponse retrieveSingleUserTransaction(
      RetrieveSingleUserRequest request) throws Exception {
    RetrieveUserTransaction transaction =
        new RetrieveUserTransaction(request.getRealm(), request.getName());
    transaction.execute();
    ProtoUser user = transaction.getUser();

    return RetrieveSingleUserResponse.newBuilder()
        .setUser(convert(user))
        .addAllPermissions(getUserPermissions(user.realm, user.name))
        .addAllCapabilities(getUserCapabilities(user))
        .build();
  }

  private Iterable<? extends UserCapabilities> getUserCapabilities(ProtoUser user) {
    LinkedList<UserCapabilities> result = new LinkedList<>();
    if (user.realm.equals(UserSources.getInternalRealm().getName())) {
      result.add(UserCapabilities.USER_CAPABILITIES_DELETE);
      result.add(UserCapabilities.USER_CAPABILITIES_UPDATE_PASSWORD);
    }
    return result;
  }

  private Iterable<? extends UserPermissions> getUserPermissions(String realm, String name) {
    LinkedList<UserPermissions> result = new LinkedList<>();
    Subject current_user = SecurityUtils.getSubject();
    if (current_user.isPermitted(ACMPermissions.PERMISSION_DELETE_USER(realm, name))) {
      result.add(UserPermissions.USER_PERMISSIONS_DELETE);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_UPDATE_USER_EMAIL(realm, name))) {
      result.add(UserPermissions.USER_PERMISSIONS_UPDATE_EMAIL);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_UPDATE_USER_STATUS(realm, name))) {
      result.add(UserPermissions.USER_PERMISSIONS_UPDATE_STATUS);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_UPDATE_USER_ROLES(realm, name))) {
      result.add(UserPermissions.USER_PERMISSIONS_UPDATE_ROLES);
    }
    if (current_user.isPermitted(ACMPermissions.PERMISSION_UPDATE_USER_PASSWORD(realm, name))) {
      result.add(UserPermissions.USER_PERMISSIONS_UPDATE_PASSWORD);
    }
    return result;
  }

  private DeleteSingleUserResponse deleteSingleUserTransaction(DeleteSingleUserRequest request)
      throws Exception {
    DeleteUserTransaction transaction =
        new DeleteUserTransaction(request.getRealm(), request.getName());
    transaction.execute();

    return DeleteSingleUserResponse.newBuilder().build();
  }

  ///////////////////////////////////// RPC Methods (API)

  @Override
  public void listUsers(
      ListUsersRequest request, StreamObserver<ListUsersResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final ListUsersResponse response = listUsersTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void listRoles(
      ListRolesRequest request, StreamObserver<ListRolesResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final ListRolesResponse response = listRolesTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void createSingleRole(
      CreateSingleRoleRequest request, StreamObserver<CreateSingleRoleResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final CreateSingleRoleResponse response = createSingleRoleTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void retrieveSingleRole(
      RetrieveSingleRoleRequest request,
      StreamObserver<RetrieveSingleRoleResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final RetrieveSingleRoleResponse response = retrieveSingleRoleTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void createSingleUser(
      CreateSingleUserRequest request, StreamObserver<CreateSingleUserResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final CreateSingleUserResponse response = createSingleUserTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void updateSingleUser(
      UpdateSingleUserRequest request, StreamObserver<UpdateSingleUserResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final UpdateSingleUserResponse response = updateSingleUserTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void retrieveSingleUser(
      RetrieveSingleUserRequest request,
      StreamObserver<RetrieveSingleUserResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final RetrieveSingleUserResponse response = retrieveSingleUserTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void deleteSingleUser(
      DeleteSingleUserRequest request, StreamObserver<DeleteSingleUserResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final DeleteSingleUserResponse response = deleteSingleUserTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void deleteSingleRole(
      DeleteSingleRoleRequest request, StreamObserver<DeleteSingleRoleResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final DeleteSingleRoleResponse response = deleteSingleRoleTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void updateSingleRole(
      UpdateSingleRoleRequest request, StreamObserver<UpdateSingleRoleResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final UpdateSingleRoleResponse response = updateSingleRoleTransaction(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  @Override
  public void listKnownPermissions(
      ListKnownPermissionsRequest request,
      StreamObserver<ListKnownPermissionsResponse> responseObserver) {
    try {
      AuthInterceptor.bindSubject();
      final ListKnownPermissionsResponse response = listKnownPermissions(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      handleException(responseObserver, e);
    }
  }

  public static void handleException(StreamObserver<?> responseObserver, Exception e) {
    String description = e.getMessage();
    if (description == null || description.isBlank()) {
      description = "Unknown Error. Please Report!";
    }
    if (e instanceof UnauthorizedException) {
      Subject subject = SecurityUtils.getSubject();
      if (AuthenticationUtils.isAnonymous(subject)) {
        responseObserver.onError(new StatusException(AuthInterceptor.PLEASE_LOG_IN));
        return;
      } else {
        responseObserver.onError(
            new StatusException(
                Status.PERMISSION_DENIED.withCause(e).withDescription(description)));
        return;
      }
    } else if (e == ServerMessages.ROLE_DOES_NOT_EXIST
        || e == ServerMessages.ACCOUNT_DOES_NOT_EXIST) {
      responseObserver.onError(
          new StatusException(Status.NOT_FOUND.withDescription(description).withCause(e)));
      return;
    }
    e.printStackTrace();
    responseObserver.onError(
        new StatusException(Status.UNKNOWN.withDescription(description).withCause(e)));
  }
}
