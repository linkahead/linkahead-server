package org.caosdb.server.grpc;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.caosdb.api.entity.v1.FileDownloadResponse;
import org.caosdb.api.entity.v1.FileTransmissionSettings;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.utils.Utils;

public class FileDownload extends FileTransmission {

  Map<String, DownloadBuffer> buffers = new HashMap<>();
  private final FileTransmissionSettings settings;

  public FileDownload(final FileTransmissionSettings settings, final String id) throws Exception {
    super(id);
    this.settings = settings;
  }

  @Override
  public void cleanUp() {
    buffers.forEach(
        (id, buffer) -> {
          buffer.cleanUp();
        });
  }

  @Override
  public FileProperties getFile(final String fileId) {
    synchronized (lock) {
      touch();
      return buffers.get(fileId).getFileProperties();
    }
  }

  @Override
  public FileTransmissionSettings getTransmissionSettings() {
    return settings;
  }

  public String append(final FileProperties fp) {
    synchronized (lock) {
      touch();
      final String id = Utils.getUID();
      buffers.put(id, new DownloadBuffer(fp));
      return id;
    }
  }

  public FileDownloadResponse getNextChunk(final String fileId)
      throws FileNotFoundException, IOException {
    synchronized (lock) {
      touch();
      return buffers.get(fileId).getNextChunk();
    }
  }
}
