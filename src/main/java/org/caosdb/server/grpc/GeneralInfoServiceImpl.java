/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.grpc;

import io.grpc.stub.StreamObserver;
import java.util.Collection;
import java.util.LinkedList;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.caosdb.api.info.v1.GeneralInfoServiceGrpc.GeneralInfoServiceImplBase;
import org.caosdb.api.info.v1.GetSessionInfoRequest;
import org.caosdb.api.info.v1.GetSessionInfoResponse;
import org.caosdb.api.info.v1.GetVersionInfoRequest;
import org.caosdb.api.info.v1.GetVersionInfoResponse;
import org.caosdb.api.info.v1.VersionInfo;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.permissions.CaosPermission;

/**
 * Implementation of the GeneralInfoService.
 *
 * <p>Currently, the only functionality is {@link #getVersionInfo(GetVersionInfoRequest,
 * StreamObserver)} which returns the current version and build metadata of this server instance to
 * the client.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class GeneralInfoServiceImpl extends GeneralInfoServiceImplBase {

  private GetVersionInfoResponse getVersionInfo(GetVersionInfoRequest request) {

    final String version[] =
        CaosDBServer.getServerProperty(ServerProperties.KEY_PROJECT_VERSION).split("[\\.-]", 4);
    final Integer major = Integer.parseInt(version[0]);
    final Integer minor = Integer.parseInt(version[1]);
    final Integer patch = Integer.parseInt(version[2]);
    final String pre_release = version.length > 3 ? version[3] : "";
    final String build = CaosDBServer.getServerProperty(ServerProperties.KEY_PROJECT_REVISTION);

    final VersionInfo versionInfo =
        VersionInfo.newBuilder()
            .setMajor(major)
            .setMinor(minor)
            .setPatch(patch)
            .setPreRelease(pre_release)
            .setBuild(build)
            .build();
    return GetVersionInfoResponse.newBuilder().setVersionInfo(versionInfo).build();
  }

  @Override
  public void getVersionInfo(
      final GetVersionInfoRequest request,
      final StreamObserver<GetVersionInfoResponse> responseObserver) {

    try {
      AuthInterceptor.bindSubject();
      GetVersionInfoResponse response = getVersionInfo(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      AccessControlManagementServiceImpl.handleException(responseObserver, e);
    }
  }

  private GetSessionInfoResponse getSessionInfo(GetSessionInfoRequest request) {
    Subject subject = SecurityUtils.getSubject();
    GetSessionInfoResponse.Builder response = GetSessionInfoResponse.newBuilder();

    Principal principal = (Principal) subject.getPrincipal();

    response.setUsername(principal.getUsername());
    response.setRealm(principal.getRealm());

    AuthorizationInfo authorizationInfo = AuthenticationUtils.getAuthorizationInfo(subject);
    Collection<String> roles = authorizationInfo.getRoles();
    if (roles != null && !roles.isEmpty()) {
      response.addAllRoles(roles);
    }

    Collection<String> permissions = new LinkedList<>();

    Collection<String> stringPermissions = authorizationInfo.getStringPermissions();
    if (stringPermissions != null && !stringPermissions.isEmpty()) {
      permissions.addAll(stringPermissions);
    }

    for (Permission p : authorizationInfo.getObjectPermissions()) {
      if (p instanceof CaosPermission) {
        permissions.addAll(((CaosPermission) p).getStringPermissions(subject));
      } else {
        permissions.add(p.toString());
      }
    }

    if (permissions != null && !permissions.isEmpty()) {
      response.addAllPermissions(permissions);
    }

    return response.build();
  }

  @Override
  public void getSessionInfo(
      GetSessionInfoRequest request, StreamObserver<GetSessionInfoResponse> responseObserver) {

    try {
      AuthInterceptor.bindSubject();
      final GetSessionInfoResponse response = getSessionInfo(request);
      responseObserver.onNext(response);
      responseObserver.onCompleted();

    } catch (final Exception e) {
      AccessControlManagementServiceImpl.handleException(responseObserver, e);
    }
  }
}
