/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Sends a permanent redirect response. */
public class HttpToHttpsRedirector extends Restlet {

  public Logger logger = LoggerFactory.getLogger(getClass());
  private final int port;

  /**
   * @author Timm Fitschen
   * @param httpsPort The port to which the redirect should point.
   */
  public HttpToHttpsRedirector(final int httpsPort) {
    this.port = httpsPort;
  }

  @Override
  public void handle(final Request request, final Response response) {
    final Reference reference = request.getOriginalRef().clone();
    reference.setScheme("https");
    reference.setHostPort(this.port);
    response.redirectPermanent(reference);
    logger.info("Redirected {} to {}", request.getOriginalRef(), reference);
  }
}
