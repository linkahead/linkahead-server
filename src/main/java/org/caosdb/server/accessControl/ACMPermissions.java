/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.accessControl;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ACMPermissions implements Comparable<ACMPermissions> {

  public static final String USER_PARAMETER = "?USER?";
  public static final String REALM_PARAMETER = "?REALM?";
  public static final String ROLE_PARAMETER = "?ROLE?";
  public static Set<ACMPermissions> ALL = new HashSet<>();
  public static final ACMPermissions GENERIC_ACM_PERMISSION =
      new ACMPermissions(
          "ACM:*",
          "Permissions to administrate the access controll management system. That includes managing users, roles, and assigning permissions to roles and roles to users.");

  protected final String permission;
  protected final String description;

  public ACMPermissions(String permission, String description) {
    if (permission == null) {
      throw new NullPointerException("Permission must no be null");
    }
    this.permission = permission;
    this.description = description;
    ALL.add(this);
  }

  @Override
  public final boolean equals(Object obj) {
    if (obj instanceof ACMPermissions) {
      ACMPermissions that = (ACMPermissions) obj;
      return this.permission.equals(that.permission);
    }
    return false;
  }

  @Override
  public final int hashCode() {
    return permission.hashCode();
  }

  @Override
  public final String toString() {
    return this.permission;
  }

  public final String getDescription() {
    return description;
  }

  public static final class UserPermission extends ACMPermissions {

    public static final ACMPermissions GENERIC_USER_PERMISSION =
        new ACMPermissions(
            "ACM:USER:*",
            "Permissions to manage users, i.e. create, retrieve, update and delete users.");

    public UserPermission(String permission, String description) {
      super(permission, description);
    }

    public String toString(String realm) {
      return toString().replace(REALM_PARAMETER, realm);
    }

    public String toString(String realm, String user) {
      return toString(realm).replace(USER_PARAMETER, user);
    }
  }

  public static final class RolePermission extends ACMPermissions {

    public static final ACMPermissions GENERIC_ROLE_PERMISSION =
        new ACMPermissions(
            "ACM:ROLE:*",
            "Permissions to manage roles, i.e. create, retrieve, update and delete roles and assign them to users.");

    public RolePermission(String permission, String description) {
      super(permission, description);
    }

    public String toString(String role) {
      return toString().replace(ROLE_PARAMETER, role);
    }
  }

  public static final String PERMISSION_ACCESS_SERVER_PROPERTIES =
      new ACMPermissions("ACCESS_SERVER_PROPERTIES", "Permission to read the server properties.")
          .toString();

  @Deprecated
  public static final String PERMISSION_RETRIEVE_SERVERLOGS =
      new ACMPermissions("SERVERLOGS:RETRIEVE", "Permission to read the server logs. (DEPRECATED)")
          .toString();

  private static UserPermission retrieve_user_roles =
      new UserPermission(
          "ACM:USER:RETRIEVE:ROLES:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to retrieve the roles of a user");

  public static final String PERMISSION_RETRIEVE_USER_ROLES(
      final String realm, final String username) {
    return retrieve_user_roles.toString(realm, username);
  }

  private static UserPermission retrieve_user_info =
      new UserPermission(
          "ACM:USER:RETRIEVE:INFO:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to retrieve the user info (email, entity, status)");

  public static final String PERMISSION_RETRIEVE_USER_INFO(
      final String realm, final String username) {
    return retrieve_user_info.toString(realm, username);
  }

  private static UserPermission delete_user =
      new UserPermission(
          "ACM:USER:DELETE:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to delete a user");

  public static String PERMISSION_DELETE_USER(final String realm, final String username) {
    return delete_user.toString(realm, username);
  }

  private static final UserPermission insert_user =
      new UserPermission(
          "ACM:USER:INSERT:" + REALM_PARAMETER, "Permission to create a user in the given realm");

  public static String PERMISSION_INSERT_USER(final String realm) {
    return insert_user.toString(realm);
  }

  private static final UserPermission update_user_password =
      new UserPermission(
          "ACM:USER:UPDATE_PASSWORD:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to set the password of a user.");

  public static String PERMISSION_UPDATE_USER_PASSWORD(final String realm, final String username) {
    return update_user_password.toString(realm, username);
  }

  private static final UserPermission update_user_email =
      new UserPermission(
          "ACM:USER:UPDATE:EMAIL:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to update the email address of a user.");

  public static String PERMISSION_UPDATE_USER_EMAIL(final String realm, final String username) {
    return update_user_email.toString(realm, username);
  }

  private static final UserPermission update_user_status =
      new UserPermission(
          "ACM:USER:UPDATE:STATUS:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to update the status of a user, i.e. marking them as ACTIVE or INACTIVE.");

  public static String PERMISSION_UPDATE_USER_STATUS(final String realm, final String username) {
    return update_user_status.toString(realm, username);
  }

  private static final UserPermission update_user_entity =
      new UserPermission(
          "ACM:USER:UPDATE:ENTITY:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to set the entity which is associated with a user.");

  public static String PERMISSION_UPDATE_USER_ENTITY(final String realm, final String username) {
    return update_user_entity.toString(realm, username);
  }

  private static final UserPermission update_user_roles =
      new UserPermission(
          "ACM:USER:UPDATE:ROLES:" + REALM_PARAMETER + ":" + USER_PARAMETER,
          "Permission to change the roles of a user.");

  public static String PERMISSION_UPDATE_USER_ROLES(final String realm, final String username) {
    return update_user_roles.toString(realm, username);
  }

  private static final RolePermission insert_role =
      new RolePermission("ACM:ROLE:INSERT", "Permission to create a new role.");

  public static String PERMISSION_INSERT_ROLE() {
    return insert_role.toString();
  }

  private static final RolePermission update_role_description =
      new RolePermission(
          "ACM:ROLE:UPDATE:DESCRIPTION:" + ROLE_PARAMETER,
          "Permission to update the description of a role.");

  public static String PERMISSION_UPDATE_ROLE_DESCRIPTION(final String role) {
    return update_role_description.toString(role);
  }

  private static final RolePermission retrieve_role_description =
      new RolePermission(
          "ACM:ROLE:RETRIEVE:DESCRIPTION:" + ROLE_PARAMETER,
          "Permission to retrieve the description of a role.");

  public static String PERMISSION_RETRIEVE_ROLE_DESCRIPTION(final String role) {
    return retrieve_role_description.toString(role);
  }

  private static final RolePermission delete_role =
      new RolePermission("ACM:ROLE:DELETE:" + ROLE_PARAMETER, "Permission to delete a role.");

  public static String PERMISSION_DELETE_ROLE(final String role) {
    return delete_role.toString(role);
  }

  private static final RolePermission update_role_permissions =
      new RolePermission(
          "ACM:ROLE:UPDATE:PERMISSIONS:" + ROLE_PARAMETER,
          "Permission to set the permissions of a role.");

  public static String PERMISSION_UPDATE_ROLE_PERMISSIONS(final String role) {
    return update_role_permissions.toString(role);
  }

  private static final RolePermission retrieve_role_permissions =
      new RolePermission(
          "ACM:ROLE:RETRIEVE:PERMISSIONS:" + ROLE_PARAMETER,
          "Permission to read the permissions of a role.");

  public static String PERMISSION_RETRIEVE_ROLE_PERMISSIONS(final String role) {
    return retrieve_role_permissions.toString(role);
  }

  private static final RolePermission assign_role =
      new RolePermission(
          "ACM:ROLE:ASSIGN:" + ROLE_PARAMETER, "Permission to assign a role (to a user).");

  public static String PERMISSION_ASSIGN_ROLE(final String role) {
    return assign_role.toString(role);
  }

  @Override
  public int compareTo(ACMPermissions that) {
    return this.toString().compareToIgnoreCase(that.toString());
  }

  public static List<ACMPermissions> getAll() {
    LinkedList<ACMPermissions> result = new LinkedList<>(ALL);
    Collections.sort(result);
    return result;
  }
}
