/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.eclipse.jetty.util.ajax.JSON;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OneTimeAuthenticationToken extends SelfValidatingAuthenticationToken {

  public static final long DEFAULT_MAX_REPLAYS = 1L;
  public static final int DEFAULT_REPLAYS_TIMEOUT_MS =
      Integer.parseInt(
          CaosDBServer.getServerProperty(ServerProperties.KEY_ONE_TIME_TOKEN_REPLAYS_TIMEOUT_MS));
  public static final int DEFAULT_TIMEOUT_MS =
      Integer.parseInt(
          CaosDBServer.getServerProperty(ServerProperties.KEY_ONE_TIME_TOKEN_EXPIRES_MS));
  public static final String REALM_NAME = "OneTimeAuthenticationToken"; // TODO move to UserSources
  public static final Logger LOGGER = LoggerFactory.getLogger(OneTimeAuthenticationToken.class);

  private long maxReplays;
  private long replaysTimeout;

  public OneTimeAuthenticationToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String checksum,
      final String[] permissions,
      final String[] roles,
      final long maxReplays,
      final long replaysTimeout) {
    super(principal, date, timeout, salt, checksum, permissions, roles);
    this.replaysTimeout = replaysTimeout;
    this.maxReplays = maxReplays;
    consume();
  }

  public long getReplaysTimeout() {
    return replaysTimeout;
  }

  public void consume() {
    OneTimeTokenConsumedInfo.consume(this);
  }

  public OneTimeAuthenticationToken(
      final Principal principal,
      final long timeout,
      final String[] permissions,
      final String[] roles,
      final Long maxReplays,
      final Long replaysTimeout) {
    super(
        principal,
        timeout,
        permissions,
        roles,
        defaultIfNull(maxReplays, DEFAULT_MAX_REPLAYS),
        defaultIfNull(replaysTimeout, DEFAULT_REPLAYS_TIMEOUT_MS));
  }

  private static final long serialVersionUID = 6811668234440927543L;

  /**
   * Return consumed.
   *
   * @param array
   * @param curry
   * @return
   */
  public static OneTimeAuthenticationToken parse(final Object[] array) {
    final Principal principal = new Principal((String) array[1], (String) array[2]);
    final String[] roles = toStringArray((Object[]) array[3]);
    final String[] permissions = toStringArray((Object[]) array[4]);
    final long date = (Long) array[5];
    final long timeout = (Long) array[6];
    final String salt = (String) array[7];
    final String checksum = (String) array[8];
    final long maxReplays = (Long) array[9];
    final long replaysTimeout = (Long) array[10];
    return new OneTimeAuthenticationToken(
        principal, date, timeout, salt, checksum, permissions, roles, maxReplays, replaysTimeout);
  }

  private static OneTimeAuthenticationToken generate(
      final Principal principal,
      final String[] permissions,
      final String[] roles,
      final long timeout,
      final long maxReplays,
      final long replaysTimeout) {

    return new OneTimeAuthenticationToken(
        principal, timeout, permissions, roles, maxReplays, replaysTimeout);
  }

  public static List<Config> loadConfig(InputStream input) throws Exception {
    List<Config> results = new LinkedList<>();
    ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
    ObjectReader reader = mapper.readerFor(Config.class);
    Iterator<Config> configs = reader.readValues(input);
    configs.forEachRemaining(results::add);
    return results;
  }

  public static Map<String, Config> getPurposeMap(List<Config> configs) throws Exception {
    Map<String, Config> result = new HashMap<>();
    for (Config config : configs) {
      if (config.getPurpose() != null && !config.getPurpose().isEmpty()) {
        if (result.containsKey(config.getPurpose())) {
          throw new Exception(
              "OneTimeAuthToken configuration contains duplicate values for the 'purpose' property.");
        }
        result.put(config.getPurpose(), config);
      }
    }
    return result;
  }

  public static OneTimeAuthenticationToken generate(Config c) {
    return generate(c, new Principal(REALM_NAME, c.getName()));
  }

  public static OneTimeAuthenticationToken generateForPurpose(String purpose, Subject user) {
    Config c = purposes.get(purpose);
    if (c != null) {
      Principal principal = (Principal) user.getPrincipal();

      return generate(c, principal);
    }
    return null;
  }

  public static OneTimeAuthenticationToken generate(Config c, Principal principal) {
    return generate(
        principal,
        c.getPermissions(),
        c.getRoles(),
        c.getExpiresAfter(),
        c.getMaxReplays(),
        c.getReplayTimeout());
  }

  static Map<String, Config> purposes = new HashMap<>();

  public static Map<String, Config> getPurposeMap() {
    return purposes;
  }

  public static void initConfig(InputStream yamlConfig) throws Exception {
    List<Config> configs = loadConfig(yamlConfig);
    initOutput(configs);
    purposes.putAll(getPurposeMap(configs));
  }

  private static void initOutput(List<Config> configs) throws IOException, SchedulerException {
    for (Config config : configs) {
      if (config.getOutput() != null) {
        config.getOutput().init(config);
      }
    }
  }

  public static void initConfig() throws Exception {
    resetConfig();
    try (FileInputStream f =
        new FileInputStream(
            CaosDBServer.getServerProperty(ServerProperties.KEY_AUTHTOKEN_CONFIG))) {
      initConfig(f);
    } catch (IOException e) {
      LOGGER.error("Could not load the auth token configuration", e);
    }
  }

  @Override
  protected void setFields(Object[] fields) {
    if (fields.length == 2) {
      this.maxReplays = (long) fields[0];
      this.replaysTimeout = (long) fields[1];
    } else {
      throw new InstantiationError("Too few fields.");
    }
  }

  @Override
  public String calcChecksum(String pepper) {
    return calcChecksum(
        "O",
        this.getRealm(),
        this.getUsername(),
        this.date,
        this.timeout,
        this.salt,
        calcChecksum((Object[]) this.permissions),
        calcChecksum((Object[]) this.roles),
        this.maxReplays,
        this.replaysTimeout,
        pepper);
  }

  @Override
  public String toString() {
    return JSON.toString(
        new Object[] {
          "O",
          this.getRealm(),
          this.getUsername(),
          this.roles,
          this.permissions,
          this.date,
          this.timeout,
          this.salt,
          this.checksum,
          this.maxReplays,
          this.replaysTimeout
        });
  }

  public long getMaxReplays() {
    return maxReplays;
  }

  public static void resetConfig() {
    purposes.clear();
  }
}
