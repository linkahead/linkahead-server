/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019, 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2019 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.caching.Cache;
import org.jvnet.libpam.PAMException;
import org.jvnet.libpam.UnixUser;

/**
 * PAM UserSource for authenticating users via the Host's pam module.
 *
 * <p>A User's existence check and the retrieval of a user's groups is done by the org.jvnet.libpam
 * library.
 *
 * <p>The authentication of a user via the password need root-access and is therefore done by a
 * special shell script running with root privileges on the host.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class Pam implements UserSource {

  private Logger logger = LogManager.getLogger(Pam.class);

  public static class DefaultPamScriptCaller implements PamScriptCaller {

    private Logger logger = LogManager.getLogger(Pam.class);

    private final String pam_script;

    public DefaultPamScriptCaller(String pam_script) {
      if (pam_script == null) {
        pam_script = DEFAULT_PAM_SCRIPT;
      }
      this.pam_script = pam_script;
    }

    public Process getProcess(final String username, final String password) throws IOException {

      final File script = new File(this.pam_script);

      final ProcessBuilder pb = new ProcessBuilder(script.getAbsolutePath(), username);
      pb.directory(script.getParentFile());
      final Process p = pb.start();
      p.getOutputStream().write(password.getBytes());
      p.getOutputStream().write('\n');
      p.getOutputStream().flush();
      return p;
    }

    @Override
    public boolean isValid(final String username, final String password) {
      Process pam_authentication;

      try {
        pam_authentication = getProcess(username, password);
        logger.trace("call pam script");
        return pam_authentication.waitFor() == 0;
      } catch (final IOException e) {
        throw new RuntimeException(e);
      } catch (final InterruptedException e) {
        throw new RuntimeException(e);
      }
    }
  }

  public static final String DEFAULT_PAM_SCRIPT = "./misc/pam_authentication/pam_authentication.sh";

  /*
   * following constants are the names of the configuration parameters and parts of parameters in
   * the user_sources.ini for the pam user source.
   */
  public static final String KEY_PAM_SCRIPT = "pam_script";
  public static final String KEY_DEFAULT_USER_STATUS = "default_status";
  public static final String KEY_GROUP = "group";
  public static final String KEY_USER = "user";
  public static final String KEY_EXCLUDE = "exclude";
  public static final String KEY_INCLUDE = "include";
  public static final String KEY_ROLES = "roles";
  public static final String SEPARATOR = ".";
  public static final String KEY_EMAIL = "email";
  public static final String REGEX_SPLIT_CSV = "\\s*,\\s*";

  public static final String CACHE_REGION_GROUPS = "PAM_UnixUserGroups";
  public static final String CACHE_REGION_USER_EXIST = "PAM_UnixUserExists";
  public static final String CACHE_REGION_AUTH = "PAM_Authentication";

  private String[] EXCLUDE_USERS = null;
  private String[] INCLUDE_USERS = null;
  private String[] EXCLUDE_GROUPS = null;
  private String[] INCLUDE_GROUPS = null;
  private Map<String, String> map = null;
  private PamScriptCaller pamScriptCaller = null;

  /* Caches for groups, user-existence and password validation */

  private ICacheAccess<String, HashSet<String>> groupsCache = Cache.getCache(CACHE_REGION_GROUPS);
  private ICacheAccess<String, Boolean> userExistsCache = Cache.getCache(CACHE_REGION_USER_EXIST);
  private ICacheAccess<String, Boolean> passwordValidCache = Cache.getCache(CACHE_REGION_AUTH);

  @Override
  public void setMap(final Map<String, String> map) {
    this.map = map;
    this.pamScriptCaller = null;
  }

  /**
   * Return the current configuration of this user source.
   *
   * @return configuration.
   */
  private Map<String, String> getMap() {
    return this.map;
  }

  /**
   * @see {@link UserSource#resolveRolesForUsername(String)}
   */
  @Override
  public Set<String> resolveRolesForUsername(final String username) {
    final Set<String> resulting_roles = new HashSet<String>();

    // resolve roles per unix user
    final String userStr = KEY_USER + SEPARATOR + username + SEPARATOR + KEY_ROLES;
    if (getMap().containsKey(userStr)) {
      final String[] roles = getMap().get(userStr).split(REGEX_SPLIT_CSV);
      for (final String role : roles) {
        resulting_roles.add(role);
      }
    }

    // resolve roles per unix group
    final Set<String> groups = getGroups(username);
    for (final String g : groups) {
      final String groupStr = KEY_GROUP + SEPARATOR + g + SEPARATOR + KEY_ROLES;
      if (getMap().containsKey(groupStr)) {
        final String[] roles = getMap().get(groupStr).split(REGEX_SPLIT_CSV);
        for (final String role : roles) {
          resulting_roles.add(role);
        }
      }
    }

    // filter non existing
    final Iterator<String> iterator = resulting_roles.iterator();
    while (iterator.hasNext()) {
      final String role = iterator.next();
      if (!UserSources.isRoleExisting(role)) {
        iterator.remove();
      }
    }
    return resulting_roles;
  }

  /**
   * Get the UNIX groups of the user.
   *
   * <p>First, try to get them from the cache. Only ask the back-end if necessary and cache the
   * results.
   *
   * @param username
   * @return A user's UNIX groups.
   */
  private Set<String> getGroups(final String username) {
    Set<String> cached = groupsCache.get(username);
    if (cached != null) {
      return cached;
    }

    Set<String> uncached = getGroupsNoCache(username);
    if (uncached instanceof HashSet) {
      groupsCache.put(username, (HashSet<String>) uncached);
    } else {
      groupsCache.put(username, new HashSet<String>(uncached));
    }
    return uncached;
  }

  /**
   * Get the UNIX groups of the user directly from the back-end.
   *
   * <p>If the user does not exist, an empty set is returned.
   *
   * @param username
   * @return A user's UNIX groups or an empty set.
   */
  private Set<String> getGroupsNoCache(final String username) {
    logger.trace("Retrieving UnixGroups", username);
    if (UnixUser.exists(username)) {
      try {
        return new UnixUser(username).getGroups();
      } catch (final PAMException e) {
        throw new AuthorizationException(e);
      }
    }
    return new HashSet<String>();
  }

  @Override
  public String getName() {
    return "PAM";
  }

  /**
   * Check if that user is known by the host's PAM.
   *
   * <p>Try the cache first. Only ask PAM directly if necessary and cache the results.
   *
   * @see {@link UserSource#isUserExisting(String)}.
   * @return true iff the user is known.
   */
  @Override
  public boolean isUserExisting(final String username) {
    Boolean cached = userExistsCache.get(username);
    if (cached != null) {
      return cached;
    }

    boolean uncached = isUserExistingNoCache(username);
    userExistsCache.put(username, uncached);
    return uncached;
  }

  /**
   * Check if that user is known by the host's PAM (without caching).
   *
   * @param username
   * @return true iff the user is known.
   */
  private boolean isUserExistingNoCache(final String username) {
    logger.trace("Check UnixUser.exists", username);
    // TODO Decide what to do here.  Checking for existence may not always be possible without
    // credentials.

    // return username != null && UnixUser.exists(username) && isIncorporated(username);
    return username != null && isIncorporated(username);
  }

  /**
   * @see {@link UserSource#isValid(String, String)}.
   */
  @Override
  public boolean isValid(final String username, final String password) {
    if (username != null && isIncorporated(username)) {
      return isValid(getPamScriptCaller(), username, password);
    }
    return false;
  }

  private PamScriptCaller getPamScriptCaller() {
    if (this.pamScriptCaller == null) {
      this.pamScriptCaller = new DefaultPamScriptCaller(getMap().get(KEY_PAM_SCRIPT));
    }
    return this.pamScriptCaller;
  }

  /**
   * Check the validity of the password for that user by asking the pam script caller.
   *
   * <p>Try the cache first, only call the pam script if necessary and cache the results.
   *
   * @param caller
   * @param username
   * @param password
   * @return true iff the password is correct.
   */
  private boolean isValid(
      final PamScriptCaller caller, final String username, final String password) {
    String key = "<" + password + "::" + username + ">";
    Boolean cached = passwordValidCache.get(key);
    if (cached != null) {
      return cached;
    }

    boolean uncached = isValidNoCache(caller, username, password);
    passwordValidCache.put(key, uncached);
    return uncached;
  }

  /**
   * Check the validity of the password for that user by asking the pam script caller.
   *
   * @param caller
   * @param username
   * @param password
   * @return true iff the password is correct.
   */
  private boolean isValidNoCache(
      final PamScriptCaller caller, final String username, final String password) {
    logger.trace("Check Password", username);
    return caller.isValid(username, password);
  }

  /**
   * Tests if a user is in the allowed groups that should be authenticated and may access this
   * database.
   *
   * @param user
   * @return true iff the user may access this database.
   * @throws PAMException
   */
  private boolean isIncorporated(final String user) {
    final Set<String> groups = getGroups(user);
    boolean ret = false;
    for (final String g : INCLUDE_GROUPS()) {
      if (groups.contains(g)) {
        ret = true;
        break;
      }
    }
    for (final String g : EXCLUDE_GROUPS()) {
      if (groups.contains(g)) {
        ret = false;
        break;
      }
    }
    for (final String n : INCLUDE_USERS()) {
      if (user.equals(n)) {
        ret = true;
        break;
      }
    }
    for (final String n : EXCLUDE_USERS()) {
      if (user.equals(n)) {
        ret = false;
        break;
      }
    }
    return ret;
  }

  private String[] EXCLUDE_USERS() {
    if (this.EXCLUDE_USERS == null) {
      final String s = this.map.get(KEY_EXCLUDE + SEPARATOR + KEY_USER);
      if (s == null) {
        this.EXCLUDE_USERS = new String[0];
      } else {
        this.EXCLUDE_USERS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.EXCLUDE_USERS;
  }

  private String[] INCLUDE_USERS() {
    if (this.INCLUDE_USERS == null) {
      final String s = this.map.get(KEY_INCLUDE + SEPARATOR + KEY_USER);
      if (s == null) {
        this.INCLUDE_USERS = new String[0];
      } else {
        this.INCLUDE_USERS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.INCLUDE_USERS;
  }

  private String[] EXCLUDE_GROUPS() {
    if (this.EXCLUDE_GROUPS == null) {
      final String s = this.map.get(KEY_EXCLUDE + SEPARATOR + KEY_GROUP);
      if (s == null) {
        this.EXCLUDE_GROUPS = new String[0];
      } else {
        this.EXCLUDE_GROUPS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.EXCLUDE_GROUPS;
  }

  private String[] INCLUDE_GROUPS() {
    if (this.INCLUDE_GROUPS == null) {
      final String s = this.map.get(KEY_INCLUDE + SEPARATOR + KEY_GROUP);
      if (s == null) {
        this.INCLUDE_GROUPS = new String[0];
      } else {
        this.INCLUDE_GROUPS = s.split("(\\s*,\\s*)|(\\s+)");
      }
    }
    return this.INCLUDE_GROUPS;
  }

  @Override
  public UserStatus getDefaultUserStatus(final String username) {
    // by groups
    final Set<String> groups = getGroups(username);
    UserStatus ret = null;
    for (final String g : groups) {
      final String groupStr = KEY_GROUP + SEPARATOR + g + SEPARATOR + KEY_DEFAULT_USER_STATUS;
      if (this.map.containsKey(groupStr)) {
        final UserStatus n = UserStatus.valueOf(this.map.get(groupStr).toUpperCase());
        if (ret == null) {
          ret = n;
        } else if (ret != n) {
          // conflict -> ignore, go for pam-wide setting
          ret = null;
          break;
        }
      }
    }

    if (ret != null) {
      return ret;
    }
    // by pam-wide setting
    if (this.map.containsKey(KEY_DEFAULT_USER_STATUS)) {
      return UserStatus.valueOf(this.map.get(KEY_DEFAULT_USER_STATUS).toUpperCase());
    }

    return UserStatus.INACTIVE;
  }

  @Override
  public String getDefaultUserEmail(final String username) {
    return this.map.get(KEY_USER + SEPARATOR + username + SEPARATOR + KEY_EMAIL);
  }
}
