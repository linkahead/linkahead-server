package org.caosdb.server.accessControl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.ExecutionException;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

public class SinglePermissionSubject implements Subject {

  private WildcardPermission permission;

  public SinglePermissionSubject(String permission) {
    this.permission = new WildcardPermission(permission);
  }

  @Override
  public Object getPrincipal() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public PrincipalCollection getPrincipals() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public boolean isPermitted(String permission) {
    return this.permission.implies(new WildcardPermission(permission));
  }

  @Override
  public boolean isPermitted(Permission permission) {
    return this.permission.implies(permission);
  }

  @Override
  public boolean[] isPermitted(String... permissions) {
    boolean[] result = new boolean[permissions.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = this.permission.implies(new WildcardPermission(permissions[i]));
    }
    return result;
  }

  @Override
  public boolean[] isPermitted(List<Permission> permissions) {
    boolean[] result = new boolean[permissions.size()];
    for (int i = 0; i < result.length; i++) {
      result[i] = this.permission.implies(permissions.get(i));
    }
    return result;
  }

  @Override
  public boolean isPermittedAll(String... permissions) {
    boolean result = true;
    for (int i = 0; i < permissions.length; i++) {
      result &= this.permission.implies(new WildcardPermission(permissions[i]));
    }
    return result;
  }

  @Override
  public boolean isPermittedAll(Collection<Permission> permissions) {
    Boolean result = true;
    Iterator<Permission> iterator = permissions.iterator();
    while (iterator.hasNext()) {
      result &= this.permission.implies(iterator.next());
    }
    return result;
  }

  @Override
  public void checkPermission(String permission) throws AuthorizationException {
    if (!isPermitted(permission)) {
      throw new AuthenticationException("Not permitted: " + permission);
    }
  }

  @Override
  public void checkPermission(Permission permission) throws AuthorizationException {
    if (!isPermitted(permission)) {
      throw new AuthenticationException("Not permitted: " + permission.toString());
    }
  }

  @Override
  public void checkPermissions(String... permissions) throws AuthorizationException {
    if (!isPermittedAll(permissions)) {
      throw new AuthenticationException("Not permitted: " + permissions.toString());
    }
  }

  @Override
  public void checkPermissions(Collection<Permission> permissions) throws AuthorizationException {
    if (!isPermittedAll(permissions)) {
      throw new AuthenticationException("Not permitted: " + permissions.toString());
    }
  }

  @Override
  public boolean hasRole(String roleIdentifier) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public boolean[] hasRoles(List<String> roleIdentifiers) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public boolean hasAllRoles(Collection<String> roleIdentifiers) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void checkRole(String roleIdentifier) throws AuthorizationException {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void checkRoles(Collection<String> roleIdentifiers) throws AuthorizationException {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void checkRoles(String... roleIdentifiers) throws AuthorizationException {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void login(AuthenticationToken token) throws AuthenticationException {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public boolean isAuthenticated() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public boolean isRemembered() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public Session getSession() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public Session getSession(boolean create) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void logout() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public <V> V execute(Callable<V> callable) throws ExecutionException {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void execute(Runnable runnable) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public <V> Callable<V> associateWith(Callable<V> callable) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public Runnable associateWith(Runnable runnable) {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public void runAs(PrincipalCollection principals)
      throws NullPointerException, IllegalStateException {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public boolean isRunAs() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public PrincipalCollection getPreviousPrincipals() {
    throw new UnsupportedOperationException("This method should never be called");
  }

  @Override
  public PrincipalCollection releaseRunAs() {
    throw new UnsupportedOperationException("This method should never be called");
  }
}
