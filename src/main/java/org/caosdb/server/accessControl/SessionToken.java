/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.eclipse.jetty.util.ajax.JSON;

/**
 * Session tokens are formatted as JSON arrays with the following elements:
 *
 * <ul>
 *   <li>Anything but "O" (upper-case "o"), preferred is "S".
 *   <li>Realm
 *   <li>name within the Realm
 *   <li>list of roles
 *   <li>list of permissions
 *   <li>time of token generation (long, ms since 1970)
 *   <li>validity duration (long, ms)
 *   <li>salt
 *   <li>checksum
 */
public class SessionToken extends SelfValidatingAuthenticationToken {

  public SessionToken(
      final Principal principal,
      final long date,
      final long timeout,
      final String salt,
      final String checksum,
      final String[] permissions,
      final String[] roles) {
    super(principal, date, timeout, salt, checksum, permissions, roles);
  }

  public SessionToken(
      final Principal principal,
      final long timeout,
      final String[] permissions,
      final String[] roles) {
    super(principal, timeout, permissions, roles);
  }

  private static final long serialVersionUID = 3739602277654194951L;

  public static SessionToken parse(final Object[] array) {
    // array[0] is not used here, it was already consumed to determine the type of token.
    final Principal principal = new Principal((String) array[1], (String) array[2]);
    final String[] roles = toStringArray((Object[]) array[3]);
    final String[] permissions = toStringArray((Object[]) array[4]);
    final long date = (Long) array[5];
    final long timeout = (Long) array[6];
    final String salt = (String) array[7];
    final String checksum = (String) array[8];
    return new SessionToken(principal, date, timeout, salt, checksum, permissions, roles);
  }

  private static SessionToken generate(
      final Principal principal, final String[] permissions, final String[] roles) {
    int timeout =
        Integer.parseInt(CaosDBServer.getServerProperty(ServerProperties.KEY_SESSION_TIMEOUT_MS));

    return new SessionToken(principal, timeout, permissions, roles);
  }

  public static SessionToken generate(Subject subject) {
    String[] permissions = new String[] {};
    String[] roles = new String[] {};
    if (subject.getPrincipal() instanceof SelfValidatingAuthenticationToken) {
      SelfValidatingAuthenticationToken p =
          (SelfValidatingAuthenticationToken) subject.getPrincipal();
      permissions = p.getPermissions().toArray(permissions);
      roles = p.getRoles().toArray(roles);
    }
    return generate((Principal) subject.getPrincipal(), permissions, roles);
  }

  /** Nothing to set in this implemention. */
  @Override
  protected void setFields(Object[] fields) {}

  @Override
  public String calcChecksum(String pepper) {
    return calcChecksum(
        "S",
        this.getRealm(),
        this.getUsername(),
        this.date,
        this.timeout,
        this.salt,
        calcChecksum((Object[]) this.permissions),
        calcChecksum((Object[]) this.roles),
        pepper);
  }

  @Override
  public String toString() {
    return JSON.toString(
        new Object[] {
          "S",
          this.getRealm(),
          this.getUsername(),
          this.roles,
          this.permissions,
          this.date,
          this.timeout,
          this.salt,
          this.checksum
        });
  }
}
