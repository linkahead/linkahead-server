/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

package org.caosdb.server.scripting;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.FileSystem;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;

/**
 * Calls server-side scripting scripts.
 *
 * <p>The script are started in a unique directory, they are provided with an authentication token
 * and a special shared directory where they may temporarily store files for download by the user.
 */
public class ServerSideScriptingCaller {

  public static final String UPLOAD_FILES_DIR = ".upload_files";
  public static final Integer STARTED = -1;
  private final String[] commandLine;
  private final int timeoutMs;
  private String absoluteScriptPath = null;
  private ScriptingUtils utils;
  private List<FileProperties> files;
  private File workingDir;
  private File tmpHome;
  private File sharedDir;
  private Object authToken;
  private Map<String, String> env;
  private File stdOutFile;
  private File stdErrFile;
  private String stdErr = null;
  private String stdOut;
  private Integer code = null;

  public Integer getCode() {
    return code;
  }

  File getUploadFilesDir() {
    return getTmpWorkingDir().toPath().resolve(UPLOAD_FILES_DIR).toFile();
  }

  public ServerSideScriptingCaller(
      String[] commandLine, Integer timeoutMs, List<FileProperties> files, Object authToken) {
    this(commandLine, timeoutMs, files, authToken, new HashMap<String, String>());
  }

  public ServerSideScriptingCaller(
      String[] commandLine,
      Integer timeoutMs,
      List<FileProperties> files,
      Object authToken,
      Map<String, String> env) {
    this(commandLine, timeoutMs, files, authToken, env, new ScriptingUtils());
  }

  public ServerSideScriptingCaller(
      String[] commandLine,
      Integer timeoutMs,
      List<FileProperties> files,
      Object authToken,
      Map<String, String> env,
      ScriptingUtils utils) {
    this(commandLine, timeoutMs, files, authToken, env, utils, utils.getTmpWorkingDir());
  }

  public ServerSideScriptingCaller(
      String[] commandLine,
      Integer timeoutMs,
      List<FileProperties> files,
      Object authToken,
      Map<String, String> env,
      ScriptingUtils utils,
      File workingDir) {
    this.utils = utils;
    this.files = files;
    this.commandLine = commandLine;
    this.timeoutMs = timeoutMs;
    this.authToken = authToken;
    this.env = env;
    this.workingDir = workingDir;
    this.tmpHome = (new File(getTmpWorkingDir(), "temphome")).getAbsoluteFile();
    this.stdOutFile = utils.getStdOutFile(workingDir);
    this.stdErrFile = utils.getStdErrFile(workingDir);
  }

  /** Does some final preparation, then calls the script and cleans up. */
  public int invoke() throws Message {
    try {
      this.absoluteScriptPath = getAbsoluteScriptPath(commandLine);
      try {
        createWorkingDir();
        putFilesInWorkingDir(files);
        createSharedDir();
        createTmpHomeDir();
        updateEnvironment();
      } catch (final Exception e) {
        e.printStackTrace();
        throw ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR;
      }

      try {
        code = callScript(this.absoluteScriptPath, this.commandLine, this.authToken, this.env);
        return code;
      } catch (TimeoutException e) {
        throw ServerMessages.SERVER_SIDE_SCRIPT_TIMEOUT;
      } catch (final Throwable e) {
        e.printStackTrace();
        throw ServerMessages.SERVER_SIDE_SCRIPT_ERROR;
      }
    } finally {
      cleanup();
    }
  }

  /**
   * Returns the absolute script path.
   *
   * <p>Throws Message when the script does not exist or when the script is not executable.
   *
   * @param commandLine
   * @return The absolute script path.
   * @throws Message
   */
  String getAbsoluteScriptPath(String[] commandLine) throws Message {
    return utils.getScriptFile(commandLine[0]).getAbsolutePath();
  }

  void putFilesInWorkingDir(final Collection<FileProperties> files)
      throws FileNotFoundException, CaosDBException {
    if (files == null) {
      return;
    }

    // create files dir
    if (!getUploadFilesDir().mkdir()) {
      throw new FileNotFoundException("Could not create the FILE_UPLOAD_DIR.");
    }
    for (final FileProperties f : files) {
      if (f.getPath() == null || f.getPath().isEmpty()) {
        throw new CaosDBException("The path must not be null or empty!");
      }
      File link = getUploadFilesDir().toPath().resolve(f.getPath()).toFile();
      if (!link.getParentFile().exists()) {
        link.getParentFile().mkdirs();
      }
      org.caosdb.server.utils.FileUtils.createSymlink(link, f.getFile());
    }
  }

  void createWorkingDir() throws Exception {
    if (getTmpWorkingDir().exists()) {
      throw new Exception("The working directory must be non-existing when the caller is invoked.");
    }

    // create working dir
    if (!getTmpWorkingDir().mkdirs()) {
      throw new Exception("Creating the temporary working dir failed.");
    }
  }

  /**
   * Create a writable "home like" directory, needed for some Python libs, e.g. caosdb-pylib.
   *
   * <p>The temporary working directory must exist when this method is called.
   */
  void createTmpHomeDir() throws Exception, IOException {
    if (!getTmpWorkingDir().exists()) {
      throw new Exception(
          "The temp working dir ("
              + getTmpWorkingDir().toString()
              + ") must exist before the home dir can be created.");
    }
    if (getTmpHomeDir().exists()) {
      throw new Exception("The home directory must be non-existing when the caller is invoked.");
    }
    File srcDir =
        new File(
            CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_SIDE_SCRIPTING_HOME_DIR));
    FileUtils.copyDirectory(srcDir, this.tmpHome);
  }

  /** Creates a temporary directory for sharing by the script and sets `sharedDir` accordingly. */
  void createSharedDir() throws Exception {
    sharedDir = new File(FileSystem.assertDir(null));
  }

  /** Sets environment variables for the script, according to the current state of the caller. */
  void updateEnvironment() throws Exception {
    env.put("SHARED_DIR", sharedDir.toString());
    env.put("HOME", tmpHome.toString());
  }

  void cleanup() {
    // catch exception and throw afterwards,
    IOException e1 = null;
    try {
      // cache script outputs
      getStdErr();
      getStdOut();
    } catch (final IOException e2) {
      e1 = e2;
    }
    try {
      deleteWorkingDir(getTmpWorkingDir());
    } catch (final IOException e2) {
      if (e1 == null) {
        e1 = e2;
      } else {
        e1.addSuppressed(e2);
      }
    }
    if (e1 != null) throw new RuntimeException("Cleanup failed.", e1);
  }

  void deleteWorkingDir(final File pwd) throws IOException {
    if (pwd.exists()) FileUtils.forceDelete(pwd);
  }

  /**
   * @fixme Should be injected into environment instead. Will be changed in v0.4 of SSS-API
   */
  String[] injectAuthToken(String[] commandLine) {
    String[] newCommandLine = new String[commandLine.length + 1];
    newCommandLine[0] = commandLine[0];
    newCommandLine[1] = "--auth-token=" + authToken.toString();
    for (int i = 2; i < newCommandLine.length; i++) {
      newCommandLine[i] = commandLine[i - 1];
    }
    return newCommandLine;
  }

  /**
   * Call the script.
   *
   * <p>The absoluteScriptPath is called with all remaining parameters from the commandLine arrays,
   * an optional additional authToken and environment variables.
   *
   * @param absoluteScriptPath
   * @param commandLine
   * @param authToken
   * @param env - environment variables
   * @return the exit code of the script call
   * @throws IOException
   * @throws InterruptedException
   * @throws TimeoutException
   */
  int callScript(
      String absoluteScriptPath, String[] commandLine, Object authToken, Map<String, String> env)
      throws IOException, InterruptedException, TimeoutException {
    String[] effectiveCommandLine;
    if (authToken != null) {
      effectiveCommandLine = injectAuthToken(commandLine);
    } else {
      effectiveCommandLine = Arrays.copyOf(commandLine, commandLine.length);
    }
    effectiveCommandLine[0] = absoluteScriptPath;
    final ProcessBuilder pb = new ProcessBuilder(effectiveCommandLine);

    // inject environment variables
    Map<String, String> pEnv = pb.environment();
    for (String key : env.keySet()) {
      pEnv.put(key, env.get(key));
    }

    pb.redirectOutput(Redirect.to(getStdOutFile()));
    pb.redirectError(Redirect.to(getStdErrFile()));
    pb.directory(getTmpWorkingDir());

    int code = STARTED;
    final TimeoutProcess process = new TimeoutProcess(pb.start(), getTimeoutMs());

    code = process.waitFor();
    return code;
  }

  public File getStdOutFile() {
    return stdOutFile;
  }

  public File getStdErrFile() {
    return stdErrFile;
  }

  public String[] getCommandLine() {
    return this.commandLine;
  }

  File getTmpWorkingDir() {
    return this.workingDir;
  }

  File getTmpHomeDir() {
    return this.tmpHome;
  }

  File getSharedDir() {
    return this.sharedDir;
  }

  int getTimeoutMs() {
    return this.timeoutMs;
  }

  public String getStdErr() throws IOException {
    if (stdErr == null && getStdErrFile().exists()) {
      stdErr = FileUtils.readFileToString(getStdErrFile(), "UTF-8");
    }
    return stdErr;
  }

  public String getStdOut() throws IOException {
    if (stdOut == null && getStdOutFile().exists()) {
      stdOut = FileUtils.readFileToString(getStdOutFile(), "UTF-8");
    }
    return stdOut;
  }
}
