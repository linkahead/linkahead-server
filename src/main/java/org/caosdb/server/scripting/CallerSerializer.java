/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.scripting;

import java.io.IOException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.utils.Serializer;
import org.jdom2.Element;

/**
 * Serializes the result of calling a server-side script into XML.
 *
 * <p>The main element is <script>, notable child elements are:
 * <li>call :: The command-line used to call the script.
 * <li>stdout :: The standard output of the script. Most communication should be via this element.
 * <li>stderr :: The error output of the script.
 * <li>shareddir :: The base dir for temporary files, which can then be retrieved via GET requests.
 * <li>
 */
public class CallerSerializer implements Serializer<ServerSideScriptingCaller, Element> {

  @Override
  public Element serialize(ServerSideScriptingCaller caller) {
    Element command = new Element("call");
    command.setText(String.join(" ", caller.getCommandLine()));

    Element stdout = new Element("stdout");
    try {
      stdout.addContent(caller.getStdOut());
    } catch (IOException e) {
      throw new CaosDBException(e);
    }

    Element stderr = new Element("stderr");
    try {
      stderr.addContent(caller.getStdErr());
    } catch (IOException e) {
      throw new CaosDBException(e);
    }

    Element tmpdir = new Element("shareddir");
    tmpdir.addContent(caller.getSharedDir().toString());

    Element script = new Element("script");
    script.setAttribute("code", caller.getCode().toString());
    script.addContent(command);
    script.addContent(stdout);
    script.addContent(stderr);
    return script;
  }
}
