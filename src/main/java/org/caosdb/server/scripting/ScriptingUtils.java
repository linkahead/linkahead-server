/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.scripting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ConfigurationException;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.Utils;

public class ScriptingUtils {

  private File[] bin_dirs;
  private File working;

  public ScriptingUtils() {
    ArrayList<File> new_bin_dirs = new ArrayList<>();
    String bin_dirs_str =
        CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_SIDE_SCRIPTING_BIN_DIRS);
    if (bin_dirs_str == null) {
      // fall-back for old server property
      bin_dirs_str =
          CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_SIDE_SCRIPTING_BIN_DIR);
    }

    // split and process
    if (bin_dirs_str != null) {
      for (String dir : bin_dirs_str.split("\\s+|\\s*,\\s*")) {

        File bin;
        try {
          bin = new File(dir).getCanonicalFile();
        } catch (IOException e) {
          throw new ConfigurationException(
              "Scripting bin dir `" + dir + "` cannot be resolved to a real path.");
        }
        if (!bin.exists()) {
          bin.mkdirs();
        }
        if (!bin.isDirectory()) {
          throw new ConfigurationException(
              "The ServerProperty `"
                  + ServerProperties.KEY_SERVER_SIDE_SCRIPTING_BIN_DIRS
                  + "` must point to directories");
        }
        new_bin_dirs.add(bin);
      }
    }

    bin_dirs = new_bin_dirs.toArray(new File[new_bin_dirs.size()]);

    this.working =
        new File(
            CaosDBServer.getServerProperty(ServerProperties.KEY_SERVER_SIDE_SCRIPTING_WORKING_DIR));

    if (!working.exists()) {
      working.mkdirs();
    }
    if (!working.isDirectory()) {
      throw new ConfigurationException(
          "The ServerProperty `"
              + ServerProperties.KEY_SERVER_SIDE_SCRIPTING_WORKING_DIR
              + "` must point to a directory");
    }
  }

  /**
   * Get the script file by the relative path.
   *
   * <p>Run through all registered bin_dirs and try to resolve the command relative to them. The
   * first matching file is used. When it is not executable throw a
   * SERVER_SIDE_SCRIPT_NOT_EXECUTABLE message. When no matching file exists throw a
   * SERVER_SIDE_SCRIPT_DOES_NOT_EXIST message.
   *
   * @param command The relative path
   * @return The script File object.
   * @throws Message
   */
  public File getScriptFile(final String command) throws Message {
    for (File bin_dir : bin_dirs) {
      File script = bin_dir.toPath().resolve(command).toFile();

      try {
        script = script.getCanonicalFile();
        if (!script.toPath().startsWith(bin_dir.toPath())) {
          // not below the allowed directory tree
          continue;
        }
      } catch (IOException e) {
        // cannot be resolved to canonical file - we treat it as non-existing.
        continue;
      }

      if (!script.exists()) {
        // doesn't exist.
        continue;
      }

      if (!script.canExecute()) {
        throw ServerMessages.SERVER_SIDE_SCRIPT_NOT_EXECUTABLE;
      }

      // we found it!
      return script;
    }
    throw ServerMessages.SERVER_SIDE_SCRIPT_DOES_NOT_EXIST;
  }

  public File getTmpWorkingDir() {
    String uid = Utils.getUID();
    return working.toPath().resolve(uid).toFile();
  }

  public File getStdOutFile(File workingDir) {
    return workingDir.toPath().resolve(".STDOUT").toFile();
  }

  public File getStdErrFile(File workingDir) {
    return workingDir.toPath().resolve(".STDERR").toFile();
  }
}
