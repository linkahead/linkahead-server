/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics, Max-Planck-Institute
 * for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.utils;

import org.caosdb.api.entity.v1.MessageCode;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;

public class ServerMessages {

  public static final Message ENTITY_HAS_BEEN_DELETED_SUCCESSFULLY =
      new Message(
          MessageType.Info,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_BEEN_DELETED_SUCCESSFULLY,
          "This entity has been deleted successfully.");

  public static final Message ENTITY_DOES_NOT_EXIST =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_DOES_NOT_EXIST,
          "Entity does not exist.");

  public static final Message ENTITY_HAS_UNQUALIFIED_PROPERTIES =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_UNQUALIFIED_PROPERTIES,
          "Entity has unqualified properties.");

  public static final Message ENTITY_HAS_UNQUALIFIED_PARENTS =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_UNQUALIFIED_PARENTS,
          "Entity has unqualified parents.");

  public static final Message PARSING_FAILED =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Parsing failed.");

  public static final Message UNKNOWN_DATATYPE =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Unknown data type.");

  public static final Message UNKNOWN_IMPORTANCE =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Unknown importance.");

  public static final Message ENTITY_HAS_NO_ID =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_ENTITY_HAS_NO_ID, "Entity has no ID.");

  public static final Message REQUIRED_BY_PERSISTENT_ENTITY =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_REQUIRED_BY_PERSISTENT_ENTITY,
          "Entity is required by other entities which are not to be deleted.");

  public static final Message PROPERTY_HAS_NO_DATATYPE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_PROPERTY_HAS_NO_DATA_TYPE,
          "Property has no data type.");

  public static final Message ENTITY_HAS_NO_DESCRIPTION =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_NO_DESCRIPTION,
          "Entity has no description.");

  public static final Message ENTITY_HAS_NO_NAME =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_ENTITY_HAS_NO_NAME, "Entity has no name.");

  public static final Message OBLIGATORY_PROPERTY_MISSING =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_OBLIGATORY_PROPERTY_MISSING,
          "An obligatory property is missing.");

  public static final Message ENTITY_HAS_NO_PARENTS =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_NO_PARENTS,
          "Entity has no parents.");

  public static final Message ENTITY_HAS_NO_PROPERTIES =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_NO_PROPERTIES,
          "Entity has no properties.");

  public static final Message FILE_HAS_NO_TARGET_PATH =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_FILE_HAS_NO_TARGET_PATH,
          "No target path specified.");

  public static final Message TARGET_PATH_NOT_ALLOWED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_TARGET_PATH_NOT_ALLOWED,
          "This target path is not allowed.");

  public static final Message TARGET_PATH_EXISTS =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_TARGET_PATH_EXISTS,
          "This target path does already exist.");

  public static final Message PROPERTY_HAS_NO_UNIT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_PROPERTY_HAS_NO_UNIT,
          "Property has no unit.");

  public static final Message CANNOT_PARSE_VALUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_PARSE_VALUE,
          "Cannot parse the value.");

  public static final Message CHECKSUM_TEST_FAILED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CHECKSUM_TEST_FAILED,
          "Checksum test failed. File is corrupted.");

  public static final Message SIZE_TEST_FAILED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_SIZE_TEST_FAILED,
          "Size test failed. File is corrupted.");

  public static final Message CANNOT_CREATE_PARENT_FOLDER =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_CREATE_PARENT_FOLDER,
          "Could not create parent folder in the file system.");

  public static final Message FILE_HAS_NOT_BEEN_UPLOAED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_FILE_HAS_NOT_BEEN_UPLOAED,
          "File has not been uploaded.");

  public static final Message THUMBNAIL_HAS_NOT_BEEN_UPLOAED =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Thumbnail has not been uploaded.");

  public static final Message CANNOT_MOVE_FILE_TO_TARGET_PATH =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_MOVE_FILE_TO_TARGET_PATH,
          "Could not move file to its target folder.");

  public static final Message CANNOT_PARSE_DATETIME_VALUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_PARSE_DATETIME_VALUE,
          "Cannot parse value to datetime format (yyyy-mm-dd'T'hh:mm:ss[.fffffffff][TimeZone]).");

  public static final Message CANNOT_PARSE_DOUBLE_VALUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_PARSE_DOUBLE_VALUE,
          "Cannot parse value to double.");

  public static final Message CANNOT_PARSE_INT_VALUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_PARSE_INT_VALUE,
          "Cannot parse value to integer.");

  public static final Message CANNOT_PARSE_BOOL_VALUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_PARSE_BOOL_VALUE,
          "Cannot parse value to boolean (either 'true' or 'false', case insensitive).");

  public static final Message CANNOT_CONNECT_TO_DATABASE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Could not connect to MySQL server.");

  public static final Message REQUEST_BODY_NOT_WELLFORMED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Request body was not a well-formed xml.");

  public static final Message REQUEST_BODY_EMPTY =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Request body was empty.");

  public static final Message REQUEST_DOESNT_CONTAIN_EXPECTED_ELEMENTS =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Request body didn't contain the expected elements.");

  public static final Message FILE_NOT_IN_DROPOFFBOX =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "File is not in drop-off box.");

  public static final Message FILE_NOT_FOUND =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_FILE_NOT_FOUND, "File could not be found.");

  public static final Message CANNOT_MOVE_FILE_TO_TMP =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Could not move file to tmp folder.");

  public static final Message CANNOT_READ_FILE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Insufficient read permission for this file. Please make it readable.");

  public static final Message CANNOT_READ_THUMBNAIL =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Insufficient read permission for this file's thumbnail. Please make it readable.");

  public static final Message NO_FILE_REPRESENTATION_SUBMITTED =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "No file representation submitted.");

  public static final Message FORM_CONTAINS_UNSUPPORTED_CONTENT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "The form contains unsupported content");

  public static final Message WARNING_OCCURED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_WARNING_OCCURED,
          "A warning occured while processing an entity with the strict flag.");

  public static final Message ENTITY_NAME_IS_NOT_UNIQUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_NAME_IS_NOT_UNIQUE,
          "Name is already in use. Choose a different name or reuse an existing entity.");

  public static final Message ROLE_NAME_IS_NOT_UNIQUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Role name is already in use. Choose a different name.");

  public static final Message ROLE_CANNOT_BE_DELETED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "Role cannot be deleted because there are still users with this role.");

  public static final Message SPECIAL_ROLE_CANNOT_BE_DELETED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This special role cannot be deleted. Ever.");

  public static final Message SPECIAL_ROLE_PERMISSIONS_CANNOT_BE_CHANGED() {
    return new Message(
        MessageType.Error,
        MessageCode.MESSAGE_CODE_UNKNOWN,
        "This special role's permissions cannot be changed. Ever.");
  }

  public static final Message QUERY_EXCEPTION =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_QUERY_EXCEPTION,
          "This query finished with errors.");

  public static final Message LOGOUT_INFO =
      new Message(
          MessageType.Info, MessageCode.MESSAGE_CODE_UNKNOWN, "You have successfully logged out.");

  public static final Message ENTITY_IS_EMPTY =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "This entity is empty.");

  public static final Message TRANSACTION_ROLL_BACK =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_TRANSACTION_ROLL_BACK,
          "An unknown error occured during the transaction and it was rolled back.");

  public static final Message FILE_UPLOAD_FAILED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "The file upload failed for an unknown reason.");

  public static final Message ACCOUNT_CANNOT_BE_ACTIVATED =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "This account cannot be activated.");

  public static final Message ACCOUNT_DOES_NOT_EXIST =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "This account does not exist.");

  public static final Message ACCOUNT_CANNOT_BE_RESET =
      new Message(
          MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "This account cannot be reset.");

  public static final Message UNKNOWN_UNIT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN_UNIT,
          "Unknown unit. Values with this unit cannot be converted to other units when used in search queries.");

  public static final Message ACCOUNT_NAME_NOT_UNIQUE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This user name is already in use. Please choose another one.");

  public static final Message ACCOUNT_HAS_BEEN_DELETED =
      new Message(
          MessageType.Info,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This user has been deleted successfully.");

  public static final Message PLEASE_SET_A_PASSWORD =
      new Message(MessageType.Info, MessageCode.MESSAGE_CODE_UNKNOWN, "Please set a password.");

  public static final Message USER_HAS_BEEN_ACTIVATED =
      new Message(
          MessageType.Info,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "User has been activated. You can now log in with your username and password.");

  public static final Message UNAUTHENTICATED =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Sign in, please.");

  public static final Message AUTHORIZATION_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_AUTHORIZATION_ERROR,
          "You are not allowed to do this.");

  public static final Message REFERENCE_IS_NOT_ALLOWED_BY_DATATYPE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_REFERENCE_IS_NOT_ALLOWED_BY_DATA_TYPE,
          "Reference not qualified. The value of this Reference Property is to be a child of its data type.");

  public static final Message CANNOT_PARSE_ENTITY_ACL =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "Cannot parse EntityACL.");

  public static final Message ROLE_DOES_NOT_EXIST =
      new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "User role does not exist.");

  public static final Message ENTITY_NAME_DUPLICATES =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_NAME_DUPLICATES,
          "Entity can not be identified due to name duplicates.");

  public static final Message DATA_TYPE_NAME_DUPLICATES =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_DATA_TYPE_NAME_DUPLICATES,
          "This data type cannot be identified due to name duplicates.");

  public static final Message ENTITY_HAS_NO_NAME_OR_ID =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_NO_NAME_OR_ID,
          "This entity cannot be identified as it didn't come with a name or id.");

  public static final Message EMAIL_NOT_WELL_FORMED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This email address is not RFC822 compliant.");

  public static final Message PASSWORD_TOO_WEAK(String policy) {
    return new Message(
        MessageType.Error,
        MessageCode.MESSAGE_CODE_UNKNOWN,
        "The password does not comply with the current policies for passwords: " + policy);
  }

  public static final Message AFFILIATION_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_AFFILIATION_ERROR,
          "Affiliation is not defined for this child-parent constellation.");

  public static final Message QUERY_TEMPLATE_HAS_NO_QUERY_DEFINITION =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This QueryTemplate has no query definition.");

  public static final Message QUERY_TEMPLATE_WITH_COUNT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "QueryTemplates may not be defined by 'COUNT' queries for consistency reasons.");

  public static final Message QUERY_TEMPLATE_WITH_SELECT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "QueryTemplates may not be defined by 'SELECT ... FROM ...' queries for consistency reasons.");

  public static final Message QUERY_PARSING_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_QUERY_PARSING_ERROR,
          "An error occured during the parsing of this query. Maybe you were using a wrong syntax?");

  public static final Message NAME_PROPERTIES_MUST_BE_TEXT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_NAME_PROPERTIES_MUST_BE_TEXT,
          "A property which has 'name' as its parent must have a TEXT data type.");

  public static final Message PARENT_DUPLICATES_WARNING =
      new Message(
          MessageType.Warning,
          MessageCode.MESSAGE_CODE_PARENT_DUPLICATES_WARNING,
          "This entity had parent duplicates. That is meaningless and only one parent has been inserted.");

  public static final Message PARENT_DUPLICATES_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_PARENT_DUPLICATES_ERROR,
          "This entity had parent duplicates. Parent duplicates are meaningless and would be ignored (and inserted only once). But these parents had diverging inheritance instructions which cannot be processed.");

  public static final Message ATOMICITY_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ATOMICITY_ERROR,
          "One or more entities are not qualified. None of them have been inserted/updated/deleted.");

  public static final Message NO_SUCH_ENTITY_ROLE(final String role) {
    return new Message(
        MessageType.Error,
        MessageCode.MESSAGE_CODE_NO_SUCH_ENTITY_ROLE,
        "There is no such role '" + role + "'.");
  }

  public static Message UNKNOWN_ERROR(final String requestID) {
    return UNKNOWN_ERROR(
        requestID, CaosDBServer.getServerProperty(ServerProperties.KEY_BUGTRACKER_URI));
  }

  public static Message UNKNOWN_ERROR(final String sRID, final String reportTo) {
    final String description = "An unexpected server error has occurred.";
    String body = "";
    if (sRID != null) {
      body += "SRID = " + sRID;
    }
    if (reportTo != null) {
      body +=
          "\n\nPlease report this server error to "
              + reportTo
              + " and include the SRID into your report.";
    }

    return new Message(
        MessageType.Error,
        MessageCode.MESSAGE_CODE_UNKNOWN,
        description,
        body.isEmpty() ? null : body);
  }

  public static final Message REQUIRED_BY_UNQUALIFIED =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_REQUIRED_BY_UNQUALIFIED,
          "This entity cannot be deleted due to dependency problems");

  public static final Message ENTITY_HAS_UNQUALIFIED_REFERENCE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_ENTITY_HAS_UNQUALIFIED_REFERENCE,
          "This entity has an unqualified reference.");

  public static final Message REFERENCED_ENTITY_DOES_NOT_EXIST =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_REFERENCED_ENTITY_DOES_NOT_EXIST,
          "Referenced entity does not exist.");

  public static final Message REFERENCE_NAME_DUPLICATES =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_REFERENCE_NAME_DUPLICATES,
          "This reference cannot be identified due to name duplicates.");

  public static final Message DATATYPE_INHERITANCE_AMBIGUOUS =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_DATA_TYPE_INHERITANCE_AMBIGUOUS,
          "The data type which is to be inherited could not be detected due to divergent data types of at least two parents.");

  public static final Message DATA_TYPE_DOES_NOT_ACCEPT_COLLECTION_VALUES =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_DATA_TYPE_DOES_NOT_ACCEPT_COLLECTION_VALUES,
          "This data type does not accept collections of values (e.g. Lists).");

  public static final Message CANNOT_PARSE_UNIT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_CANNOT_PARSE_UNIT,
          "This unit cannot be parsed.");

  public static final Message SERVER_SIDE_SCRIPT_DOES_NOT_EXIST =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This server-side script does not exist. Did you install it?");

  public static final Message SERVER_SIDE_SCRIPT_NOT_EXECUTABLE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This server-side script is not executable.");

  public static final Message SERVER_SIDE_SCRIPT_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "The invocation of this server-side script failed.");

  public static final Message SERVER_SIDE_SCRIPT_SETUP_ERROR =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "The setup routine for the server-side script failed. This might indicate a misconfiguration of the server. Please contact the administrator.");

  public static final Message SERVER_SIDE_SCRIPT_TIMEOUT =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "This server-side script did not finish in time.");

  public static final Message SERVER_SIDE_SCRIPT_MISSING_CALL =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_UNKNOWN,
          "You must specify the `call` field.");

  public static final Message ADDITIONAL_PROPERTY =
      new Message(
          MessageType.Warning,
          MessageCode.MESSAGE_CODE_ADDITIONAL_PROPERTY,
          "This property is an additional property which has no corresponding property among the properties of the parents.");

  public static final Message PROPERTY_WITH_DATATYPE_OVERRIDE =
      new Message(
          MessageType.Warning,
          MessageCode.MESSAGE_CODE_PROPERTY_WITH_DATA_TYPE_OVERRIDE,
          "This property overrides the data type.");

  public static final Message PROPERTY_WITH_DESC_OVERRIDE =
      new Message(
          MessageType.Warning,
          MessageCode.MESSAGE_CODE_PROPERTY_WITH_DESCRIPTION_OVERRIDE,
          "This property overrides the description.");

  public static final Message PROPERTY_WITH_NAME_OVERRIDE =
      new Message(
          MessageType.Warning,
          MessageCode.MESSAGE_CODE_PROPERTY_WITH_NAME_OVERRIDE,
          "This property overrides the name.");

  public static final Message INTEGER_OUT_OF_RANGE =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_INTEGER_VALUE_OUT_OF_RANGE,
          "The integer value is out of range. This server only supports signed 32 bit integers.");

  public static final Message ERROR_INTEGRITY_VIOLATION =
      new Message(
          MessageType.Error,
          MessageCode.MESSAGE_CODE_INTEGRITY_VIOLATION,
          "This entity caused an unexpected integrity violation. This is a strong indicator for a server bug. Please report.");

  public static final Message INVALID_USER_NAME(String policy) {
    return new Message(
        MessageType.Error,
        MessageCode.MESSAGE_CODE_UNKNOWN,
        "The user name does not comply with the current policies for user names: " + policy);
  }

  public static final Message CANNOT_DELETE_YOURSELF() {
    return new Message(
        MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, "You cannot delete yourself.");
  }

  public static Message QUERY_PARSING_ERROR(String message) {
    return new Message(MessageType.Error, MessageCode.MESSAGE_CODE_QUERY_PARSING_ERROR, message);
  }
}
