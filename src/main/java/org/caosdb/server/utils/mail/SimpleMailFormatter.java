/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils.mail;

import javax.mail.internet.MailDateFormat;

public class SimpleMailFormatter implements MailFormatter {

  @Override
  public String format(Mail mail) {
    StringBuffer sb = new StringBuffer();

    sb.append("DATE: ");
    MailDateFormat fmt = new MailDateFormat();
    String dateFormatted = fmt.format(mail.getDate().getTime());
    sb.append(dateFormatted);

    sb.append("\nFROM: ");
    if (mail.getFromName() != null) {
      sb.append(mail.getFromName());
      sb.append(' ');
    }
    sb.append('<');
    sb.append(mail.getFromAddr());

    sb.append(">\nTO: ");
    if (mail.getToName() != null) {
      sb.append(mail.getToName());
      sb.append(' ');
    }
    sb.append('<');
    sb.append(mail.getToAddr());

    sb.append(">\nSUBJECT: ");
    if (mail.getSubject() != null) {
      sb.append(mail.getSubject());
    }
    sb.append('\n');
    sb.append('\n');

    if (mail.getTextData() != null) {
      sb.append(mail.getTextData());
    }

    sb.append("\n.\n");

    return sb.toString();
  }
}
