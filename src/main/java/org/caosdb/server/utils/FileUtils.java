/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import static java.nio.file.Files.isSameFile;
import static java.nio.file.Files.isSymbolicLink;
import static java.nio.file.Files.readSymbolicLink;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.LinkOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.FileSystem;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.Message;
import org.eclipse.jetty.io.RuntimeIOException;

public class FileUtils {

  // This flag controls whether the chown-script is enabled:
  // (This would make more sense in a singleton-context.)
  private static boolean chown_enabled = true;

  /**
   * Guess the MIME type of a file. This is based on the unix program 'file --mime-type' and might
   * not work on other systems.
   *
   * @param file
   * @return
   */
  public static String getMimeType(final File file) {
    if (!file.exists()) {
      throw new RuntimeIOException("File does not exist.");
    }
    try {
      final StringBuffer outputStringBuffer = new StringBuffer();
      final String path = file.getAbsolutePath();
      final Process cmd =
          Runtime.getRuntime().exec(new String[] {"file", "--mime-type", "--dereference", path});
      final int status = cmd.waitFor();

      final Reader r = new InputStreamReader(cmd.getInputStream());
      final BufferedReader buf = new BufferedReader(r);
      String line;
      while ((line = buf.readLine()) != null) {
        outputStringBuffer.append(line);
      }
      if (outputStringBuffer.length() > 0) {
        final String ret = outputStringBuffer.toString().split(":")[1].trim();
        if (ret.contains("cannot open")) {
          throw new RuntimeIOException("STATUS=" + status + " - " + ret);
        }
        return ret;
      } else {
        throw new RuntimeException("Output of `file` command was empty.");
      }

    } catch (final IOException e) {
      throw new RuntimeException(e);
    } catch (final InterruptedException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * Convert a byte array to HEX
   *
   * @param b
   * @return hexadecimal (as String).
   */
  public static String toHex(final byte[] binary) {
    final StringBuffer buf = new StringBuffer(2 * binary.length + 1);
    for (final byte b : binary) {
      int nibble = b >> 4 & 0x0F;
      if (nibble >= 0 && nibble < 10) {
        buf.append((char) ('0' + nibble));
      } else {
        buf.append((char) ('a' + (nibble - 10)));
      }
      nibble = b & 0x0F;
      if (nibble >= 0 && nibble < 10) {
        buf.append((char) ('0' + nibble));
      } else {
        buf.append((char) ('a' + (nibble - 10)));
      }
    }
    return buf.toString();
  }

  private static String getChecksumSingleFile(final File file) {
    try {
      final InputStream stream = new FileInputStream(file);
      final MessageDigest md = MessageDigest.getInstance("SHA-512");
      final byte[] buf = new byte[65536];
      int size = 0;
      while ((size = stream.read(buf)) != -1) {
        md.update(buf, 0, size);
      }
      stream.close();
      final byte[] result = md.digest();
      final String ret = FileUtils.toHex(result);
      return ret;
    } catch (final IOException e) {
      throw new TransactionException(e);
    } catch (final NoSuchAlgorithmException e) {
      throw new TransactionException(e);
    }
  }

  /**
   * Get the HEXed SHA-512 checksum of a file or a folder. The Checksum C of a folder f is
   * calculated by (recursively):<br>
   * C(f) = C(C(f_name) + C(f_subfile_1) + ... + C(f_subfile_N))<br>
   * while f_name is the (String) name of f and f_subfile_1 to f_subfile_N are the alphabetically
   * ordered subfiles and subfolders of f. The alphabetical order is defined by UTF16.<br>
   *
   * @param file
   * @return SHA-512 Checksum
   */
  public static String getChecksum(final File file) {

    if (file.isDirectory()) {
      final StringBuffer buffer = new StringBuffer();
      final String[] childNames = file.list();

      // This uses utf16 sorting.
      Arrays.sort(childNames);

      for (final String name : childNames) {
        final File child = new File(file, name);
        if (child.isDirectory()) {
          buffer.append(name);
        }
        buffer.append(getChecksum(child));
      }
      return Utils.sha512(buffer.toString(), null, 1);
    } else {
      return FileUtils.getChecksumSingleFile(file);
    }
  }

  /**
   * Get the size of a file or folder.
   *
   * @param file
   * @return
   */
  public static Long getSize(final File file) {
    if (file.isFile()) {
      return file.length();
    } else {
      Long size = 0L;
      for (final File child : file.listFiles()) {
        size += getSize(child);
      }
      return size;
    }
  }

  public static String chownScript =
      CaosDBServer.getServerProperty(ServerProperties.KEY_CHOWN_SCRIPT);

  public static void runChownScript(final File file) {
    if (chown_enabled) {
      try {
        final String sudopw = CaosDBServer.getServerProperty(ServerProperties.KEY_SUDO_PASSWORD);
        final Process cmd =
            Runtime.getRuntime()
                .exec(
                    new String[] {
                      "/bin/bash",
                      "-c",
                      "echo '"
                          + sudopw
                          + "' | sudo -S -- "
                          + chownScript
                          + " "
                          + file.getAbsolutePath()
                    });
        if (cmd.waitFor() != 0) {
          throw new TransactionException("caosdb_chown_dropoffbox failed.");
        }
      } catch (final InterruptedException e) {
        throw new TransactionException(e);
      } catch (final IOException e) {
        throw new TransactionException(e);
      }
    }
  }

  /**
   * @deprecated Soon to be removed.
   * @throws IOException
   * @throws InterruptedException
   * @throws CaosDBException
   */
  @Deprecated
  public static void testChownScript() throws IOException, InterruptedException, CaosDBException {
    final String sudopw = CaosDBServer.getServerProperty(ServerProperties.KEY_SUDO_PASSWORD);
    final Process cmd =
        Runtime.getRuntime()
            .exec(
                new String[] {
                  "/bin/bash", "-c", "echo '" + sudopw + "' | sudo -S -- " + chownScript + " --test"
                });
    if (cmd.waitFor() != 0) {

      final BufferedReader bufferedReader =
          new BufferedReader(new InputStreamReader(cmd.getErrorStream()));
      String line = null;
      while ((line = bufferedReader.readLine()) != null) {
        System.err.println(line);
      }

      final BufferedReader bufferedReader2 =
          new BufferedReader(new InputStreamReader(cmd.getInputStream()));
      while ((line = bufferedReader2.readLine()) != null) {
        System.err.println(line);
      }
      System.out.println("WARNING: chown for dropoffbox will be disabled.");
      chown_enabled = false;
    }
  }

  public static Undoable createFolders(final File folder) throws Message {
    if (folder.getParentFile().exists()) {
      if (!folder.mkdir()) {
        throw ServerMessages.CANNOT_CREATE_PARENT_FOLDER;
      } else {
        return new Undoable() {
          @Override
          public void undo() {
            if (folder.exists()) {
              folder.delete();
            }
          }

          @Override
          public void cleanUp() {}
        };
      }
    } else {
      final UndoHandler ret = new UndoHandler();
      try {
        ret.append(createFolders(folder.getParentFile()));
        folder.mkdir();
        ret.append(
            new Undoable() {
              @Override
              public void undo() {
                if (folder.exists()) {
                  folder.delete();
                }
              }

              @Override
              public void cleanUp() {}
            });
        return ret;
      } catch (final Message m) {
        ret.undo();
        throw m;
      } catch (final Exception e) {
        ret.undo();
        e.printStackTrace();
        throw ServerMessages.CANNOT_CREATE_PARENT_FOLDER;
      }
    }
  }

  private static File getTmpFile(String prefix) {
    return new File(new File(FileSystem.getTmp()), prefix + Utils.getUID());
  }

  private static boolean exists(File file) {
    return java.nio.file.Files.exists(file.toPath(), LinkOption.NOFOLLOW_LINKS);
  }

  private static boolean isDir(File file) {
    return java.nio.file.Files.isDirectory(file.toPath(), LinkOption.NOFOLLOW_LINKS);
  }

  private static void moveReplace(File file, File target)
      throws IOException, CaosDBException, InterruptedException {
    if (exists(target)) {
      org.apache.commons.io.FileUtils.forceDelete(target);
    }
    if (isDir(file)) {
      org.apache.commons.io.FileUtils.moveDirectory(file, target);
    } else if (isSymlink(file)) {
      moveSymlink(file, target);
    } else {
      org.apache.commons.io.FileUtils.moveFile(file, target);
    }
  }

  /**
   * @param oldlink
   * @param newlink
   * @throws IOException
   * @throws InterruptedException
   * @throws CaosDBException
   */
  private static void moveSymlink(File oldlink, File newlink)
      throws IOException, CaosDBException, InterruptedException {
    File target = oldlink.toPath().toRealPath().toFile();
    createSymlink(newlink, target);
    callPosixUnlink(oldlink);
  }

  public static Undoable rename(final File file, final File target) throws Message, IOException {
    final File backup;
    if (target.getAbsoluteFile().equals(file.getAbsoluteFile())) {
      return new UndoHandler() {
        @Override
        public void undo() {}

        @Override
        public void cleanUp() {}
      };
    }
    try {
      if (exists(target)) {
        // in case this is a update transaction, the old version of the file
        // must be stored somewhere until the transaction is done.
        final File tmp = getTmpFile(target.getName());
        moveReplace(target, tmp);
        backup = tmp;
      } else {
        backup = null;
      }
      moveReplace(file, target);
      return new UndoHandler() {

        private File _backup = backup;

        @Override
        public void undo() {
          try {
            moveReplace(target, file);
            if (_backup != null) {
              moveReplace(_backup, target);
            }
          } catch (Exception e) {
            if (_backup != null) {
              System.err.println("UNDO ERROR. Backup File in " + _backup.getAbsolutePath());
              // prevent _backup from being deleted during cleanup
              _backup = null;
            }
            e.printStackTrace();
          }
        }

        @Override
        public void cleanUp() {
          if (_backup != null) {
            try {
              deleteFinally(_backup);
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }

        @Override
        public String toString() {
          return "UndoHandler: restore backup";
        }
      };
    } catch (Exception e) {
      e.printStackTrace();
      throw ServerMessages.CANNOT_MOVE_FILE_TO_TARGET_PATH;
    }
  }

  private static void deleteFinally(File file) throws IOException {
    org.apache.commons.io.FileUtils.forceDelete(file);
  }

  public static Undoable delete(final File file)
      throws IOException, CaosDBException, InterruptedException {
    return delete(file, false);
  }

  /**
   * Delete a file undoable. If file is a directory and 'recursive' is true the directory and all of
   * its sub folders are deleted. If file is a directory and 'recursive' is false file is deleted
   * unless it has sub folders. If file is an actual file (and not a directory) the file is deleted.
   * Otherwise, nothing happens.
   *
   * @param file
   * @param recursive
   * @return
   * @throws IOException
   * @throws InterruptedException
   * @throws CaosDBException
   */
  public static Undoable delete(final File file, final boolean recursive)
      throws IOException, CaosDBException, InterruptedException {
    if (file.exists() && file.isFile()) {
      // delete file
      final File backup = getTmpFile(file.getName());

      // move to tmp file
      moveReplace(file, backup);
      return new Undoable() {

        private File _backup = backup;

        @Override
        public void undo() {
          try {
            moveReplace(_backup, file);
          } catch (Exception e) {
            System.err.println("UNDO ERROR. Backup File in " + _backup.getAbsolutePath());
            // prevent _backup from being deleted during cleanup
            _backup = null;
            e.printStackTrace();
          }
        }

        @Override
        public void cleanUp() {
          if (_backup != null) {
            try {
              deleteFinally(_backup);
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
      };
    } else if (file.exists() && file.isDirectory()) {
      // directory
      if (recursive && file.listFiles().length > 0) {
        // delete directory recursively with all of its sub folders
        final File backup = getTmpFile(file.getName());

        // move to tmp folder
        moveReplace(file, backup);
        return new Undoable() {

          private File _backup = backup;

          @Override
          public void undo() {
            try {
              moveReplace(_backup, file);
            } catch (Exception e) {
              System.err.println("UNDO ERROR. Backup File in " + _backup.getAbsolutePath());
              // prevent _backup from being deleted during cleanup
              _backup = null;
              e.printStackTrace();
            }
          }

          @Override
          public void cleanUp() {
            if (_backup != null) {
              try {
                deleteFinally(_backup);
              } catch (IOException e) {
                e.printStackTrace();
              }
            }
          }
        };
      } else if (file.listFiles().length == 0) {
        // non-recursive mode: delete empty directory
        deleteFinally(file);
        return new Undoable() {
          @Override
          public void undo() {
            file.mkdir();
          }

          @Override
          public void cleanUp() {}
        };
      }
    }
    // do nothing
    return new Undoable() {

      @Override
      public void undo() {}

      @Override
      public void cleanUp() {}
    };
  }

  public static boolean isSymlink(final File file) {
    return isSymbolicLink(file.toPath());
  }

  private static void callPosixUnlink(File file) throws IOException, InterruptedException {
    final Process cmd = Runtime.getRuntime().exec(new String[] {"unlink", file.getAbsolutePath()});
    if (cmd.waitFor() != 0) {
      throw new CaosDBException("could not unlink " + file.getAbsolutePath());
    }
  }

  public static Undoable unlink(final File file) throws IOException, InterruptedException {
    callPosixUnlink(file);

    final String path = file.getCanonicalPath();
    return new Undoable() {
      @Override
      public void undo() {
        try {
          Runtime.getRuntime().exec("ln -s " + path + " " + file.getAbsolutePath());
        } catch (final IOException e) {
          e.printStackTrace();
        }
      }

      @Override
      public void cleanUp() {}
    };
  }

  public static File createSymlink(final File link, final File target) {
    if (target == null) {
      throw new NullPointerException("target was null.");
    }
    if (!target.exists()) {
      throw new TransactionException("target does not exist.");
    }
    try {

      final String targetPath = target.getCanonicalPath();
      final String linkPath = link.getCanonicalPath();
      final Process cmd =
          Runtime.getRuntime().exec(new String[] {"ln", "-s", targetPath, linkPath});

      if (cmd.waitFor() != 0) {
        if (!link.getParentFile().canWrite()) {
          throw new TransactionException(
              "symlink cannot be created. No permission to write in this directory: \n"
                  + link.getParent()
                  + "\nsymlink creation terminated with error.\n"
                  + Utils.InputStream2String(cmd.getErrorStream()));
        }
        throw new TransactionException(
            "symlink creation terminated with error.\n"
                + Utils.InputStream2String(cmd.getErrorStream()));
      }
    } catch (final InterruptedException e) {
      throw new TransactionException(e);
    } catch (final IOException e) {
      throw new TransactionException(e);
    }
    return link;
  }

  /**
   * Return true iff `file` is not a simlink to a directory and `root` is not a simlink.
   *
   * <p>Symlinks are not allowed for the root. Symlinks are allowed only for a File object if it is
   * a file symlink, not a directory symlink.
   *
   * @param file
   * @param root
   * @throws RuntimeException, if subdir is symlink on a directory or if the root is a simlink.
   * @return
   */
  public static boolean isSubDirSymlinkSave(final File file, final File root) {
    if ((isSymlink(file) && file.isDirectory()) || isSymlink(root)) {
      throw new RuntimeException("Illegal symlink found!");
    }
    final File parent = file.getAbsoluteFile().getParentFile();
    final File abs_root = root.getAbsoluteFile();
    return parent != null && (abs_root.equals(parent) || isSubDirSymlinkSave(parent, abs_root));
  }

  /**
   * @param symlink
   * @param target
   * @return true, if symlink is a symbolic link and pointing to target. false otherwise
   */
  public static boolean isSymlinkPointingTo(final File symlink, final File target) {
    try {
      return isSymbolicLink(symlink.toPath())
          && isSameFile(readSymbolicLink(symlink.toPath()), target.toPath());
    } catch (final IOException e) {
      throw new RuntimeException(e);
    }
  }
}
