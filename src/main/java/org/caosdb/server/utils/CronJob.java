/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import java.util.HashMap;
import org.caosdb.server.CaosDBServer;

public class CronJob {

  private static HashMap<String, CronJob> jobs = new HashMap<String, CronJob>();
  private static Timer timer = new Timer();
  private final Runnable r;
  private Long lastExecution = null;
  private final long ms;

  public CronJob(final String name, final Runnable r, final int seconds, final boolean onShutdown) {
    this.ms = seconds * 1000;
    this.r = r;
    synchronized (jobs) {
      jobs.put(name, this);
      timer.execute(this);
      synchronized (timer) {
        timer.notifyAll();
      }
    }
    if (onShutdown) {
      CaosDBServer.addPreShutdownHook(
          new Runnable() {
            @Override
            public void run() {
              try {
                timer.execute(CronJob.this);
                System.err.print("Running " + name + " onShutdown [OK]\n");
              } catch (final Exception e) {
                System.err.print("Running " + name + " onShutdown [failed]\n");
                e.printStackTrace();
              }
            }
          });
    }
  }

  public CronJob(final String name, final Runnable r, final int seconds) {
    this(name, r, seconds, false);
  }

  private long nextExecution() {
    return this.lastExecution + this.ms;
  }

  private static class Timer extends Thread {

    public Timer() {
      start();
    }

    @Override
    public void run() {
      while (true) {
        final long now = System.currentTimeMillis();
        CronJob next = null;
        synchronized (jobs) {
          for (final CronJob j : jobs.values()) {
            if (j.nextExecution() <= now) {
              execute(j, now);
            }

            if (next == null || next.nextExecution() > j.nextExecution()) {
              next = j;
            }
          }
        }
        try {
          synchronized (this) {
            if (next != null) {
              wait(next.nextExecution() - now);
            } else {
              wait();
            }
          }
        } catch (final InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

    private void execute(final CronJob cronJob, final long time) {
      new Thread(cronJob.r).start();
      cronJob.lastExecution = time;
    }

    void execute(final CronJob cronJob) {
      execute(cronJob, System.currentTimeMillis());
    }
  }
}
