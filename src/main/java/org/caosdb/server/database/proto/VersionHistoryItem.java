package org.caosdb.server.database.proto;

import java.io.Serializable;
import java.util.List;

/**
 * This class is a flat, data-only representation of a single item of version information. This
 * class is an intermediate representation which abstracts away the data base results and comes in a
 * form which is easily cacheable.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public class VersionHistoryItem implements Serializable {

  private static final long serialVersionUID = 428030617967255942L;
  public String id = null;
  public List<String> parents = null;
  public Long seconds = null;
  public Integer nanos = null;
  public String username = null;
  public String realm = null;

  @Override
  public String toString() {
    return id;
  }
}
