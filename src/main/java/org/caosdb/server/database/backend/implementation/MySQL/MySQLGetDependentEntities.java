/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetDependentEntitiesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

/**
 * Get a list of ids of all entities which require the given entity to exists.
 *
 * <p>That is the list of all direct child entities (by is-a relation), referenced entities (i.e.
 * the given entity is a property's value), all properties which have this entity as their data type
 * and all implemented properties which use this entity as their abtract property.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLGetDependentEntities extends MySQLTransaction
    implements GetDependentEntitiesImpl {

  public MySQLGetDependentEntities(final Access access) {
    super(access);
  }

  public static final String STMT_GET_DEPENDENT_ENTITIES = "call getDependentEntities(?)";

  @Override
  public List<EntityID> execute(final EntityID entity) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_GET_DEPENDENT_ENTITIES);

      stmt.setString(1, entity.toString());

      final ResultSet rs = stmt.executeQuery();
      try {
        final List<EntityID> ret = new LinkedList<>();
        while (rs.next()) {
          ret.add(new EntityID(rs.getString(1)));
        }
        return ret;
      } finally {
        rs.close();
      }
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
