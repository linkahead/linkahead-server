/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.transaction;

import java.util.Set;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.UpdateUserRolesImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class UpdateUserRoles extends BackendTransaction {

  private final String user;
  private final Set<String> roles;
  private final String realm;

  public UpdateUserRoles(final String realm, final String user, final Set<String> roles) {
    this.realm = realm;
    this.user = user;
    this.roles = roles;
  }

  @Override
  protected void execute() throws TransactionException {
    RetrieveUser.removeCached(new Principal(this.realm, this.user));
    final UpdateUserRolesImpl t = getImplementation(UpdateUserRolesImpl.class);
    t.updateUserRoles(this.realm, this.user, this.roles);
  }
}
