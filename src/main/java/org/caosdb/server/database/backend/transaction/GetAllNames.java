package org.caosdb.server.database.backend.transaction;

import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.GetAllNamesImpl;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;

public class GetAllNames extends BackendTransaction {

  private List<SparseEntity> entities;

  @Override
  protected void execute() {
    final GetAllNamesImpl t = getImplementation(GetAllNamesImpl.class);

    this.entities = t.execute();
  }

  public List<EntityInterface> getEntities() {
    ArrayList<EntityInterface> ret = new ArrayList<>();
    for (SparseEntity e : entities) {
      ret.add(new RetrieveEntity().parseSparseEntity(e));
    }
    return ret;
  }
}
