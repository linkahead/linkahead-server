/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.backend.interfaces.InsertEntityPropertiesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityInterface;

public class InsertEntityProperties extends BackendTransaction {

  private final EntityInterface entity;

  public InsertEntityProperties(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  public void execute() throws TransactionException {

    final InsertEntityPropertiesImpl t = getImplementation(InsertEntityPropertiesImpl.class);
    t.execute(this.entity);
  }
}
