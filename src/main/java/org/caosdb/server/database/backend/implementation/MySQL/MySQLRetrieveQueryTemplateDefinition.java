/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveQueryTemplateDefinitionImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

public class MySQLRetrieveQueryTemplateDefinition extends MySQLTransaction
    implements RetrieveQueryTemplateDefinitionImpl {

  public MySQLRetrieveQueryTemplateDefinition(final Access access) {
    super(access);
  }

  public static final String STMT_RETRIEVE_QUERY_TEMPLATE_DEF =
      "call retrieveQueryTemplateDef(?,?)";

  @Override
  public String retrieve(final EntityID id, final String version) {
    try {

      final PreparedStatement stmt = prepareStatement(STMT_RETRIEVE_QUERY_TEMPLATE_DEF);
      stmt.setString(1, id.toString());
      if (version == null) {
        stmt.setNull(2, Types.VARBINARY);
      } else {
        stmt.setString(2, version);
      }
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        return rs.getString("definition");
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return null;
  }
}
