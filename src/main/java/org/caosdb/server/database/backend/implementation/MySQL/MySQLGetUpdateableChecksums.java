/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetUpdateableChecksumsImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

/**
 * Retrieve the entity ids of all files which have a checksum which should be updated (because the
 * checksum is NULL, currently).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLGetUpdateableChecksums extends MySQLTransaction
    implements GetUpdateableChecksumsImpl {

  private final String GET_UPDATEABLE_CHECKSUMS =
      "SELECT (SELECT id FROM entity_ids WHERE internal_id = files.file_id) AS entity_id FROM files WHERE hash IS NULL LIMIT 1";

  public MySQLGetUpdateableChecksums(final Access access) {
    super(access);
  }

  @Override
  public EntityID execute() throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(this.GET_UPDATEABLE_CHECKSUMS);
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        return new EntityID(rs.getString(1));
      } else {
        return null;
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
