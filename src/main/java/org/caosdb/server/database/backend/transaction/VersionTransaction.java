/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.interfaces.RetrieveVersionHistoryImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.VersionHistoryItem;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Version;

/**
 * Abstract base class which retrieves and caches the full, but flat version history. The
 * implementations then use the flat version history to construct either single version information
 * items (see {@link RetrieveVersionInfo}) or the complete history as a tree (see {@link
 * RetrieveVersionHistory})
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public abstract class VersionTransaction
    extends CacheableBackendTransaction<EntityID, HashMap<String, VersionHistoryItem>> {

  private static final ICacheAccess<EntityID, HashMap<String, VersionHistoryItem>> cache =
      Cache.getCache("BACKEND_RetrieveVersionHistory");
  private final EntityInterface entity;

  /** A map of all history items which belong to this entity. The keys are the version ids. */
  private HashMap<String, VersionHistoryItem> historyItems;

  /**
   * Invalidate a cache item. This should be called upon update of entities.
   *
   * @param entityId
   */
  public static void removeCached(final EntityID entityId) {
    cache.remove(entityId);
  }

  public VersionTransaction(final EntityInterface e) {
    super(cache);
    this.entity = e;
  }

  @Override
  public HashMap<String, VersionHistoryItem> executeNoCache() throws TransactionException {
    final RetrieveVersionHistoryImpl impl = getImplementation(RetrieveVersionHistoryImpl.class);
    return impl.execute(getKey());
  }

  /** After this method call, the version map is available to the object. */
  @Override
  protected void process(final HashMap<String, VersionHistoryItem> historyItems)
      throws TransactionException {
    this.historyItems = historyItems;
  }

  @Override
  protected EntityID getKey() {
    return entity.getId();
  }

  public HashMap<String, VersionHistoryItem> getHistoryItems() {
    return this.historyItems;
  }

  public EntityInterface getEntity() {
    return this.entity;
  }

  /**
   * Return a list of direct predecessors. The predecessors are constructed by {@link
   * #getVersion(String)}.
   *
   * <p>If transitive is true, this function is called recursively on the predecessors as well,
   * resulting in a list of trees of predecessors, with the direct predecessors at the root(s).
   *
   * @param versionId
   * @param transitive
   * @return A list of predecessors.
   */
  protected List<Version> getPredecessors(final String versionId, final boolean transitive) {
    final LinkedList<Version> result = new LinkedList<>();
    if (getHistoryItems().containsKey(versionId)
        && getHistoryItems().get(versionId).parents != null) {
      for (final String p : getHistoryItems().get(versionId).parents) {
        final Version predecessor = getVersion(p);
        if (transitive) {
          predecessor.setPredecessors(getPredecessors(p, transitive));
        }
        result.add(predecessor);
      }
    }
    return result;
  }

  /**
   * To be implemented by the base class. The idea is, that the base class decides which information
   * is being included into the Version instance.
   *
   * @param versionId - the id of the version
   * @return
   */
  protected abstract Version getVersion(String versionId);

  /**
   * Return a list of direct successors. The successors are constructed by {@link
   * #getVersion(String)}.
   *
   * <p>If transitive is true, this function is called recursively on the successors as well,
   * resulting in a list of trees of successors, with the direct successors at the root(s).
   *
   * @param versionId
   * @param transitive
   * @return A list of successors.
   */
  protected List<Version> getSuccessors(final String versionId, final boolean transitive) {
    final LinkedList<Version> result = new LinkedList<>();

    outer:
    for (final VersionHistoryItem i : getHistoryItems().values()) {
      if (i.parents != null) {
        for (final String p : i.parents) {
          if (versionId.equals(p)) {
            final Version successor = getVersion(i.id);
            result.add(successor);
            if (transitive) {
              successor.setSuccessors(getSuccessors(i.id, transitive));
            }
            continue outer;
          }
        }
      }
    }
    return result;
  }
}
