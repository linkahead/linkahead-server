package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.GetAllNamesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;

/** Retrieve name, role and acl of all entities from the MySQL backend. */
public class MySQLGetAllNames extends MySQLTransaction implements GetAllNamesImpl {

  public MySQLGetAllNames(Access access) {
    super(access);
  }

  /** Retrieve tuples (EntityName, EntityRole, ACL) for all entities which have a name. */
  public static final String STMT_GET_ALL_NAMES =
      "SELECT d.value as EntityName, e.role AS EntityRole, a.acl AS ACL "
          + "FROM name_data AS d JOIN entities AS e JOIN entity_acl AS a "
          + "ON (d.domain_id = 0 AND d.property_id = 20 AND d.entity_id = e.id AND a.id = e.acl) "
          + "WHERE e.role != 'ROLE' AND e.role != 'DATATYPE'";

  @Override
  public List<SparseEntity> execute() {
    ArrayList<SparseEntity> ret = new ArrayList<>();

    try {
      PreparedStatement stmt = prepareStatement(STMT_GET_ALL_NAMES);

      try (ResultSet resultSet = stmt.executeQuery()) {
        while (resultSet.next()) {
          SparseEntity spe = DatabaseUtils.parseNameRoleACL(resultSet);
          ret.add(spe);
        }

      } catch (SQLException e1) {
        throw new TransactionException(e1);
      }
    } catch (SQLException | ConnectionException e2) {
      throw new TransactionException(e2);
    }

    return ret;
  }
}
