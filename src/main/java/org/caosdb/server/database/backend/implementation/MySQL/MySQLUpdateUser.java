/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.UpdateUserImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoUser;

public class MySQLUpdateUser extends MySQLTransaction implements UpdateUserImpl {

  private static final String STMT_UPDATE_USER =
      "INSERT INTO user_info (realm, name, status, email, entity) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE status=?, email=?, entity=?";

  public MySQLUpdateUser(final Access access) {
    super(access);
  }

  @Override
  public void execute(final ProtoUser user) throws TransactionException {
    try {

      final PreparedStatement stmt = prepareStatement(STMT_UPDATE_USER);
      stmt.setString(1, user.realm);
      stmt.setString(2, user.name);
      stmt.setString(3, user.status.toString());
      stmt.setString(4, user.email);
      if (user.entity != null) {
        stmt.setString(5, user.entity);
      } else {
        stmt.setNull(5, Types.INTEGER);
      }

      stmt.setString(6, user.status.toString());
      stmt.setString(7, user.email);
      if (user.entity != null) {
        stmt.setString(8, user.entity);
      } else {
        stmt.setNull(8, Types.INTEGER);
      }
      stmt.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
