/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.SetPermissionRulesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.permissions.PermissionRule;
import org.eclipse.jetty.util.ajax.JSON;

public class MySQLSetPermissionRules extends MySQLTransaction implements SetPermissionRulesImpl {

  public MySQLSetPermissionRules(final Access access) {
    super(access);
  }

  public static final String STMT_DELETE_PERMISSION_RULES = "DELETE FROM permissions WHERE role=?";
  public static final String STMT_INSERT_PERMISSION_RULES =
      "INSERT INTO permissions (role, permissions) VALUES (?,?)";

  @Override
  public void updatePermissionRules(final String role, final Set<PermissionRule> rules)
      throws TransactionException {
    try {
      final PreparedStatement delete_stmt = prepareStatement(STMT_DELETE_PERMISSION_RULES);
      delete_stmt.setString(1, role);
      delete_stmt.execute();

      if (rules != null && !rules.isEmpty()) {
        final PreparedStatement insert_stmt = prepareStatement(STMT_INSERT_PERMISSION_RULES);

        final String json = toJSON(rules);
        insert_stmt.setString(1, role);
        insert_stmt.setString(2, json);
        insert_stmt.execute();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }

  private String toJSON(final Set<PermissionRule> rules) {
    final HashSet<Map<String, String>> maps = new HashSet<Map<String, String>>();
    for (final PermissionRule rule : rules) {
      maps.add(rule.getMap());
    }
    return JSON.toString(maps.toArray());
  }
}
