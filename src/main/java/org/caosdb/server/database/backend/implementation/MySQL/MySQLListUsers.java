/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.accessControl.UserStatus;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.ListUsersImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoUser;

public class MySQLListUsers extends MySQLTransaction implements ListUsersImpl {

  public MySQLListUsers(Access access) {
    super(access);
  }

  public static final String STMT_LIST_USERS =
      "SELECT status, realm, name, email, entity FROM user_info";

  @Override
  public List<ProtoUser> execute() {
    List<ProtoUser> users = new LinkedList<>();
    try {
      final PreparedStatement stmt = prepareStatement(STMT_LIST_USERS);
      final ResultSet rs = stmt.executeQuery();
      try {
        while (rs.next()) {
          final ProtoUser user = new ProtoUser();
          user.name = rs.getString("name");
          user.realm = rs.getString("realm");
          user.email = rs.getString("email");
          user.entity = rs.getString("entity");
          if (user.entity == null || user.entity.isBlank() || user.entity.equals("0")) {
            user.entity = null;
          }
          user.status = UserStatus.valueOf(rs.getString("status"));
          users.add(user);
        }
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return users;
  }
}
