/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.SetQueryTemplateDefinitionImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

/**
 * Implements {@link SetQueryTemplateDefinitionImpl} for a MariaDB back-end.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class MySQLSetQueryTemplateDefinition extends MySQLTransaction
    implements SetQueryTemplateDefinitionImpl {

  public MySQLSetQueryTemplateDefinition(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_QUERY_TEMPLATE_DEF =
      "INSERT INTO query_template_def (id, definition) VALUES ((SELECT internal_id FROM entity_ids WHERE entity_ids.id = ?),?) ON DUPLICATE KEY UPDATE definition=?;";

  @Override
  public void insert(final EntityID id, final String definition) {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_INSERT_QUERY_TEMPLATE_DEF);
      stmt.setString(1, id.toString());
      stmt.setString(2, definition);
      stmt.setString(3, definition);
      stmt.execute();
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
