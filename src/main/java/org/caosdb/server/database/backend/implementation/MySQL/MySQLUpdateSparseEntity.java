/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Types;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.UpdateSparseEntityImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;

public class MySQLUpdateSparseEntity extends MySQLTransaction implements UpdateSparseEntityImpl {

  public MySQLUpdateSparseEntity(final Access access) {
    super(access);
  }

  public static final String STMT_UPDATE_ENTITY = "call updateEntity(?,?,?,?,?,?,?)";
  public static final String STMT_UPDATE_FILE_PROPS = "call setFileProperties(?,?,?,?)";

  @Override
  public void execute(final SparseEntity spe) throws TransactionException {
    try {
      // file properties;
      final PreparedStatement updateFilePropsStmt = prepareStatement(STMT_UPDATE_FILE_PROPS);
      updateFilePropsStmt.setString(1, spe.id);
      if (spe.filePath != null) {
        updateFilePropsStmt.setString(2, spe.filePath);
        updateFilePropsStmt.setLong(3, spe.fileSize);
        if (spe.fileHash != null) {
          updateFilePropsStmt.setString(4, spe.fileHash);
        } else {
          updateFilePropsStmt.setNull(4, Types.VARCHAR);
        }
      } else {
        updateFilePropsStmt.setNull(2, Types.VARCHAR);
        updateFilePropsStmt.setNull(3, Types.BIGINT);
        updateFilePropsStmt.setNull(4, Types.VARCHAR);
      }
      updateFilePropsStmt.execute();

      // very sparse entity
      final PreparedStatement updateEntityStmt = prepareStatement(STMT_UPDATE_ENTITY);
      updateEntityStmt.setString(1, spe.id);
      updateEntityStmt.setString(2, spe.name);
      updateEntityStmt.setString(3, spe.description);
      updateEntityStmt.setString(4, spe.role);
      if (spe.datatype_id != null) {
        updateEntityStmt.setString(5, spe.datatype_id);
      } else {
        updateEntityStmt.setNull(5, Types.VARCHAR);
      }
      updateEntityStmt.setString(6, spe.collection);
      updateEntityStmt.setString(7, spe.acl);
      ResultSet rs = updateEntityStmt.executeQuery();

      if (rs.next()) {
        spe.versionId = DatabaseUtils.bytes2UTF8(rs.getBytes("Version"));
      }

    } catch (final SQLIntegrityConstraintViolationException e) {
      throw new IntegrityException(e);
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
