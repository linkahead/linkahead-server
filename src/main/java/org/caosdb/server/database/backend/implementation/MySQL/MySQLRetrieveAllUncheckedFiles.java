/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveAllUncheckedFilesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.SparseEntity;

public class MySQLRetrieveAllUncheckedFiles extends MySQLTransaction
    implements RetrieveAllUncheckedFilesImpl {

  private static final String STMT_RETRIEVE_ALL =
      "SELECT file_id, path, size, hex(hash) AS file_hash, checked_timestamp FROM files WHERE checked_timestamp<? AND path LIKE ?";

  public MySQLRetrieveAllUncheckedFiles(final Access access) {
    super(access);
  }

  @Override
  public Iterator<SparseEntity> execute(final long ts, final String location)
      throws TransactionException {
    try {
      final PreparedStatement stmt = getMySQLHelper().prepareStatement(STMT_RETRIEVE_ALL);
      stmt.setLong(1, ts);
      stmt.setString(2, location + "%");
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        final SparseEntity first = new SparseEntity();
        first.id = rs.getString("file_id");
        first.filePath = rs.getString("path");
        first.fileHash = rs.getString("file_hash");
        first.fileSize = rs.getLong("size");
        first.fileChecked = rs.getLong("checked_timestamp");
        return new Iterator<SparseEntity>() {

          SparseEntity next = first;

          @Override
          public boolean hasNext() {
            return this.next != null;
          }

          @Override
          public SparseEntity next() {
            final SparseEntity ret = this.next;
            try {
              if (rs.next()) {
                this.next = new SparseEntity();
                this.next.id = rs.getString("file_id");
                this.next.filePath = rs.getString("path");
                this.next.fileHash = rs.getString("file_hash");
                this.next.fileSize = rs.getLong("size");
                this.next.fileChecked = rs.getLong("checked_timestamp");
              } else {
                this.next = null;
                rs.close();
              }
            } catch (final SQLException e) {
              e.printStackTrace();
            }
            return ret;
          }

          @Override
          public void remove() {}

          @Override
          protected void finalize() throws Throwable {
            super.finalize();
            rs.close();
          }
        };
      }

    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return null;
  }
}
