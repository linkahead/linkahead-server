/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.IsSubTypeImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

public class MySQLIsSubType extends MySQLTransaction implements IsSubTypeImpl {

  public MySQLIsSubType(final Access access) {
    super(access);
  }

  public static final String STMT_IS_SUBTYPE = "call isSubtype(?,?)";

  @Override
  public boolean execute(final EntityID child, final EntityID parent) throws TransactionException {
    try {
      final PreparedStatement isSubtypeStmt = prepareStatement(STMT_IS_SUBTYPE);

      isSubtypeStmt.setString(1, child.toString());
      isSubtypeStmt.setString(2, parent.toString());

      final ResultSet rs = isSubtypeStmt.executeQuery();
      try {
        if (rs.next()) {
          return rs.getBoolean(1);
        } else {
          throw new TransactionException("No return value.");
        }
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
