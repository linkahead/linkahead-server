/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.ListRolesImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLListRoles extends MySQLTransaction implements ListRolesImpl {

  public MySQLListRoles(Access access) {
    super(access);
  }

  public static final String STMT_LIST_ROLES = "SELECT name, description FROM roles";

  @Override
  public List<Role> execute() {
    List<Role> roles = new LinkedList<>();
    try {
      final PreparedStatement stmt = prepareStatement(STMT_LIST_ROLES);
      final ResultSet rs = stmt.executeQuery();
      try {
        while (rs.next()) {
          final Role role = new Role();
          role.name = rs.getString("name");
          role.description = rs.getString("description");
          roles.add(role);
        }
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return roles;
  }
}
