/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import com.google.common.base.Objects;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import snaq.db.ConnectionPool;

/**
 * DatabaseConnectionPool provides reusable MySQL Connections. They are configured and ready to use.
 *
 * <p>Singleton
 *
 * @author Timm Fitschen
 */
class DatabaseConnectionPool {

  public static Logger logger = LoggerFactory.getLogger(DatabaseConnectionPool.class);

  static Connection getConnection() throws ConnectionException {
    try {
      if (instance == null) {
        instance = createConnectionPool();
      }
      Connection ret = instance.getConnection();
      while (!ret.isValid(1000)) {
        ret.close();
        ret = instance.getConnection();
      }
      ret.setReadOnly(true);
      ret.setAutoCommit(true);
      return ret;
    } catch (final Exception e) {
      if (instance != null) {
        instance.release();
      }
      instance = null;
      throw new ConnectionException(e);
    }
  }

  /** Singleton - Hidden Constructor. */
  private DatabaseConnectionPool() {}

  private static ConnectionPool instance = null;

  private static synchronized ConnectionPool createConnectionPool()
      throws ClassNotFoundException,
          InstantiationException,
          IllegalAccessException,
          SQLException,
          ConnectionException,
          CaosDBException {

    final String url =
        "jdbc:mysql://"
            + CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_HOST)
            + ":"
            + CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_PORT)
            + "/"
            + CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_DATABASE_NAME)
            + "?noAccessToProcedureBodies=true&autoReconnect=true&serverTimezone=UTC&characterEncoding=UTF-8";
    final String user = CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_USER_NAME);
    final String pwd = CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_USER_PASSWORD);
    final ConnectionPool pool = new ConnectionPool("MySQL Pool", 2, 5, 0, 0, url, user, pwd);
    pool.removeShutdownHook();
    CaosDBServer.addPostShutdownHook(
        new Thread("SHUTDOWN_MYSQL_CONNECTION_POOL") {
          @Override
          public void run() {
            try {
              final Thread t =
                  new Thread() {
                    @Override
                    public void run() {
                      pool.release();
                    }
                  };
              t.start();
              t.join(5000);
              if (!pool.isReleased()) {
                pool.releaseImmediately();
              }
              logger.debug("Stopping MySQL connection pool [OK]\n");
            } catch (final Exception e) {
              logger.error("Stopping MySQL connection pool [failed]\n", e);
            }
          }
        });
    pool.init();

    checkVersion(pool.getConnection());
    printSettings(pool);

    return pool;
  }

  static void printSettings(final ConnectionPool pool) throws SQLException {
    final Connection connection = pool.getConnection();
    Statement statement = connection.createStatement();
    ResultSet query = statement.executeQuery("SHOW VARIABLES LIKE '%char%'");
    int columnCount = query.getMetaData().getColumnCount();
    while (query.next()) {
      String p = "|";
      for (int i = 1; i <= columnCount; i++) {
        p += query.getString(i);
        p += "|";
      }
      logger.debug(p);
    }
    query.close();
    statement.close();

    statement = connection.createStatement();
    query = statement.executeQuery("SHOW VARIABLES LIKE '%coll%'");
    columnCount = query.getMetaData().getColumnCount();
    while (query.next()) {
      String p = "|";
      for (int i = 1; i <= columnCount; i++) {
        p += query.getString(i);
        p += "|";
      }
      logger.debug(p);
    }
    query.close();
    statement.close();

    statement = connection.createStatement();
    query = statement.executeQuery("SHOW VARIABLES LIKE '%max%'");
    columnCount = query.getMetaData().getColumnCount();
    while (query.next()) {
      String p = "|";
      for (int i = 1; i <= columnCount; i++) {
        p += query.getString(i);
        p += "|";
      }
      logger.debug(p);
    }
    query.close();
    statement.close();
    connection.close();
  }

  static void printStatus() {
    logger.debug("###### POOL ########");
    logger.debug("#free   : " + instance.getFreeCount());
    logger.debug("#checked: " + instance.getCheckedOut());
    logger.debug("#params : " + instance.getParametersString());
    logger.debug("####################");
  }

  /**
   * Check the version of the SQL server's database.
   *
   * <p>Current behaviour: Major versions must match and the database's minor version must be at
   * least the expected minor version as defined in {@link
   * ServerProperties.KEY_MYSQL_SCHEMA_VERSION}.
   *
   * <p>@todo Patch versions are not handled yet.
   */
  private static void checkVersion(final Connection con)
      throws SQLException, ConnectionException, CaosDBException {
    try {
      final String[] expected_version =
          CaosDBServer.getServerProperty(ServerProperties.KEY_MYSQL_SCHEMA_VERSION)
              .toLowerCase()
              .split("\\.");
      final String expected_major = expected_version[0];
      final String expected_minor = expected_version[1];

      con.setReadOnly(false);
      final PreparedStatement prepared = con.prepareStatement("SELECT CaosDBVersion()");
      try {
        final ResultSet executeQuery = prepared.executeQuery();
        if (executeQuery.next()) {

          final String actual_version = executeQuery.getString(1).toLowerCase();
          final String[] actual_version_split = actual_version.split("\\.");
          final String actual_major = actual_version_split[0];
          final String actual_minor = actual_version_split[1];

          if (!Objects.equal(actual_major, expected_major)) {
            if (Integer.parseInt(actual_major.replace("v", ""))
                < Integer.parseInt(expected_major.replace("v", ""))) {
              logger.error(
                  "Version of the MySQL/MariaDB schema is incompatible.\n\tExpected: {}.{}\n\tActual: {}\nPlease upgrade the MySQL backend.\n\n",
                  expected_major,
                  expected_minor,
                  actual_version);
            } else {
              logger.error(
                  "Version of the MySQL/MariaDB schema is incompatible.\n\tExpected: {}.{}\n\tActual: {}\nPlease upgrade the CaosDB server.\n\n",
                  expected_major,
                  expected_minor,
                  actual_version);
            }
            System.exit(1);
          }
          if (!Objects.equal(actual_minor, expected_minor)) {
            if (Integer.parseInt(actual_minor) < Integer.parseInt(expected_minor)) {
              logger.error(
                  "Version of the MySQL/MariaDB schema is incompatible.\n\tExpected: {}.{}\n\tActual: {}\nPlease upgrade the MySQL backend.\n\n",
                  expected_major,
                  expected_minor,
                  actual_version);
              System.exit(1);
            }
          }
        } else {
          logger.error(
              "Version of the MySQL schema could not be determined. This probably means that the version is too old.\n\tExpected: {}.{}\n\tActual: unknown\nPlease upgrade the MySQL backend.\n\n",
              expected_major,
              expected_minor);
          System.exit(1);
        }
      } catch (final SQLException e) {
        logger.error("Could not check the version of the MySQL schema.", e);
        System.exit(1);
      }

    } finally {
      con.close();
    }
  }
}
