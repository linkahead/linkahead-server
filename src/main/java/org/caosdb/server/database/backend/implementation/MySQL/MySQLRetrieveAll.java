/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveAllImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.Role;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.permissions.EntityPermission;

public class MySQLRetrieveAll extends MySQLTransaction implements RetrieveAllImpl {

  public MySQLRetrieveAll(final Access access) {
    super(access);
  }

  public static final String STMT_GET_ALL_HEAD =
      "SELECT e.id AS ID, a.acl AS ACL FROM entities AS e JOIN entity_acl AS a ON (e.acl = a.id) WHERE e.id > 99";
  public static final String STMT_ENTITY_WHERE_CLAUSE =
      " AND ( e.role='"
          + Role.Record
          + "' OR e.role='"
          + Role.RecordType
          + "' OR e.role='"
          + Role.Property
          + "' OR e.role='"
          + Role.File
          + "'"
          + " )";
  public static final String STMT_OTHER_ROLES = " AND e.role=?";

  @Override
  public List<EntityID> execute(final String role) throws TransactionException {
    try {
      final String STMT_GET_ALL =
          STMT_GET_ALL_HEAD
              + (role.equalsIgnoreCase("ENTITY") ? STMT_ENTITY_WHERE_CLAUSE : STMT_OTHER_ROLES);
      final PreparedStatement stmt = prepareStatement(STMT_GET_ALL);

      if (!role.equalsIgnoreCase("ENTITY")) {
        stmt.setString(1, role);
      }

      final ResultSet rs = stmt.executeQuery();
      try {
        final List<EntityID> ret = new LinkedList<>();
        while (rs.next()) {
          final String acl = DatabaseUtils.bytes2UTF8(rs.getBytes("ACL"));
          if (EntityACL.deserialize(acl)
              .isPermitted(SecurityUtils.getSubject(), EntityPermission.RETRIEVE_ENTITY)) {
            ret.add(new EntityID(rs.getString("ID")));
          }
        }
        return ret;
      } finally {
        rs.close();
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
