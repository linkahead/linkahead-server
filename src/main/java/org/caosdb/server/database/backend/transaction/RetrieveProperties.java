/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 * Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.transaction;

import java.util.LinkedList;
import java.util.List;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.CacheableBackendTransaction;
import org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils;
import org.caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.proto.ProtoProperty;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Property;

public class RetrieveProperties
    extends CacheableBackendTransaction<String, LinkedList<ProtoProperty>> {

  private final EntityInterface entity;
  public static final String CACHE_REGION = "BACKEND_EntityProperties";
  private static final ICacheAccess<String, LinkedList<ProtoProperty>> cache =
      Cache.getCache(CACHE_REGION);

  /**
   * To be called by DeleteEntityProperties on execution.
   *
   * @param id
   */
  protected static void removeCached(final String idVersion) {
    if (idVersion != null && cache != null) {
      cache.remove(idVersion);
    }
  }

  public RetrieveProperties(final EntityInterface entity) {
    super(cache);
    this.entity = entity;
  }

  @Override
  public LinkedList<ProtoProperty> executeNoCache() throws TransactionException {
    final RetrievePropertiesImpl t = getImplementation(RetrievePropertiesImpl.class);
    return t.execute(
        this.entity.getId(), this.entity.getVersion().getId(), this.entity.getVersion().isHead());
  }

  @Override
  protected void process(final LinkedList<ProtoProperty> t) throws TransactionException {
    this.entity.getProperties().clear();

    final List<Property> props = DatabaseUtils.parseFromProtoProperties(this.entity, t);
    this.entity.getProperties().addAll(props);

    retrieveSparseSubproperties(props);
  }

  private void retrieveSparseSubproperties(List<Property> properties) {
    for (final Property p : properties) {
      // retrieve sparse properties stage 1

      // handle corner case where a record type is used as a property with an overridden name.
      boolean isNameOverride = p.isNameOverride();
      String overrideName = p.getName();
      p.setNameOverride(false);

      final RetrieveSparseEntity t1 = new RetrieveSparseEntity(p);
      execute(t1);
      String originalName = p.getName();
      if (isNameOverride) {
        p.setNameOverride(isNameOverride);
        p.setName(overrideName);
      }

      if (!p.hasDatatype() && p.getRole() == Role.RecordType) {
        p.setDatatype(originalName);
      }

      // retrieve sparse properties stage 2
      retrieveSparseSubproperties(p.getProperties());
    }
  }

  @Override
  protected String getKey() {
    return this.entity.getIdVersion();
  }
}
