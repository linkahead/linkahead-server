/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.LogUserVisitImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLLogUserVisit extends MySQLTransaction implements LogUserVisitImpl {

  public MySQLLogUserVisit(Access access) {
    super(access);
  }

  public static final String LOG_USER_VISIT =
      "SELECT status FROM user_info WHERE realm = ? AND name = ?";

  // TODO Replace by "UPDATE user_info SET last_seen = ?";

  /** Return true if this is not the first visit of this user. */
  @Override
  public boolean logUserReturnIsKnown(long timestamp, String realm, String username, String type) {
    try (final PreparedStatement stmt = prepareStatement(LOG_USER_VISIT)) {
      stmt.setString(1, realm);
      stmt.setString(2, username);
      ResultSet executeQuery = stmt.executeQuery();
      if (!executeQuery.next()) {
        // first login of this user
        return false;
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
    return true;
  }
}
