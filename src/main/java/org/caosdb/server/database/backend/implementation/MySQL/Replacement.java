/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.StatementStatusInterface;
import org.caosdb.server.entity.wrapper.Property;

enum ReplacementStatus implements StatementStatusInterface {
  REPLACEMENT
}

/**
 * A wrapper of {@link Property} objects used by the back-end implementation for the MySQL/MariaDB
 * back-end.
 *
 * <p>This class helps to transform deeply nested properties, properties with overridden data types,
 * and much more to the flat (row-like) representation used internally by the back-end (which is an
 * implementation detail of the back-end or part of the non-public API of the back-end from that
 * point of view).
 */
public class Replacement extends Property {

  public Property replacement;
  private boolean stage2Replacement;

  @Override
  public EntityID getId() {
    return replacement.getId();
  }

  @Override
  public boolean hasId() {
    return replacement.hasId();
  }

  public void setReplacementId(EntityID id) {
    replacement.getId().link(id);
  }

  public Replacement(Property p) {
    super(p);
    replacement = new Property(new RetrieveEntity());

    replacement.setDomain(p.getDomainEntity());
    replacement.setId(new EntityID());
    replacement.setStatementStatus(ReplacementStatus.REPLACEMENT);
    replacement.setValue(new ReferenceValue(p.getId()));
    replacement.setPIdx(p.getPIdx());
  }

  public void setStage2Replacement(boolean t) {
    this.stage2Replacement = t;
  }

  public boolean isStage2Replacement() {
    return stage2Replacement;
  }
}
