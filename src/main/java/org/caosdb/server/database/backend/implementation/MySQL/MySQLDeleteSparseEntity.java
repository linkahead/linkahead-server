/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.DeleteSparseEntityImpl;
import org.caosdb.server.database.exceptions.IntegrityException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

public class MySQLDeleteSparseEntity extends MySQLTransaction implements DeleteSparseEntityImpl {

  public MySQLDeleteSparseEntity(final Access access) {
    super(access);
  }

  public static final String STMT_DELETE_SPARSE_ENTITY = "call deleteEntity(?)";

  @Override
  public void execute(final EntityID id) throws TransactionException {

    try {
      final PreparedStatement stmt = prepareStatement(STMT_DELETE_SPARSE_ENTITY);

      stmt.setString(1, id.toString());
      stmt.execute();
    } catch (final SQLIntegrityConstraintViolationException exc) {
      throw new IntegrityException(exc);
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
