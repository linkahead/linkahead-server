/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import com.google.common.base.Objects;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.caosdb.server.database.proto.FlatProperty;
import org.caosdb.server.database.proto.ProtoProperty;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.datatype.AbstractCollectionDatatype;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.wrapper.Property;

public class DatabaseUtils {

  private static final String DELETED_REFERENCE_IN_PREVIOUS_VERSION =
      "DELETED_REFERENCE_IN_PREVIOUS_VERSION";

  public static final String bytes2UTF8(final byte[] bytes) {
    if (bytes == null) {
      return null;
    } else {
      return new String(bytes);
    }
  }

  public static int deriveStage1Inserts(
      final List<Property> stage1Inserts, final EntityInterface e) {
    // entity value
    if (e.hasValue()) {
      final Property p = new Property(e);
      p.setStatementStatus(StatementStatus.FIX);
      p.setDatatype(e.getDatatype());
      p.setValue(e.getValue());
      p.setPIdx(0);
      processPropertiesStage1(stage1Inserts, p, e);
    }

    for (final Property p : e.getProperties()) {
      processPropertiesStage1(stage1Inserts, p, e);
    }
    int replacementCount = 0;
    for (EntityInterface pp : stage1Inserts) {
      if (pp instanceof Replacement) {
        replacementCount++;
      }
    }
    return replacementCount;
  }

  private static void processPropertiesStage1(
      final List<Property> stage1Inserts, final Property p, final EntityInterface e) {
    if (!p.isDescOverride()
        && !p.isNameOverride()
        && !p.isDatatypeOverride()
        && (!p.hasProperties() || hasUniquePropertyId(p, e))
        && !(p.getDatatype() instanceof AbstractCollectionDatatype)) {
      stage1Inserts.add(p);
      processSubPropertiesStage1(stage1Inserts, p);
    } else {
      Replacement r = new Replacement(p);
      stage1Inserts.add(r);
      stage1Inserts.add(r.replacement);
      processSubPropertiesStage1(stage1Inserts, r);
    }
  }

  public static void deriveStage2Inserts(
      final List<Property> stage2Inserts,
      final List<Property> stage1Inserts,
      Deque<EntityID> replacementIds,
      EntityInterface entity) {
    for (final Property p : stage1Inserts) {
      if (p instanceof Replacement) {
        if (!p.hasId() || p.getId().isTemporary()) {
          EntityID replacementId = replacementIds.pop();
          ((Replacement) p).setReplacementId(replacementId);
        }
      }
      if (p.hasProperties()) {
        if (p instanceof Replacement && ((Replacement) p).isStage2Replacement()) {
          stage2Inserts.add(((Replacement) p).replacement);
        }
        for (Property subP : p.getProperties()) {
          if (!subP.hasProperties()) {
            stage2Inserts.add(subP);
          }
        }
      }
    }
  }

  private static boolean hasUniquePropertyId(final EntityInterface p, final EntityInterface e) {
    final EntityID id = p.getId();
    for (final EntityInterface p2 : e.getProperties()) {
      if (Objects.equal(p2.getId(), id) && p2 != p) {
        return false;
      }
    }
    return true;
  }

  private static void processSubPropertiesStage1(
      final List<Property> stage1Inserts, final EntityInterface p) {
    for (final Property subP : p.getProperties()) {
      subP.setDomain(p);
      if (subP.hasProperties()) {
        Replacement r = new Replacement(subP);
        r.setStage2Replacement(true);
        stage1Inserts.add(r);
        processSubPropertiesStage1(stage1Inserts, r);
      }
    }
  }

  public static void parseOverrides(final List<ProtoProperty> properties, final ResultSet rs)
      throws SQLException {
    while (rs.next()) {
      String property_id = rs.getString("property_id");
      if (rs.wasNull()) property_id = rs.getString("InternalPropertyID");
      for (final FlatProperty p : properties) {
        if (p.id.equals(property_id)) {
          final String name = bytes2UTF8(rs.getBytes("name_override"));
          if (name != null) {
            p.name = name;
          }
          final String desc = bytes2UTF8(rs.getBytes("desc_override"));
          if (desc != null) {
            p.desc = desc;
          }
          final String type_id = bytes2UTF8(rs.getBytes("type_id_override"));
          if (type_id != null) {
            p.type_id = type_id;
            p.type_name = bytes2UTF8(rs.getBytes("type_name_override"));
          }
          final String coll = bytes2UTF8(rs.getBytes("collection_override"));
          if (coll != null) {
            p.collection = coll;
          }
        }
      }
    }
  }

  public static List<ProtoProperty> parsePropertyResultset(final ResultSet rs) throws SQLException {
    final List<ProtoProperty> ret = new LinkedList<>();
    while (rs.next()) {
      ProtoProperty pp = new ProtoProperty();
      pp.id = rs.getString("PropertyID");
      if (rs.wasNull()) {
        pp.id = rs.getString("InternalPropertyID");
      }
      pp.value = bytes2UTF8(rs.getBytes("PropertyValue"));
      pp.status = bytes2UTF8(rs.getBytes("PropertyStatus"));
      pp.idx = rs.getInt("PropertyIndex");
      ret.add(pp);
    }
    return ret;
  }

  /**
   * Helper function for parsing MySQL results.
   *
   * <p>This function creates SparseEntities and parses only the name, the role and the acl of an
   * entity.
   *
   * <p>Never returns null.
   */
  public static SparseEntity parseNameRoleACL(final ResultSet rs) throws SQLException {
    final SparseEntity ret = new SparseEntity();
    ret.role = bytes2UTF8(rs.getBytes("EntityRole"));
    ret.name = bytes2UTF8(rs.getBytes("EntityName"));
    ret.acl = bytes2UTF8(rs.getBytes("ACL"));
    return ret;
  }

  /**
   * Helper function for parsing MySQL results.
   *
   * <p>This function creates SparseEntities and parses all fields which belong to a SparseEntity:
   * id, name, role, acl, description, datatype, and the file properties.
   *
   * <p>Never returns null.
   */
  public static SparseEntity parseEntityResultSet(final ResultSet rs) throws SQLException {
    final SparseEntity ret = parseNameRoleACL(rs);
    ret.id = rs.getString("EntityID");
    ret.description = bytes2UTF8(rs.getBytes("EntityDesc"));
    ret.datatype_id = bytes2UTF8(rs.getBytes("DatatypeID"));
    ret.datatype_name = bytes2UTF8(rs.getBytes("DatatypeName"));
    ret.collection = bytes2UTF8(rs.getBytes("Collection"));

    ret.filePath = bytes2UTF8(rs.getBytes("FilePath"));
    ret.fileSize = rs.getLong("FileSize");
    ret.fileHash = bytes2UTF8(rs.getBytes("FileHash"));

    ret.versionId = bytes2UTF8(rs.getBytes("Version"));
    return ret;
  }

  public static LinkedList<VerySparseEntity> parseParentResultSet(final ResultSet rs)
      throws SQLException {
    final LinkedList<VerySparseEntity> ret = new LinkedList<>();
    while (rs.next()) {
      final VerySparseEntity vsp = new VerySparseEntity();
      vsp.id = rs.getString("ParentID");
      vsp.name = bytes2UTF8(rs.getBytes("ParentName"));
      vsp.description = bytes2UTF8(rs.getBytes("ParentDescription"));
      vsp.role = bytes2UTF8(rs.getBytes("ParentRole"));
      vsp.acl = bytes2UTF8(rs.getBytes("ACL"));
      ret.add(vsp);
    }
    return ret;
  }

  public static ArrayList<Property> parseFromProtoProperties(
      EntityInterface entity, List<ProtoProperty> protos) {
    final ArrayList<Property> ret = new ArrayList<Property>();
    for (final ProtoProperty pp : protos) {
      if (pp.id.equals(entity.getId().toString())) {
        if (pp.value != null) {
          entity.setValue(new GenericValue(pp.value));
        }
        if (pp.collValues != null) {
          CollectionValue value = new CollectionValue();
          for (Object next : pp.collValues) {
            if (next == null) {
              value.add(null);
            } else {
              value.add(new GenericValue(next.toString()));
            }
          }
          entity.setValue(value);
        }

      } else {
        Message warning = null;
        if (pp.status == DELETED_REFERENCE_IN_PREVIOUS_VERSION) {
          pp.status = StatementStatus.FIX.name();
          warning =
              new Message(
                  "The referenced entity has been deleted in the mean time and is not longer available.");
        }
        final Property property = parseFlatProperty(pp);
        if (warning != null) {
          property.addWarning(warning);
        }
        ret.add(property);
      }
    }
    return ret;
  }

  private static Property parseFlatProperty(final ProtoProperty fp) {
    final Property property = new Property(new RetrieveEntity());
    property.parseProtoProperty(fp);
    return property;
  }

  @SuppressWarnings("unchecked")
  public static LinkedList<ProtoProperty> transformToDeepPropertyTree(
      List<ProtoProperty> properties, boolean isHead) {
    LinkedList<ProtoProperty> result = new LinkedList<>();
    Map<String, ProtoProperty> replacements = new HashMap<>();
    for (ProtoProperty pp : properties) {
      if (pp.status.equals(ReplacementStatus.REPLACEMENT.name())) {
        replacements.put(pp.value, null);
      }
      if (pp.subProperties != null) {
        for (ProtoProperty subP : pp.subProperties) {
          if (subP.status.equals(ReplacementStatus.REPLACEMENT.name())) {

            replacements.put(subP.value, null);
          }
        }
      }
    }
    Iterator<ProtoProperty> iterator = properties.iterator();
    while (iterator.hasNext()) {
      ProtoProperty pp = iterator.next();
      if (replacements.containsKey(pp.id.toString())) {
        ProtoProperty other = replacements.get(pp.id.toString());
        // handle collection values
        if (other != null) {
          if (other.collValues == null) {
            other.collValues = new LinkedList<>();
            Entry<Integer, String> obj = new SimpleEntry<>(other.idx, other.value);
            other.collValues.add(obj);
            other.value = null;
          }
          other.collValues.add(new SimpleEntry<>(pp.idx, pp.value));
        } else {
          replacements.put(pp.id.toString(), pp);
        }
        iterator.remove();
      }
    }
    for (ProtoProperty pp : properties) {
      if (pp.status.equals(ReplacementStatus.REPLACEMENT.name())) {
        replace(pp, replacements.get(pp.value), isHead);
      }
      if (pp.subProperties != null) {
        for (ProtoProperty subP : pp.subProperties) {
          if (subP.status.equals(ReplacementStatus.REPLACEMENT.name())) {
            replace(subP, replacements.get(subP.value), isHead);
          }
        }
      }
      if (pp.collValues != null) {
        // sort
        pp.collValues.sort(
            (x, y) -> {
              return ((Entry<Integer, String>) x).getKey() - ((Entry<Integer, String>) y).getKey();
            });
        pp.collValues =
            pp.collValues.stream()
                .map((x) -> (Object) ((Entry<Integer, String>) x).getValue())
                .collect(Collectors.toList());
      }
      result.add(pp);
    }
    return result;
  }

  /*
   * This replace function is used during the retrieval of properties. It is
   * basically the opposite of the Replacement class. It copies the information
   * from the replacement object back into the Property.
   */
  private static void replace(ProtoProperty pp, ProtoProperty replacement, boolean isHead) {
    if (replacement == null) {
      if (isHead) {
        throw new NullPointerException("Replacement was null");
      }
      // entity has been deleted (we are processing properties of an old entity version)
      pp.value = null;
      pp.status = DELETED_REFERENCE_IN_PREVIOUS_VERSION;
      return;
    }
    pp.desc = replacement.desc;
    pp.name = replacement.name;
    pp.type_id = replacement.type_id;
    pp.type_name = replacement.type_name;
    pp.collection = replacement.collection;
    pp.value = replacement.value;
    pp.collValues = replacement.collValues;
    pp.status = replacement.status;
    pp.subProperties = replacement.subProperties;
  }
}
