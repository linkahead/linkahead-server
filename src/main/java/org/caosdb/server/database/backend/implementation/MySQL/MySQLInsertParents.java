/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.InsertParentsImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;

public class MySQLInsertParents extends MySQLTransaction implements InsertParentsImpl {

  public MySQLInsertParents(final Access access) {
    super(access);
  }

  public static final String STMT_INSERT_ISA = "call insertIsa(?,?)";

  @Override
  public void execute(final EntityID entity, final EntityID parent) throws TransactionException {
    try {
      final PreparedStatement stmt = prepareStatement(STMT_INSERT_ISA);
      stmt.setString(1, entity.toString());
      stmt.setString(2, parent.toString());
      stmt.execute();
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }
}
