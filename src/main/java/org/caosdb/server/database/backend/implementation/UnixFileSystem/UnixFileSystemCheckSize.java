/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.database.backend.implementation.UnixFileSystem;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.FileCheckSize;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.Message;

public class UnixFileSystemCheckSize extends UnixFileSystemTransaction implements FileCheckSize {

  public UnixFileSystemCheckSize(final Access access) {
    super(access);
  }

  @Override
  public boolean execute(final String path, final long size) throws TransactionException {
    try {
      final File file = getFile(path);
      return size == file.length();
    } catch (final IOException | Message e) {
      throw new TransactionException(e);
    }
  }
}
