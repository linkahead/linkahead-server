package org.caosdb.server.database.backend.interfaces;

import java.util.HashMap;
import org.caosdb.server.database.proto.VersionHistoryItem;
import org.caosdb.server.entity.EntityID;

public interface RetrieveVersionHistoryImpl extends BackendTransactionImpl {

  public HashMap<String, VersionHistoryItem> execute(EntityID entityId);
}
