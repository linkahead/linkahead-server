package org.caosdb.server.database.backend.interfaces;

import org.caosdb.server.entity.EntityID;

/**
 * Store the checksum and timestamp of the checksum for a File entity.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public interface SetFileChecksumImpl extends BackendTransactionImpl {

  /**
   * Store the checksum of a File entity.
   *
   * <p>The entity is identified by the id.
   *
   * @param id
   * @param checksum
   */
  void execute(EntityID id, String checksum);
}
