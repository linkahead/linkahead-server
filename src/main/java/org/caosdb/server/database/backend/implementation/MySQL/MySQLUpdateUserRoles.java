/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Set;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.UpdateUserRolesImpl;
import org.caosdb.server.database.exceptions.TransactionException;

public class MySQLUpdateUserRoles extends MySQLTransaction implements UpdateUserRolesImpl {

  public MySQLUpdateUserRoles(final Access access) {
    super(access);
  }

  public static final String STMT_DELETE_USER_ROLES =
      "DELETE FROM user_roles WHERE realm=? and user=?";
  public static final String STMT_INSERT_USER_ROLES =
      "INSERT INTO user_roles (realm, user, role) VALUES (?,?,?);";

  @Override
  public void updateUserRoles(final String realm, final String user, final Set<String> roles)
      throws TransactionException {
    try {
      final PreparedStatement delete_stmt = prepareStatement(STMT_DELETE_USER_ROLES);
      delete_stmt.setString(1, realm);
      delete_stmt.setString(2, user);
      delete_stmt.execute();

      if (roles != null && !roles.isEmpty()) {
        final PreparedStatement insert_stmt = prepareStatement(STMT_INSERT_USER_ROLES);
        for (final String role : roles) {
          insert_stmt.setString(1, realm);
          insert_stmt.setString(2, user);
          insert_stmt.setString(3, role);
          insert_stmt.execute();
        }
      }
    } catch (final SQLException e) {
      throw new TransactionException(e);
    } catch (final ConnectionException e) {
      throw new TransactionException(e);
    }
  }
}
