/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database;

import java.util.HashMap;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLDeleteEntityProperties;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLDeletePassword;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLDeleteRole;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLDeleteSparseEntity;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLDeleteUser;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLGetAllNames;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLGetDependentEntities;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLGetFileRecordByPath;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLGetIDByName;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLGetInfo;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLGetUpdateableChecksums;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertEntityDatatype;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertEntityProperties;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertLinCon;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertParents;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertRole;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertSparseEntity;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLInsertTransactionHistory;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLIsSubType;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLListRoles;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLListUsers;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLLogUserVisit;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveAll;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveAllUncheckedFiles;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveCurrentMaxId;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveDatatypes;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveEntityACL;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveParents;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrievePasswordValidator;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrievePermissionRules;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveProperties;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveQueryTemplateDefinition;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveRole;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveSparseEntity;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveUser;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLRetrieveVersionHistory;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLSetFileCheckedTimestampImpl;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLSetFileChecksum;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLSetPassword;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLSetPermissionRules;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLSetQueryTemplateDefinition;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLSyncStats;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLUpdateSparseEntity;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLUpdateUser;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLUpdateUserRoles;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemCheckHash;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemCheckSize;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemFileExists;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemFileWasModifiedAfter;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemGetFileIterator;
import org.caosdb.server.database.backend.interfaces.BackendTransactionImpl;
import org.caosdb.server.database.backend.interfaces.DeleteEntityPropertiesImpl;
import org.caosdb.server.database.backend.interfaces.DeletePasswordImpl;
import org.caosdb.server.database.backend.interfaces.DeleteRoleImpl;
import org.caosdb.server.database.backend.interfaces.DeleteSparseEntityImpl;
import org.caosdb.server.database.backend.interfaces.DeleteUserImpl;
import org.caosdb.server.database.backend.interfaces.FileCheckHash;
import org.caosdb.server.database.backend.interfaces.FileCheckSize;
import org.caosdb.server.database.backend.interfaces.FileExists;
import org.caosdb.server.database.backend.interfaces.FileWasModifiedAfter;
import org.caosdb.server.database.backend.interfaces.GetAllNamesImpl;
import org.caosdb.server.database.backend.interfaces.GetDependentEntitiesImpl;
import org.caosdb.server.database.backend.interfaces.GetFileIteratorImpl;
import org.caosdb.server.database.backend.interfaces.GetFileRecordByPathImpl;
import org.caosdb.server.database.backend.interfaces.GetIDByNameImpl;
import org.caosdb.server.database.backend.interfaces.GetInfoImpl;
import org.caosdb.server.database.backend.interfaces.GetUpdateableChecksumsImpl;
import org.caosdb.server.database.backend.interfaces.InsertEntityDatatypeImpl;
import org.caosdb.server.database.backend.interfaces.InsertEntityPropertiesImpl;
import org.caosdb.server.database.backend.interfaces.InsertLinConImpl;
import org.caosdb.server.database.backend.interfaces.InsertParentsImpl;
import org.caosdb.server.database.backend.interfaces.InsertRoleImpl;
import org.caosdb.server.database.backend.interfaces.InsertSparseEntityImpl;
import org.caosdb.server.database.backend.interfaces.InsertTransactionHistoryImpl;
import org.caosdb.server.database.backend.interfaces.IsSubTypeImpl;
import org.caosdb.server.database.backend.interfaces.ListRolesImpl;
import org.caosdb.server.database.backend.interfaces.ListUsersImpl;
import org.caosdb.server.database.backend.interfaces.LogUserVisitImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveAllImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveAllUncheckedFilesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveCurrentMaxIdImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveDatatypesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveEntityACLImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveParentsImpl;
import org.caosdb.server.database.backend.interfaces.RetrievePasswordValidatorImpl;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.backend.interfaces.RetrievePropertiesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveQueryTemplateDefinitionImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveSparseEntityImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveVersionHistoryImpl;
import org.caosdb.server.database.backend.interfaces.SetFileCheckedTimestampImpl;
import org.caosdb.server.database.backend.interfaces.SetFileChecksumImpl;
import org.caosdb.server.database.backend.interfaces.SetPasswordImpl;
import org.caosdb.server.database.backend.interfaces.SetPermissionRulesImpl;
import org.caosdb.server.database.backend.interfaces.SetQueryTemplateDefinitionImpl;
import org.caosdb.server.database.backend.interfaces.SyncStatsImpl;
import org.caosdb.server.database.backend.interfaces.UpdateSparseEntityImpl;
import org.caosdb.server.database.backend.interfaces.UpdateUserImpl;
import org.caosdb.server.database.backend.interfaces.UpdateUserRolesImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.utils.UndoHandler;
import org.caosdb.server.utils.Undoable;

/**
 * Abstract class for backend transactions.
 *
 * <p>This class is the glue between the Transaction layer and the actual implementation of the
 * communication with the back end.
 *
 * <p>This class also acts as a registry which stores which implementation is being used for which
 * backend transaction interface.
 *
 * @see {@link Transaction}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public abstract class BackendTransaction implements Undoable {

  private final UndoHandler undoHandler = new UndoHandler();
  private Access access;
  private TransactionBenchmark benchmark;
  private static HashMap<
          Class<? extends BackendTransactionImpl>, Class<? extends BackendTransactionImpl>>
      impl = new HashMap<>();

  protected abstract void execute();

  /** Like execute(), but with benchmarking measurement. */
  public final void executeTransaction() {
    final long t1 = System.currentTimeMillis();
    execute();
    final long t2 = System.currentTimeMillis();
    this.addMeasurement(this, t2 - t1);
  }

  /**
   * Initialize the adapters to the database backend.
   *
   * <p>Currently this is hard-coded to the MySQL-Backend but the architecture of this class is
   * designed to make it easy in the future to choose other implementations (i.e. other back-ends)
   */
  public static void init() {
    if (impl.isEmpty()) {
      setImpl(GetAllNamesImpl.class, MySQLGetAllNames.class);
      setImpl(DeleteEntityPropertiesImpl.class, MySQLDeleteEntityProperties.class);
      setImpl(DeleteSparseEntityImpl.class, MySQLDeleteSparseEntity.class);
      setImpl(GetDependentEntitiesImpl.class, MySQLGetDependentEntities.class);
      setImpl(GetIDByNameImpl.class, MySQLGetIDByName.class);
      setImpl(GetInfoImpl.class, MySQLGetInfo.class);
      setImpl(InsertEntityPropertiesImpl.class, MySQLInsertEntityProperties.class);
      setImpl(InsertLinConImpl.class, MySQLInsertLinCon.class);
      setImpl(InsertParentsImpl.class, MySQLInsertParents.class);
      setImpl(InsertSparseEntityImpl.class, MySQLInsertSparseEntity.class);
      setImpl(InsertTransactionHistoryImpl.class, MySQLInsertTransactionHistory.class);
      setImpl(IsSubTypeImpl.class, MySQLIsSubType.class);
      setImpl(UpdateSparseEntityImpl.class, MySQLUpdateSparseEntity.class);
      setImpl(RetrieveAllImpl.class, MySQLRetrieveAll.class);
      setImpl(RetrieveDatatypesImpl.class, MySQLRetrieveDatatypes.class);
      setImpl(RetrieveUserImpl.class, MySQLRetrieveUser.class);
      setImpl(RetrieveParentsImpl.class, MySQLRetrieveParents.class);
      setImpl(GetFileRecordByPathImpl.class, MySQLGetFileRecordByPath.class);
      setImpl(RetrievePropertiesImpl.class, MySQLRetrieveProperties.class);
      setImpl(RetrieveSparseEntityImpl.class, MySQLRetrieveSparseEntity.class);
      setImpl(SyncStatsImpl.class, MySQLSyncStats.class);
      setImpl(FileExists.class, UnixFileSystemFileExists.class);
      setImpl(FileWasModifiedAfter.class, UnixFileSystemFileWasModifiedAfter.class);
      setImpl(FileCheckHash.class, UnixFileSystemCheckHash.class);
      setImpl(GetFileIteratorImpl.class, UnixFileSystemGetFileIterator.class);
      setImpl(SetFileCheckedTimestampImpl.class, MySQLSetFileCheckedTimestampImpl.class);
      setImpl(RetrieveAllUncheckedFilesImpl.class, MySQLRetrieveAllUncheckedFiles.class);
      setImpl(UpdateUserImpl.class, MySQLUpdateUser.class);
      setImpl(DeleteUserImpl.class, MySQLDeleteUser.class);
      setImpl(SetPasswordImpl.class, MySQLSetPassword.class);
      setImpl(RetrievePasswordValidatorImpl.class, MySQLRetrievePasswordValidator.class);
      setImpl(DeletePasswordImpl.class, MySQLDeletePassword.class);
      setImpl(GetUpdateableChecksumsImpl.class, MySQLGetUpdateableChecksums.class);
      setImpl(FileCheckSize.class, UnixFileSystemCheckSize.class);
      setImpl(InsertRoleImpl.class, MySQLInsertRole.class);
      setImpl(RetrieveRoleImpl.class, MySQLRetrieveRole.class);
      setImpl(ListRolesImpl.class, MySQLListRoles.class);
      setImpl(DeleteRoleImpl.class, MySQLDeleteRole.class);
      setImpl(SetPermissionRulesImpl.class, MySQLSetPermissionRules.class);
      setImpl(RetrievePermissionRulesImpl.class, MySQLRetrievePermissionRules.class);
      setImpl(UpdateUserRolesImpl.class, MySQLUpdateUserRoles.class);
      setImpl(SetQueryTemplateDefinitionImpl.class, MySQLSetQueryTemplateDefinition.class);
      setImpl(
          RetrieveQueryTemplateDefinitionImpl.class, MySQLRetrieveQueryTemplateDefinition.class);
      setImpl(InsertEntityDatatypeImpl.class, MySQLInsertEntityDatatype.class);
      setImpl(RetrieveVersionHistoryImpl.class, MySQLRetrieveVersionHistory.class);
      setImpl(SetFileChecksumImpl.class, MySQLSetFileChecksum.class);
      setImpl(ListUsersImpl.class, MySQLListUsers.class);
      setImpl(LogUserVisitImpl.class, MySQLLogUserVisit.class);
      setImpl(RetrieveEntityACLImpl.class, MySQLRetrieveEntityACL.class);
      setImpl(RetrieveCurrentMaxIdImpl.class, MySQLRetrieveCurrentMaxId.class);
    }
  }

  /**
   * Execute this BackendTransaction, using the implementation given as an argument.
   *
   * <p>The implementation's benchmark is set to the corresponding sub-benchmark of this object's
   * benchmark.
   *
   * @param t This BackendTransaction's execute() method will be called.
   * @return The BackendTransaction which was passed as an argument.
   */
  protected <K extends BackendTransaction> K execute(final K t) {
    assert t != this;
    this.undoHandler.append(t);
    t.setAccess(this.access);
    if (benchmark != null) {
      t.setTransactionBenchmark(benchmark.getBenchmark(t.getClass()));
    }
    final long t1 = System.currentTimeMillis();
    t.execute();
    final long t2 = System.currentTimeMillis();
    this.addMeasurement(t, t2 - t1);
    return t;
  }

  public static <K extends BackendTransactionImpl, L extends K> void setImpl(
      final Class<K> k, final Class<L> l) {
    impl.put(k, l);
  }

  public void setAccess(final Access access) {
    this.access = access;
  }

  @SuppressWarnings("unchecked")
  protected <T extends BackendTransactionImpl> T getImplementation(final Class<T> clz) {
    init();
    try {
      final Class<?> implclz = impl.get(clz);
      final T ret = (T) implclz.getConstructor(Access.class).newInstance(this.access);
      if (ret instanceof Undoable) {
        this.undoHandler.append((Undoable) ret);
      }
      if (benchmark != null) {
        ret.setTransactionBenchmark(benchmark.getBenchmark(ret.getClass()));
      }
      return ret;
    } catch (final Exception e) {
      throw new TransactionException(e);
    }
  }

  protected UndoHandler getUndoHandler() {
    return this.undoHandler;
  }

  @Override
  public final void undo() {
    this.undoHandler.undo();
  }

  @Override
  public final void cleanUp() {
    this.undoHandler.cleanUp();
  }

  boolean useCache() {
    return this.access.useCache();
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }

  /** Set the benchmark object for this AbstractTransaction. */
  public void setTransactionBenchmark(final TransactionBenchmark b) {
    this.benchmark = b;
  }

  public void addMeasurement(final Object o, final long time) {
    if (this.benchmark != null) {
      this.benchmark.addMeasurement(o, time);
    }
  }
}
