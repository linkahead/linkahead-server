/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.access;

import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.utils.UseCacheResource;

/**
 * Access Interface.
 *
 * <p>The access object is part of a Transactions state and it is the glue between the Transaction
 * and the actual backend implementation of those transactions. It represents different levels of
 * access (RO, RW) and provides all the helper objects shared by all the single calls to the backend
 * (package .../database/backend/implementation/...).
 *
 * <p>It is further used to commit or roll-back the owning transaction.
 *
 * <p>The DatabaseAccessManager issues the Access to a requesting Transaction.
 *
 * @see {@link Transaction}
 * @see {@link DatabaseAccessManager}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public interface Access extends UseCacheResource {

  public abstract void setHelper(String name, DBHelper helper);

  public abstract DBHelper getHelper(String name);

  public abstract void release();

  public abstract void commit() throws Exception;
}
