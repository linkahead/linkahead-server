/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.access;

import java.util.HashMap;
import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.database.misc.RollBackHandler;
import org.caosdb.server.transaction.TransactionInterface;
import org.caosdb.server.utils.UseCacheResourceDelegate;

/**
 * Abstract implementation of an Access.
 *
 * @see {@link Access}
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public abstract class AbstractAccess<T extends TransactionInterface>
    extends UseCacheResourceDelegate implements Access {

  private HashMap<String, DBHelper> helpers = new HashMap<String, DBHelper>();
  private final T transaction;
  private boolean released = false;

  public AbstractAccess(final T transaction) {
    this.transaction = transaction;
    setHelper("RollBack", new RollBackHandler());
  }

  @Override
  public void setHelper(final String name, final DBHelper helper) {
    if (!this.released) {
      helper.setHelped(this.transaction);
      synchronized (this.helpers) {
        this.helpers.put(name, helper);
      }
    }
  }

  @Override
  public DBHelper getHelper(final String name) {
    if (!this.released) {
      synchronized (this.helpers) {
        return this.helpers.get(name);
      }
    }
    throw new IllegalStateException("This Access is released");
  }

  @Override
  public void release() {
    if (this.released == false) {
      this.released = true;
      synchronized (this.helpers) {
        for (final DBHelper h : this.helpers.values()) {
          h.cleanUp();
        }
        this.helpers.clear();
        this.helpers = null;
      }
    }
  }

  @Override
  public void commit() throws Exception {
    synchronized (this.helpers) {
      for (final DBHelper h : this.helpers.values()) {
        h.commit();
      }
    }
  }
}
