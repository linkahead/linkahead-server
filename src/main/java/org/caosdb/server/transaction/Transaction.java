/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.transaction;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.shiro.subject.Subject;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.database.DatabaseAccessManager;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.InsertTransactionHistory;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.EntityIdRegistry;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.jobs.Job;
import org.caosdb.server.jobs.Schedule;
import org.caosdb.server.jobs.ScheduledJob;
import org.caosdb.server.jobs.TransactionStage;
import org.caosdb.server.jobs.core.AccessControl;
import org.caosdb.server.jobs.core.CheckDatatypePresent;
import org.caosdb.server.jobs.core.CheckEntityACLRoles;
import org.caosdb.server.jobs.core.JobFailureSeverity;
import org.caosdb.server.jobs.core.PickUp;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.utils.AbstractObservable;
import org.caosdb.server.utils.Info;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.UseCacheResource;
import org.caosdb.server.utils.UseCacheResourceDelegate;

/**
 * The NoOpCache is used when the caching is diabled (globally or for the current transaction.
 *
 * <p>It results in much simpler code when the callers do not need to check whether the caching is
 * active all the time.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
@SuppressWarnings("rawtypes")
class NoOpCache implements Map {

  @Override
  public int size() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public boolean isEmpty() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean containsKey(Object key) {
    return false;
  }

  @Override
  public boolean containsValue(Object value) {
    return false;
  }

  @Override
  public Object get(Object key) {
    return null;
  }

  @Override
  public Object put(Object key, Object value) {
    return null;
  }

  @Override
  public Object remove(Object key) {
    return null;
  }

  @Override
  public void putAll(Map m) {}

  @Override
  public void clear() {}

  @Override
  public Set keySet() {
    return Collections.EMPTY_SET;
  }

  @Override
  public Collection values() {
    return Collections.EMPTY_LIST;
  }

  @Override
  public Set entrySet() {
    return Collections.EMPTY_SET;
  }
}

/**
 * Abstract base implementation for all EntityTransactions (Transaction involving entities).
 *
 * <p>Handles caching, benchmark timing, the access to the back end, generates and checks entity
 * ids, holds the state of the transaction and triggers the scheduler to run the jobs.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public abstract class Transaction<C extends TransactionContainer> extends AbstractObservable
    implements EntityTransactionInterface, UseCacheResource {

  @Override
  public TransactionBenchmark getTransactionBenchmark() {
    return getContainer().getTransactionBenchmark();
  }

  private static final DatabaseAccessManager monitor = DatabaseAccessManager.getInstance();

  public static final String CLEAN_UP = "TransactionCleanUp";

  protected EntityIdRegistry idRegistry = null;
  private final C container;
  private Access access = null;
  private final Schedule schedule = new Schedule();

  protected Transaction(final C container) {
    this(container, Info.getInstance());
  }

  protected Transaction(final C container, final Observer o) {
    this.container = container;
    if (o != null) {
      acceptObserver(o);
    }
  }

  public static DatabaseAccessManager getAccessManager() {
    return monitor;
  }

  public C getContainer() {
    return this.container;
  }

  /**
   * Implementation note: Not called in this class, but may be used by subclasses.
   *
   * <p>E.g. in {@link Retrieve} and {@link WriteTransaction}.
   */
  protected void makeSchedule() {
    // load flag jobs
    final Job loadContainerFlags =
        Job.getJob("LoadContainerFlagJobs", JobFailureSeverity.ERROR, null, this);
    final ScheduledJob scheduledJob = this.schedule.add(loadContainerFlags);
    this.schedule.runJob(scheduledJob);

    // AccessControl
    this.schedule.add(
        Job.getJob(AccessControl.class.getSimpleName(), JobFailureSeverity.ERROR, null, this));
    this.schedule.add(
        Job.getJob(
            CheckEntityACLRoles.class.getSimpleName(), JobFailureSeverity.ERROR, null, this));

    // load permanent container jobs
    this.schedule.addAll(Job.loadPermanentContainerJobs(this));

    for (final EntityInterface e : getContainer()) {
      if (e.skipJob()) continue;
      final List<Job> loadJobs = loadContainerFlags.loadJobs(e, this);
      this.schedule.addAll(loadJobs);

      // additionally load datatype job
      if (e.hasValue() || e.hasDatatype()) {
        this.schedule.add(new CheckDatatypePresent().init(JobFailureSeverity.ERROR, e, this));
      }

      // load pickup job if necessary
      if (e.hasFileProperties() && e.getFileProperties().isPickupable()) {
        this.schedule.add(new PickUp().init(JobFailureSeverity.ERROR, e, this));
      }
    }
  }

  /**
   * The main transaction execution method.
   *
   * <p>This method calls the following other internal methods and scheduled jobs stored in the
   * {@link getSchedule() internal Schedule object}:
   *
   * <ol>
   *   <li>{@link init} - Make {@link Schedule}, resolve names to ids, aquire read access.
   *   <li>{@link Schedule.runJobs(INIT)} - See {@link TransactionStage#INIT}.
   *   <li>{@link preCheck} - Load/generate {@link EntityACL}s, check if any updates are to be
   *       processed.
   *   <li>{@link Schedule.runJobs(PRE_CHECK)} - See {@link TransactionStage#PRE_CHECK}.
   *   <li>{@link check} - only run the jobs in the CHECK stage, see {@link TransactionStage#CHECK}.
   *   <li>{@link Schedule.runJobs(POST_CHECK)} - See {@link TransactionStage#POST_CHECK}.
   *   <li>{@link postCheck} - currently, nothing happens here (just there for consistency).
   *   <li>{@link preTransaction} - acquire write access (if necessary)
   *   <li>{@link Schedule.runJobs(PRE_TRANSACTION)} - See {@link TransactionStage#PRE_TRANSACTION}.
   *   <li>{@link transaction}: This is typically the main method of a Transaction.
   *   <li>{@link Schedule.runJobs(POST_TRANSACTION)} - See {@link
   *       TransactionStage#POST_TRANSACTION}.
   *   <li>{@link postTransaction} - Add success messages
   *   <li>{@link writeHistory} - write the transaction history logs
   *   <li>{@link commit} - commit the changes
   *   <li>{@link rollBack}: Only in the case of errors - rollback any changes (also file-system
   *       changes).
   *   <li>{@link cleanUp}: Always - cleanup the transaction (e.g. remove temporary files).
   *   <li>{@link notifyObservers(CLEAN_UP)}: Also always - for any jobs that do their own clean-up.
   *
   * @see {@link TransactionStage}.
   */
  @Override
  public final void execute() throws Exception {
    long t1 = System.currentTimeMillis();
    try {
      init();
      this.schedule.runJobs(TransactionStage.INIT);
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".init", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      preCheck();
      this.schedule.runJobs(TransactionStage.PRE_CHECK);
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".pre_check", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      check();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".check", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      this.schedule.runJobs(TransactionStage.POST_CHECK);
      postCheck();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".post_check", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      preTransaction();
      this.schedule.runJobs(TransactionStage.PRE_TRANSACTION);
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".pre_transaction",
              System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      transaction();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".transaction", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      this.schedule.runJobs(TransactionStage.POST_TRANSACTION);
      postTransaction();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".post_transaction",
              System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      writeHistory();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".history", System.currentTimeMillis() - t1);

      t1 = System.currentTimeMillis();
      commit();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".commit", System.currentTimeMillis() - t1);

    } catch (final Exception e) {
      rollBack();
      throw e;
    } finally {
      t1 = System.currentTimeMillis();
      cleanUp();
      getContainer()
          .getTransactionBenchmark()
          .addMeasurement(
              this.getClass().getSimpleName() + ".cleanup", System.currentTimeMillis() - t1);
      notifyObservers(CLEAN_UP);
    }
  }

  /**
   * Return the internal {@link Schedule} object.
   *
   * <p>The Schedule stores jobs which are also triggered by this transaction (see {@link execute()}
   * for details).
   */
  public Schedule getSchedule() {
    return this.schedule;
  }

  public Subject getTransactor() {
    return getContainer().getOwner();
  }

  /** Return true iff this transaction should be logged in the transaction history logs. */
  public abstract boolean logHistory();

  public UTCDateTime getTimestamp() {
    return UTCDateTime.SystemMillisToUTCDateTime(getContainer().getTimestamp());
  }

  // TODO move to post-transaction job
  private void writeHistory() throws TransactionException, Message {
    if (logHistory()) {
      final String realm = ((Principal) getTransactor().getPrincipal()).getRealm();
      final String username = ((Principal) getTransactor().getPrincipal()).getUsername();
      execute(
          new InsertTransactionHistory(getContainer(), realm, username, getTimestamp()),
          getAccess());
    }
  }

  /**
   * @see {@link #execute()}
   */
  protected void rollBack() {
    this.schedule.runJobs(TransactionStage.ROLL_BACK);
  }

  /**
   * @see {@link #execute()}
   */
  protected abstract void init() throws Exception;

  /**
   * @see {@link #execute()}
   */
  protected abstract void preCheck() throws InterruptedException, Exception;

  /**
   * @see {@link #execute()}
   */
  protected final void check() {
    this.schedule.runJobs(TransactionStage.CHECK);
  }

  /**
   * @see {@link #execute()}
   */
  protected abstract void postCheck();

  /**
   * @see {@link #execute()}
   */
  protected abstract void preTransaction() throws InterruptedException;

  /**
   * @see {@link #execute()}
   */
  protected abstract void transaction() throws Exception;

  /**
   * @see {@link #execute()}
   */
  protected abstract void postTransaction() throws Exception;

  /**
   * @see {@link #execute()}
   */
  protected abstract void cleanUp();

  /**
   * @see {@link #execute()}
   */
  protected void commit() throws Exception {}

  public final Access getAccess() {
    return this.access;
  }

  protected void setAccess(final Access a) {
    this.access = a;
    if (access != null) {
      this.access.setUseCacheResourceDelegate(useCacheResourceDelegate);
    }
  }

  @SuppressWarnings("unchecked")
  public <S, T> Map<S, T> getCache(final String name) {
    if (!useCache()) {
      return NO_OP_CACHE;
    }
    if (!this.caches.containsKey(name)) {
      this.caches.put(name, new HashMap<S, T>());
    }
    return this.caches.get(name);
  }

  private static final NoOpCache NO_OP_CACHE = new NoOpCache();

  @SuppressWarnings("rawtypes")
  private final Map<String, Map> caches = new HashMap<>();

  private final UseCacheResource useCacheResourceDelegate = new UseCacheResourceDelegate();

  @Override
  public UseCacheResource getUseCacheResourceDelegate() {
    return this.useCacheResourceDelegate.getUseCacheResourceDelegate();
  }

  @Override
  public void setUseCacheResourceDelegate(UseCacheResource delegate) {
    this.useCacheResourceDelegate.setUseCacheResourceDelegate(delegate);
  }

  @Override
  public boolean matchIdPattern(String id) {
    if (this.idRegistry == null) {
      this.idRegistry = new EntityIdRegistry(this);
    }
    return idRegistry.matchIdPattern(id);
  }
}
