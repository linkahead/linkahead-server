/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.transaction;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.DeleteEntityTransaction;
import org.caosdb.server.database.backend.transaction.InsertEntityTransaction;
import org.caosdb.server.database.backend.transaction.RetrieveFullEntityTransaction;
import org.caosdb.server.database.backend.transaction.UpdateEntityTransaction;
import org.caosdb.server.database.misc.RollBackHandler;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityIdRegistry;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.container.WritableContainer;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.jobs.Job;
import org.caosdb.server.jobs.Schedule;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.permissions.Permission;
import org.caosdb.server.query.Query;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

/**
 * This class is responsible for inserting, updating and deleting entities which are held in the
 * {@link TransactionContainer}.
 *
 * <p>This class initializes and runs the {@link Schedule} of {@link Job}s, calls the {@link
 * BackendTransaction}s for each entity, and handles exceptions, roll-back (if necessary) and clean
 * up afterwards.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class WriteTransaction extends Transaction<WritableContainer>
    implements WriteTransactionInterface {

  private boolean noIdIsError = true;

  public WriteTransaction(final WritableContainer container) {
    super(container);
  }

  /** Set if it is an error, if an Entity has no ID but just a name upon update. */
  public void setNoIdIsError(final boolean noIdIsError) {
    this.noIdIsError = noIdIsError;
  }

  @Override
  protected final void preTransaction() throws InterruptedException {
    // Acquire strong access. No other thread can have access until this strong access is released.
    setAccess(getAccessManager().acquireWriteAccess(this));
  }

  @Override
  protected void commit() throws Exception {
    getAccess().commit();
    Query.clearCache();
  }

  @Override
  protected void rollBack() {
    final RollBackHandler handler = (RollBackHandler) getAccess().getHelper("RollBack");
    handler.rollBack();
    super.rollBack();
  }

  private void update(final TransactionContainer container, final Access access) throws Exception {
    execute(new UpdateEntityTransaction(container), access);
  }

  private void insert(final TransactionContainer container, final Access access) throws Exception {
    execute(new InsertEntityTransaction(container), access);
  }

  private void delete(final TransactionContainer container, final Access access) throws Exception {
    execute(new DeleteEntityTransaction(container), access);
  }

  @Override
  protected final void cleanUp() {
    // release strong access. Other threads can acquire or allocate
    // access again.
    getAccess().release();
    ChecksumUpdater.start();
    // remove unused tmp files
    for (final FileProperties f : getContainer().getFiles().values()) {
      if (f != null) {
        f.cleanUpTmpDir();
      }
    }
  }

  @Override
  public boolean logHistory() {
    return true;
  }

  @Override
  protected void init() throws Exception {
    // collect all ids of the entities which are to be updated.
    final TransactionContainer oldContainer = new TransactionContainer();
    // collect all ids of the entities which are to be deleted.
    final TransactionContainer deleteContainer = new TransactionContainer();
    for (final EntityInterface entity : getContainer()) {
      if (entity instanceof UpdateEntity) {
        // entity has no id -> it cannot be updated.
        if (!entity.hasId()) {
          entity.addError(ServerMessages.ENTITY_HAS_NO_ID);
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          continue;
        }

        // add entity with this id to the updateContainer
        if (entity.getEntityStatus() == EntityStatus.QUALIFIED) {
          final EntityInterface oldEntity = new RetrieveEntity(entity.getId());
          oldContainer.add(oldEntity);
        }
      } else if (entity instanceof DeleteEntity) {
        deleteContainer.add(entity);
      }
    }

    // Reserve write access. Only one thread can do this at a time. But read access can still be
    // acquired by other threads until the reserved write access is actually acquired.
    setAccess(getAccessManager().reserveWriteAccess(this));

    // Retrieve a container which contains all IDs of those entities
    // which are to be updated.
    execute(new RetrieveFullEntityTransaction(oldContainer, getTransactor()), getAccess());

    // Retrieve all entities which are to be deleted.
    execute(new RetrieveFullEntityTransaction(deleteContainer, getTransactor()), getAccess());

    // Check if any updates are to be processed.
    for (final EntityInterface entity : getContainer()) {
      if (entity instanceof UpdateEntity && entity.getEntityStatus() == EntityStatus.QUALIFIED) {
        innerLoop:
        for (final EntityInterface oldEntity : oldContainer) {
          if (oldEntity.getId().equals(entity.getId())) {
            if (oldEntity.getEntityStatus() == EntityStatus.NONEXISTENT) {
              entity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
              entity.setEntityStatus(EntityStatus.UNQUALIFIED);
            } else {
              // dereference files (upload only)
              if (entity.hasFileProperties()
                  && entity.getFileProperties().hasTmpIdentifier()
                  && !entity.getFileProperties().isPickupable()) {

                // get file by tmpIdentifier
                final FileProperties f =
                    getContainer().getFiles().get(entity.getFileProperties().getTmpIdentifier());

                // is it there?
                if (f != null) {
                  entity.getFileProperties().setFile(f.getFile());

                  // has it a thumbnail?
                  if (f.getThumbnail() != null) {
                    entity.getFileProperties().setThumbnail(f.getThumbnail());
                  } else {
                    final FileProperties thumbnail =
                        getContainer()
                            .getFiles()
                            .get(entity.getFileProperties().getTmpIdentifier() + ".thumbnail");
                    if (thumbnail != null) {
                      entity.getFileProperties().setThumbnail(thumbnail.getFile());
                    } else {
                      entity.addWarning(ServerMessages.THUMBNAIL_HAS_NOT_BEEN_UPLOAED);
                    }
                  }
                } else {
                  entity.addError(ServerMessages.FILE_HAS_NOT_BEEN_UPLOAED);
                  entity.setEntityStatus(EntityStatus.UNQUALIFIED);
                }

              } else if (entity.hasFileProperties()
                  && !entity.getFileProperties().hasTmpIdentifier()) {
                // in case no file has been uploaded,
                // the file is expected to stay
                // unchanged. Therefore we let the file
                // object point to the original file in
                // the file system.
                entity
                    .getFileProperties()
                    .setFile(oldEntity.getFileProperties().retrieveFromFileSystem());
              }

              ((UpdateEntity) entity).setOriginal(oldEntity);
            }
            break innerLoop;
          }
        }
      } else if (entity instanceof DeleteEntity) {
        if (entity.getEntityStatus() == EntityStatus.NONEXISTENT) {
          entity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
          continue;
        }

        // check permissions
        try {
          entity.checkPermission(EntityPermission.DELETE);
        } catch (final AuthorizationException exc) {
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          entity.addError(ServerMessages.AUTHORIZATION_ERROR);
          entity.addInfo(exc.getMessage());
        }

        // no standard entities are to be deleted.
        if (entity.hasId() && EntityID.isReserved(entity.getId())) {
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
          entity.addInfo("This entity cannot be deleted");
        }

      } else if (entity instanceof InsertEntity
          && entity.hasFileProperties()
          && entity.getFileProperties().hasTmpIdentifier()
          && !entity.getFileProperties().isPickupable()) {
        // dereference files (file upload only)
        final FileProperties f =
            getContainer().getFiles().get(entity.getFileProperties().getTmpIdentifier());
        if (f != null) {
          entity.getFileProperties().setFile(f.getFile());
          if (f.getThumbnail() != null) {
            entity.getFileProperties().setThumbnail(f.getThumbnail());
          } else {
            final FileProperties thumbnail =
                getContainer()
                    .getFiles()
                    .get(entity.getFileProperties().getTmpIdentifier() + ".thumbnail");
            if (thumbnail != null) {
              entity.getFileProperties().setThumbnail(thumbnail.getFile());
            } else {
              entity.addWarning(ServerMessages.THUMBNAIL_HAS_NOT_BEEN_UPLOAED);
            }
          }
        } else {
          entity.addError(ServerMessages.FILE_HAS_NOT_BEEN_UPLOAED);
          entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        }
      }
    }

    makeSchedule();
  }

  /** Check if the user has all permissions */
  public static void checkPermissions(
      final EntityInterface entity, final Set<Permission> permissions) {
    for (final Permission p : permissions) {
      entity.checkPermission(p);
    }
  }

  @Override
  protected void preCheck() throws InterruptedException, Exception {
    for (final EntityInterface entity : getContainer()) {
      try {
        if (entity.getEntityStatus() == EntityStatus.QUALIFIED) {
          checkPermissions(entity, deriveUpdate(entity, ((UpdateEntity) entity).getOriginal()));
        }
      } catch (final AuthorizationException exc) {
        entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        entity.addError(ServerMessages.AUTHORIZATION_ERROR);
        entity.addInfo(exc.getMessage());
      } catch (final ClassCastException exc) {
        // not an update entity. ignore.
      }

      // set default EntityACL if none present
      if (entity.getEntityACL() == null) {
        entity.setEntityACL(EntityACL.getOwnerACLFor(SecurityUtils.getSubject()));
      }
    }
  }

  @Override
  protected void transaction() throws Exception {
    if (getContainer().getStatus().ordinal() >= EntityStatus.QUALIFIED.ordinal()) {

      // split up the TransactionContainer into three containers, one for each
      // type of writing transaction.
      final TransactionContainer inserts = new TransactionContainer();
      final TransactionContainer updates = new TransactionContainer();
      final TransactionContainer deletes = new TransactionContainer();
      for (final EntityInterface entity : getContainer()) {
        if (entity instanceof InsertEntity) {
          inserts.add(entity);
        } else if (entity instanceof UpdateEntity) {
          updates.add(entity);
        } else if (entity instanceof DeleteEntity) {
          deletes.add(entity);
        }
      }

      // The order is crucial: 1) Update-entities may reference new entities which
      // have to be inserted first. 2) Update-Entities which (originally)
      // reference old entities which are to be deleted, have be updated before
      // the others can be deleted.
      insert(inserts, getAccess());
      update(updates, getAccess());
      delete(deletes, getAccess());
    }
  }

  @Override
  protected void postCheck() {}

  @Override
  protected void postTransaction() throws Exception {
    // set entityStatus to DELETED and add deletion info message for deleted entities.
    if (getContainer().getStatus().ordinal() >= EntityStatus.QUALIFIED.ordinal()) {
      for (final EntityInterface entity : getContainer()) {
        if (entity instanceof DeleteEntity && entity.getEntityStatus() == EntityStatus.VALID) {
          entity.setEntityStatus(EntityStatus.DELETED);
          entity.addInfo(ServerMessages.ENTITY_HAS_BEEN_DELETED_SUCCESSFULLY);
        }
      }
    }
  }

  /**
   * The entity is set to VALID iff there are no updates to be processed. The entity is set to
   * QUALIFIED otherwise.
   *
   * @param newEntity
   * @param oldEntity
   * @throws CaosDBException
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  public HashSet<Permission> deriveUpdate(
      final EntityInterface newEntity, final EntityInterface oldEntity)
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final HashSet<Permission> needPermissions = new HashSet<>();
    boolean updatetable = false;

    // @review Florian Spreckelsen 2022-03-15

    // new acl?
    if (newEntity.hasEntityACL() && !newEntity.getEntityACL().equals(oldEntity.getEntityACL())) {
      oldEntity.checkPermission(EntityPermission.EDIT_ACL);
      if (!newEntity
          .getEntityACL()
          .getPriorityEntityACL()
          .equals(oldEntity.getEntityACL().getPriorityEntityACL())) {
        // priority acl is to be changed?
        oldEntity.checkPermission(EntityPermission.EDIT_PRIORITY_ACL);
      }
      updatetable = true;
    } else if (!newEntity.hasEntityACL()) {
      newEntity.setEntityACL(oldEntity.getEntityACL());
    }

    // new query template definition?
    if (!Objects.equals(
        newEntity.getQueryTemplateDefinition(), oldEntity.getQueryTemplateDefinition())) {
      needPermissions.add(EntityPermission.UPDATE_QUERY_TEMPLATE_DEFINITION);
      updatetable = true;
    }

    // new datatype?
    if ((newEntity.hasDatatype()
            && oldEntity.hasDatatype()
            && !newEntity.getDatatype().equals(oldEntity.getDatatype()))
        || newEntity.hasDatatype() ^ oldEntity.hasDatatype()) {
      needPermissions.add(EntityPermission.UPDATE_DATA_TYPE);
      updatetable = true;
    } else {
      if (oldEntity.hasDatatype()) {
        newEntity.setDatatypeOverride(oldEntity.isDatatypeOverride());
        newEntity.setDatatype(oldEntity.getDatatype());
      }
    }

    // entity role
    if (!(newEntity instanceof Property && oldEntity instanceof Property)
            && newEntity.hasRole()
            && oldEntity.hasRole()
            && !newEntity.getRole().equals(oldEntity.getRole())
        || newEntity.hasRole() ^ oldEntity.hasRole()) {
      needPermissions.add(EntityPermission.UPDATE_ROLE);
      updatetable = true;
    }

    // entity value
    if (newEntity.hasValue() && oldEntity.hasValue()) {
      try {
        newEntity.parseValue();
        oldEntity.parseValue();
      } catch (NullPointerException | Message m) {
        // ignore, parsing is handled elsewhere
      }
      if (!newEntity.getValue().equals(oldEntity.getValue())) {
        needPermissions.add(EntityPermission.UPDATE_VALUE);
        updatetable = true;
      }
    } else if (newEntity.hasValue() ^ oldEntity.hasValue()) {
      needPermissions.add(EntityPermission.UPDATE_VALUE);
      updatetable = true;
    }

    // entity name
    if (newEntity.hasName()
            && oldEntity.hasName()
            && !newEntity.getName().equals(oldEntity.getName())
        || newEntity.hasName() ^ oldEntity.hasName()) {
      needPermissions.add(EntityPermission.UPDATE_NAME);
      updatetable = true;
    } else {
      newEntity.setNameOverride(oldEntity.isNameOverride());
    }

    // entity description
    if (newEntity.hasDescription()
            && oldEntity.hasDescription()
            && !newEntity.getDescription().equals(oldEntity.getDescription())
        || newEntity.hasDescription() ^ oldEntity.hasDescription()) {
      needPermissions.add(EntityPermission.UPDATE_DESCRIPTION);
      updatetable = true;
    } else {
      newEntity.setDescOverride(oldEntity.isDescOverride());
    }

    // file properties
    if (newEntity.hasFileProperties() || oldEntity.hasFileProperties()) {
      if (newEntity.hasFileProperties() && !oldEntity.hasFileProperties()) {
        // add a file
        needPermissions.add(EntityPermission.UPDATE_ADD_FILE);
        updatetable = true;
      } else if (!newEntity.hasFileProperties() && oldEntity.hasFileProperties()) {
        // remove a file
        needPermissions.add(EntityPermission.UPDATE_REMOVE_FILE);
        updatetable = true;
      } else {
        // change file
        final FileProperties newFile = newEntity.getFileProperties();
        final FileProperties oldFile = oldEntity.getFileProperties();

        // file path
        if (newFile.hasPath() && oldFile.hasPath() && !newFile.getPath().equals(oldFile.getPath())
            || newFile.hasPath() ^ oldFile.hasPath()) {
          // this means, the location of the file is to be changed
          needPermissions.add(EntityPermission.UPDATE_MOVE_FILE);
          updatetable = true;
        }

        // change actual file (different byte code!)
        if (!oldFile.retrieveFromFileSystem().equals(newFile.getFile())) {
          // CHANGE A FILE is like REMOVE AND ADD
          needPermissions.add(EntityPermission.UPDATE_REMOVE_FILE);
          needPermissions.add(EntityPermission.UPDATE_ADD_FILE);
          updatetable = true;
        }
      }
    }

    // properties
    for (final Property newProperty : newEntity.getProperties()) {

      // find corresponding oldProperty for this new property and make a diff (existing property,
      // same property index in this entity, equal content?).
      final Property oldProperty = findOldEntity(newProperty, oldEntity.getProperties());
      if (oldProperty != null) {
        // do not check again.
        oldEntity.getProperties().remove(oldProperty);

        if (oldProperty.getPIdx() != newProperty.getPIdx()) {
          // change order of properties
          needPermissions.add(EntityPermission.UPDATE_ADD_PROPERTY);
          needPermissions.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
          updatetable = true;
        }

        deriveUpdate(newProperty, oldProperty);
        if (newProperty.getEntityStatus() == EntityStatus.QUALIFIED) {
          needPermissions.add(EntityPermission.UPDATE_ADD_PROPERTY);
          needPermissions.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
          updatetable = true;
        }

      } else {
        // no corresponding property found -> this property is new.
        needPermissions.add(EntityPermission.UPDATE_ADD_PROPERTY);
        updatetable = true;
      }
    }

    // some old properties left (and not matched with new ones) -> there are
    // properties to be deleted.
    if (!oldEntity.getProperties().isEmpty()) {
      needPermissions.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
      updatetable = true;
    }

    // update parents
    for (final Parent newParent : newEntity.getParents()) {

      // find corresponding oldParent
      final Parent oldProperty = findOldEntity(newParent, oldEntity.getParents());

      if (oldProperty != null) {
        // do not check again.
        oldEntity.getParents().remove(oldProperty);

      } else {
        // no corresponding parent found -> this parent is new.
        needPermissions.add(EntityPermission.UPDATE_ADD_PARENT);
        updatetable = true;
      }
    }

    // some old parents left (and not matched with new ones) -> there are
    // parents to be deleted.
    if (!oldEntity.getParents().isEmpty()) {
      needPermissions.add(EntityPermission.UPDATE_REMOVE_PARENT);
      updatetable = true;
    }

    // nothing to be updated
    if (!updatetable) {
      newEntity.setEntityStatus(EntityStatus.VALID);
      newEntity.addInfo("Nothing to be updated.");
    }

    return needPermissions;
  }

  /**
   * Attempt to find a (sparse) entity among a list of entities.
   *
   * <p>If no match by ID can be found, matching by name is attempted next, but only if noIdIsError
   * is false.
   */
  private <T extends EntityInterface> T findOldEntity(
      final EntityInterface newEntity, final List<T> oldEntities) {
    if (newEntity.hasId()) {
      for (final T oldEntity : oldEntities) {
        if (Objects.equals(oldEntity.getId(), newEntity.getId())) {
          return oldEntity;
        }
      }
    } else if (noIdIsError) {
      newEntity.addError(ServerMessages.ENTITY_HAS_NO_ID);
      newEntity.addInfo("On updates, always specify the id not just the name.");
    } else if (newEntity.hasName()) {
      for (final T oldEntity : oldEntities) {
        if (oldEntity.getName().equals(newEntity.getName())) {
          return oldEntity;
        }
      }
    } else {
      newEntity.addError(ServerMessages.ENTITY_HAS_NO_NAME_OR_ID);
    }
    return null;
  }

  @Override
  public String getSRID() {
    return getContainer().getRequestId();
  }

  @Override
  public String generateId() {
    if (this.idRegistry == null) {
      this.idRegistry = new EntityIdRegistry(this);
    }

    return this.idRegistry.generate();
  }
}
