/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.RetrieveFullEntityTransaction;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.entity.xml.IdAndServerMessagesOnlyStrategy;
import org.caosdb.server.jobs.ScheduledJob;
import org.caosdb.server.jobs.core.JobFailureSeverity;
import org.caosdb.server.jobs.core.RemoveDuplicates;
import org.caosdb.server.jobs.core.ResolveNames;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;

public class Retrieve extends Transaction<RetrieveContainer> {

  public Retrieve(final RetrieveContainer container) {
    super(container);
  }

  @Override
  protected void init() throws Exception {
    // acquire weak access
    setAccess(getAccessManager().acquireReadAccess(this));

    // resolve names
    {
      final ResolveNames r = new ResolveNames();
      r.init(JobFailureSeverity.WARN, null, this);
      final ScheduledJob scheduledJob = getSchedule().add(r);
      getSchedule().runJob(scheduledJob);
    }

    {
      final RemoveDuplicates job = new RemoveDuplicates();
      job.init(JobFailureSeverity.ERROR, null, this);
      final ScheduledJob scheduledJob = getSchedule().add(job);
      getSchedule().runJob(scheduledJob);
    }

    // make schedule for all parsed entities
    makeSchedule();
  }

  @Override
  protected void preCheck() throws InterruptedException {}

  @Override
  protected void postCheck() {}

  @Override
  protected void preTransaction() {}

  @Override
  protected void postTransaction() {
    // @review Florian Spreckelsen 2022-03-22

    // generate Error for missing RETRIEVE:ENTITY Permission.
    for (final EntityInterface e : getContainer()) {
      if (e.hasEntityACL()) {
        try {
          e.checkPermission(EntityPermission.RETRIEVE_ENTITY);
        } catch (final AuthorizationException exc) {
          e.setSerializeFieldStrategy(new IdAndServerMessagesOnlyStrategy());
          e.setEntityStatus(EntityStatus.UNQUALIFIED);
          e.addError(ServerMessages.AUTHORIZATION_ERROR);
          e.addInfo(exc.getMessage());
        }
      }
    }
    // generate Error for non-existent entities.
    for (final EntityInterface wrappedEntity : getContainer()) {
      if (wrappedEntity.getEntityStatus() == EntityStatus.NONEXISTENT) {
        wrappedEntity.addError(ServerMessages.ENTITY_DOES_NOT_EXIST);
      }
    }
  }

  @Override
  protected void cleanUp() {
    // release weak access
    getAccess().release();
  }

  @Override
  protected void transaction() throws Exception {
    // retrieve entities from mysql database
    retrieveFullEntities(getContainer(), getAccess());
  }

  private void retrieveFullEntities(final RetrieveContainer container, final Access access)
      throws Exception {
    execute(new RetrieveFullEntityTransaction(container, getTransactor()), access);
  }

  @Override
  public boolean logHistory() {
    return false;
  }

  public static class Paging {
    public Paging(int startIndex, int endIndex) {
      this.startIndex = startIndex;
      this.endIndex = endIndex;
    }

    public final int startIndex;
    public final int endIndex;
  }

  private Retrieve.Paging paging = null;

  public boolean hasPaging() {
    return this.paging != null;
  }

  public void setPaging(int startIndex, int endIndex) {
    this.paging = new Retrieve.Paging(startIndex, endIndex);
  }

  public Retrieve.Paging getPaging() {
    return paging;
  }
}
