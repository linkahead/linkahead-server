/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.transaction;

import org.caosdb.server.entity.EntityID;

/**
 * This interface is implemented by all transactions of entities (as opposed to transaction which
 * only concern users or other kinds of transactions).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public interface EntityTransactionInterface extends TransactionInterface {

  public boolean matchIdPattern(String id);

  public default boolean matchIdPattern(EntityID id) {
    return id != null && matchIdPattern(id.toString());
  }
}
