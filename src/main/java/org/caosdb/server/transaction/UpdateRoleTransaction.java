/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.backend.transaction.InsertRole;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.database.backend.transaction.SetPermissionRules;
import org.caosdb.server.entity.Message;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.utils.ServerMessages;

public class UpdateRoleTransaction extends AccessControlTransaction {

  private final Role role;
  private Set<PermissionRule> newPermissionRules = null;

  public UpdateRoleTransaction(final Role role) {
    this.role = role;
  }

  private void checkPermissions() throws Message {

    Subject subject = SecurityUtils.getSubject();
    subject.checkPermission(ACMPermissions.PERMISSION_UPDATE_ROLE_DESCRIPTION(this.role.name));

    Role oldRole = execute(new RetrieveRole(this.role.name), getAccess()).getRole();
    if (oldRole == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }

    if (this.role.permission_rules != null) {
      Set<PermissionRule> oldPermissions = Set.copyOf(oldRole.permission_rules);
      Set<PermissionRule> newPermissions = Set.copyOf(role.permission_rules);
      if (!oldPermissions.equals(newPermissions)) {
        if (org.caosdb.server.permissions.Role.ADMINISTRATION.toString().equals(this.role.name)) {
          throw ServerMessages.SPECIAL_ROLE_PERMISSIONS_CANNOT_BE_CHANGED();
        }
        subject.checkPermission(ACMPermissions.PERMISSION_UPDATE_ROLE_PERMISSIONS(this.role.name));
        this.newPermissionRules = newPermissions;
      }
    }
  }

  @Override
  protected void transaction() throws Exception {
    checkPermissions();

    execute(new InsertRole(this.role), getAccess());
    if (this.newPermissionRules != null) {
      execute(new SetPermissionRules(this.role.name, newPermissionRules), getAccess());
    }
    RetrieveRole.removeCached(this.role.name);
  }
}
