/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.backend.transaction.RetrieveUser;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class RetrieveUserTransaction extends AccessControlTransaction {

  private final Principal principal;
  private ProtoUser user;
  private final Subject currentUser;

  public RetrieveUserTransaction(final String realm, final String name) {
    this(realm, name, SecurityUtils.getSubject());
  }

  public RetrieveUserTransaction(String realm, String username, Subject transactor) {
    currentUser = transactor;
    this.principal = new Principal(realm, username);
  }

  @Override
  protected void transaction() throws Exception {
    if (!UserSources.isUserExisting(this.principal)
        || !currentUser.isPermitted(
            ACMPermissions.PERMISSION_RETRIEVE_USER_INFO(
                this.principal.getRealm(), this.principal.getUsername()))) {
      throw ServerMessages.ACCOUNT_DOES_NOT_EXIST;
    }
    this.user = execute(new RetrieveUser(this.principal), getAccess()).getUser();

    if (user != null && user.roles != null) {
      if (!currentUser.isPermitted(
          ACMPermissions.PERMISSION_RETRIEVE_USER_ROLES(user.realm, user.name))) {
        user.roles = null;
      }
    }
  }

  public static Element getUserElement(final ProtoUser user) {
    final Element ret = new Element("User");
    ret.setAttribute("name", user.name);
    if (user.realm != null) {
      ret.setAttribute("realm", user.realm);
    }
    if (user.email != null) {
      ret.setAttribute("email", user.email);
    }
    if (user.entity != null) {
      ret.setAttribute("entity", user.entity);
    }
    if (user.status != null) {
      ret.setAttribute("status", user.status.toString());
    }
    return ret;
  }

  public Element getUserElement() {
    return getUserElement(this.user);
  }

  public Set<String> getRoles() {
    return this.user.roles;
  }

  public ProtoUser getUser() {
    return user;
  }
}
