/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import static org.caosdb.server.query.Query.clearCache;

import org.apache.shiro.authz.AuthorizationException;
import org.caosdb.server.database.backend.transaction.RetrieveFullEntityTransaction;
import org.caosdb.server.database.backend.transaction.UpdateEntityTransaction;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.jobs.Job;
import org.caosdb.server.jobs.core.CheckEntityACLRoles;
import org.caosdb.server.jobs.core.JobFailureSeverity;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.utils.EntityStatus;

public class UpdateACL extends Transaction<TransactionContainer>
    implements WriteTransactionInterface {

  public UpdateACL(TransactionContainer t) {
    super(t);
  }

  @Override
  public boolean logHistory() {
    return false;
  }

  @Override
  protected void init() throws Exception {
    getSchedule()
        .add(
            Job.getJob(
                CheckEntityACLRoles.class.getSimpleName(), JobFailureSeverity.ERROR, null, this));
    // reserve write access. Other thread may read until the write access is
    // actually acquired.
    setAccess(getAccessManager().reserveWriteAccess(this));
  }

  @Override
  protected void preCheck() throws InterruptedException, Exception {
    TransactionContainer oldContainer = new TransactionContainer();

    for (EntityInterface e : getContainer()) {
      oldContainer.add(new UpdateEntity(e.getId(), null));
    }

    RetrieveFullEntityTransaction t =
        new RetrieveFullEntityTransaction(oldContainer, getTransactor());
    execute(t, getAccess());

    // the entities in this container only have an id and an ACL. -> Replace
    // with full entity and move ACL to full entity if permissions are
    // sufficient, otherwise add an error.
    getContainer()
        .replaceAll(
            (e) -> {
              EntityInterface result = oldContainer.getEntityById(e.getId());

              // Check ACL update is permitted (against the old ACL) and set the new ACL afterwards.
              EntityACL oldAcl = result.getEntityACL();
              EntityACL newAcl = e.getEntityACL();
              if (oldAcl != null
                  && oldAcl.isPermitted(getTransactor(), EntityPermission.EDIT_ACL)) {
                if (oldAcl.equals(newAcl)) {
                  // nothing to be done
                  result.setEntityStatus(EntityStatus.IGNORE);
                } else {
                  if (!oldAcl.getPriorityEntityACL().equals(newAcl.getPriorityEntityACL())
                      && !oldAcl.isPermitted(getTransactor(), EntityPermission.EDIT_PRIORITY_ACL)) {
                    throw new AuthorizationException(
                        "You are not permitted to change prioritized permission rules of this entity.");
                  }

                  // we're good to go. set new entity acl
                  result.setEntityACL(newAcl);
                  result.setEntityStatus(EntityStatus.QUALIFIED);
                }
              } else if (oldAcl != null
                  && oldAcl.isPermitted(getTransactor(), EntityPermission.RETRIEVE_ENTITY)) {
                // the user knows that this entity exists
                throw new AuthorizationException(
                    "You are not permitted to change permission rules of this entity.");
              } else {
                // we pretend this entity doesn't exist
                result.addError(org.caosdb.server.utils.ServerMessages.ENTITY_DOES_NOT_EXIST);
              }
              return result;
            });
  }

  @Override
  protected void postCheck() {}

  @Override
  protected void preTransaction() throws InterruptedException {

    setAccess(getAccessManager().acquireWriteAccess(this));
  }

  @Override
  protected void transaction() throws Exception {
    UpdateEntityTransaction t = new UpdateEntityTransaction(getContainer());
    execute(t, getAccess());
  }

  @Override
  protected void postTransaction() throws Exception {}

  @Override
  protected void cleanUp() {
    getAccess().release();
  }

  @Override
  protected void commit() throws Exception {
    getAccess().commit();
    clearCache();
  }

  @Override
  public String getSRID() {
    return getContainer().getRequestId();
  }

  @Override
  public String generateId() {
    throw new UnsupportedOperationException(
        "This is not implemented on purpose. This exception indicates a usage error of this UpdateACL class.");
  }
}
