/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.transaction;

import org.apache.shiro.SecurityUtils;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.database.backend.transaction.DeleteRole;
import org.caosdb.server.database.backend.transaction.RetrieveRole;
import org.caosdb.server.database.backend.transaction.SetPermissionRules;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;

public class DeleteRoleTransaction extends AccessControlTransaction {

  private final String name;

  public DeleteRoleTransaction(final String name) {
    this.name = name;
  }

  @Override
  protected void transaction() throws Exception {
    SecurityUtils.getSubject().checkPermission(ACMPermissions.PERMISSION_DELETE_ROLE(this.name));

    if (this.name == "administration" || this.name == "anonymous") {
      throw ServerMessages.SPECIAL_ROLE_CANNOT_BE_DELETED;
    }
    if (execute(new RetrieveRole(this.name), getAccess()).getRole() == null) {
      throw ServerMessages.ROLE_DOES_NOT_EXIST;
    }
    execute(new SetPermissionRules(this.name, null), getAccess());
    try {
      execute(new DeleteRole(this.name), getAccess());
    } catch (TransactionException e) {
      if (e.getCause() == ServerMessages.ROLE_CANNOT_BE_DELETED) {
        throw (Message) e.getCause();
      }
    }
  }
}
