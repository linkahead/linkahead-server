package org.caosdb.server.transaction;

import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.access.Access;

public interface WriteTransactionInterface extends TransactionInterface {

  public Access getAccess();

  public Subject getTransactor();

  public String getSRID();

  public String generateId();
}
