/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.transaction;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.database.DatabaseAccessManager;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.transaction.GetUpdateableChecksums;
import org.caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import org.caosdb.server.database.backend.transaction.SetFileChecksum;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.container.WritableContainer;
import org.caosdb.server.utils.FileUtils;

/**
 * Retrieves all file without a checksum, calculates one and stores it to the database (method
 * 'run'). This is meant for asynchronous checksum calculation.
 *
 * @author tf
 */
public class ChecksumUpdater extends WriteTransaction
    implements Runnable, WriteTransactionInterface {

  private Boolean running = false;

  private static final ChecksumUpdater instance = new ChecksumUpdater();

  private ChecksumUpdater() {
    super(new WritableContainer());
  }

  /** Retrieves all file without a checksum, calculates one and stores it to the database */
  @Override
  public void run() {
    try {
      while (true) {
        final EntityInterface fileEntity = getNextUpdateableFileEntity();
        if (fileEntity == null) {
          // there is no next fileEntity
          return;
        }

        if (!isStillToBeUpdated(fileEntity)) {
          continue;
        }

        // get lastModified...
        final long lastModified =
            fileEntity.getFileProperties().retrieveFromFileSystem().lastModified();
        final String checksum = calcChecksum(fileEntity);
        // ... therefore check again:
        Access strongAccess = null;

        try {
          strongAccess = DatabaseAccessManager.getInstance().reserveWriteAccess(this);
          if (wasChangedInTheMeantime(fileEntity, strongAccess, lastModified)) {
            continue;
          }

          fileEntity.getFileProperties().setChecksum(checksum);

          DatabaseAccessManager.getInstance().acquireWriteAccess(this);

          // update
          execute(new SetFileChecksum(fileEntity), strongAccess);
          strongAccess.commit();

        } catch (final InterruptedException e) {
          e.printStackTrace();
        } catch (final TransactionException e) {
          e.printStackTrace();
        } finally {
          if (strongAccess != null) {
            strongAccess.release();
          }
        }
      }
    } catch (final Throwable t) {
      t.printStackTrace();
    }
  }

  private boolean wasChangedInTheMeantime(
      final EntityInterface fileEntity, final Access access, final long lastModified)
      throws NoSuchAlgorithmException, IOException, CaosDBException, Message {
    final EntityInterface checkEntity =
        execute(new RetrieveSparseEntity(fileEntity), access).getEntity();
    final FileProperties thatFP = checkEntity.getFileProperties();
    final FileProperties thisFP = fileEntity.getFileProperties();

    return !(isStillToBeUpdated(checkEntity)
        && thatFP.getPath().equals(thisFP.getPath())
        && thatFP.retrieveFromFileSystem().lastModified() == lastModified);
  }

  private String calcChecksum(final EntityInterface fileEntity) {
    try {
      return FileUtils.getChecksum(fileEntity.getFileProperties().retrieveFromFileSystem());
    } catch (final NoSuchAlgorithmException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    } catch (final CaosDBException e) {
      e.printStackTrace();
    }
    return null;
  }

  private EntityInterface getNextUpdateableFileEntity() throws InterruptedException {
    final Access weakAccess = DatabaseAccessManager.getInstance().acquireReadAccess(this);
    try {
      synchronized (instance.running) {

        // test for updatable checksums
        final EntityID id = execute(new GetUpdateableChecksums(), weakAccess).getID();
        if (id == null) {
          // nothing to be updated...
          instance.running = false;
          return null;
        }
        return execute(new RetrieveSparseEntity(id, null), weakAccess).getEntity();
      }
    } catch (final Exception e) {
      e.printStackTrace();
      instance.running = false;
      return null;
    } finally {
      weakAccess.release();
    }
  }

  private static boolean isStillToBeUpdated(final EntityInterface fileEntity) {
    return fileEntity.hasFileProperties() && !fileEntity.getFileProperties().hasChecksum();
  }

  @Override
  public boolean logHistory() {
    return false;
  }

  @Override
  protected void init() throws Exception {}

  @Override
  protected void preCheck() throws InterruptedException, Exception {}

  @Override
  protected void postCheck() {}

  @Override
  protected void transaction() throws Exception {}

  @Override
  protected void postTransaction() throws Exception {}

  public static void start() {
    synchronized (instance.running) {
      if (instance.running) {
        return;
      }
      instance.running = true;

      // run
      new Thread(instance).start();
    }
  }
}
