/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import java.util.HashMap;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.container.Container;
import org.reflections.Reflections;

public abstract class AbstractDatatype {

  public enum Table {
    double_data,
    reference_data,
    text_data,
    integer_data,
    datetime_data,
    enum_data,
    date_data,
    null_data,
    name_data
  };

  protected AbstractDatatype() {}

  private static HashMap<String, AbstractDatatype> instances =
      new HashMap<String, AbstractDatatype>();

  protected String name = null;
  protected EntityID id = null;
  protected String desc = null;

  public abstract Value parseValue(Object value) throws Message;

  public static AbstractDatatype datatypeFactory(final String datatype) {
    AbstractDatatype ret = instances.get(datatype.toUpperCase());
    if (ret == null) {
      ret = AbstractCollectionDatatype.collectionDatatypeFactory(datatype);
    }
    if (ret == null) {
      return new ReferenceDatatype2(datatype);
    }
    return ret;
  }

  public static AbstractDatatype datatypeFactory(final EntityID id, String name) {
    AbstractDatatype dt;
    if (id.hasId()) {
      dt = datatypeFactory(id);

      if (name != null) {
        dt.setName(name);
      }
    } else {
      dt = AbstractDatatype.datatypeFactory(name);
    }
    return dt;
  }

  protected void setName(String name) {
    this.name = name;
  }

  public static AbstractDatatype datatypeFactory(final EntityID datatype) {
    for (final AbstractDatatype abstractDatatype : instances.values()) {
      if (abstractDatatype.getId().equals(datatype)) {
        return abstractDatatype;
      }
    }
    return new ReferenceDatatype2(datatype);
  }

  public static void initializeDatatypes(final Container<? extends EntityInterface> dts)
      throws Exception {
    loadClasses();
    for (final EntityInterface e : dts) {
      final AbstractDatatype dt = instances.get(e.getName().toUpperCase());
      if (dt != null) {
        dt.id = e.getId();
        dt.desc = e.getDescription();
      }
    }
  }

  private static void loadClasses() throws Exception {
    final Reflections datatypePackage = new Reflections("org.caosdb.server.datatype");
    for (final Class<? extends AbstractDatatype> c :
        datatypePackage.getSubTypesOf(AbstractDatatype.class)) {
      if (c.isAnnotationPresent(DatatypeDefinition.class)) {
        final AbstractDatatype dt = c.getDeclaredConstructor().newInstance();
        final String name = c.getAnnotation(DatatypeDefinition.class).name().toUpperCase();
        dt.name = name;
        instances.put(name, dt);
      }
    }
  }

  public EntityID getId() {
    return this.id;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof AbstractDatatype) {
      final AbstractDatatype that = (AbstractDatatype) obj;
      if (getId() != null && that.getId() != null) {
        return that.getId().equals(getId());
      }
      return toString().equals(that.toString());
    }
    return false;
  }

  public String getName() {
    return this.name;
  }

  @Override
  public String toString() {
    return "DT." + (getName() != null ? getName() : getId().toString());
  }
}
