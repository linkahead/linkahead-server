/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.caosdb.server.entity.EntityID;

public abstract class AbstractCollectionDatatype extends AbstractDatatype {

  public static final String LEFT_DELIMITER = "<";
  public static final String RIGHT_DELIMITER = ">";

  private static HashMap<String, CollectionDatatypeFactory> instances =
      new HashMap<String, CollectionDatatypeFactory>();
  static Pattern p =
      Pattern.compile(
          "^([^"
              + LEFT_DELIMITER
              + "]+)"
              + LEFT_DELIMITER
              + "([^"
              + RIGHT_DELIMITER
              + "]+)"
              + RIGHT_DELIMITER
              + "$");

  public static AbstractCollectionDatatype collectionDatatypeFactory(final String datatype) {
    final Matcher matcher = p.matcher(datatype);
    if (matcher.matches()) {
      final String col = matcher.group(1).toUpperCase();
      final String type = matcher.group(2);
      return collectionDatatypeFactory(col, null, type);
    }
    return null;
  }

  static {
    addFactory(new ListDatatypeFactory());
  }

  public abstract String getCollectionName();

  private static void addFactory(final CollectionDatatypeFactory f) {
    instances.put(f.createDataType(null).getCollectionName(), f);
  }

  public abstract AbstractDatatype getDatatype();

  @Override
  public String getName() {
    return getCollectionName() + LEFT_DELIMITER + getDatatype().getName() + RIGHT_DELIMITER;
  }

  public static AbstractCollectionDatatype collectionDatatypeFactory(
      final String col, final String id, String name) {
    return instances
        .get(col)
        .createDataType(AbstractDatatype.datatypeFactory(new EntityID(id), name));
  }
}
