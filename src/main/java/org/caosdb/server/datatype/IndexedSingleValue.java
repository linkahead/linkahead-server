/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import java.util.Objects;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.jdom2.Element;

public class IndexedSingleValue implements SingleValue, Comparable<IndexedSingleValue> {

  private final SingleValue wrapped;
  private int index;

  public IndexedSingleValue(final SingleValue v) {
    this(0, v);
  }

  public IndexedSingleValue(final int i, final SingleValue v) {
    this.index = i;
    this.wrapped = v;
  }

  @Override
  public void addToElement(final Element e) {
    if (this.wrapped != null) {
      this.wrapped.addToElement(e);
    }
  }

  @Override
  public Table getTable() {
    if (this.wrapped == null) {
      return Table.null_data;
    }
    return this.wrapped.getTable();
  }

  @Override
  public String toDatabaseString() {
    if (this.wrapped == null) {
      return null;
    }
    return this.wrapped.toDatabaseString();
  }

  public void setIndex(final int i) {
    this.index = i;
  }

  public int getIndex() {
    return this.index;
  }

  @Override
  public int compareTo(final IndexedSingleValue o) {
    return this.index - o.index;
  }

  public SingleValue getWrapped() {
    return this.wrapped;
  }

  @Override
  public boolean equals(Value val) {
    if (val instanceof IndexedSingleValue) {
      IndexedSingleValue that = (IndexedSingleValue) val;
      return Objects.equals(that.wrapped, this.wrapped);
    }
    return false;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Value) {
      return this.equals((Value) obj);
    }
    return false;
  }
}
