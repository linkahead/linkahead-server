/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import org.caosdb.datetime.Date;
import org.caosdb.datetime.DateTimeFactory2;
import org.caosdb.datetime.DateTimeInterface;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;

@DatatypeDefinition(name = "DateTime")
public class DateTimeDatatype extends AbstractDatatype {

  @Override
  public DateTimeInterface parseValue(final Object value) throws Message {
    try {
      if (value instanceof UTCDateTime || value instanceof Date) {
        return (DateTimeInterface) value;
      }

      DateTimeInterface result;
      if (value instanceof GenericValue) {
        result = DateTimeFactory2.valueOf(((GenericValue) value).toDatabaseString());
      } else {
        result = DateTimeFactory2.valueOf(value.toString());
      }

      // Test if this is storable
      result.toDatabaseString();
      return result;
    } catch (final UnsupportedOperationException | IllegalArgumentException e) {
      throw ServerMessages.CANNOT_PARSE_DATETIME_VALUE;
    }
  }
}
