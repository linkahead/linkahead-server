/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.datatype;

import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;

public class ReferenceDatatype2 extends ReferenceDatatype {

  private final ReferenceValue refid;

  public ReferenceDatatype2(final EntityID entity) {
    refid = ReferenceValue.parseReference(entity);
  }

  public ReferenceDatatype2(final Object entity) {
    try {
      this.refid = ReferenceValue.parseReference(entity);
    } catch (final Message e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public EntityID getId() {
    return this.refid.getId();
  }

  public void setId(final EntityID id) {
    this.refid.setId(id);
  }

  public void setEntity(final EntityInterface datatypeEntity) {
    this.refid.setEntity(datatypeEntity, false);
  }

  @Override
  public String getName() {
    return this.refid.getName();
  }

  @Override
  protected void setName(String name) {
    this.refid.setName(name);
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof ReferenceDatatype2) {
      final ReferenceDatatype2 that = (ReferenceDatatype2) obj;
      if (getId() != null && that.getId() != null) {
        return getId().equals(that.getId());
      } else if (getName() != null && that.getName() != null) {
        return getName().equals(that.getName());
      }
    }
    return false;
  }
}
