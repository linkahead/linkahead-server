/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @review Daniel Hornung 2022-03-04
 */
package org.caosdb.server.datatype;

import java.util.Objects;
import org.caosdb.server.datatype.AbstractDatatype.Table;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

/**
 * A ReferenceValue represents the value of a reference property to another entity.
 *
 * <p>Differently from other properties, they may be versioned, i.e. they may reference to a
 * specific version of an entity.
 *
 * <p>Ways to specify a reference value: {id}@{version} using either the version id (a hex'ed sha256
 * string) or "HEAD", "HEAD~1", "HEAD~2" and so on. Note: "HEAD" always means the "HEAD"
 * <i>after</i> the transaction. So, if you are changing the referenced entity in the same
 * transaction and you want to reference the entity in the version before the change you need to use
 * "HEAD~1" because that is the old version of the future.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class ReferenceValue implements SingleValue {
  private EntityInterface entity = null;
  private String name = null;
  private EntityID id = null;
  private String version = null;
  private boolean versioned = false;

  public static ReferenceValue parseReference(final EntityID reference) {
    return new ReferenceValue(reference);
  }

  public static ReferenceValue parseReference(final Object reference) throws Message {
    if (reference == null) {
      return null;
    }
    if (reference instanceof EntityInterface) {
      return new ReferenceValue(
          (EntityInterface) reference, ((EntityInterface) reference).hasVersion());
    } else if (reference instanceof ReferenceValue) {
      return (ReferenceValue) reference;
    } else if (reference instanceof GenericValue) {
      final String str = ((GenericValue) reference).toDatabaseString();
      return parseFromString(str);
    } else if (reference instanceof CollectionValue) {
      throw ServerMessages.DATA_TYPE_DOES_NOT_ACCEPT_COLLECTION_VALUES;
    } else {
      return new ReferenceValue(reference.toString());
    }
  }

  /**
   * Split a reference string into an entity part and a version part, if there is a version part.
   *
   * <p>If parsing the entity ID part to an integer fails, a NumberFormatException may be thrown.
   */
  public static ReferenceValue parseIdVersion(final String str) {
    final String[] split = str.split("@", 2);
    if (split.length == 2) {
      return new ReferenceValue(split[0], split[1]);
    } else {
      return new ReferenceValue(str);
    }
  }

  /**
   * Create a ReferenceValue from a string.
   *
   * <p>If the string looks like a valid "entityID@version" string, the result will have the
   * corresponding entity and version parts.
   */
  public static ReferenceValue parseFromString(final String str) {
    try {
      return parseIdVersion(str);
    } catch (final NumberFormatException e) {
      return new ReferenceValue(str);
    }
  }

  /**
   * Produce a nice but short string:
   *
   * <p>Case 1 "versioned" (reference to an entity without specifying that entity's version):
   * Produces a string like "1234" or "Experiment". Note that referencing via name is never
   * versioned.
   *
   * <p>Case 2 "unversioned" (reference to an entity with a specified version): Produces a string
   * like "1234@ab987f".
   */
  @Override
  public String toString() {
    if (this.entity != null && versioned) { // Was specified as "versioned", with resolved entity
      return this.entity.getIdVersion();
    } else if (this.entity != null) { // resolved, but unversioned
      return this.entity.getId().toString();
    } else if (this.id == null
        && this.name != null) { // Only name is available, no id (and thus no resolved entity)
      return this.name;
    }
    // Specification via id is the only remaining possibility
    return getIdVersion(); // if version is null, returns ID only
  }

  public String getIdVersion() {
    if (this.version != null) {
      return new StringBuilder().append(this.id).append("@").append(this.version).toString();
    }
    return this.id.toString();
  }

  public ReferenceValue(final EntityInterface entity, final boolean versioned) {
    this.versioned = versioned;
    this.entity = entity;
  }

  public ReferenceValue(final EntityID id) {
    this(id, null);
  }

  public ReferenceValue(final EntityID id, final String version) {
    this.id = id;
    this.version = version;
    this.versioned = version != null;
  }

  /** If the reference is given by name, versioning is not possible (at the moment). */
  public ReferenceValue(final String name) {
    this.name = name;
    this.id = new EntityID(name);
  }

  public ReferenceValue(String name, String version) {
    this.name = name;
    this.id = new EntityID(name);
    this.version = version;
  }

  public final EntityInterface getEntity() {
    return this.entity;
  }

  public final void setEntity(final EntityInterface entity, final boolean versioned) {
    this.versioned = versioned;
    this.entity = entity;
  }

  public final String getName() {
    if (this.entity != null && this.entity.hasName()) {
      return this.entity.getName();
    }
    return this.name;
  }

  public final EntityID getId() {
    if (this.entity != null && this.entity.hasId()) {
      return this.entity.getId();
    }
    return this.id;
  }

  public final String getVersion() {
    if (this.entity != null && versioned && this.entity.hasVersion()) {
      return this.entity.getVersion().getId();
    }
    return this.version;
  }

  public final void setId(final EntityID id) {
    this.id = id;
  }

  @Override
  public void addToElement(final Element e) {
    e.addContent(toString());
  }

  @Override
  public Table getTable() {
    return Table.reference_data;
  }

  @Override
  public String toDatabaseString() {
    return toString();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof Value) {
      return equals((Value) obj);
    }
    return false;
  }

  /**
   * Test if this is equal to the other object.
   *
   * <p>Two references are equal, if 1) they both have IDs and their content is equal or 2) at least
   * one does not have an ID, but their names are equal. Otherwise they are considered unequal.
   *
   * @param val The other object.
   */
  @Override
  public boolean equals(Value val) {
    if (val instanceof ReferenceValue) {
      final ReferenceValue that = (ReferenceValue) val;
      if (that.getId() != null && getId() != null) {
        return that.getId().equals(getId())
            && Objects.deepEquals(that.getVersion(), this.getVersion());
      } else if (that.getName() != null && getName() != null) {
        return Objects.equals(that.getName(), this.getName());
      }
    }
    return false;
  }

  public void setName(String name) {
    this.name = name;
  }
}
