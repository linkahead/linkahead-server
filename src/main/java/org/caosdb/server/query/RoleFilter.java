/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.caosdb.server.query.Query.QueryException;
import org.caosdb.server.query.Query.Role;
import org.jdom2.Element;

public class RoleFilter implements EntityFilterInterface {

  private final Role role;
  private final String operator;
  private boolean versioned;

  /**
   * Guarantees that all entities in the result set do have ("=") or do not have ("!=") the role in
   * question.
   *
   * @param role The role in question.
   * @param operator Either "!=" or "=".
   * @throws NullPointerException If role or operator is null.
   * @throws IllegalArgumentException If operator is not "=" or "!=".
   */
  public RoleFilter(final Role role, final String operator, final boolean versioned) {
    this.versioned = versioned;
    if (role == null) {
      throw new NullPointerException("The role must not be null.");
    }
    if (operator == null) {
      throw new NullPointerException("The operator must not be null.");
    }
    if (operator.equals("!=") || operator.equals("=")) {
      this.role = role;
      this.operator = operator;
    } else {
      throw new IllegalArgumentException("The parameter 'operator' is to be either '=' or '!='.");
    }
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    final long t1 = System.currentTimeMillis();
    try {
      if (query.getTargetSet() == null) {
        doApplyRoleFilter(query.getConnection(), getOperator(), getRole(), query.getSourceSet());
      } else if (query.getSourceSet() == null) {
        doApplyRoleFilter(query.getConnection(), getOperator(), getRole(), query.getTargetSet());
      } else {
        doApplyRoleFilter(
            query.getConnection(),
            getOperator(),
            getRole(),
            query.getSourceSet(),
            query.getTargetSet(),
            this.versioned);
      }
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  private static void doApplyRoleFilter(
      final Connection connection,
      final String operator,
      final String role,
      final String sourceSet,
      final String targetSet,
      final boolean versioned)
      throws SQLException {
    if (!sourceSet.equals("entities")) {
      throw new UnsupportedOperationException("SourceSet is supposed to be the `entities` table.");
    }
    final String sql =
        ("INSERT IGNORE INTO `"
            + targetSet
            + (versioned
                ? "` (id, _iversion) SELECT e.id, _get_head_iversion(e.id) FROM `entities` AS e WHERE e.role "
                    + operator
                    + " ? UNION SELECT a.id, a._iversion FROM `archive_entities` AS a WHERE a.role"
                    + operator
                    + "?"
                : "` (id) SELECT e.id FROM `entities` AS e WHERE e.role" + operator + "?"));
    final PreparedStatement filterRoleStmt = connection.prepareCall(sql);
    int params = (versioned ? 2 : 1);
    while (params > 0) {
      filterRoleStmt.setString(params, role);
      params--;
    }
    filterRoleStmt.execute();
  }

  private static void doApplyRoleFilter(
      final Connection connection, final String operator, final String role, final String sourceSet)
      throws SQLException {
    final PreparedStatement filterRoleStmt =
        connection.prepareCall(
            "DELETE FROM `"
                + sourceSet
                + "` WHERE EXISTS (SELECT 1 FROM entities AS e WHERE e.id=`"
                + sourceSet
                + "`.id AND NOT e.role"
                + operator
                + "?);");
    filterRoleStmt.setString(1, role);
    filterRoleStmt.execute();
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("RoleFilter");
    if (getOperator() != null) {
      ret.setAttribute("operator", getOperator());
    }
    if (getRole() != null) {
      ret.setAttribute("role", getRole());
    }
    return ret;
  }

  private String getRole() {
    return this.role.toString();
  }

  private String getOperator() {
    return this.operator;
  }

  @Override
  public String getCacheKey() {
    // unused
    return null;
  }
}
