/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.query;

import static java.sql.Types.DOUBLE;
import static java.sql.Types.INTEGER;
import static java.sql.Types.VARCHAR;
import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import de.timmfitschen.easyunits.parser.ParserException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.caosdb.datetime.DateTimeFactory2;
import org.caosdb.datetime.DateTimeInterface;
import org.caosdb.datetime.Interval;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.query.Query.QueryException;
import org.caosdb.unit.CaosDBSystemOfUnits;
import org.caosdb.unit.Unit;
import org.jdom2.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Applies a POV (Property-operator-value) filter to the query's current result set.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class POV implements EntityFilterInterface {
  public static final Pattern NUMBER_PATTERN =
      Pattern.compile(
          "^((?:[+-]\\s*)?[0-9]+(?:\\.[0-9]+)?(?:\\s*[eE]\\s*[+-]?\\s*[0-9]+)?)\\s*([^-]*)$");
  private SubProperty subp = null;
  public static int retry = 10;
  private int retry_count = 0;
  private final Query.Pattern property;
  private Integer pid;
  private final String operator;
  private final String value;
  private Integer vInt;
  private Double vDouble;
  private DateTimeInterface vDatetime;
  private final String aggregate;
  private String targetSet = null;
  private String unitStr = null;
  private Unit unit = null;
  private Long stdUnitSig = null;
  private Double vDoubleConvertedToStdUnit = null;
  private Connection connection;
  private String propertiesTable = null;
  private String refIdsTable = null;
  private final HashMap<String, String> statistics = new HashMap<>();
  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final Stack<String> prefix = new Stack<>();

  private Unit getUnit(final String s) throws ParserException {
    return CaosDBSystemOfUnits.getUnit(s);
  }

  /**
   * For clauses like WITH [$aggregate] $property [ $operator $value ]
   *
   * @param property
   * @param operator
   * @param value
   * @param aggregate
   */
  public POV(
      final Query.Pattern property,
      final String operator,
      final String value,
      final String aggregate) {
    prefix.add("POV");
    if (property != null && property.type != Query.Pattern.TYPE_NORMAL) {
      throw new UnsupportedOperationException(
          "Regular Expression and Like Patterns are not implemented for properties yet.");
    }
    this.aggregate = aggregate;
    this.property = property;

    // try and parse the p-part as an integer (i.e. an ID)
    try {
      this.pid = Integer.parseInt(property.toString());
    } catch (final NullPointerException e) {
      this.pid = null;
    } catch (final NumberFormatException e) {
      this.pid = null;
    }
    this.operator = operator;

    this.value = value;

    // parse value to int/double/datetime
    if (this.value != null) {

      // try and parse as integer
      try {
        final Matcher m = NUMBER_PATTERN.matcher(this.value);
        if (!m.matches()) {
          throw new NumberFormatException();
        }
        final String vIntStr = m.group(1);
        this.vInt = Integer.parseInt(vIntStr.replaceAll("\\s", ""));
        if (vIntStr.matches(".*\\s.*")) {
          // empty space in scientific notation is a common typo
          throw new Query.ParsingException(
              "You typed \""
                  + vIntStr
                  + "\". Empty spaces are not allowed in numbers. Did you mean \""
                  + vIntStr.replaceAll("\\s", "")
                  + "\"?");
        }
        this.unitStr = m.group(2);
      } catch (final NumberFormatException e) {
        this.vInt = null;
      }

      // Try and parse as double, if integer parsing was unsuccessful.
      if (this.vInt == null) {
        try {
          // Doubles are allowed without dots, for example when the integer overflows.
          final Matcher m = NUMBER_PATTERN.matcher(this.value);
          if (!m.matches()) {
            throw new NumberFormatException();
          }
          final String vDoubleStr = m.group(1);

          this.vDouble = Double.parseDouble(vDoubleStr.replaceAll("\\s", ""));
          if (vDoubleStr.matches(".*\\s.*")) {
            // empty space in scientific notation is a common typo
            throw new Query.ParsingException(
                "You typed \""
                    + vDoubleStr
                    + "\". Empty spaces are not allowed in numbers. Did you mean \""
                    + vDoubleStr.replaceAll("\\s", "")
                    + "\"?");
          }
          this.unitStr = m.group(2);
        } catch (final NumberFormatException e) {
          this.vDouble = null;
        }
      }

      if ((this.vDouble != null || this.vInt != null)
          && this.unitStr != null
          && this.unitStr.length() > 0) {
        try {
          this.unit = getUnit(this.unitStr);
        } catch (final ParserException e) {
          e.printStackTrace();
          throw new UnsupportedOperationException("Could not parse the unit:");
        }

        this.stdUnitSig = this.unit.normalize().getSignature();
      }
      // try and parse as datetime
      try {
        this.vDatetime = DateTimeFactory2.valueOf(value);
      } catch (final ClassCastException e) {
        this.vDatetime = null;
      } catch (final IllegalArgumentException e) {
        if (this.operator.contains("(")) {
          throw new Query.ParsingException("the value is expected to be a date time");
        }
        this.vDatetime = null;
      }
    } else {
      this.vDatetime = null;
      this.vDouble = null;
      this.vInt = null;
    }
    return;
  }

  public void setSubProperty(final SubProperty subp) {
    this.subp = subp;
  }

  public boolean hasSubProperty() {
    return this.subp != null;
  }

  public SubProperty getSubProperty() {
    return this.subp;
  }

  public String getProperty() {
    if (this.property != null) {
      return this.property.toString();
    }
    return null;
  }

  public String getOperator() {
    return this.operator;
  }

  public String getValue() {
    return this.value;
  }

  @Override
  public String toString() {
    return "POV(" + getProperty() + "," + getOperator() + "," + getValue() + ")";
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    if (query.isVersioned() && hasSubProperty()) {
      throw new UnsupportedOperationException(
          "Versioned queries are not supported for subqueries yet. Please file a feature request.");
    }
    final long t1 = System.currentTimeMillis();
    // Add type-converted substitutes for ints/doubles.
    final Integer vIntSubst =
        (this.vDouble != null && this.vDouble % 1 == 0) ? (int) Math.rint(this.vDouble) : null;
    final Double vDoubleSubst = (this.vInt != null) ? (double) this.vInt : null;

    try {
      this.connection = query.getConnection();
      this.targetSet = query.getTargetSet();

      prefix.add("#initPOV");
      initPOV(query);
      prefix.pop();

      // applyPOV(sourceSet, targetSet, propertiesTable, refIdsTable, o,
      // vText, vInt,
      // vDouble,
      // vDatetime, vDateTimeDotNotation, agg, pname, versioned)
      final CallableStatement callPOV =
          this.connection.prepareCall("call applyPOV(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      callPOV.setString(1, query.getSourceSet()); // sourceSet
      this.statistics.put("sourceSet", query.getSourceSet());
      this.statistics.put(
          "sourceSetCountBefore", Utils.countTable(query.getConnection(), query.getSourceSet()));
      if (this.targetSet != null) { // targetSet
        callPOV.setString(2, this.targetSet);
        this.statistics.put("targetSet", this.targetSet);
        this.statistics.put(
            "targetSetCountBefore", Utils.countTable(query.getConnection(), this.targetSet));
      } else {
        callPOV.setNull(2, VARCHAR);
      }
      if (this.propertiesTable != null) { // propertiesTable
        callPOV.setString(3, this.propertiesTable);
        this.statistics.put("propertiesTable", this.propertiesTable);
        this.statistics.put(
            "propertiesTableCountBefore",
            Utils.countTable(query.getConnection(), this.propertiesTable));
      } else {
        callPOV.setNull(3, VARCHAR);
      }
      if (this.refIdsTable != null) { // refIdsTable
        callPOV.setString(4, this.refIdsTable);
        this.statistics.put("refIdsTable", this.refIdsTable);
        this.statistics.put(
            "refIdsTableCountBefore", Utils.countTable(query.getConnection(), this.refIdsTable));
      } else {
        callPOV.setNull(4, VARCHAR);
      }
      if (getOperator() != null) { // o
        callPOV.setString(5, getOperator());
      } else {
        callPOV.setNull(5, VARCHAR);
      }
      if (getValue() != null) { // vText
        callPOV.setString(6, getValue());
      } else {
        callPOV.setNull(6, VARCHAR);
      }
      if (this.vInt != null || this.vDouble != null) { // Some numeric
        if (this.vInt != null) {
          callPOV.setInt(7, this.vInt);
          callPOV.setDouble(8, vDoubleSubst);
        } else {
          if (vIntSubst == null) {
            callPOV.setNull(7, INTEGER);
          } else {
            callPOV.setInt(7, vIntSubst);
          }
          callPOV.setDouble(8, this.vDouble);
        }
        // finally: do unit conversion
        if (this.unitStr != null && this.unitStr.length() > 0) {
          this.vDoubleConvertedToStdUnit =
              this.unit.convert(this.vDouble != null ? this.vDouble : vDoubleSubst);
        }
      } else {
        callPOV.setNull(7, INTEGER);
        callPOV.setNull(8, DOUBLE);
      }
      if (this.unit != null) {
        final long unitSig = this.unit.getSignature();
        callPOV.setLong(9, unitSig);
        callPOV.setDouble(10, this.vDoubleConvertedToStdUnit);
        callPOV.setLong(11, this.stdUnitSig);
      } else {
        callPOV.setNull(9, Types.BIGINT);
        callPOV.setNull(10, DOUBLE);
        callPOV.setNull(11, Types.BIGINT);
      }
      if (this.vDatetime == null) { // vDatetime
        callPOV.setNull(12, Types.VARCHAR);
        callPOV.setNull(13, Types.VARCHAR);
      } else if (this.vDatetime instanceof Interval) {
        final Interval interval = (Interval) this.vDatetime;
        final String ilb_nf1 = interval.getILB_NF1();
        final String eub_nf1 = interval.getEUB_NF1();
        if (eub_nf1 != null) {
          callPOV.setString(12, ilb_nf1 + "--" + eub_nf1);
        } else {
          callPOV.setString(12, ilb_nf1);
        }

        final String ilb_nf2 = interval.getILB_NF2();
        final String eub_nf2 = interval.getEUB_NF2();
        if (eub_nf2 != null) {
          callPOV.setString(13, ilb_nf2 + "--" + eub_nf2);
        } else {
          callPOV.setString(13, ilb_nf2);
        }
      } else {
        throw new UnsupportedOperationException("DateTime value needs to be an interval.");
      }

      if (getAggregate() != null) { // agg
        if (query.isVersioned()) {
          throw new UnsupportedOperationException(
              "Versioned queries are not supported for aggregate functions like GREATES or SMALLEST in the filters.");
        }
        callPOV.setString(14, getAggregate());
      } else {
        callPOV.setNull(14, VARCHAR);
      }

      if (getProperty() != null) {
        callPOV.setString(15, getProperty()); // pname
      } else {
        callPOV.setNull(15, VARCHAR);
      }
      callPOV.setBoolean(16, query.isVersioned());
      prefix.add("#executeStmt");
      executeStmt(callPOV, query);
      prefix.pop();

      callPOV.close();
    } catch (final SQLException e) {
      logger.error("This POV filter caused an error: " + this.toString());
      throw new QueryException(e);
    }
    query.addBenchmark(measurement(""), System.currentTimeMillis() - t1);
  }

  private void initPOV(final QueryInterface query) throws SQLException {

    final long t1 = System.currentTimeMillis();
    try (PreparedStatement stmt =
        query.getConnection().prepareCall("call initPOVRefidsTable(?,?)")) {
      // stmt = this.connection.prepareCall("call initPOV(?,?,?,?,?)");
      // initPOVRefidsTable(in vInt INT, in vText VARCHAR(255))
      if (this.value != null) {
        stmt.setString(1, this.value);
      } else {
        stmt.setNull(1, Types.INTEGER);
      }

      if (this.value != null) {
        stmt.setString(2, this.value);
      } else {
        stmt.setNull(2, Types.VARCHAR);
      }
      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        this.refIdsTable = bytes2UTF8(rs.getBytes("refIdsTable"));
      }
    }
    final long t2 = System.currentTimeMillis();
    query.addBenchmark(measurement(".initPOVRefidsTable()"), t2 - t1);

    if (this.refIdsTable != null) {
      query.getQuery().filterIntermediateResult(this.refIdsTable);
    }
    final long t3 = System.currentTimeMillis();
    query.addBenchmark(measurement(".filterRefidsWithoutRetrievePermission"), t3 - t2);
    try (PreparedStatement stmt =
        query.getConnection().prepareCall("call initPOVPropertiesTable(?,?,?)")) {
      // initPOVPropertiesTable(in pid INT UNSIGNED, in pname
      // VARCHAR(255), in sourceSet VARC HAR(255))
      if (this.pid != null) {
        stmt.setInt(1, this.pid);
      } else {
        stmt.setNull(1, Types.INTEGER);
      }

      if (getProperty() != null) {
        stmt.setString(2, getProperty());
      } else {
        stmt.setNull(2, Types.VARCHAR);
      }

      stmt.setString(3, query.getSourceSet());

      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        this.propertiesTable = bytes2UTF8(rs.getBytes("propertiesTable"));
        this.statistics.put(
            "initPOVPropertiesTableStmt1", rs.getString("initPOVPropertiesTableStmt1"));
        this.statistics.put(
            "initPOVPropertiesTableStmt2", rs.getString("initPOVPropertiesTableStmt2"));
        this.statistics.put("replTblStmt1", rs.getString("replTblStmt1"));
        this.statistics.put("replTblStmt2", rs.getString("replTblStmt2"));
        final long st1 = rs.getLong("t1");
        final long st2 = rs.getLong("t2");
        final long st3 = rs.getLong("t3");
        final long st4 = rs.getLong("t4");
        final long st5 = rs.getLong("t5");
        final long st6 = rs.getLong("t6");
        if (st2 - st1 > 0) {
          query.addBenchmark(measurement("#initPropertiesTableByName"), st2 - st1);
        }
        if (st3 - st2 > 0) {
          query.addBenchmark(measurement("#initPropertiesTableById"), st3 - st2);
        }
        if (st4 - st3 > 0) {
          query.addBenchmark(measurement("#getChildren"), st4 - st3);
        }
        if (st5 - st4 > 0) {
          query.addBenchmark(measurement("#findReplacements"), st5 - st4);
        }
        if (st6 - st5 > 0) {
          query.addBenchmark(measurement("#addReplacements"), st6 - st5);
        }
      }
    }
    final long t4 = System.currentTimeMillis();
    query.addBenchmark(measurement(""), t4 - t3);

    if (this.refIdsTable != null) {
      query.getQuery().applyQueryTemplates(query, this.refIdsTable);
      query.addBenchmark(measurement(".applyQueryTemplates()"), System.currentTimeMillis() - t4);
    }

    if (hasSubProperty() && this.targetSet != null) {
      try (PreparedStatement stmt =
          query.getConnection().prepareStatement("call initEmptyTargetSet(?, ?)")) {
        stmt.setNull(1, VARCHAR);
        stmt.setBoolean(2, query.isVersioned());
        // generate new targetSet
        final ResultSet rs = stmt.executeQuery();
        if (rs.next()) {
          this.targetSet = bytes2UTF8(rs.getBytes("newTableName"));
        }
      }
    }
  }

  private void executeStmt(final CallableStatement callPOV, final QueryInterface query)
      throws SQLException, QueryException, TransactionException {
    try {
      final long t1 = System.currentTimeMillis();
      final ResultSet rs = callPOV.executeQuery();
      query.addBenchmark(measurement(".callPOV"), System.currentTimeMillis() - t1);
      if (rs.next()) {
        final int c = rs.getMetaData().getColumnCount();
        for (int i = 0; i < c; i++) {
          final String key = rs.getMetaData().getColumnLabel(i + 1);
          this.statistics.put(key, rs.getString(i + 1));
          if (key.equals("applyPOVIntermediateResultSet")) {
            this.statistics.put(
                "applyPOVIntermediateResultSetCount",
                Utils.countTable(query.getConnection(), rs.getString(i + 1)));
          }
        }
      }

      if (hasSubProperty()) {
        final long t2 = System.currentTimeMillis();
        getSubProperty().apply(query, this.targetSet, this.propertiesTable, this.refIdsTable);
        query.addBenchmark(measurement(".applySubProperty()"), System.currentTimeMillis() - t2);
      }
    } catch (final SQLException e) {
      if (e.getMessage().trim().startsWith("Can't reopen table:") && retry > this.retry_count++) {
        System.err.println("Can't reopen table...");
        executeStmt(callPOV, query);
      } else {
        Utils.printVar(connection, "@stmtPOVStr", System.err);
        Utils.printVar(connection, "@stmtPOVkeepTblStr", System.err);
        throw e;
      }
    }
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("POV");
    if (getProperty() != null) {
      ret.setAttribute("property", getProperty());
    }
    if (getOperator() != null) {
      ret.setAttribute("operator", getOperator());
    }
    if (getValue() != null) {
      ret.setAttribute("value", getValue());
    }
    if (getAggregate() != null) {
      ret.setAttribute("aggregate", getAggregate());
    }
    if (hasSubProperty()) {
      ret.addContent(getSubProperty().toElement());
    }
    if (this.statistics != null) {
      for (final Entry<String, String> entry : this.statistics.entrySet()) {
        final Element key = new Element(entry.getKey());
        key.addContent(entry.getValue());
        ret.addContent(key);
      }
    }
    return ret;
  }

  /** Return the Int value, which may be null. */
  public Integer getVInt() {
    if (this.vInt != null) {
      return Integer.valueOf(vInt);
    }
    return null;
  }

  /** Return the Double value, which may be null. */
  public Double getVDouble() {
    if (this.vDouble != null) {
      return Double.valueOf(vDouble);
    }
    return null;
  }

  /** Return the Datetime value, which may be null. */
  public DateTimeInterface getVDatetime() {
    return this.vDatetime;
  }

  public String getAggregate() {
    return this.aggregate;
  }

  private String measurement(final String m) {
    return String.join("", prefix) + m;
  }

  @Override
  public String getCacheKey() {
    final StringBuilder sb = new StringBuilder();
    if (this.getAggregate() != null) {
      sb.append(this.aggregate);
    }
    sb.append(toString());
    if (this.hasSubProperty()) {
      sb.append(getSubProperty().getCacheKey());
    }
    return sb.toString();
  }
}
