/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import static java.sql.Types.VARCHAR;
import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map.Entry;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.query.Query.Pattern;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class Backreference implements EntityFilterInterface, QueryInterface {

  public static int retry = 10;
  private int retry_count = 0;
  private final Pattern property;
  private Integer pid;
  private final Pattern entity;
  private Integer eid;
  private String targetSet;
  private String sourceSet;
  private QueryInterface query = null;
  private String propertiesTable;
  private String entitiesTable;
  private final HashMap<String, String> statistics = new HashMap<>();

  /**
   * For query clauses like `REFERENCED BY entity [AS property]`
   *
   * @param entity
   * @param property
   */
  public Backreference(final Query.Pattern entity, final Query.Pattern property) {
    if (property != null && property.type != Query.Pattern.TYPE_NORMAL) {
      throw new UnsupportedOperationException(
          "Regular Expression and Like Patterns are not implemented for the BackReference filter yet.");
    }

    this.property = property;
    try {
      // id of property
      this.pid = Integer.parseInt(property.toString());
    } catch (final NumberFormatException | NullPointerException exc) {
      this.pid = null;
    }

    this.entity = entity;
    try {
      // id of entity
      this.eid = Integer.parseInt(entity.toString());
    } catch (final NumberFormatException | NullPointerException exc) {
      this.eid = null;
    }
  }

  public String getProperty() {
    if (this.property != null) {
      return this.property.toString();
    }
    return null;
  }

  public String getEntity() {
    if (this.entity != null) {
      return this.entity.toString();
    }
    return null;
  }

  @Override
  public String toString() {
    return "@(" + getEntity() + "," + getProperty() + ")";
  }

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    if (query.isVersioned() && hasSubProperty()) {
      throw new UnsupportedOperationException(
          "Versioned queries are not supported for subqueries yet. Please file a feature request.");
    }
    final long t1 = System.currentTimeMillis();
    this.query = query;
    this.targetSet = query.getTargetSet();
    try {
      initBackRef(query);

      final CallableStatement callApplyBackRef =
          getConnection().prepareCall("call applyBackReference(?,?,?,?,?,?)");
      callApplyBackRef.setString(1, getSourceSet()); // sourceSet
      this.statistics.put("sourceSet", getSourceSet());
      this.statistics.put(
          "sourceSetCountBefore", Utils.countTable(query.getConnection(), getSourceSet()));
      if (this.targetSet != null) { // targetSet
        callApplyBackRef.setString(2, this.targetSet);
        this.statistics.put("targetSet", this.targetSet);
        this.statistics.put(
            "targetSetCountBefore", Utils.countTable(query.getConnection(), this.targetSet));
      } else {
        callApplyBackRef.setNull(2, VARCHAR);
      }
      if (this.propertiesTable != null) { // propertiesTable
        getQuery().filterIntermediateResult(this.propertiesTable);
        callApplyBackRef.setString(3, this.propertiesTable);
        this.statistics.put("propertiesTable", this.propertiesTable);
        this.statistics.put(
            "propertiesTableCountBefore",
            Utils.countTable(query.getConnection(), this.propertiesTable));
      } else {
        callApplyBackRef.setNull(3, VARCHAR);
      }
      if (this.entitiesTable != null) { // entitiesTable
        getQuery().filterIntermediateResult(this.entitiesTable);
        callApplyBackRef.setString(4, this.entitiesTable);
        this.statistics.put("entitiesTable", this.entitiesTable);
        this.statistics.put(
            "entitiesTableCountBefore",
            Utils.countTable(query.getConnection(), this.entitiesTable));
      } else {
        callApplyBackRef.setNull(4, VARCHAR);
      }
      callApplyBackRef.setBoolean(5, hasSubProperty()); // subQuery?
      callApplyBackRef.setBoolean(6, this.isVersioned());

      executeStmt(query, callApplyBackRef);
      callApplyBackRef.close();
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(this.getClass().getSimpleName().toString(), System.currentTimeMillis() - t1);
  }

  private void initBackRef(final QueryInterface query) throws SQLException {
    final long t1 = System.currentTimeMillis();
    try (PreparedStatement stmt =
        query.getConnection().prepareCall("call initBackReference(?,?,?,?)")) {
      if (this.pid != null) {
        stmt.setInt(1, this.pid);
      } else {
        stmt.setNull(1, Types.INTEGER);
      }

      if (getProperty() != null) {
        stmt.setString(2, getProperty());
      } else {
        stmt.setNull(2, Types.VARCHAR);
      }

      if (this.eid != null) {
        stmt.setInt(3, this.eid);
      } else {
        stmt.setNull(3, Types.INTEGER);
      }

      if (getEntity() != null) {
        stmt.setString(4, getEntity());
      } else {
        stmt.setNull(4, Types.VARCHAR);
      }

      final ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        this.propertiesTable = bytes2UTF8(rs.getBytes("propertiesTable"));
        this.entitiesTable = bytes2UTF8(rs.getBytes("entitiesTable"));
      }
    }
    final long t2 = System.currentTimeMillis();
    addBenchmark("initBackreference()", t2 - t1);

    if (this.entitiesTable != null) {
      getQuery().applyQueryTemplates(query, this.entitiesTable);
      addBenchmark("applyQueryTemplates()", System.currentTimeMillis() - t2);
    }
  }

  private void executeStmt(final QueryInterface query, final CallableStatement callBackRef)
      throws SQLException, QueryException, TransactionException {
    try {
      final long t1 = System.currentTimeMillis();
      if (hasSubProperty()) {
        final ResultSet rs = callBackRef.executeQuery();
        addBenchmark("executeStmt()", System.currentTimeMillis() - t1);
        if (rs.next()) {
          this.sourceSet = bytes2UTF8(rs.getBytes("list"));
          this.statistics.put("subPropertySourceSet", this.sourceSet);
          this.statistics.put(
              "subPropertySourceSetCount", Utils.countTable(query.getConnection(), this.sourceSet));
          this.targetSet = null;
          getSubProperty().getFilter().apply(this);

          final long t3 = System.currentTimeMillis();
          try (final PreparedStatement callFinishSubProperty =
              getConnection().prepareCall("call finishSubProperty(?,?,?,?)")) {
            callFinishSubProperty.setString(1, query.getSourceSet()); // sourceSet
            if (query.getTargetSet() != null) { // targetSet
              callFinishSubProperty.setString(2, query.getTargetSet());
            } else {
              callFinishSubProperty.setNull(2, VARCHAR);
            }
            callFinishSubProperty.setString(3, this.sourceSet); // list
            callFinishSubProperty.setBoolean(4, this.isVersioned());
            final ResultSet rs2 = callFinishSubProperty.executeQuery();
            rs2.next();
            this.statistics.put("finishSubPropertyStmt", rs2.getString("finishSubPropertyStmt"));
          }
          addBenchmark("finishSubProperty()", System.currentTimeMillis() - t3);
        }
      } else {
        callBackRef.execute();
        addBenchmark("executeStmt()", System.currentTimeMillis() - t1);
      }
    } catch (final SQLException e) {
      if (e.getMessage().trim().startsWith("Can't reopen table:") && retry > this.retry_count++) {
        executeStmt(query, callBackRef);
      } else {
        throw e;
      }
    }
  }

  public SubProperty getSubProperty() {
    return this.subProperty;
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("BackReference");
    if (getEntity() != null) {
      ret.setAttribute("entity", getEntity());
    }
    if (getProperty() != null) {
      ret.setAttribute("property", getProperty());
    }
    if (hasSubProperty()) {
      ret.addContent(getSubProperty().toElement());
    }
    if (this.statistics != null) {
      for (final Entry<String, String> entry : this.statistics.entrySet()) {
        final Element key = new Element(entry.getKey());
        key.addContent(entry.getValue());
        ret.addContent(key);
      }
    }
    return ret;
  }

  public void setSubProperty(final SubProperty subp) {
    this.subProperty = subp;
  }

  private SubProperty subProperty = null;

  public boolean hasSubProperty() {
    return this.subProperty != null;
  }

  @Override
  public Query getQuery() {
    return this.query.getQuery();
  }

  @Override
  public String getSourceSet() {
    if (this.sourceSet != null) {
      return this.sourceSet;
    } else {
      return this.query.getSourceSet();
    }
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public Connection getConnection() {
    return this.query.getConnection();
  }

  @Override
  public int getTargetSetCount() {
    return -1;
  }

  @Override
  public Access getAccess() {
    return this.query.getAccess();
  }

  @Override
  public Subject getUser() {
    return this.query.getUser();
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    this.query.addBenchmark(this.getClass().getSimpleName() + "." + str, time);
  }

  @Override
  public boolean isVersioned() {
    return this.query.isVersioned();
  }

  @Override
  public String getCacheKey() {
    StringBuilder sb = new StringBuilder();
    sb.append(toString());
    if (this.hasSubProperty()) sb.append(getSubProperty().getCacheKey());
    return sb.toString();
  }
}
