/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Utils {

  /**
   * For debugging. Prints an SQL table with given name;
   *
   * @param c
   * @param name
   * @param ps
   * @throws SQLException
   */
  public static void printTable(final Connection c, final String name, final PrintStream ps) {
    if (name == null) {
      return;
    }
    ps.println("TABLE " + name);
    try (final Statement stmt = c.createStatement()) {
      final ResultSet rs = stmt.executeQuery("SELECT * FROM `" + name + "`");

      final int cc = rs.getMetaData().getColumnCount();
      try {
        int j = 0;
        while (j < cc) {
          j++;
          ps.print(rs.getMetaData().getColumnName(j) + "|");
        }
      } catch (final SQLException e) {
        e.printStackTrace();
      }
      ps.println();
      while (rs.next()) {
        int i = 0;
        try {
          while (i < cc) {
            i++;
            ps.print(rs.getString(i) + "|");
          }
        } catch (final SQLException e) {
          e.printStackTrace();
        }
        ps.println();
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * For debugging. Prints an SQL variable with a given name.
   *
   * @param c
   * @param name
   * @throws SQLException
   */
  public static void printVar(final Connection c, final String name, final PrintStream ps) {
    if (name == null) {
      return;
    }
    ps.println("VAR " + name);
    try (final Statement stmt = c.createStatement()) {
      final ResultSet rs = stmt.executeQuery("SELECT " + name + " as var");
      while (rs.next()) {
        ps.println(rs.getString("var"));
      }
    } catch (final SQLException e) {
      e.printStackTrace();
    }
  }

  public static String countTable(final Connection c, final String table) {
    if (table == null) {
      return null;
    }

    try (Statement stmt = c.createStatement()) {
      final ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS c FROM `" + table + "`");
      rs.next();
      return rs.getString("c");
    } catch (final Throwable e) {
      e.printStackTrace();
      return e.toString();
    }
  }
}
