/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.query;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.jcs.access.behavior.ICacheAccess;
import org.apache.shiro.subject.Subject;
import org.caosdb.api.entity.v1.MessageCode;
import org.caosdb.datetime.UTCDateTime;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.caching.Cache;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.database.backend.implementation.MySQL.MySQLHelper;
import org.caosdb.server.database.backend.transaction.RetrieveFullEntityTransaction;
import org.caosdb.server.database.backend.transaction.RetrieveSparseEntity;
import org.caosdb.server.database.misc.DBHelper;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.TransactionContainer;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.query.CQLParser.CqContext;
import org.caosdb.server.query.CQLParsingErrorListener.ParsingError;
import org.caosdb.server.transaction.EntityTransactionInterface;
import org.caosdb.server.transaction.Retrieve;
import org.caosdb.server.transaction.Transaction;
import org.caosdb.server.transaction.WriteTransaction;
import org.jdom2.Element;
import org.slf4j.Logger;

/**
 * This class represents a single, complete Query execution from the parsing of the query string to
 * the resulting list of entity ids.
 *
 * <p>This class handles caching of queries and checking retrieve permissions as well. It does not,
 * however, retrieve the resulting entities; this is handled by the {@link Retrieve} class.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class Query implements QueryInterface, ToElementable, EntityTransactionInterface {

  /** Class which represents the selection of (sub)properties. */
  public static class Selection {
    private final String selector;
    private Selection subselection = null;

    /**
     * These magic non-properties are those which are in the results of a {@link
     * RetrieveSparseEntity} transaction which is why they do not need to be retrieved with a {@link
     * RetrieveEntityProperties} transaction.
     */
    static final Set<String> MAGIC_NON_PROPERTIES =
        new HashSet<>(
            Arrays.asList(
                new String[] {"value", "parent", "id", "name", "description", "datatype"}));

    public Selection setSubSelection(final Selection sub) {
      if (this.subselection != null) {
        throw new UnsupportedOperationException("SubSelection is immutable!");
      }
      this.subselection = sub;
      return this;
    }

    /** No parsing, just sets the selector string. */
    public Selection(final String selector) {
      this.selector = selector.trim();
    }

    public String getSelector() {
      return this.selector;
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder(this.selector);
      if (this.subselection != null) {
        sb.append(".");
        sb.append(this.subselection.toString());
      }
      return sb.toString();
    }

    public Selection getSubselection() {
      return this.subselection;
    }

    public Element toElement() {
      final Element ret = new Element("Selector");
      ret.setAttribute("name", toString());
      return ret;
    }

    /**
     * Return true iff this selector selects the parent of an entity. If not, the retrieval in
     * {@link RetrieveFullEntityTransaction} might be optimized by not retrieving the parents at
     * all.
     */
    public boolean isParent() {
      return this.selector.equalsIgnoreCase("parent");
    }

    /**
     * Return true iff this selector selects anything that is most likely a property. If not, the
     * retrieval in {@link RetrieveFullEntityTransaction} might be optimized by not retrieving the
     * properties at all.
     */
    public boolean isProperty() {
      return !MAGIC_NON_PROPERTIES.contains(this.selector.toLowerCase());
    }

    /** Return true iff this selector selects the id of an entity. */
    public boolean isId() {
      return this.selector.equalsIgnoreCase("id");
    }
  }

  public enum Role {
    RECORD,
    RECORDTYPE,
    PROPERTY,
    ENTITY,
    FILE,
    QUERYTEMPLATE
  }

  public enum Type {
    FIND,
    COUNT,
    SELECT,
  };

  public static final class Pattern {
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_LIKE = 1;
    public static final int TYPE_REGEXP = 2;
    public final int type;
    public final String str;

    public Pattern(final String str, final int type) {
      this.type = type;
      this.str = str;
    }

    @Override
    public String toString() {
      switch (this.type) {
        case TYPE_LIKE:
          return this.str.replaceAll("\\*", "%").replace("_", "\\_");
        default:
          return this.str;
      }
    }
  }

  public static final class ParsingException extends Query.QueryException {

    public ParsingException(final String string) {
      super(string);
    }

    /** */
    private static final long serialVersionUID = -7142620266283649346L;
  }

  public static class QueryException extends RuntimeException {

    private static final long serialVersionUID = -7142620266283649346L;

    public QueryException(final String string) {
      super(string);
    }

    public QueryException(final Throwable t) {
      super(t);
    }
  }

  /** A data class for storing triplets of (Entity ID, version hash, ACL string) */
  public static class IdVersionAclTriplet {
    public IdVersionAclTriplet(final String id, final String version, final String acl) {
      this.id = id;
      this.version = version;
      this.acl = acl;
    }

    public String id;
    public String version;
    public String acl;

    @Override
    public String toString() {
      if (version == null) {
        return id;
      }
      return id + "@" + version;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj instanceof IdVersionAclTriplet) {
        final IdVersionAclTriplet that = (IdVersionAclTriplet) obj;
        // checking ID and version hash should be sufficient
        if (this.id == that.id && this.version == that.version) {
          if (this.acl != that.acl) {
            throw new RuntimeException("Implementation error! ACL should not differ");
          }
          return true;
        }
        ;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return toString().hashCode();
    }

    public boolean isInternal() {
      try {
        return Integer.parseInt(id) < 100;
      } catch (NumberFormatException e) {
        return false;
      }
    }
  }

  private final boolean filterEntitiesWithoutRetrievePermisions =
      !CaosDBServer.getServerProperty(
              ServerProperties.KEY_QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS)
          .equalsIgnoreCase("FALSE");

  private final Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());
  List<IdVersionAclTriplet> resultSet = null;
  private final String query;
  private Pattern entity = null;
  private Role role = null;
  private String sourceSet = null;
  private String targetSet = null;
  private List<Selection> selections = null;
  private String parseTree = null;
  private final TransactionContainer container;
  private EntityFilterInterface filter = null;
  private CQLParsingErrorListener el = null;
  private final Subject user;
  private Type type = null;
  private final ArrayList<ToElementable> messages = new ArrayList<>();
  private Access access;
  private boolean versioned = false;

  /**
   * This key-value cache stores lists of of (id, version hash, acl string) triplets. Those values
   * are the result sets of queries. The keys are created such that they are different for different
   * for different queries (@see {@link #getCacheKey}). The key includes realm and username of a
   * subject, if the query result must not be shared among users. If intermediate permission checks
   * are done (e.g. in a subproperty query filter), the query result will be stored using a user
   * specific key. The final permission check has not yet been applied to the result set that is
   * stored in the cache. This allows (some) cache entries to be shared among users since the final
   * check is applied after the retrieval of the result set from the cache (@see {@link
   * filterEntitiesWithoutRetrievePermission}) The cache is invalidated whenever there is a write
   * operation (@see {@link #clearCache} which is called in the {@link WriteTransaction#commit}).
   */
  private static ICacheAccess<String, Serializable> cache =
      Cache.getCache("HIGH_LEVEL_QUERY_CACHE");

  /** Cached=true means that the results of this query have actually been pulled from the cache. */
  private boolean cached = false;

  /**
   * Cachable=false means that the results of this query cannot be used for the cache, because the
   * query evaluation contains complex permission checking which could only be cached on a per-user
   * basis (maybe in the future).
   */
  private boolean filteredIntermediateResult = false;

  private EntityTransactionInterface transaction;

  /**
   * Tags the query cache and is renewed each time the cache is being cleared, i.e. each time the
   * database is being updated.
   *
   * <p>As the name suggests, the idea is similar to the ETag header of the HTTP protocol.
   */
  private static String cacheETag = UUID.randomUUID().toString();

  public Type getType() {
    return this.type;
  }

  public Query(final String query) {
    this(query, (Subject) null);
  }

  public Query(final String query, final Subject user) {
    this(query, user, null, null, null);
  }

  public Query(final String query, final Transaction<? extends TransactionContainer> transaction) {
    this(
        query,
        transaction.getTransactor(),
        transaction.getContainer(),
        transaction,
        transaction.getAccess());
  }

  public Query(
      final String query,
      final Subject user,
      final TransactionContainer container,
      final EntityTransactionInterface transaction,
      final Access access) {
    this.container = container;
    this.transaction = transaction;
    this.query = query;
    this.user = user;
    this.access = access;
  }

  /**
   * Fill the initially empty resultSet with all entities which match the name, or the id. Then
   * calculate the transitive closure of child entities and add it to the resultSet.
   *
   * @throws SQLException
   */
  private void initResultSetWithNameIDAndChildren() throws SQLException {
    final CallableStatement callInitEntity =
        getConnection().prepareCall("call initEntity(?,?,?,?,?,?)");

    if (matchIdPattern(this.entity.toString())) {
      callInitEntity.setString(1, this.entity.toString());
    } else {
      callInitEntity.setNull(1, Types.INTEGER);
    }
    switch (this.entity.type) {
      case Pattern.TYPE_NORMAL:
        callInitEntity.setString(2, this.entity.toString());
        callInitEntity.setNull(3, Types.VARCHAR);
        callInitEntity.setNull(4, Types.VARCHAR);
        break;
      case Pattern.TYPE_LIKE:
        callInitEntity.setNull(2, Types.VARCHAR);
        callInitEntity.setString(3, this.entity.toString());
        callInitEntity.setNull(4, Types.VARCHAR);
        break;
      case Pattern.TYPE_REGEXP:
        callInitEntity.setNull(2, Types.VARCHAR);
        callInitEntity.setNull(3, Types.VARCHAR);
        callInitEntity.setString(4, this.entity.toString());
        break;
      default:
        break;
    }
    callInitEntity.setString(5, this.sourceSet);
    callInitEntity.setBoolean(6, this.versioned);
    callInitEntity.execute();
    callInitEntity.close();
  }

  /**
   * @return The result set table name.
   * @throws QueryException
   */
  private String sourceStrategy(final String sourceSet) throws QueryException {
    try {
      this.sourceSet = sourceSet;
      initResultSetWithNameIDAndChildren();

      if (this.role != Role.QUERYTEMPLATE) {
        applyQueryTemplates(this, getSourceSet());
      }

      if (this.role != null && this.role != Role.ENTITY) {
        final RoleFilter roleFilter = new RoleFilter(this.role, "=", this.versioned);
        roleFilter.apply(this);
      }

      if (this.filter != null) {
        this.filter.apply(this);
      }

      return this.sourceSet;
    } catch (final SQLException e) {
      e.printStackTrace();
      throw new QueryException(e);
    }
  }

  /**
   * Finds all QueryTemplates in the resultSet and applies them to the same resultSet. The IDs of
   * the QueryTemplates themselves are then removed from the resultSet. If the current user doesn't
   * have the RETRIEVE:ENTITY permission for a particular QueryTemplate it will be ignored.
   *
   * @param resultSet
   * @throws QueryException
   */
  public void applyQueryTemplates(final QueryInterface query, final String resultSet)
      throws QueryException {
    try {
      final Map<EntityID, String> queryTemplates = getQueryTemplates(query, resultSet);

      final PreparedStatement removeQTStmt =
          query
              .getConnection()
              .prepareStatement(
                  "DELETE FROM `"
                      + resultSet
                      + "` WHERE EXISTS (SELECT 1 FROM entity_ids AS eids WHERE eids.id=? AND eids.internal_id=`"
                      + resultSet
                      + "`.id)");

      // Run thru all QTs found...
      for (final Entry<EntityID, String> q : queryTemplates.entrySet()) {
        // ... remove the QT from resultSet...
        removeQTStmt.setString(1, q.getKey().toString());
        removeQTStmt.execute();

        // ... check for RETRIEVE:ENTITY permission...
        final EntityInterface e =
            execute(new RetrieveSparseEntity(q.getKey(), null), query.getAccess()).getEntity();
        final EntityACL entityACL = e.getEntityACL();
        if (!entityACL.isPermitted(query.getUser(), EntityPermission.RETRIEVE_ENTITY)) {
          // ... and ignore if not.
          continue;
        }

        // ... apply them...
        final Query subQuery =
            new Query(q.getValue(), query.getUser(), null, transaction, query.getAccess());
        subQuery.parse();

        // versioning for QueryTemplates is not supported and probably never will.
        final String subResultSet = subQuery.executeStrategy(false);

        // ... and merge the resultSets.
        union(query, resultSet, subResultSet);
      }
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
  }

  private static void union(final QueryInterface query, final String target, final String source)
      throws SQLException {
    final PreparedStatement unionStmt =
        query.getConnection().prepareCall("call calcUnion('" + target + "','" + source + "')");
    unionStmt.execute();
  }

  /**
   * Return the definitions of all QueryTemplates in the resultSet as a List.
   *
   * @param resultSet
   * @return A list of query strings.
   * @throws SQLException
   */
  private static Map<EntityID, String> getQueryTemplates(
      final QueryInterface query, final String resultSet) throws SQLException {
    ResultSet rs = null;
    try {
      final Map<EntityID, String> ret = new HashMap<>();
      final CallableStatement stmt =
          query
              .getConnection()
              .prepareCall(
                  "SELECT (SELECT eids.id FROM entity_ids AS eids WHERE eids.internal_id = q.id) as id, q.definition FROM query_template_def AS q INNER JOIN `"
                      + resultSet
                      + "` AS r ON (r.id=q.id);");
      rs = stmt.executeQuery();
      while (rs.next()) {
        ret.put(new EntityID(rs.getString("id")), rs.getString("definition"));
      }
      return ret;
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * @return The result set table name.
   * @throws QueryException
   */
  private String targetStrategy(final String targetSet) throws QueryException {
    this.targetSet = targetSet;

    // set entities table as source
    this.sourceSet = "entities";

    if (this.filter != null) {

      // apply filters
      this.filter.apply(this);

      this.sourceSet = null;
    }

    // filter by role
    if (this.role != null && this.role != Role.ENTITY) {
      final RoleFilter roleFilter = new RoleFilter(this.role, "=", this.versioned);
      roleFilter.apply(this);
    }

    return this.targetSet;
  }

  private MySQLHelper getMySQLHelper(final Access access) {
    final DBHelper h = access.getHelper("MySQL");
    if (h == null) {
      final MySQLHelper ret = new MySQLHelper();
      access.setHelper("MySQL", ret);
      return ret;
    } else {
      return (MySQLHelper) h;
    }
  }

  private String initQuery(final boolean versioned) throws QueryException {
    final String sql = "call initQuery(" + versioned + ")";
    try (final CallableStatement callInitQuery = getConnection().prepareCall(sql)) {
      ResultSet initQueryResult = null;
      initQueryResult = callInitQuery.executeQuery();
      if (!initQueryResult.next()) {
        throw new QueryException("No resultSet table created.");
      }
      return bytes2UTF8(initQueryResult.getBytes("tablename"));
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
  }

  /**
   * Parse the query and run optimize() if the parameter `optimize` is true.
   *
   * @param optimize whether to run optimize() immediately.
   * @throws ParsingException
   */
  public void parse(final boolean optimize) throws ParsingException {
    final long t1 = System.currentTimeMillis();
    CQLLexer lexer;
    lexer = new CQLLexer(CharStreams.fromString(this.query));
    final CommonTokenStream tokens = new CommonTokenStream(lexer);

    final CQLParser parser = new CQLParser(tokens);
    parser.removeErrorListeners();
    this.el = new CQLParsingErrorListener(CQLLexer.UNKNOWN_CHAR);
    parser.addErrorListener(this.el);
    final CqContext cq = parser.cq();
    if (this.el.hasErrors()) {
      throw new ParsingException("Parsing finished with errors.");
    }

    this.entity = cq.e;
    this.versioned = cq.v != VersionFilter.UNVERSIONED;
    this.role = cq.r;
    this.parseTree = cq.toStringTree(parser);
    this.type = cq.t;
    this.filter = cq.filter;
    this.selections = cq.s;
    final long t2 = System.currentTimeMillis();
    if (t2 - t1 > 1000) {
      addBenchmark("LONG_PARSING: " + this.query, t2 - t1);
    }

    if (optimize) {
      optimize();
    }
  }

  /**
   * Parse the query and run optimize() immediately.
   *
   * @throws ParsingException
   */
  public void parse() throws ParsingException {
    parse(true);
  }

  /**
   * Optimize the query after parsing. The optimization is purely based on formal rules.
   *
   * <p>Implemented rules:
   *
   * <ol>
   *   <li>FIND * -> FIND ENTITY (which basically prevents to copy the complete entity table just to
   *       read out the IDs immediately).
   * </ol>
   */
  public void optimize() {
    // "FIND Person" is interpreted as "FIND RECORD Person"
    if (this.role == null) {
      try {
        this.role =
            Role.valueOf(
                CaosDBServer.getServerProperty(ServerProperties.KEY_FIND_QUERY_DEFAULT_ROLE));
      } catch (IllegalArgumentException e) {
        addError(
            "Unsupportet value of the server property "
                + ServerProperties.KEY_FIND_QUERY_DEFAULT_ROLE
                + ": "
                + CaosDBServer.getServerProperty(ServerProperties.KEY_FIND_QUERY_DEFAULT_ROLE));
        throw new UnsupportedOperationException(e);
      }
    }

    // "FIND *" is interpreted as "FIND RECORD", "FIND <ROLE> *" as "FIND <ROLE>"
    if (this.entity != null
        && this.entity.type == Pattern.TYPE_LIKE
        && this.entity.str.equals("*")) {
      this.entity = null;
    }
  }

  private String executeStrategy(final boolean versioned) throws QueryException {
    if (this.entity != null) {
      return sourceStrategy(initQuery(versioned));
    } else if (this.role == Role.ENTITY && this.filter == null) {
      return "entity_ids";
    } else {
      return targetStrategy(initQuery(versioned));
    }
  }

  /**
   * Generate a SQL statement which reads out the resulting IDs and ACL strings (and version IDs if
   * `versioned` is true).
   *
   * <p>There are four variants: Where the parameter `resultSetTableName` is "entities" and
   * otherwise and where `versioned` is true and otherwise.
   *
   * @param resultSetTableName name of the table with all the resulting entities
   * @param versioned whether the query was versioned
   * @return an SQL statement
   */
  private String generateSelectStatementForResultSet(
      final String resultSetTableName, final boolean versioned) {
    if (resultSetTableName.equals("entity_ids")) {
      final String baseStatement =
          "SELECT entities.id AS internal_id, (SELECT id FROM entity_ids WHERE internal_id = entities.id) as id, entity_acl.acl FROM entities INNER JOIN entity_acl ON entity_acl.id=entities.acl";
      if (!versioned) {
        return baseStatement + ";";
      }
      // if versioned, the statement is surrounded with another SELECT and JOIN
      return ("SELECT id, acl, version FROM ("
          + baseStatement
          + ") AS tmp JOIN entity_version ON entity_version.entity_id=tmp.internal_id;");
    } else {
      if (!versioned) {
        return (" SELECT (SELECT id FROM entity_ids WHERE internal_id = tmp.id) AS id, entity_acl.acl FROM "
                + " (SELECT results.id AS id, entities.acl AS acl_id FROM `"
                + resultSetTableName
                + "` AS results JOIN entities ON results.id=entities.id) AS tmp"
                + " JOIN entity_acl ON entity_acl.id=tmp.acl_id")
            + ";";
      }
      // if versioned, the statement is surrounded with another SELECT and JOIN
      return ("SELECT (SELECT id FROM entity_ids WHERE internal_id = tmp2.id) AS id, acl, version FROM (SELECT tmp.id, entity_acl.acl, tmp._iversion AS _iversion FROM "
          + " (SELECT results.id AS id, entities.acl AS acl_id, results._iversion AS _iversion FROM `"
          + resultSetTableName
          + "` AS results JOIN entities ON results.id=entities.id) AS tmp"
          + " JOIN entity_acl ON entity_acl.id=tmp.acl_id) as tmp2  "
          + "join entity_version on (entity_version.entity_id=tmp2.id AND tmp2._iversion = entity_version._iversion);");
    }
  }

  /**
   * Return a list of all resulting entities (versions of entities if `versioned` is true).
   *
   * @param resultSetTableName name of the table with all the resulting entities
   * @param versioned whether the query was versioned
   * @return list of results of this query.
   * @throws QueryException
   */
  private List<IdVersionAclTriplet> getResultSet(
      final String resultSetTableName, final boolean versioned) throws QueryException {
    ResultSet finishResultSet = null;
    try {
      final String sql = generateSelectStatementForResultSet(resultSetTableName, versioned);
      final PreparedStatement finish = getConnection().prepareStatement(sql);
      finishResultSet = finish.executeQuery();
      final List<IdVersionAclTriplet> rs = new LinkedList<>();
      while (finishResultSet.next()) {
        final String id = finishResultSet.getString("id");
        if (finishResultSet.wasNull()) {
          continue;
        }
        final String version = versioned ? finishResultSet.getString("version") : null;
        final String acl = finishResultSet.getString("acl");

        rs.add(new IdVersionAclTriplet(id, version, acl));
      }
      return rs;
    } catch (final SQLException e) {
      throw new QueryException(e);
    } finally {
      if (finishResultSet != null) {
        try {
          finishResultSet.close();
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Try to set the `resultSet` member variable using the values stored in the high level query
   * cache.
   */
  private void getResultFromCache() {
    // try key with username and realm
    // TODO include this again to activate the user-specific caching
    // this.resultSet = getCached(getCacheKey(true));
    if (this.resultSet == null) {
      // try key without username and realm
      this.resultSet = getCached(getCacheKey(false));
    }
  }

  /** Store the content of `resultSet` member in the high level query cache. */
  private void storeResultInCache() {
    // Decide whether user specific cache needs to be used or not
    // Currently, this is solely determined via filteredIntermediateResult.
    if (this.filteredIntermediateResult) {
      // TODO include this again to activate user-specific caching
      // cacheItem(getCacheKey(true), this.resultSet);
    } else {
      cacheItem(getCacheKey(false), this.resultSet);
    }
  }

  /** Fill entities from `resultSet` into `container`. */
  private void fillContainerWithResult() {
    if (this.container != null && (this.type == Type.FIND || this.type == Type.SELECT)) {
      for (final IdVersionAclTriplet t : this.resultSet) {

        final Entity e = new RetrieveEntity(new EntityID(t.id), t.version);

        // if query has select-clause:
        if (this.selections != null && !this.selections.isEmpty()) {
          e.addSelections(this.selections);
        }
        this.container.add(e);
      }
    }
  }

  /**
   * Execute the query.
   *
   * <p>First try the cache and only then use the back-end.
   *
   * @throws ParsingException, QueryException
   */
  @Override
  public void execute() throws ParsingException, QueryException {
    try {
      parse();
      setAccess(access);
      if (access.useCache()) {
        getResultFromCache();
      }
      if (this.resultSet != null) {
        this.logger.debug("Using cached result for {}", this.query);
        this.cached = true;

      } else {
        executeQueryInBackend(access);
        storeResultInCache();
        this.logger.debug("Uncached query {}", this.query);
      }

      this.resultSet = filterEntitiesWithoutRetrievePermission(this.resultSet);
      this.resultSet = removeInternalEntitiesFromResultSet();
      fillContainerWithResult();
    } catch (final SQLException e) {
      e.printStackTrace();
      throw new QueryException(e);
    }
  }

  /** Remove all cached queries from the cache. */
  public static void clearCache() {
    cacheETag = UUID.randomUUID().toString();
    cache.clear();
  }

  /** There are internal Entities (with ID<100) that should never be returned. */
  private List<IdVersionAclTriplet> removeInternalEntitiesFromResultSet() {

    final List<IdVersionAclTriplet> filtered = new ArrayList<>();
    for (final IdVersionAclTriplet triplet : resultSet) {

      if (!triplet.isInternal()) {
        filtered.add(triplet);
      }
    }
    return filtered;
  }

  /**
   * Cache a query result.
   *
   * @param key
   * @param resultSet
   */
  private void cacheItem(final String key, final List<IdVersionAclTriplet> resultSet) {
    synchronized (cache) {
      if (resultSet instanceof Serializable) {
        cache.put(key, (Serializable) resultSet);
      } else {
        cache.put(key, new ArrayList<>(resultSet));
      }
    }
  }

  /**
   * Retrieve a result set of entity IDs (and the version) from the cache.
   *
   * @param key
   * @return
   */
  @SuppressWarnings("unchecked")
  private List<IdVersionAclTriplet> getCached(final String key) {
    return (List<IdVersionAclTriplet>) cache.get(key);
  }

  protected void executeQueryInBackend(final Access access) throws SQLException {
    try {
      this.resultSet = getResultSet(executeStrategy(this.versioned), this.versioned);
    } finally {
      cleanUp();
    }
  }

  private void addWarning(final String w) {
    this.messages.add(new Message(MessageType.Warning, MessageCode.MESSAGE_CODE_UNKNOWN, w));
  }

  private void addError(final String e) {
    this.messages.add(new Message(MessageType.Error, MessageCode.MESSAGE_CODE_UNKNOWN, e));
  }

  private void cleanUp() {
    if (getConnection() != null) {
      ResultSet rs = null;
      try {
        rs = getConnection().prepareCall("call cleanUpQuery()").executeQuery();
        while (rs.next()) {
          addWarning(bytes2UTF8(rs.getBytes("warning")));
        }
      } catch (final SQLException e) {
        throw new QueryException(e);
      } finally {
        try {
          if (rs != null) {
            rs.close();
          }
        } catch (final SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void setAccess(final Access access) {
    this.access = access;
  }

  /**
   * Filter out all entities which may not be retrieved by this user due to a missing RETRIEVE
   * permission. This function is also designed for filtering of intermediate results.
   *
   * @param tabname
   * @throws SQLException
   */
  public void filterIntermediateResult(final String tabname) throws SQLException {
    if (!filterEntitiesWithoutRetrievePermisions) {
      return;
    }
    filteredIntermediateResult = true;

    /*
     * The following creates a table with the columns (entity ID, acl) from
     * a given table with entity IDs. Here, acl is the string representation
     * of the acl.
     *
     * TODO:In future, one might want to retrieve only a distinct set of acl
     * with (acl_id, acl) and a table with (entity_id, acl_id) to reduce the
     * amount of data being transfered.
     */

    try (final Statement stmt = this.getConnection().createStatement()) {
      final String query =
          ("SELECT entity_n_acl.id, entity_acl.acl from "
              + "(select entities.id, entities.acl from entities "
              + "inner join `"
              + tabname
              + "` as rs on entities.id=rs.id) "
              + "as entity_n_acl "
              + "left join entity_acl on entity_n_acl.acl=entity_acl.id;");
      final ResultSet entitiesRS = stmt.executeQuery(query);
      final ResultSetIterator entitiesWithACL = new ResultSetIterator(entitiesRS);
      final List<String> toBeDeleted = collectIdsWithoutPermission(entitiesWithACL);
      try (final PreparedStatement pstmt =
          this.getConnection().prepareStatement("DELETE FROM `" + tabname + "` WHERE id = ?")) {
        for (final String id : toBeDeleted) {
          pstmt.setString(1, id);
          pstmt.execute();
        }
      }
    }
  }

  /**
   * Creates a new list that contains only the entities from the `resultSet` for which the current
   * subject has RETRIEVE permission.
   *
   * <p>Note, unlike the public version of this function `resultSet` is not altered but the filtered
   * list is returned.
   *
   * @param resultSet
   * @return list without the entities with insufficient permissions
   */
  private List<IdVersionAclTriplet> filterEntitiesWithoutRetrievePermission(
      final List<IdVersionAclTriplet> resultSet) {
    final List<String> toBeDeleted = collectIdsWithoutPermission(resultSet.iterator());
    final List<IdVersionAclTriplet> filtered = new LinkedList<>();
    for (final IdVersionAclTriplet triplet : resultSet) {
      if (-1 == toBeDeleted.indexOf(triplet.id)) {
        filtered.add(triplet);
      }
    }
    return filtered;
  }

  /**
   * Creates a list with IDs of those entities that do not have sufficient RETRIEVE permission
   *
   * @param entityIterator Iterator over the result set consisting of (ID, version hash, acl string)
   *     triplets.
   * @return compiled list
   */
  private List<String> collectIdsWithoutPermission(Iterator<IdVersionAclTriplet> entityIterator) {
    final Map<String, Boolean> acl_cache = new HashMap<>();
    final List<String> toBeDeleted = new LinkedList<>();
    while (entityIterator.hasNext()) {
      final long t1 = System.currentTimeMillis();

      final IdVersionAclTriplet triplet = entityIterator.next();

      if (!acl_cache.containsKey(triplet.acl)) {
        acl_cache.put(
            triplet.acl,
            EntityACL.deserialize(triplet.acl)
                .isPermitted(this.getUser(), EntityPermission.RETRIEVE_ENTITY));
      }

      if (!acl_cache.get(triplet.acl)) {
        toBeDeleted.add(triplet.id);
      }

      final long t2 = System.currentTimeMillis();
      this.addBenchmark("filterEntitiesWithoutRetrievePermission", t2 - t1);
    }
    return toBeDeleted;
  }

  @Override
  public String toString() {
    return this.query;
  }

  @Override
  public String getSourceSet() {
    return this.sourceSet;
  }

  @Override
  public Connection getConnection() {
    try {
      return getMySQLHelper(getAccess()).getConnection();
    } catch (final SQLException e) {
      e.printStackTrace();
    } catch (final ConnectionException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public Access getAccess() {
    return this.access;
  }

  /**
   * @return the number of entities in the resultset. Might be updated by the filters.
   */
  @Override
  public int getTargetSetCount() {
    return this.targetSetCount;
  }

  public void setTargetSetCount(final Integer c) {
    this.targetSetCount = c;
  }

  private int targetSetCount = -1;

  private TransactionBenchmark benchmark;

  @Override
  public void addToElement(final Element parent) {
    final Element ret = new Element("Query");
    if (this.query == null) {
      parent.addContent(ret);
      return;
    }
    ret.setAttribute("string", this.query);
    ret.setAttribute("results", Integer.toString(getCount()));
    ret.setAttribute("cached", Boolean.toString(this.cached));
    ret.setAttribute("etag", cacheETag);

    final Element parseTreeElem = new Element("ParseTree");
    if (this.el.hasErrors()) {
      for (final ParsingError m : this.el.getErrors()) {
        parseTreeElem.addContent(m.toElement());
      }
    } else {
      parseTreeElem.setText(this.parseTree);
    }
    ret.addContent(parseTreeElem);

    final Element roleElem = new Element("Role");
    if (this.role != null) {
      roleElem.setText(this.role.toString());
    }
    ret.addContent(roleElem);

    final Element entityElem = new Element("Entity");
    try {
      entityElem.setText(this.entity.toString());
    } catch (final NullPointerException exc) {
    }
    ret.addContent(entityElem);

    if (this.filter != null) {
      final Element filterElem = new Element("Filter");
      filterElem.addContent(this.filter.toElement());
      ret.addContent(filterElem);
    }

    for (final ToElementable m : this.messages) {
      m.addToElement(ret);
    }
    if (getSelections() != null && !getSelections().isEmpty()) {
      final Element selection = new Element("Selection");
      for (final Selection s : getSelections()) {
        selection.addContent(s.toElement());
      }
      ret.addContent(selection);
    }

    parent.addContent(ret);
  }

  @Override
  public Subject getUser() {
    return this.user;
  }

  @Override
  public Query getQuery() {
    return this;
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  public List<Selection> getSelections() {
    return this.selections;
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    getTransactionBenchmark()
        .addMeasurement(this.getClass().getSimpleName().toString() + "." + str, time);
  }

  @Override
  public TransactionBenchmark getTransactionBenchmark() {
    if (benchmark == null) {
      if (container != null) {
        benchmark = container.getTransactionBenchmark().getBenchmark(getClass());
      } else {
        benchmark = TransactionBenchmark.getRootInstance().getBenchmark(getClass());
      }
    }
    return benchmark;
  }

  @Override
  public boolean isVersioned() {
    return this.versioned;
  }

  /**
   * Return a key for the query cache. The key should describe the query with all the filters but
   * without the FIND, COUNT and SELECT ... FROM parts.
   *
   * @return A Cache key.
   */
  String getCacheKey(boolean addUser) {
    final StringBuilder sb = new StringBuilder();
    if (addUser && (this.user != null)) {
      sb.append("U_");
      String principal_desc =
          ((Principal) this.user.getPrincipal()).getUsername()
              + Principal.REALM_SEPARATOR
              + ((Principal) this.user.getPrincipal()).getRealm();
      sb.append(principal_desc);
    }
    if (this.versioned) {
      sb.append("V_");
    }
    if (this.role != null) {
      sb.append("R_");
      sb.append(this.role.toString());
    }
    if (this.entity != null) {
      sb.append("E_");
      sb.append(this.entity.toString());
    }
    if (this.filter != null) {
      sb.append("F_");
      sb.append(this.filter.getCacheKey());
    }
    return sb.toString();
  }

  public Pattern getEntity() {
    return this.entity;
  }

  public Role getRole() {
    return this.role;
  }

  /**
   * Return the ETag.
   *
   * <p>The ETag tags the query cache and is renewed each time the cache is being cleared, i.e. each
   * time the database is being updated.
   *
   * @return The ETag
   */
  public static String getETag() {
    return cacheETag;
  }

  public int getCount() {
    if (this.resultSet != null) {
      return this.resultSet.size();
    } else {
      return -1;
    }
  }

  @Override
  public UTCDateTime getTimestamp() {
    return null;
  }

  @Override
  public boolean matchIdPattern(String id) {
    return transaction.matchIdPattern(id);
  }
}
