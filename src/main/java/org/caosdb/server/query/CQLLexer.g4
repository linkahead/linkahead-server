/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
lexer grammar CQLLexer;

/** */
AS_A:
    [Aa][Ss] (WHITE_SPACE_f? A)? WHITE_SPACE_f?
;

/** */
IS_REFERENCED:
    (IS_f WHITE_SPACE_f?)? [Rr][Ee][Ff][Ee][Rr][Ee][Nn][Cc][Ee][Dd] WHITE_SPACE_f?
;

/** */
BY:
    [Bb][Yy] WHITE_SPACE_f?
;

/** */
fragment
OF_f:
    [Oo][Ff]
;

/** */
fragment
ANY_f:
    [Aa][Nn][Yy]
;

/** */
fragment
VERSION_f:
    [Vv][Ee][Rr][Ss][Ii][Oo][Nn]
;

/** */
ANY_VERSION_OF:
    (ANY_f WHITE_SPACE_f VERSION_f WHITE_SPACE_f OF_f) WHITE_SPACE_f?
;

/** */
SELECT:
    [Ss][Ee][Ll][Ee][Cc][Tt] WHITE_SPACE_f? -> pushMode(SELECT_MODE)
;

/** */
INSERTED:
    [Ii][Nn][Ss][Ee][Rr][Tt][Ee][Dd] WHITE_SPACE_f?
;

/** */
CREATED:
    [Cc][Rr][Ee][Aa][Tt][Ee][Dd] WHITE_SPACE_f?
;

/** */
UPDATED:
    [Uu][Pp][Dd][Aa][Tt][Ee][Dd] WHITE_SPACE_f?
;

/** */
ON:
    [Oo][Nn] WHITE_SPACE_f?
;

/** */
IN:
    [Ii][Nn] WHITE_SPACE_f?
;

/** */
AFTER:
    [Aa][Ff][Tt][Ee][Rr] WHITE_SPACE_f?
;

/** */
BEFORE:
    [Bb][Ee][Ff][Oo][Rr][Ee] WHITE_SPACE_f?
;

/** */
UNTIL:
    [Uu][Nn][Tt][Ii][Ll] WHITE_SPACE_f?
;

/** */
SINCE:
    [Ss][Ii][Nn][Cc][Ee] WHITE_SPACE_f?
;

/** */
IS_STORED_AT:
    (IS_f WHITE_SPACE_f?)? [Ss][Tt][Oo][Rr][Ee][Dd] (WHITE_SPACE_f? AT)? WHITE_SPACE_f?
;

/** */
AT:
    [Aa][Tt] WHITE_SPACE_f?
;

/** */
FIND:
    [Ff][Ii][Nn][Dd] WHITE_SPACE_f?
;

/** */
COUNT:
    [Cc][Oo][Uu][Nn][Tt] WHITE_SPACE_f?
;

/** */
AND:
    (
        (
            [Aa][Nn][Dd]
        )
        | '&'
    ) WHITE_SPACE_f?
;

/** */
OR:
    (
        (
            [Oo][Rr]
        )
        | '|'
    ) WHITE_SPACE_f?
;

/** */
LPAREN:
    '(' WHITE_SPACE_f?
;

/** */
RPAREN:
    ')' WHITE_SPACE_f?
;

/** */
SINGLE_QUOTE_START:
    '\'' -> pushMode(SINGLE_QUOTE_MODE)
;

/** */
DOUBLE_QUOTE_START:
    '"' -> pushMode(DOUBLE_QUOTE_MODE)
;

/** */
OPERATOR:
    '='
    | '<'
    | '<='
    | '>='
    | '>'
    | '!='
    | '->'
    | [Rr][Ee][Ff][Ee][Rr][Ee][Nn][Cc][Ee]([Ss]|WHITE_SPACE_f? [Tt][Oo]) (WHITE_SPACE_f? A {_input.LA(1) == ' '}?)? {setText("->");}
;

/** */
LIKE:
    [Ll][Ii][Kk][Ee] WHITE_SPACE_f?
;

/** */
IS_NULL:
    IS_f WHITE_SPACE_f NULL_f WHITE_SPACE_f?
;

/** */
IS_NOT_NULL:
    IS_f WHITE_SPACE_f NOT_f WHITE_SPACE_f NULL_f WHITE_SPACE_f?
;

/** */
fragment
NULL_f:
    [Nn][Uu][Ll][Ll]
;

/** */
fragment
DOES_f:
    [Dd][Oo][Ee][Ss]
;

/** */
fragment
NOT_f:
    [Nn][Oo][Tt]
;

/** */
fragment
DOESNT_f:
    DOES_f WHITE_SPACE_f? NOT_f
    | DOES_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
ISNT_f:
    IS_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
WERE_f:
    [Ww][Ee][Rr][Ee]
;

/** */
fragment
WERENT_f:
    WERE_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
HAVENT_f:
    HAVE_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
HADNT_f:
    HAD_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
HAD_f:
    [Hh][Aa][Dd]
;

/** */
fragment
HAVE_f:
    [Hh][Aa][Vv][Ee]
;

/** */
fragment
HAS_f:
    [Hh][Aa][Ss]
;

/** */
fragment
HASNT_f:
    HAS_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
BEEN_f:
    [Bb][Ee][Ee][Nn]
;

/** */
fragment
HAVE_A_f:
    HAVE_f (WHITE_SPACE? A)?
;

/** */
fragment
DO_f:
    [Dd][Oo]
;

/** */
fragment
DONT_f:
    DO_f NOT_f
    | DO_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
fragment
WAS_f:
    [Ww][Aa][Ss]
;

/** */
fragment
WASNT_f:
    WAS_f [Nn] SINGLE_QUOTE [Tt]
;

/** */
NEGATION:
    (
        '!'
        | DOESNT_f (WHITE_SPACE? HAVE_A_f)?
        | DONT_f (WHITE_SPACE? HAVE_A_f)?
        | HASNT_f (WHITE_SPACE? BEEN_f)?
        | ISNT_f (WHITE_SPACE? BEEN_f)?
        | NOT_f (WHITE_SPACE? BEEN_f)?
        | WERENT_f (WHITE_SPACE? BEEN_f)?
        | WASNT_f (WHITE_SPACE? BEEN_f)?
        | HAVENT_f (WHITE_SPACE? BEEN_f)?
        | HADNT_f (WHITE_SPACE? BEEN_f)?
    ) WHITE_SPACE_f?
;

/** */
WITH_A:
    [Ww][Ii][Tt][Hh] (WHITE_SPACE_f? A)? WHITE_SPACE_f?
;

/** */
THE:
    [Tt][Hh][Ee] WHITE_SPACE_f?
;

/** */
GREATEST:
    [Gg][Rr][Ee][Aa][Tt][Ee][Ss][Tt] WHITE_SPACE_f?
;

/** */
SMALLEST:
    [Ss][Mm][Aa][Ll][Ll][Ee][Ss][Tt] WHITE_SPACE_f?
;

/** */
A:
    [Aa][Nn]? WHITE_SPACE_f?
;

/** */
ME:
    [Mm][Ee] WHITE_SPACE_f?
;

/** */
SOMEONE:
    [Ss][Oo][Mm][Ee][Oo][Nn][Ee] WHITE_SPACE_f?
;

/** */
ELSE:
    [Ee][Ll][Ss][Ee] WHITE_SPACE_f?
;

/** */
WHERE:
    [Ww][Hh][Ee][Rr][Ee] WHITE_SPACE_f?
;

/** */
WHICH:
    [Ww][Hh][Ii][Cc][Hh] WHITE_SPACE_f?
;

/** */
HAS_A:
    (
        (HAS_f | HAD_f | HAVE_f | WERE_f | WAS_f | IS_f)
        (
            (WHITE_SPACE_f? A)
            | (WHITE_SPACE_f? BEEN_f)
        )?
    ) WHITE_SPACE_f?
;

/** */
PROPERTY:
    [Pp][Rr][Oo][Pp][Ee][Rr][Tt]([Yy]|[Ii][Ee][Ss]) WHITE_SPACE_f?
;

/** */
RECORDTYPE:
    [Rr][Ee][Cc][Oo][Rr][Dd][Tt][Yy][Pp][Ee]([Ss])? WHITE_SPACE_f?
;

/** */
RECORD:
    [Rr][Ee][Cc][Oo][Rr][Dd]([Ss])? WHITE_SPACE_f?
;

/** */
FILE:
    [Ff][Ii][Ll][Ee]([Ss])? WHITE_SPACE_f?
;

/** */
ENTITY:
    [Ee][Nn][Tt][Ii][Tt]([Yy]|[Ii][Ee][Ss]) WHITE_SPACE_f?
;

/** */
QUERYTEMPLATE:
    [Qq][Uu][Ee][Rr][yY][Tt][Ee][Mm][Pp][Ll][Aa][Tt][Ee] WHITE_SPACE_f?
;

/** */
fragment
IS_f:
    [Ii][Ss]
;

/** */
fragment
WHITE_SPACE_f:
    [ \t\n\r]+
;

/** */
WHITE_SPACE:
    WHITE_SPACE_f
;

/** */
fragment
DOUBLE_QUOTE:
    '"'
;

/** */
fragment
SINGLE_QUOTE:
    '\''
;

/** */
REGEXP_MARKER:
    '#'
;

/** */
REGEXP_BEGIN:
    '<<'
;

/** */
REGEXP_END:
    '>>'
;

/** */
ID:
    [Ii][Dd] WHITE_SPACE_f?
;

/** */
SLASH:
    '/'
;

/** */
STAR:
    '*'
;

/** */
DOT:
    '.'
;

/** */
QMARK:
    '?' WHITE_SPACE_f?
;

/** */
BUT:
    [Bb][Uu][Tt] WHITE_SPACE_f?
;

/** */
ESC_REGEXP_END:
    ESC_MARKER
    '>>' WHITE_SPACE_f?
;

/** */
ESC_STAR:
    ESC_MARKER
    '*' WHITE_SPACE_f?
;

/** */
ESC_BS:
    ESC_MARKER
    '\\' WHITE_SPACE_f?
;

/** */
fragment
ESC_MARKER:
    '\\'
;

/** */
TODAY:
    [Tt][Oo][Dd][Aa][Yy] WHITE_SPACE_f?
;

/** */
COLON:
    ':'
;

/** Matches signed and unsigned numbers with decimal points, also numbers in scientific notation. */
DECIMAL_NUMBER:
    ((HYPHEN_f | PLUS ) WHITE_SPACE_f?)?
    ( NUM_f? DOT NUM_f (WHITE_SPACE_f? E_NOTATION_f)?
    | NUM_f (WHITE_SPACE_f? E_NOTATION_f)?
    )
;

/** */
HYPHEN:
    HYPHEN_f
;

/** */
PLUS:
	'+'
;

/** */
fragment
HYPHEN_f:
	'-'
;

/** Matches only unsigned integer numbers. */
UNSIGNED_INT:
    NUM_f
;

/** */
fragment
NUM_f:
    ('0'..'9')+
;

/** */
fragment
E_NOTATION_f:
	[Ee] WHITE_SPACE_f? [+-]? WHITE_SPACE_f? NUM_f
;

/** */
TXT:
    ('a'..'z' | 'A'..'Z' | NUM_f | '_' | '-' {_input.LA(1) != '>'}? | PLUS | '&' | ';' | ',' | '$' | ':' | '%' | '^' | '~' {_input.LA(1) != '='}? | '`' | '´' | 'ö' | 'ä' | 'ß' | 'ü' | 'Ö' | 'Ä' | 'Ü' | '@' | '[' | ']' | '{' | '}' )+
;

/** */
UNKNOWN_CHAR: . ;

/** */
mode SINGLE_QUOTE_MODE;

    /** */
    SINGLE_QUOTE_ESCAPED_CHAR:
        ESC_MARKER
        ( '\'' | '\\' | '*' )
    ;

    /** */
    SINGLE_QUOTE_END:
        '\'' -> mode(DEFAULT_MODE)
    ;

    /** */
    SINGLE_QUOTE_STAR:
        '*'
    ;

    /** */
    SINGLE_QUOTE_ANY_CHAR:
        ~('\''|'\\'|'*')+
    ;

/** */
mode DOUBLE_QUOTE_MODE;

    /** */
    DOUBLE_QUOTE_ESCAPED_CHAR:
        ESC_MARKER
        ( '"' | '\\' | '*' )
    ;

    /** */
    DOUBLE_QUOTE_END:
        '"' -> mode(DEFAULT_MODE)
    ;

    /** */
    DOUBLE_QUOTE_STAR:
        '*'
    ;

    /** */
    DOUBLE_QUOTE_ANY_CHAR:
        ~('"'|'\\'|'*')+
    ;


/** */
mode SELECT_DOUBLE_QUOTED;

    /** */
    SELECT_DOUBLE_QUOTE_ESCAPED:
        ESC_MARKER
        '"'
    ;

    /** */
    SELECT_DOUBLE_QUOTE_END:
        '"' WHITE_SPACE_f? {setText("");} -> mode(SELECT_MODE)
    ;

    /** */
    SELECT_DOUBLE_QUOTE_TXT:
        .
    ;

/** */
mode SELECT_SINGLE_QUOTED;

    /** */
    SELECT_SINGLE_QUOTE_ESCAPED:
        ESC_MARKER
        '\''
    ;

    /** */
    SELECT_SINGLE_QUOTE_END:
        '\'' WHITE_SPACE_f? {setText("");} -> mode(SELECT_MODE)
    ;

    /** */
    SELECT_SINGLE_QUOTE_TXT:
        .
    ;

/** */
mode SELECT_MODE;

    /** */
    FROM:
         [Ff][Rr][Oo][Mm]([ \t\n\r])* -> mode(DEFAULT_MODE)
    ;

    /** */
    SELECT_ESCAPED:
        ESC_MARKER
        ( '"' | '\\' | '\'' | ',' | '.' ) {setText(getText().substring(1));}
    ;

    /** */
    SELECT_DOT:
        '.'
        WHITE_SPACE_f?
    ;

    /** */
    SELECT_DOUBLE_QUOTE:
        '"' {setText("");} -> mode(SELECT_DOUBLE_QUOTED)
    ;

    /** */
    SELECT_SINGLE_QUOTE:
        '\'' {setText("");} -> mode(SELECT_SINGLE_QUOTED)
    ;

    /** */
    SELECT_COMMA:
        ','
        WHITE_SPACE_f?
    ;

    /** */
    SELECTOR_TXT:
        .
    ;
