/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.query;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.query.Query.QueryException;
import org.jdom2.Element;

public class Disjunction extends EntityFilterContainer
    implements QueryInterface, EntityFilterInterface {

  private String targetSet = null;
  private int targetSetCount = -1;
  private QueryInterface query = null;

  @Override
  public void apply(final QueryInterface query) throws QueryException {
    final long t1 = System.currentTimeMillis();
    this.query = query;
    try {
      // need new targetSet?
      if (query.getTargetSet() == null) {
        // initDisjunctionFilter() - returns name of new empty
        // targetTable
        // which will be used to collect the entities.
        final PreparedStatement callInitDisjunctionFilter =
            getConnection().prepareStatement("call initDisjunctionFilter(?)");
        callInitDisjunctionFilter.setBoolean(1, this.isVersioned());
        final ResultSet initDisjuntionFilterResultSet = callInitDisjunctionFilter.executeQuery();
        if (initDisjuntionFilterResultSet.next()) {
          this.targetSet = bytes2UTF8(initDisjuntionFilterResultSet.getBytes("newTableName"));
        }
        initDisjuntionFilterResultSet.close();
      } else {
        // use query.getTargetSet() as targetSet
        this.targetSet = query.getTargetSet();
      }

      // apply filters
      for (final EntityFilterInterface filter : getFilters()) {
        filter.apply(this);
      }

      if (query.getTargetSet() == null) {
        // calculate the difference and store to sourceSet
        final CallableStatement callFinishDisjunctionFilter =
            getConnection().prepareCall("call calcIntersection(?,?,?)");
        callFinishDisjunctionFilter.setString(1, getSourceSet());
        callFinishDisjunctionFilter.setString(2, this.targetSet);
        callFinishDisjunctionFilter.setBoolean(3, this.isVersioned());
        callFinishDisjunctionFilter.execute();
      }
      // ELSE: the query.getTargetSet() is identical to targetSet and is
      // not
      // to be merged with the query.getSourceSet()
    } catch (final SQLException e) {
      throw new QueryException(e);
    }
    query.addBenchmark(getClass().getSimpleName(), System.currentTimeMillis() - t1);
  }

  @Override
  public String getSourceSet() {
    return this.query.getSourceSet();
  }

  @Override
  public Connection getConnection() {
    return this.query.getConnection();
  }

  @Override
  public int getTargetSetCount() {
    return this.targetSetCount;
  }

  public void setTargetSetCount(final int resultSetCount) {
    this.targetSetCount = resultSetCount;
  }

  @Override
  public Element toElement() {
    final Element ret = new Element("Disjunction");
    for (final EntityFilterInterface filter : getFilters()) {
      ret.addContent(filter.toElement());
    }
    return ret;
  }

  @Override
  public Query getQuery() {
    return this.query.getQuery();
  }

  @Override
  public String getTargetSet() {
    return this.targetSet;
  }

  @Override
  public Access getAccess() {
    return this.query.getAccess();
  }

  @Override
  public Subject getUser() {
    return this.query.getUser();
  }

  @Override
  public void addBenchmark(final String str, final long time) {
    this.query.addBenchmark(this.getClass().getSimpleName() + "." + str, time);
  }

  @Override
  public boolean isVersioned() {
    return this.query.isVersioned();
  }

  @Override
  public String getCacheKey() {
    StringBuilder sb = new StringBuilder();
    sb.append("Disj(");
    for (EntityFilterInterface filter : getFilters()) {
      sb.append(filter.getCacheKey());
    }
    sb.append(")");
    return sb.toString();
  }
}
