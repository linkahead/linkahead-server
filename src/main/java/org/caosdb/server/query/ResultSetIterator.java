package org.caosdb.server.query;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.bytes2UTF8;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.query.Query.IdVersionAclTriplet;

/**
 * A class for iterating over {@link ResultSet}
 *
 * <p>{@link ResultSet} only provides a `next` function which moves the cursor. The behavior is here
 * mapped onto the functions of the Iterator interface. TODO Move this generic function? Check again
 * if an implementation is available from elsewhere.
 */
public class ResultSetIterator implements Iterator<IdVersionAclTriplet> {
  public ResultSetIterator(final ResultSet resultset) {
    this.resultSet = resultset;
  }

  private ResultSet resultSet;
  private boolean cursorHasMoved = false;
  private boolean currentIsValid = true;

  public boolean hasNext() {
    if (!this.cursorHasMoved) {
      try {
        this.currentIsValid = this.resultSet.next();
      } catch (SQLException e) {
        throw new TransactionException(e);
      }
      this.cursorHasMoved = true;
    }
    return this.currentIsValid;
  }
  ;

  public IdVersionAclTriplet next() {
    if (!this.cursorHasMoved) {
      try {
        this.currentIsValid = this.resultSet.next();
      } catch (SQLException e) {
        throw new TransactionException(e);
      }
    }
    this.cursorHasMoved = false;
    if (!this.currentIsValid) {
      throw new NoSuchElementException();
    }
    try {
      final String id = resultSet.getString("id");
      final String acl_str = bytes2UTF8(resultSet.getBytes("ACL"));
      return new IdVersionAclTriplet(id, "", acl_str);
    } catch (SQLException e) {
      throw new TransactionException(e);
    }
  }
}
