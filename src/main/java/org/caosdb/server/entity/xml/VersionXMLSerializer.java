/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.entity.xml;

import java.util.TimeZone;
import org.caosdb.server.entity.Version;
import org.jdom2.Element;

/**
 * Creates a JDOM Element for a Version instance.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
class VersionXMLSerializer {

  public Element toElement(Version version) {
    return toElement(version, "Version");
  }

  private Element toElement(Version version, String tag) {
    Element element = new Element(tag);
    setAttributes(version, element);
    if (version.getPredecessors() != null) {
      for (Version p : version.getPredecessors()) {
        element.addContent(toElement(p, "Predecessor"));
      }
    }
    if (version.getSuccessors() != null) {
      for (Version s : version.getSuccessors()) {
        element.addContent(toElement(s, "Successor"));
      }
    }
    return element;
  }

  private void setAttributes(Version version, Element element) {
    element.setAttribute("id", version.getId());
    if (version.getUsername() != null) {
      element.setAttribute("username", version.getUsername());
    }
    if (version.getRealm() != null) {
      element.setAttribute("realm", version.getRealm());
    }
    if (version.getDate() != null) {
      element.setAttribute("date", version.getDate().toDateTimeString(TimeZone.getDefault()));
    }
    if (version.isHead()) {
      element.setAttribute("head", "true");
    }
    if (version.isCompleteHistory()) {
      element.setAttribute("completeHistory", "true");
    }
  }
}
