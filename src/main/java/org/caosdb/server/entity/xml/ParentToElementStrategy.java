/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.wrapper.Parent;
import org.jdom2.Element;

/**
 * Generates a JDOM (XML) representation of an entity's parent.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class ParentToElementStrategy extends EntityToElementStrategy {

  public ParentToElementStrategy() {
    super("Parent");
  }

  @Override
  public Element toElement(
      final EntityInterface entity, final SerializeFieldStrategy setFieldStrategy) {
    final Element element = new Element(this.tagName);
    sparseEntityToElement(element, entity, setFieldStrategy);
    final Parent parent = (Parent) entity;
    if (parent.getAffiliation() != null) {
      element.setAttribute("affiliation", parent.getAffiliation().toString());
    }
    return element;
  }
}
