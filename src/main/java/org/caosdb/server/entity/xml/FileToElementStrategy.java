/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.xml;

import org.caosdb.server.entity.EntityInterface;
import org.jdom2.Attribute;
import org.jdom2.Element;

public class FileToElementStrategy extends EntityToElementStrategy {

  public FileToElementStrategy() {
    super("File");
  }

  @Override
  public Element toElement(
      final EntityInterface entity, final SerializeFieldStrategy serializeFieldStrategy) {
    final Element element = super.toElement(entity, serializeFieldStrategy);

    if (entity.hasFileProperties()) {
      if (serializeFieldStrategy.isToBeSet("checksum")
          && entity.getFileProperties().hasChecksum()) {
        element.setAttribute(new Attribute("checksum", entity.getFileProperties().getChecksum()));
      }
      if (serializeFieldStrategy.isToBeSet("path") && entity.getFileProperties().hasPath()) {
        element.setAttribute(new Attribute("path", "/" + entity.getFileProperties().getPath()));
      }
      if (serializeFieldStrategy.isToBeSet("size") && entity.getFileProperties().hasSize()) {
        element.setAttribute(
            new Attribute("size", Long.toString(entity.getFileProperties().getSize())));
      }
    }
    return element;
  }
}
