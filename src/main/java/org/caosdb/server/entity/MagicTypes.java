/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import java.util.HashMap;
import java.util.Objects;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.transaction.Retrieve;

/** Some types correspond to entities in the database with magic IDs. */
public enum MagicTypes {
  UNIT,
  NAME,
  DESCRIPTION;

  private static final EntityID UNIT_ID = new EntityID("21");
  private static final EntityID DESCRIPTION_ID = new EntityID("24");
  private static final EntityID NAME_ID = new EntityID("20");

  public EntityID getId() {
    switch (this) {
      case UNIT:
        return UNIT_ID;
      case NAME:
        return NAME_ID;
      case DESCRIPTION:
        return DESCRIPTION_ID;
      default:
        return null;
    }
  }

  public static MagicTypes getType(final EntityID id) {
    switch (id.toString()) {
      case "21":
        return UNIT;
      case "20":
        return NAME;
      case "24":
        return DESCRIPTION;
      default:
        return null;
    }
  }

  private static HashMap<MagicTypes, EntityInterface> entities = null;

  public EntityInterface getEntity() {
    if (entities == null) {
      try {
        entities = loadMagicTypes();
      } catch (final Exception e) {
        e.printStackTrace();
        // here is something very wrong
        System.exit(1);
      }
    }
    return entities.get(this);
  }

  private static HashMap<MagicTypes, EntityInterface> loadMagicTypes() throws Exception {
    final RetrieveContainer container =
        new RetrieveContainer(null, System.currentTimeMillis(), null, null);
    for (final MagicTypes mt : MagicTypes.values()) {
      container.add(mt.getId(), null);
    }
    final Retrieve retrieve = new Retrieve(container);
    retrieve.execute();

    final HashMap<MagicTypes, EntityInterface> ret = new HashMap<MagicTypes, EntityInterface>();
    for (final MagicTypes mt : MagicTypes.values()) {
      for (final EntityInterface e : container) {
        if (Objects.equals(e.getId(), mt.getId())) {
          ret.put(mt, e);
        }
      }
    }
    return ret;
  }
}
