/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.wrapper;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.database.proto.SparseEntity;
import org.caosdb.server.database.proto.VerySparseEntity;
import org.caosdb.server.datatype.AbstractDatatype;
import org.caosdb.server.datatype.Value;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.StatementStatusInterface;
import org.caosdb.server.entity.Version;
import org.caosdb.server.entity.container.ParentContainer;
import org.caosdb.server.entity.container.PropertyContainer;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.permissions.EntityACL;
import org.caosdb.server.query.Query.Selection;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.TransactionLogMessage;
import org.caosdb.unit.Unit;
import org.jdom2.Element;

public abstract class EntityWrapper implements EntityInterface {

  protected EntityInterface entity;
  private ToElementStrategy s;

  public EntityInterface getWrapped() {
    return this.entity;
  }

  public EntityWrapper(final EntityInterface entity) {
    this.entity = entity;
  }

  @Override
  public void checkPermission(final Subject subject, final Permission permission) {
    this.entity.checkPermission(subject, permission);
  }

  @Override
  public void checkPermission(final Permission permission) {
    this.entity.checkPermission(permission);
  }

  @Override
  public boolean hasEntityACL() {
    return this.entity.hasEntityACL();
  }

  @Override
  public EntityACL getEntityACL() {
    return this.entity.getEntityACL();
  }

  @Override
  public void setEntityACL(final EntityACL acl) {
    this.entity.setEntityACL(acl);
  }

  @Override
  public SparseEntity getSparseEntity() {
    return this.entity.getSparseEntity();
  }

  @Override
  public EntityID getId() {
    return this.entity.getId();
  }

  @Override
  public void setId(final EntityID id) {
    this.entity.setId(id);
  }

  @Override
  public boolean hasId() {
    return this.entity.hasId();
  }

  @Override
  public StatementStatusInterface getStatementStatus() {
    return this.entity.getStatementStatus();
  }

  @Override
  public void setStatementStatus(final StatementStatusInterface replacement) {
    this.entity.setStatementStatus(replacement);
  }

  @Override
  public void setStatementStatus(final String statementStatus) {
    this.entity.setStatementStatus(statementStatus);
  }

  @Override
  public boolean hasStatementStatus() {
    return this.entity.hasStatementStatus();
  }

  @Override
  public EntityStatus getEntityStatus() {
    return this.entity.getEntityStatus();
  }

  @Override
  public void setEntityStatus(final EntityStatus entityStatus) {
    this.entity.setEntityStatus(entityStatus);
  }

  @Override
  public boolean hasEntityStatus() {
    return this.entity.hasEntityStatus();
  }

  @Override
  public boolean hasCuid() {
    return this.entity.hasCuid();
  }

  @Override
  public String getCuid() {
    return this.entity.getCuid();
  }

  @Override
  public void setCuid(final String cuid) {
    this.entity.setCuid(cuid);
  }

  @Override
  public void addProperty(final Property property) {
    this.entity.addProperty(property);
  }

  @Override
  public boolean hasProperties() {
    return this.entity.hasProperties();
  }

  @Override
  public PropertyContainer getProperties() {
    return this.entity.getProperties();
  }

  @Override
  public void addParent(final Parent parent) {
    this.entity.addParent(parent);
  }

  @Override
  public boolean hasParents() {
    return this.entity.hasParents();
  }

  @Override
  public ParentContainer getParents() {
    return this.entity.getParents();
  }

  @Override
  public void setName(final String name) {
    this.entity.setName(name);
  }

  @Override
  public String getName() {
    return this.entity.getName();
  }

  @Override
  public boolean hasName() {
    return this.entity.hasName();
  }

  @Override
  public String getDescription() {
    return this.entity.getDescription();
  }

  @Override
  public void setDescription(final String description) {
    this.entity.setDescription(description);
  }

  @Override
  public boolean hasDescription() {
    return this.entity.hasDescription();
  }

  @Override
  public Role getRole() {
    return this.entity.getRole();
  }

  @Override
  public void setRole(final Role role) {
    this.entity.setRole(role);
  }

  @Override
  public void setRole(final String role) throws IllegalArgumentException {
    this.entity.setRole(role);
  }

  @Override
  public boolean hasRole() {
    return this.entity.hasRole();
  }

  @Override
  public Value getValue() {
    return this.entity.getValue();
  }

  @Override
  public void setValue(final Value value) {
    this.entity.setValue(value);
  }

  @Override
  public boolean hasValue() {
    return this.entity.hasValue();
  }

  @Override
  public AbstractDatatype getDatatype() {
    return this.entity.getDatatype();
  }

  @Override
  public void setDatatype(final AbstractDatatype datatype) {
    this.entity.setDatatype(datatype);
  }

  @Override
  public void setDatatype(final String datatype) {
    this.entity.setDatatype(datatype);
  }

  @Override
  public boolean hasDatatype() {
    return this.entity.hasDatatype();
  }

  @Override
  public void setToElementStragegy(final ToElementStrategy s) {
    this.s = s;
  }

  @Override
  public Element toElement() {
    return this.entity.toElement();
  }

  @Override
  public void addToElement(final Element element) {
    this.entity.addToElement(element);
  }

  @Override
  public FileProperties getFileProperties() {
    return this.entity.getFileProperties();
  }

  @Override
  public void setFileProperties(final FileProperties fileProperties) {
    this.entity.setFileProperties(fileProperties);
  }

  @Override
  public boolean hasFileProperties() {
    return this.entity.hasFileProperties();
  }

  @Override
  public Set<ToElementable> getMessages() {
    return this.entity.getMessages();
  }

  @Override
  public boolean hasMessages() {
    return this.entity.hasMessages();
  }

  @Override
  public boolean hasMessage(final String type) {
    return this.entity.hasMessage(type);
  }

  @Override
  public void removeMessage(final Message m) {
    this.entity.removeMessage(m);
  }

  @Override
  public List<Message> getMessages(final String type) {
    return this.entity.getMessages(type);
  }

  @Override
  public void addMessage(final ToElementable m) {
    this.entity.addMessage(m);
  }

  @Override
  public void addError(final Message m) {
    this.entity.addError(m);
  }

  @Override
  public void addInfo(final String description) {
    this.entity.addInfo(description);
  }

  @Override
  public void addInfo(final Message m) {
    this.entity.addInfo(m);
  }

  @Override
  public void parseValue() throws Message {
    this.entity.parseValue();
  }

  @Override
  public void parseFromElement(final Element element) {
    this.entity.parseFromElement(element);
  }

  @Override
  public void addWarning(final Message m) {
    this.entity.addWarning(m);
  }

  @Override
  public void addProperty(final List<Property> properties) {
    this.entity.addProperty(properties);
  }

  @Override
  public void setProperties(final PropertyContainer properties) {
    this.entity.setProperties(properties);
  }

  @Override
  public Map<String, String> getFlags() {
    return this.entity.getFlags();
  }

  @Override
  public void setFlag(final String key, final String value) {
    this.entity.setFlag(key, value);
  }

  @Override
  public EntityInterface setDescOverride(final boolean b) {
    return this.entity.setDescOverride(b);
  }

  @Override
  public boolean isDescOverride() {
    return this.entity.isDescOverride();
  }

  @Override
  public EntityInterface setNameOverride(final boolean b) {
    return this.entity.setNameOverride(b);
  }

  @Override
  public boolean isNameOverride() {
    return this.entity.isNameOverride();
  }

  @Override
  public EntityInterface setDatatypeOverride(final boolean b) {
    return this.entity.setDatatypeOverride(b);
  }

  @Override
  public boolean isDatatypeOverride() {
    return this.entity.isDatatypeOverride();
  }

  @Override
  public void addTransactionLog(final TransactionLogMessage transactionLogMessage) {
    this.entity.addTransactionLog(transactionLogMessage);
  }

  @Override
  public List<TransactionLogMessage> getTransactionLogMessages() {
    return this.entity.getTransactionLogMessages();
  }

  @Override
  public boolean hasTransactionLogMessages() {
    return this.entity.hasTransactionLogMessages();
  }

  @Override
  public String getFlag(final String key) {
    return this.entity.getFlag(key);
  }

  @Override
  public ToElementStrategy getToElementStrategy() {
    if (this.s != null) {
      return this.s;
    }
    return this.entity.getToElementStrategy();
  }

  @Override
  public void setUnit(final Unit unit) {
    this.entity.setUnit(unit);
  }

  @Override
  public Unit getUnit() {
    return this.entity.getUnit();
  }

  @Override
  public boolean hasUnit() {
    return this.entity.hasUnit();
  }

  @Override
  public VerySparseEntity getVerySparseEntity() {
    return this.entity.getVerySparseEntity();
  }

  @Override
  public EntityID getDomain() {
    return this.entity.getDomain();
  }

  @Override
  public EntityInterface parseSparseEntity(final SparseEntity spe) {
    return this.entity.parseSparseEntity(spe);
  }

  @Override
  public EntityInterface linkIdToEntity(final EntityInterface link) {
    return this.entity.linkIdToEntity(link);
  }

  @Override
  public boolean acceptObserver(final Observer o) {
    return this.entity.acceptObserver(o);
  }

  @Override
  public void notifyObservers(final String e) {
    this.entity.notifyObservers(e);
  }

  @Override
  public List<Selection> getSelections() {
    return this.entity.getSelections();
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj instanceof EntityInterface) {
      return obj.equals(this.entity);
    }
    return super.equals(obj);
  }

  @Override
  public String getQueryTemplateDefinition() {
    return this.entity.getQueryTemplateDefinition();
  }

  @Override
  public void setQueryTemplateDefinition(final String query) {
    this.entity.setQueryTemplateDefinition(query);
  }

  @Override
  public boolean skipJob() {
    return this.entity.skipJob();
  }

  @Override
  public boolean hasPermission(final Permission permission) {
    return this.entity.hasPermission(permission);
  }

  @Override
  public boolean hasPermission(final Subject subject, final Permission permission) {
    return this.entity.hasPermission(subject, permission);
  }

  @Override
  public Version getVersion() {
    return this.entity.getVersion();
  }

  @Override
  public boolean hasVersion() {
    return this.entity.hasVersion();
  }

  @Override
  public void setVersion(final Version version) {
    this.entity.setVersion(version);
  }

  @Override
  public String getIdVersion() {
    return this.entity.getIdVersion();
  }

  @Override
  public void addToElement(final Element element, final SerializeFieldStrategy strategy) {
    this.entity.addToElement(element, strategy);
  }

  @Override
  public boolean isReference() {
    return this.entity.isReference();
  }

  @Override
  public boolean isReferenceList() {
    return this.entity.isReferenceList();
  }

  @Override
  public void setSerializeFieldStrategy(SerializeFieldStrategy s) {
    // @review Florian Spreckelsen 2022-03-22
    this.entity.setSerializeFieldStrategy(s);
  }

  @Override
  public SerializeFieldStrategy getSerializeFieldStrategy() {
    // @review Florian Spreckelsen 2022-03-22
    return this.entity.getSerializeFieldStrategy();
  }
}
