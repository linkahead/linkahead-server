/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.query.Query.Selection;
import org.caosdb.server.utils.EntityStatus;
import org.jdom2.Element;

public interface TransactionEntity {

  public abstract EntityStatus getEntityStatus();

  public abstract void setEntityStatus(EntityStatus entityStatus);

  public abstract boolean hasEntityStatus();

  public abstract boolean hasCuid();

  public abstract String getCuid();

  public abstract void setCuid(String cuid);

  public abstract void setToElementStragegy(ToElementStrategy s);

  public abstract void setSerializeFieldStrategy(SerializeFieldStrategy s);

  public abstract Element toElement();

  public abstract Set<ToElementable> getMessages();

  public abstract boolean hasMessages();

  public abstract boolean hasMessage(String type);

  public abstract void removeMessage(Message m);

  public abstract List<Message> getMessages(String type);

  public abstract void addMessage(ToElementable m);

  public abstract void addError(Message m);

  public abstract void addInfo(String description);

  public abstract void addInfo(Message m);

  public abstract void addWarning(Message m);

  public abstract ToElementStrategy getToElementStrategy();

  public abstract Map<String, String> getFlags();

  public abstract void setFlag(String key, String value);

  public abstract String getFlag(String key);

  public abstract List<Selection> getSelections();
}
