/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity.container;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.entity.Message.MessageType;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.jobs.JobTarget;
import org.caosdb.server.query.Query;
import org.caosdb.server.utils.EntityStatus;
import org.jdom2.Element;

/**
 * Base class for transaction containers used by classes such as {@link RetrieveContainer} or {@link
 * WritableContainer}.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class TransactionContainer extends Container<EntityInterface>
    implements ToElementable, JobTarget {

  private EntityStatus status = EntityStatus.QUALIFIED;
  private final Subject owner;
  private final Long timestamp;
  private final String requestId;

  public TransactionContainer() {
    this(null, null, null);
  }

  public TransactionContainer(final Subject owner, final Long timestamp, final String requestId) {
    this.requestId = requestId;
    this.owner = owner;
    this.timestamp = timestamp;
  }

  public TransactionContainer(
      final Subject user,
      final Long timestamp,
      final String srid,
      final HashMap<String, String> flags) {
    this(user, timestamp, srid);
    setFlags(flags);
  }

  public String getRequestId() {
    return this.requestId;
  }

  public Long getTimestamp() {
    return this.timestamp;
  }

  public EntityStatus getStatus() {
    return this.status;
  }

  public void setStatus(final EntityStatus status) {
    this.status = status;
  }

  private final LinkedList<ToElementable> messages = new LinkedList<>();

  public List<ToElementable> getMessages() {
    return messages;
  }

  public void addMessage(final ToElementable m) {
    this.messages.add(m);
  }

  private HashMap<String, String> flags = null;

  public HashMap<String, String> getFlags() {
    return this.flags;
  }

  private static final long serialVersionUID = -6062911849319971397L;

  /**
   * @param element
   * @return Add List of Elements which represent the contained Entities to an predefined element.
   */
  @Override
  public void addToElement(final Element element) {
    element.setAttribute("count", Integer.toString(size()));
    if (this.benchmark != null && CaosDBServer.isDebugMode()) {
      element.addContent(this.benchmark.toElement());
    }
    for (final ToElementable m : this.messages) {
      m.addToElement(element);
    }
    for (final EntityInterface entity : this) {
      entity.addToElement(element);
    }
  }

  /** The files that have been uploaded. */
  private HashMap<String, FileProperties> files = new HashMap<String, FileProperties>();

  private TransactionBenchmark benchmark;
  private Query query;

  public void setFiles(final HashMap<String, FileProperties> files) {
    this.files = files;
  }

  public HashMap<String, FileProperties> getFiles() {
    return this.files;
  }

  public void addFile(String name, final FileProperties file) {
    if (name.endsWith("/")) {
      name = name.substring(0, name.length() - 1);
    }
    if (!name.contains("/")) {
      this.files.put(name, file);
    }
  }

  public Subject getOwner() {
    return this.owner;
  }

  /**
   * Get the first entity from this container which has this name.
   *
   * <p>Return null if no matching is in this container.
   *
   * @param name
   */
  public EntityInterface getEntityByName(final String name) {
    if (name != null) {
      for (final EntityInterface e : this) {
        if (e.hasName() && e.getName().equals(name)) {
          return e;
        }
      }
    }
    return null;
  }

  public void setFlags(final HashMap<String, String> flags) {
    this.flags = flags;
  }

  public TransactionBenchmark getTransactionBenchmark() {
    if (this.benchmark == null) {
      this.benchmark = TransactionBenchmark.getRootInstance().createBenchmark(getClass());
    }
    return this.benchmark;
  }

  @Override
  public boolean skipJob() {
    return false;
  }

  @Override
  public void addError(Message m) {
    setStatus(EntityStatus.UNQUALIFIED);
    if (m.getType().equalsIgnoreCase(MessageType.Error.toString())) {
      addMessage(m);
    } else {
      addMessage(new Message(MessageType.Error, m.getCode(), m.getDescription(), m.getBody()));
    }
  }

  public void setQuery(Query query) {
    this.query = query;
  }

  public Query getQuery() {
    return query;
  }
}
