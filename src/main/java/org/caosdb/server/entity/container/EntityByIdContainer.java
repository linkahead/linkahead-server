/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.container;

import java.util.HashMap;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.entity.EntityID;

public abstract class EntityByIdContainer extends TransactionContainer {
  private static final long serialVersionUID = 7997517210502207523L;

  public EntityByIdContainer(
      final Subject user,
      final Long timestamp,
      final String srid,
      final HashMap<String, String> flags) {
    super(user, timestamp, srid, flags);
  }

  public abstract void add(EntityID id, String version);
}
