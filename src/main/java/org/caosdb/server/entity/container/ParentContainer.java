/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity.container;

import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.wrapper.Parent;
import org.caosdb.server.entity.xml.ParentToElementStrategy;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.entity.xml.ToElementStrategy;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class ParentContainer extends Container<Parent> {

  static ToElementStrategy s = new ParentToElementStrategy();

  private static final long serialVersionUID = 3334236170549992385L;

  private final EntityInterface child;

  public EntityInterface getChild() {
    return this.child;
  }

  public ParentContainer(final EntityInterface child) {
    this.child = child;
  }

  public Element addToElement(final Element element) {
    final SerializeFieldStrategy serializeFieldStrategy =
        new SerializeFieldStrategy(this.child.getSelections()).forProperty("parent");
    for (final EntityInterface entity : this) {
      s.addToElement(entity, element, serializeFieldStrategy);
    }
    return element;
  }

  @Override
  public void add(final int index, final Parent parent) {
    addStatusObserver(this.child, parent);
    super.add(index, parent);
  }

  @Override
  public boolean add(final Parent parent) {
    addStatusObserver(this.child, parent);
    return super.add(parent);
  }

  private boolean doCheck(final EntityInterface entity, final EntityInterface parent) {
    if (entity.getEntityStatus() == EntityStatus.UNQUALIFIED) {
      return false;
    }

    switch (parent.getEntityStatus()) {
      case UNQUALIFIED:
      case DELETED:
      case NONEXISTENT:
        entity.addError(ServerMessages.ENTITY_HAS_UNQUALIFIED_PARENTS);
        entity.setEntityStatus(EntityStatus.UNQUALIFIED);
        return false;
      case VALID:
        return false;
      default:
        return true;
    }
  }

  private void addStatusObserver(final EntityInterface entity, final Parent parent) {
    if (doCheck(entity, parent)) {
      parent.acceptObserver(
          new Observer() {
            @Override
            public boolean notifyObserver(final String e, final Observable sender) {
              if (entity.getEntityStatus() == EntityStatus.UNQUALIFIED) {
                return false;
              }
              if (e == Entity.ENTITY_STATUS_CHANGED_EVENT) {
                return doCheck(entity, parent);
              }
              return true;
            }
          });
    }
  }
}
