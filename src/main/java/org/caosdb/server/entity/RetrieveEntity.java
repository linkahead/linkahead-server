/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 *   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

/**
 * Entity which is to be retrieved (i.e. read-only).
 *
 * <p>This class only exposes those constructors which are necessary for Entity which are to be
 * retrieved (e.g. Entity(id)) and hide the others (e.g. Entity(XML-Represenation)).
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class RetrieveEntity extends Entity {

  public RetrieveEntity() {
    super();
  }

  public RetrieveEntity(final EntityID id) {
    super(id);
  }

  public RetrieveEntity(final EntityID id, final Role role) {
    super(id, role);
  }

  public RetrieveEntity(final String name) {
    super(name);
  }

  public RetrieveEntity(final EntityID id, final String version) {
    super(id);
    this.setVersion(new Version(version));
  }

  public RetrieveEntity(final EntityID id, String name, final String version) {
    this(id, version);
    this.setName(name);
  }

  public RetrieveEntity(final String name, final String version) {
    super(name);
    this.setVersion(new Version(version));
  }

  public RetrieveEntity(final String name, final Role role) {
    super(name, role);
  }
}
