/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.entity;

import java.util.HashMap;
import java.util.Map;
import org.caosdb.api.entity.v1.MessageCode;
import org.caosdb.server.entity.xml.ToElementable;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Element;

public class Message extends Exception implements Comparable<Message>, ToElementable {

  protected final String type;
  private final MessageCode code;
  private final String description;
  private final String body;

  @Override
  public String getMessage() {
    return description;
  }

  @Deprecated private static final Map<String, String> legacy_codes_mapping = new HashMap<>();

  static void init() {
    legacy_codes_mapping.put(
        ServerMessages.ENTITY_HAS_BEEN_DELETED_SUCCESSFULLY.coreToString(), "10");
    legacy_codes_mapping.put(ServerMessages.ATOMICITY_ERROR.coreToString(), "12");
    legacy_codes_mapping.put(ServerMessages.ENTITY_DOES_NOT_EXIST.coreToString(), "101");
    legacy_codes_mapping.put(ServerMessages.PROPERTY_HAS_NO_DATATYPE.coreToString(), "110");
    legacy_codes_mapping.put(
        ServerMessages.ENTITY_HAS_UNQUALIFIED_PROPERTIES.coreToString(), "114");
    legacy_codes_mapping.put(ServerMessages.ENTITY_HAS_UNQUALIFIED_PARENTS.coreToString(), "116");
    legacy_codes_mapping.put(ServerMessages.WARNING_OCCURED.coreToString(), "128");
    legacy_codes_mapping.put(ServerMessages.ENTITY_NAME_IS_NOT_UNIQUE.coreToString(), "152");
    legacy_codes_mapping.put(ServerMessages.REQUIRED_BY_UNQUALIFIED.coreToString(), "192");
    legacy_codes_mapping.put(ServerMessages.REFERENCED_ENTITY_DOES_NOT_EXIST.coreToString(), "235");
    legacy_codes_mapping.put(ServerMessages.AUTHORIZATION_ERROR.coreToString(), "403");
    legacy_codes_mapping.put(ServerMessages.ROLE_DOES_NOT_EXIST.coreToString(), "1104");
    legacy_codes_mapping.put(ServerMessages.ENTITY_NAME_DUPLICATES.coreToString(), "0");
  }

  private static final long serialVersionUID = 1837110172902899264L;

  public enum MessageType {
    Warning,
    Error,
    Info
  };

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

  private String coreToString() {
    return " ("
        + (this.code != null ? this.code.toString() : "0")
        + ") - "
        + (this.description != null ? this.description : "");
  }

  @Override
  public String toString() {
    return this.type.toString() + coreToString();
  }

  @Override
  public boolean equals(final Object obj) {
    return obj.toString().equals(toString());
  }

  public final String getDescription() {
    return this.description;
  }

  public final String getBody() {
    return this.body;
  }

  public final String getType() {
    return this.type;
  }

  public Message(final String type, final String description, final String body) {
    this(type, null, description, body);
  }

  public Message(final String type, final MessageCode code) {
    this(type, code, null, null);
  }

  public Message(final MessageCode code, final String description) {
    this(MessageType.Info, code, description);
  }

  public Message(final MessageType type, final MessageCode code, final String description) {
    this(type.toString(), code, description, null);
  }

  public Message(
      final MessageType type, final MessageCode code, final String description, final String body) {
    this(type.toString(), code, description, body);
  }

  public Message(final String type, final MessageCode code, final String description) {
    this(type, code, description, null);
  }

  public Message(final MessageType type, final String description) {
    this(type.toString(), MessageCode.MESSAGE_CODE_UNKNOWN, description);
  }

  public Message(
      final String type, final MessageCode code, final String description, final String body) {
    this.code = code;
    this.description = description;
    this.body = body;
    this.type = type;
  }

  public Message(
      final MessageCode code, final String description, final String body, final MessageType type) {
    this.code = code;
    this.description = description;
    this.body = body;
    this.type = type.toString();
  }

  public Message(final String string) {
    this.code = MessageCode.MESSAGE_CODE_UNKNOWN;
    this.description = string;
    this.body = null;
    this.type = MessageType.Info.toString();
  }

  public MessageCode getCode() {
    return this.code;
  }

  public Element toElement() {
    if (legacy_codes_mapping.isEmpty()) {
      init();
    }
    final Element e = new Element(this.type);
    if (this.code != null) {
      if (legacy_codes_mapping.containsKey(this.coreToString())) {
        e.setAttribute("code", legacy_codes_mapping.get(this.coreToString()));
      } else if (this.code == MessageCode.MESSAGE_CODE_UNKNOWN) {
        e.setAttribute("code", "0");
      } else {
        e.setAttribute("code", Integer.toString(this.code.getNumber()));
      }
    }
    if (this.description != null) {
      e.setAttribute("description", this.description);
    }
    if (this.body != null) {
      e.setText(this.body);
    }
    return e;
  }

  @Override
  public void addToElement(final Element parent) {
    final Element e = toElement();
    parent.addContent(e);
  }

  @Override
  public int compareTo(final Message o) {
    final int tc = this.type.compareToIgnoreCase(o.type);
    if (tc != 0) {
      return tc;
    }
    if (this.code == o.code) {
      return 0;
    }
    if (this.code == null) {
      return -1;
    }
    if (o.code == null) {
      return +1;
    }
    return this.code.compareTo(o.code);
  }
}
