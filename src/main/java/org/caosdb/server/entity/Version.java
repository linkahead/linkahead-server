/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

package org.caosdb.server.entity;

import java.util.List;
import org.caosdb.datetime.UTCDateTime;

/**
 * Plain old java object (POJO) for an entity's version.
 *
 * @author Timm Fitschen <t.fitschen@indiscale.com>
 */
public class Version {

  private String id = null;
  private String username = null;
  private String realm = null;
  private List<Version> predecessors = null;
  private List<Version> successors = null;
  private UTCDateTime date = null;
  private boolean isHead = false;
  private boolean isCompleteHistory = false;

  public Version(String id, long seconds, int nanos) {
    this(id, UTCDateTime.UTCSeconds(seconds, nanos), null, null);
  }

  public Version(String id, UTCDateTime date, String username, String realm) {
    this.id = id;
    this.date = date;
    this.username = username;
    this.realm = realm;
  }

  public Version(String id) {
    this(id, null, null, null);
  }

  public Version() {}

  public Version(String id, UTCDateTime timestamp) {
    this(id, timestamp, null, null);
  }

  public UTCDateTime getDate() {
    return date;
  }

  public void setDate(UTCDateTime date) {
    this.date = date;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<Version> getSuccessors() {
    return successors;
  }

  public void setSuccessors(List<Version> successors) {
    this.successors = successors;
  }

  public List<Version> getPredecessors() {
    return predecessors;
  }

  public void setPredecessors(List<Version> predecessors) {
    this.predecessors = predecessors;
  }

  public String getRealm() {
    return realm;
  }

  public void setRealm(String realm) {
    this.realm = realm;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public void setDate(Long timestamp) {
    this.date = UTCDateTime.SystemMillisToUTCDateTime(timestamp);
  }

  public boolean isHead() {
    return isHead;
  }

  public void setHead(boolean isHead) {
    this.isHead = isHead;
  }

  public boolean isCompleteHistory() {
    return isCompleteHistory;
  }

  public void setCompleteHistory(boolean isCompleteHistory) {
    this.isCompleteHistory = isCompleteHistory;
  }
}
