/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

/**
 * The statement status has two purposes:
 *
 * <p>1. Storing the importance of an entity (any of ``OBLIGATORY``, ``RECOMMENDED``, ``SUGGESTED``,
 * or ``FIX``). ``OBLIGATORY``, ``RECOMMENDED``, and ``SUGGESTED`` specify whether this Entity must,
 * should or may be present, and whether the server will throw an error, a warning or nothing,
 * respectively, if it is missing. The importance is also used to specify the level of inheritance
 * whereby ``FIX`` means that the entity is never inherited. See <a
 * href="../../../../../specification/RecordType.html">the documentation of RecordTypes</a> for more
 * information.
 *
 * @author Timm Fitschen (t.fitschen@indiscale.com)
 */
public enum StatementStatus implements StatementStatusInterface {
  OBLIGATORY,
  RECOMMENDED,
  SUGGESTED,
  FIX,
}
