/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019-2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2019-2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Properties;
import org.caosdb.server.query.Query;
import org.caosdb.server.utils.AbstractObservable;
import org.caosdb.server.utils.Observable;
import org.caosdb.server.utils.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServerProperties extends Properties implements Observable {

  private Observable delegateObservable = new AbstractObservable() {};
  private static final long serialVersionUID = -6178774548807398071L;
  private static Logger logger = LoggerFactory.getLogger(ServerProperties.class.getName());

  public static final String KEY_FILE_SYSTEM_ROOT = "FILE_SYSTEM_ROOT";
  public static final String KEY_DROP_OFF_BOX = "DROP_OFF_BOX";
  public static final String KEY_TMP_FILES = "TMP_FILES";
  public static final String KEY_SHARED_FOLDER = "SHARED_FOLDER";
  public static final String KEY_USER_FOLDERS = "USER_FOLDERS";
  public static final String KEY_CHOWN_SCRIPT = "CHOWN_SCRIPT";
  public static final String KEY_AUTH_OPTIONAL = "AUTH_OPTIONAL";

  public static final String KEY_MYSQL_HOST = "MYSQL_HOST";
  public static final String KEY_MYSQL_PORT = "MYSQL_PORT";
  public static final String KEY_MYSQL_DATABASE_NAME = "MYSQL_DATABASE_NAME";
  public static final String KEY_MYSQL_USER_NAME = "MYSQL_USER_NAME";
  public static final String KEY_MYSQL_USER_PASSWORD = "MYSQL_USER_PASSWORD";
  public static final String KEY_MYSQL_SCHEMA_VERSION = "MYSQL_SCHEMA_VERSION";

  public static final String KEY_BASE_PATH = "BASE_PATH";
  public static final String KEY_FILE_POLICY = "FILE_POLICY";
  public static final String KEY_FILE_MESSAGES = "FILE_MESSAGES";
  public static final String KEY_CONTEXT_ROOT = "CONTEXT_ROOT";
  public static final String KEY_POLICY_COMPONENT = "POLICY_COMPONENT";

  public static final String KEY_SERVER_BIND_ADDRESS = "SERVER_BIND_ADDRESS";
  public static final String KEY_SERVER_PORT_HTTPS = "SERVER_PORT_HTTPS";
  public static final String KEY_SERVER_PORT_HTTP = "SERVER_PORT_HTTP";
  public static final String KEY_REDIRECT_HTTP_TO_HTTPS_PORT = "REDIRECT_HTTP_TO_HTTPS_PORT";
  public static final String KEY_GRPC_SERVER_PORT_HTTPS = "GRPC_SERVER_PORT_HTTPS";
  public static final String KEY_GRPC_SERVER_PORT_HTTP = "GRPC_SERVER_PORT_HTTP";

  public static final String KEY_HTTPS_ENABLED_PROTOCOLS = "HTTPS_ENABLED_PROTOCOLS";
  public static final String KEY_HTTPS_DISABLED_PROTOCOLS = "HTTPS_DISABLED_PROTOCOLS";
  public static final String KEY_HTTPS_ENABLED_CIPHER_SUITES = "HTTPS_ENABLED_CIPHER_SUITES";
  public static final String KEY_HTTPS_DISABLED_CIPHER_SUITES = "HTTPS_DISABLED_CIPHER_SUITES";

  public static final String KEY_CERTIFICATES_KEY_STORE_PATH = "CERTIFICATES_KEY_STORE_PATH";
  public static final String KEY_CERTIFICATES_KEY_STORE_PASSWORD =
      "CERTIFICATES_KEY_STORE_PASSWORD";
  public static final String KEY_CERTIFICATES_KEY_PASSWORD = "CERTIFICATES_KEY_PASSWORD";

  public static final String KEY_INITIAL_CONNECTIONS = "INITIAL_CONNECTIONS";

  public static final String KEY_MAX_CONNECTIONS = "MAX_CONNECTIONS";

  public static final String KEY_MAIL_HANDLER_CLASS = "MAIL_HANDLER_CLASS";
  public static final String KEY_MAIL_TO_FILE_HANDLER_LOC = "MAIL_TO_FILE_HANDLER_LOC";

  public static final String KEY_ADMIN_NAME = "ADMIN_NAME";
  public static final String KEY_ADMIN_EMAIL = "ADMIN_EMAIL";
  public static final String KEY_BUGTRACKER_URI = "BUGTRACKER_URI";

  public static final String KEY_SESSION_TIMEOUT_MS = "SESSION_TIMEOUT_MS";
  public static final String KEY_ONE_TIME_TOKEN_EXPIRES_MS = "ONE_TIME_TOKEN_EXPIRES_MS";
  public static final String KEY_ONE_TIME_TOKEN_REPLAYS_TIMEOUT_MS =
      "ONE_TIME_TOKEN_REPLAYS_TIMEOUT_MS";

  public static final String KEY_CACHE_CONF_LOC = "CACHE_CONF_LOC";
  public static final String KEY_CACHE_DISABLE = "CACHE_DISABLE";

  public static final String KEY_TRANSACTION_BENCHMARK_ENABLED = "TRANSACTION_BENCHMARK_ENABLED";

  public static final String KEY_INSERT_FILES_IN_DIR_ALLOWED_DIRS =
      "INSERT_FILES_IN_DIR_ALLOWED_DIRS"; // see
  // server/jobs/core/InsertFilesInDir.java

  public static final String KEY_SUDO_PASSWORD = "SUDO_PASSWORD";

  public static final String KEY_USER_SOURCES_INI_FILE = "USER_SOURCES_INI_FILE";

  public static final String KEY_NEW_USER_DEFAULT_ACTIVITY = "NEW_USER_DEFAULT_ACTIVITY";

  public static final String KEY_QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS =
      "QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS";
  public static final String KEY_FIND_QUERY_DEFAULT_ROLE = "FIND_QUERY_DEFAULT_ROLE";

  public static final String KEY_SERVER_NAME = "SERVER_NAME";

  public static final String KEY_SERVER_OWNER = "SERVER_OWNER";

  public static final String KEY_SERVER_SIDE_SCRIPTING_BIN_DIR = "SERVER_SIDE_SCRIPTING_BIN_DIR";
  public static final String KEY_SERVER_SIDE_SCRIPTING_BIN_DIRS = "SERVER_SIDE_SCRIPTING_BIN_DIRS";
  public static final String KEY_SERVER_SIDE_SCRIPTING_HOME_DIR = "SERVER_SIDE_SCRIPTING_HOME_DIR";
  public static final String KEY_SERVER_SIDE_SCRIPTING_WORKING_DIR =
      "SERVER_SIDE_SCRIPTING_WORKING_DIR";

  public static final String KEY_NO_REPLY_EMAIL = "NO_REPLY_EMAIL";

  public static final String KEY_NO_REPLY_NAME = "NO_REPLY_NAME";

  public static final String KEY_CHECK_ENTITY_ACL_ROLES_MODE = "CHECK_ENTITY_ACL_ROLES_MODE";

  public static final String KEY_GLOBAL_ENTITY_PERMISSIONS_FILE = "GLOBAL_ENTITY_PERMISSIONS_FILE";
  public static final String KEY_TIMEZONE = "TIMEZONE";
  public static final String KEY_WEBUI_HTTP_HEADER_CACHE_MAX_AGE =
      "WEBUI_HTTP_HEADER_CACHE_MAX_AGE";
  public static final String KEY_AUTHTOKEN_CONFIG = "AUTHTOKEN_CONFIG";
  public static final String KEY_JOB_RULES_CONFIG = "JOB_RULES_CONFIG";

  public static final String KEY_PROJECT_VERSION = "project.version";
  public static final String KEY_PROJECT_NAME = "project.name";
  public static final String KEY_PROJECT_REVISTION = "project.revision";
  public static final String KEY_BUILD_TIMESTAMP = "build.timestamp";

  public static final String KEY_USER_NAME_VALID_REGEX = "USER_NAME_VALID_REGEX";
  public static final String KEY_USER_NAME_INVALID_MESSAGE = "USER_NAME_INVALID_MESSAGE";

  public static final String KEY_PASSWORD_STRENGTH_REGEX = "PASSWORD_VALID_REGEX";
  public static final String KEY_PASSWORD_WEAK_MESSAGE = "PASSWORD_INVALID_MESSAGE";
  public static final String KEY_REST_RESPONSE_LOG_FORMAT = "REST_RESPONSE_LOG_FORMAT";
  public static final String KEY_GRPC_RESPONSE_LOG_FORMAT = "GRPC_RESPONSE_LOG_FORMAT";

  /**
   * Read the config files and initialize the server properties.
   *
   * @throws IOException
   */
  public static ServerProperties initServerProperties() throws IOException {
    final ServerProperties serverProperties = new ServerProperties();
    final String basepath = System.getProperty("user.dir");

    final URL url = CaosDBServer.class.getResource("/build.properties");
    serverProperties.load(url.openStream());

    // load standard config
    loadConfigFile(serverProperties, new File(basepath + "/conf/core/server.conf"));

    // load ext/server.conf
    loadConfigFile(serverProperties, new File(basepath + "/conf/ext/server.conf"));

    // load ext/server.conf.d/ (ordering is determined by system collation)
    final File confDir = new File(basepath + "/conf/ext/server.conf.d");
    if (confDir.exists() && confDir.isDirectory()) {
      final String[] confFiles = confDir.list();
      Arrays.sort(confFiles, Comparator.naturalOrder());
      for (final String confFile : confFiles) {
        // prevent backup files from being read
        if (confFile.endsWith(".conf")) {
          loadConfigFile(serverProperties, new File(confDir, confFile));
        }
      }
    }

    // load env vars
    for (final java.util.Map.Entry<String, String> envvar : System.getenv().entrySet()) {
      if (envvar.getKey().startsWith("CAOSDB_CONFIG_") && envvar.getKey().length() > 14) {
        serverProperties.setProperty(envvar.getKey().substring(14), envvar.getValue());
      }
    }

    // log configuration alphabetically
    if (logger.isInfoEnabled()) {
      final ArrayList<String> names = new ArrayList<>(serverProperties.stringPropertyNames());
      Collections.sort(names);
      for (final String name : names) {
        final String val =
            name.contains("PASSW") || name.contains("SECRET")
                ? "****"
                : serverProperties.getProperty(name);
        logger.info(name + "=" + val);
      }
    }

    sanityCheck(serverProperties);

    return serverProperties;
  }

  private static void sanityCheck(ServerProperties serverProperties) {
    try {
      Query.Role.valueOf(serverProperties.getProperty(KEY_FIND_QUERY_DEFAULT_ROLE));
    } catch (IllegalArgumentException e) {
      logger.error(
          "Unsupported value for server property "
              + KEY_FIND_QUERY_DEFAULT_ROLE
              + ": "
              + serverProperties.getProperty(KEY_FIND_QUERY_DEFAULT_ROLE));
      System.exit(1);
    }
  }

  private static void loadConfigFile(final Properties serverProperties, final File confFile)
      throws IOException {
    if (confFile.exists() && confFile.isFile()) {
      logger.info("Reading configuration from " + confFile.getAbsolutePath());
      final BufferedInputStream sp_in = new BufferedInputStream(new FileInputStream(confFile));
      serverProperties.load(sp_in);
      sp_in.close();
    }
  }

  @Override
  public synchronized Object setProperty(String key, String value) {
    Object ret = super.setProperty(key, value);
    delegateObservable.notifyObservers(key);
    return ret;
  }

  @Override
  public boolean acceptObserver(Observer o) {
    return delegateObservable.acceptObserver(o);
  }

  @Override
  public void notifyObservers(String e) {
    delegateObservable.notifyObservers(e);
  }
}
