/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.HashSet;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.accessControl.ACMPermissions;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.transaction.RetrievePermissionRulesTransaction;
import org.caosdb.server.transaction.UpdatePermissionRulesTransaction;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Document;
import org.jdom2.Element;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

public class PermissionRulesResource extends AbstractCaosDBServerResource {

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException,
          IOException,
          SQLException,
          CaosDBException,
          NoSuchAlgorithmException,
          Exception {
    final String role = getRequestedItems()[0];

    getUser().checkPermission(ACMPermissions.PERMISSION_RETRIEVE_ROLE_PERMISSIONS(role));

    final RetrievePermissionRulesTransaction t = new RetrievePermissionRulesTransaction(role);
    try {
      t.execute();
      final Element root = generateRootElement();
      if (t.getRules() != null) {
        for (final PermissionRule p : t.getRules()) {
          root.addContent(p.toElement());
        }
      }
      return ok(new Document(root));
    } catch (final Message m) {
      if (m == ServerMessages.ROLE_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      } else {
        throw m;
      }
    }
  }

  @Override
  public Representation httpPutInChildClass(final Representation entity) throws Exception {
    final Element root = parseEntity(entity).getRootElement();

    final String role = getRequestedItems()[0];
    final HashSet<PermissionRule> rules = new HashSet<PermissionRule>();

    for (final Element e : root.getChildren()) {
      try {
        final PermissionRule rule = PermissionRule.parse(e);
        rules.add(rule);
      } catch (final Exception exc) {
        return error(ServerMessages.REQUEST_BODY_NOT_WELLFORMED, Status.CLIENT_ERROR_BAD_REQUEST);
      }
    }

    final UpdatePermissionRulesTransaction t = new UpdatePermissionRulesTransaction(role, rules);
    try {
      t.execute();
    } catch (final Message m) {
      if (m == ServerMessages.ROLE_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      } else {
        throw m;
      }
    }

    return new StringRepresentation("ok");
  }
}
