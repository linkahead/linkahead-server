/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import static org.caosdb.server.FileSystem.getFromFileSystem;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.JDOMException;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.FileRepresentation;
import org.restlet.representation.Representation;

public class ThumbnailsResource extends AbstractCaosDBServerResource {

  /**
   * Download a File from the CaosDBFileSystem. Only one File per Request.
   *
   * @author Timm Fitschen
   * @return InputRepresentation
   * @throws IOException
   */
  @Override
  protected Representation httpGetInChildClass() throws Exception {
    final String specifier = (String) getRequest().getAttributes().get("path");

    if (specifier.equalsIgnoreCase("file")) {
      final FileRepresentation ret = new FileRepresentation(fileThumbnail, MediaType.IMAGE_PNG);
      ret.setDisposition(new Disposition(Disposition.TYPE_ATTACHMENT));
      return ret;
    } else if (specifier.equalsIgnoreCase("dir")) {
      final FileRepresentation ret = new FileRepresentation(folderThumbnail, MediaType.IMAGE_PNG);
      ret.setDisposition(new Disposition(Disposition.TYPE_ATTACHMENT));
      return ret;
    }

    final File file = getFromFileSystem(specifier);
    File thumbnail = null;

    if (file == null) {
      return error(ServerMessages.ENTITY_DOES_NOT_EXIST, Status.CLIENT_ERROR_NOT_FOUND);
    }

    final String thumbnailPath = file.getParent() + "/.thumbnails/" + file.getName();
    thumbnail = new File(thumbnailPath);
    if (!thumbnail.exists()) {
      if (file.isDirectory()) {
        getResponse().redirectTemporary(getRootRef().toString() + "/Thumbnails/dir");
        thumbnail = folderThumbnail;
      } else {
        getResponse().redirectTemporary(getRootRef().toString() + "/Thumbnails/file");
        thumbnail = fileThumbnail;
      }
    }

    final FileRepresentation ret = new FileRepresentation(thumbnail, MediaType.IMAGE_PNG);
    ret.setDisposition(new Disposition(Disposition.TYPE_ATTACHMENT));

    return ret;
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException {
    this.setStatus(org.restlet.data.Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
    return null;
  }

  private static File folderThumbnail =
      new File("resources/oxygen-icons-png-master/oxygen/22x22/places/folder.png");
  private static File fileThumbnail =
      new File(
          "resources/oxygen-icons-png-master/oxygen/22x22/mimetypes/application-x-kgetlist.png");
}
