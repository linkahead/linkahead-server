/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2019 IndiScale GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

package org.caosdb.server.resource;

import com.ibm.icu.text.Collator;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.FileSystem;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.OneTimeAuthenticationToken;
import org.caosdb.server.accessControl.SessionToken;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.scripting.CallerSerializer;
import org.caosdb.server.scripting.ScriptingPermissions;
import org.caosdb.server.scripting.ServerSideScriptingCaller;
import org.caosdb.server.utils.Serializer;
import org.caosdb.server.utils.ServerMessages;
import org.caosdb.server.utils.Utils;
import org.jdom2.Element;
import org.restlet.data.CharacterSet;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.engine.header.ContentType;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;

public class ScriptingResource extends AbstractCaosDBServerResource {

  private ServerSideScriptingCaller caller;
  private Collection<FileProperties> deleteFiles = new LinkedList<>();

  @Override
  public Logger getLogger() {
    return Logger.getLogger(this.getClass().getName());
  }

  public Element generateRootElement(ServerSideScriptingCaller caller) {
    Serializer<ServerSideScriptingCaller, Element> xmlSerializer = new CallerSerializer();
    Element callerElement = xmlSerializer.serialize(caller);

    return generateRootElement(callerElement);
  }

  /**
   * Handles a POST request to server-side scripting.
   *
   * @param entity Representation of the request.
   */
  @Override
  protected Representation httpPostInChildClass(Representation entity) throws Exception {

    MediaType mediaType = entity.getMediaType();
    try {
      if (mediaType.equals(MediaType.MULTIPART_FORM_DATA, true)) {
        handleMultiparts(entity);
      } else if (mediaType.equals(MediaType.APPLICATION_WWW_FORM)) {
        handleForm(new Form(entity));
      } else {
        getResponse().setStatus(Status.CLIENT_ERROR_UNSUPPORTED_MEDIA_TYPE);
        return null;
      }
    } catch (Message m) {
      if (m == ServerMessages.SERVER_SIDE_SCRIPT_DOES_NOT_EXIST) {
        return error(m, Status.CLIENT_ERROR_NOT_FOUND);
      } else if (m == ServerMessages.SERVER_SIDE_SCRIPT_MISSING_CALL) {
        return error(m, Status.CLIENT_ERROR_BAD_REQUEST);
      } else if (m == ServerMessages.SERVER_SIDE_SCRIPT_NOT_EXECUTABLE) {
        return error(m, Status.CLIENT_ERROR_BAD_REQUEST);
      } else if (m == ServerMessages.SERVER_SIDE_SCRIPT_TIMEOUT) {
        return error(m, Status.CLIENT_ERROR_BAD_REQUEST);
      }

      return error(m, Status.SERVER_ERROR_INTERNAL);
    } finally {
      deleteTmpFiles();
    }
    return ok(generateRootElement(this.caller));
  }

  private void deleteTmpFiles() {
    for (FileProperties p : deleteFiles) {
      try {
        p.getFile().delete();
      } catch (Exception t) {
        if (getLogger().isLoggable(Level.WARNING)) {
          getLogger()
              .warning(
                  "Could not delete tmp file: " + p.getPath() + "\nException: " + t.toString());
        }
      }
    }
  }

  public int handleMultiparts(Representation entity)
      throws FileUploadException, IOException, NoSuchAlgorithmException, Message {
    final DiskFileItemFactory factory = new DiskFileItemFactory();
    factory.setSizeThreshold(1000240);
    final RestletFileUpload upload = new RestletFileUpload(factory);
    final FileItemIterator iter = upload.getItemIterator(entity);

    List<FileProperties> files = new ArrayList<>();
    Form form = new Form();

    while (iter.hasNext()) {
      FileItemStream item = iter.next();
      if (item.isFormField()) {
        // put plain text form field into the form
        CharacterSet characterSet = ContentType.readCharacterSet(item.getContentType());
        String value =
            Utils.InputStream2String(
                item.openStream(),
                (characterSet != null ? characterSet.toString() : CharacterSet.UTF_8.toString()));
        form.add(new Parameter(item.getFieldName(), value));
      } else {
        // -> this is a file, store in tmp dir
        final FileProperties file = FileSystem.upload(item, this.getSRID());
        deleteTmpFileAfterTermination(file);
        file.setPath(item.getName());
        file.setTmpIdentifier(item.getFieldName());
        files.add(file);
        form.add(
            new Parameter(
                item.getFieldName(),
                ServerSideScriptingCaller.UPLOAD_FILES_DIR + "/" + item.getName()));
      }
    }
    return callScript(form, files);
  }

  private void deleteTmpFileAfterTermination(FileProperties file) {
    deleteFiles.add(file);
  }

  public int handleForm(Form form) throws Message {
    return callScript(form, null);
  }

  public List<String> form2CommandLine(Form form) throws Message {
    ArrayList<String> commandLine = new ArrayList<>(form.size());
    ArrayList<Parameter> positionalArgs = new ArrayList<>(form.size() - 1);

    commandLine.add(""); // will be replaced by the "call" value

    for (Parameter p : form) {
      if (p.getName().startsWith("-p")) {
        positionalArgs.add(p);
      } else if (p.getName().startsWith("-O")) {
        commandLine.add(String.format("--%s=%s", p.getName().substring(2), p.getValue()));
      } else if (p.getName().equals("call")) {
        commandLine.set(0, p.getValue());
      }
    }

    // sort positional arguments.
    positionalArgs.sort((o1, o2) -> Collator.getInstance().compare(o1.getName(), o2.getName()));
    for (Parameter p : positionalArgs) {
      commandLine.add(p.getValue());
    }

    if (commandLine.get(0).length() == 0) {
      // first item still empty
      throw ServerMessages.SERVER_SIDE_SCRIPT_MISSING_CALL;
    }
    return commandLine;
  }

  public int callScript(Form form, List<FileProperties> files) throws Message {
    List<String> commandLine = form2CommandLine(form);
    String call = commandLine.get(0);

    checkExecutionPermission(getUser(), call);
    Integer timeoutMs = Integer.valueOf(form.getFirstValue("timeout", "-1"));
    return callScript(commandLine, timeoutMs, files);
  }

  public int callScript(List<String> commandLine, Integer timeoutMs, List<FileProperties> files)
      throws Message {
    return callScript(commandLine, timeoutMs, files, generateAuthToken(commandLine.get(0)));
  }

  public boolean isAnonymous() {
    return AuthenticationUtils.isAnonymous(getUser());
  }

  /**
   * Generate and return a token for the purpose of the given call. If the user is not anonymous and
   * the call is not configured to be called by everyone, a SessionToken is returned instead.
   */
  public Object generateAuthToken(String call) {
    String purpose = "SCRIPTING:EXECUTE:" + call;
    Object authtoken = OneTimeAuthenticationToken.generateForPurpose(purpose, getUser());
    if (authtoken != null || isAnonymous()) {
      return authtoken;
    }
    return SessionToken.generate(getUser());
  }

  public void checkExecutionPermission(Subject user, String call) {
    user.checkPermission(ScriptingPermissions.PERMISSION_EXECUTION(call));
  }

  public int callScript(
      List<String> commandLine, Integer timeoutMs, List<FileProperties> files, Object authToken)
      throws Message {
    caller =
        new ServerSideScriptingCaller(
            commandLine.toArray(new String[commandLine.size()]), timeoutMs, files, authToken);
    return caller.invoke();
  }

  @Override
  protected Representation httpGetInChildClass() throws Exception {
    getResponse().setStatus(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED);
    return null;
  }
}
