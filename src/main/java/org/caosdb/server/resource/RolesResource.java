/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.entity.Message;
import org.caosdb.server.transaction.DeleteRoleTransaction;
import org.caosdb.server.transaction.InsertRoleTransaction;
import org.caosdb.server.transaction.RetrieveRoleTransaction;
import org.caosdb.server.transaction.UpdateRoleTransaction;
import org.caosdb.server.utils.ServerMessages;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.data.Form;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

public class RolesResource extends AbstractCaosDBServerResource {

  @Override
  protected Representation httpGetInChildClass()
      throws ConnectionException,
          IOException,
          SQLException,
          CaosDBException,
          NoSuchAlgorithmException,
          Exception {
    final Element root = generateRootElement();
    final Document document = new Document();

    if (getRequestedItems().length > 0) {
      final String name = getRequestedItems()[0];
      if (name != null) {
        final RetrieveRoleTransaction t = new RetrieveRoleTransaction(name);
        try {
          t.execute();
          root.addContent(t.getRole().toElement());
        } catch (final Message m) {
          if (m == ServerMessages.ROLE_DOES_NOT_EXIST) {
            return error(m, Status.CLIENT_ERROR_NOT_FOUND);
          } else {
            throw m;
          }
        }
      }
    }

    document.setRootElement(root);
    return ok(document);
  }

  @Override
  protected Representation httpDeleteInChildClass()
      throws ConnectionException,
          SQLException,
          CaosDBException,
          IOException,
          NoSuchAlgorithmException,
          Exception {
    if (getRequestedItems().length > 0) {
      final String name = getRequestedItems()[0];
      if (name != null) {
        final DeleteRoleTransaction t = new DeleteRoleTransaction(name);
        try {
          t.execute();
        } catch (final Message m) {
          if (m == ServerMessages.ROLE_DOES_NOT_EXIST) {
            return error(m, Status.CLIENT_ERROR_NOT_FOUND);
          } else {
            throw m;
          }
        }
      }
    }

    return new StringRepresentation("ok");
  }

  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException,
          SQLException,
          CaosDBException,
          IOException,
          NoSuchAlgorithmException,
          xmlNotWellFormedException,
          JDOMException,
          Exception {

    String name = null;
    String description = null;
    final Form f = new Form(entity);
    if (!f.isEmpty()) {
      name = f.getFirstValue("role_name");
      description = f.getFirstValue("role_description");
    }

    if (name != null) {
      final Role role = new Role();
      role.name = name;
      role.description = description;
      final InsertRoleTransaction t = new InsertRoleTransaction(role);
      try {
        t.execute();
      } catch (final Message m) {
        if (m == ServerMessages.ROLE_NAME_IS_NOT_UNIQUE) {
          return error(m, Status.CLIENT_ERROR_CONFLICT);
        } else {
          throw m;
        }
      }
    }

    return new StringRepresentation("ok");
  }

  @Override
  protected Representation httpPutInChildClass(final Representation entity)
      throws ConnectionException, JDOMException, Exception, xmlNotWellFormedException {

    if (getRequestedItems().length > 0) {
      final String name = getRequestedItems()[0];
      String description = null;

      final Form f = new Form(entity);
      if (!f.isEmpty()) {
        description = f.getFirstValue("role_description");
      }

      if (name != null && description != null) {
        final Role role = new Role();
        role.name = name;
        role.description = description;
        final UpdateRoleTransaction t = new UpdateRoleTransaction(role);
        try {
          t.execute();
        } catch (final Message m) {
          if (m == ServerMessages.ROLE_DOES_NOT_EXIST) {
            return error(m, Status.CLIENT_ERROR_NOT_FOUND);
          } else {
            throw m;
          }
        }
      }
    }

    return new StringRepresentation("ok");
  }
}
