/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.resource;

import java.util.TimeZone;
import org.caosdb.datetime.UTCTimeZoneShift;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.utils.FlagInfo;
import org.caosdb.server.utils.Info;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.restlet.representation.Representation;

/** This class represents the information retrieved by /Info requests (only GET) to CaosDB. */
public class InfoResource extends AbstractCaosDBServerResource {

  /**
   * The response to the HTTP GET request is generated here.
   *
   * @return The response code for OK and the resulting info XML document.
   */
  @Override
  protected Representation httpGetInChildClass() throws Exception {
    final Document doc = new Document();
    final Element root = generateRootElement();
    root.addContent(getTimeZone());
    root.addContent(Info.toElement());
    root.addContent(FlagInfo.toElement());
    root.addContent(TransactionBenchmark.getRootInstance().toElement());
    doc.setRootElement(root);
    return ok(doc);
  }

  public Element getTimeZone() {
    TimeZone d = TimeZone.getDefault();
    long currentDate = System.currentTimeMillis(); // show time zone offset for the current date.
    return new Element("TimeZone")
        .setAttribute("offset", UTCTimeZoneShift.getISO8601Offset(d, currentDate))
        .setAttribute("id", d.getID())
        .addContent(d.getDisplayName());
  }

  /** There is no POST request specified for the /Info resource. */
  @Override
  protected Representation httpPostInChildClass(final Representation entity)
      throws ConnectionException, JDOMException {
    return null;
  }
}
