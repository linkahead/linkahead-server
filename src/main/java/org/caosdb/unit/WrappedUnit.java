/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.unit;

import de.timmfitschen.easyunits.conversion.LinearConverter;

public class WrappedUnit implements Unit {

  private final de.timmfitschen.easyunits.Unit wrapped;

  public WrappedUnit(final de.timmfitschen.easyunits.Unit unit) {
    this.wrapped = unit;
  }

  @Override
  public Unit normalize() {
    if (this.wrapped.isNormal()) {
      return this;
    } else {
      return new WrappedUnit(this.wrapped.getNormalizedUnit());
    }
  }

  @Override
  public Long getSignature() {
    return this.wrapped.getSignature();
  }

  @Override
  public Double convert(final Double vDouble) {
    return this.wrapped.getConverter().convert(vDouble).doubleValue();
  }

  @Override
  public Converter getConverter() {
    if (this.wrapped.getConverter() instanceof LinearConverter) {
      return new WrappedLinearConverter((LinearConverter) this.wrapped.getConverter());
    }
    return new WrappedConverter(this.wrapped.getConverter());
  }
}
