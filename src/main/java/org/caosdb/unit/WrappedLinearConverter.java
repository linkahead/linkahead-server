/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.unit;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class WrappedLinearConverter implements LinearConverter {

  private final de.timmfitschen.easyunits.conversion.LinearConverter wrapped;

  public WrappedLinearConverter(
      final de.timmfitschen.easyunits.conversion.LinearConverter converter) {
    this.wrapped = converter;
  }

  @Override
  public double get_offset_a() {
    return this.wrapped.getA().doubleValue();
  }

  @Override
  public long get_dividend() {
    return double2rational(this.wrapped.getB().doubleValue())[1];
  }

  @Override
  public long get_divisor() {
    return double2rational(this.wrapped.getB().doubleValue())[0];
  }

  @Override
  public double get_offset_c() {
    return this.wrapped.getC().doubleValue();
  }

  /**
   * Return array [divisor, dividend]
   *
   * @param d
   * @return
   */
  private long[] double2rational(final double d) {
    final DecimalFormatSymbols b = new DecimalFormatSymbols(Locale.US);
    final DecimalFormat myFormatter =
        new DecimalFormat("#0.0##############################################", b);
    final String output = myFormatter.format(d);
    final String[] str = output.split("\\.");
    final long[] ret = {1, 1};
    final int l = str[0].length();
    if (l == 1 && str[0].equals("0")) {
      ret[0] = (long) Math.pow(10, str[1].length());
      ret[1] = Long.valueOf(str[1].replaceFirst("^0*", ""));
    } else if (str[1].length() == 1 && str[1].equals("0")) {
      ret[0] = 1;
      ret[1] = Long.valueOf(str[0]);
    } else {
      ret[0] = (long) Math.pow(10, str[1].length() + l);
      ret[1] = Long.valueOf(str[0] + str[1]);
    }
    return ret;
  }
}
