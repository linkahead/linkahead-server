/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.datetime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZoneId;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.database.backend.implementation.MySQL.ConnectionException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class DateTimeTest {

  @BeforeAll
  public static void setup() throws InterruptedException, IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.initTimeZone();
  }

  @Test
  public void testGetTimeZoneFromDate() throws InterruptedException, IOException {
    String tz = CaosDBServer.getTimeZoneFromDate();
    ZoneId of = java.time.ZoneId.of(tz);
    assertNotNull(of);
  }

  @Test
  public void testUTCDateTimeLeapSeconds() {
    final GregorianCalendar gc1 = new GregorianCalendar();
    gc1.clear();
    gc1.set(1970, 0, 1, 0, 0, 0);
    gc1.setTimeZone(TimeZone.getTimeZone("UTC"));
    assertEquals(0, gc1.getTimeInMillis());

    final DateTimeInterface dt1 = DateTimeFactory2.valueOf(gc1);
    assertTrue(dt1 instanceof UTCDateTime);
    assertEquals(0, ((UTCDateTime) dt1).getUTCSeconds());
    assertEquals((Integer) 0, ((UTCDateTime) dt1).getNanoseconds());
    assertEquals("1970-01-01T00:00:00.0+0000", dt1.toDateTimeString(TimeZone.getTimeZone("UTC")));

    final GregorianCalendar gc2 = new GregorianCalendar();
    gc2.clear();
    gc2.set(1972, 5, 30, 23, 59, 59);
    gc2.setTimeZone(TimeZone.getTimeZone("UTC"));
    assertEquals(78796799000L, gc2.getTimeInMillis());

    final DateTimeInterface dt2 = DateTimeFactory2.valueOf(gc2);
    assertTrue(dt2 instanceof UTCDateTime);
    assertEquals(78796799, ((UTCDateTime) dt2).getUTCSeconds());
    assertEquals((Integer) 0, ((UTCDateTime) dt2).getNanoseconds());
    assertEquals("1972-06-30T23:59:59.0+0000", dt2.toDateTimeString(TimeZone.getTimeZone("UTC")));

    final GregorianCalendar gc3 = new GregorianCalendar();
    gc3.clear();
    gc3.set(1972, 6, 1, 0, 0, 0);
    gc3.setTimeZone(TimeZone.getTimeZone("UTC"));
    assertEquals(78796800000L, gc3.getTimeInMillis());

    final DateTimeInterface dt3 = DateTimeFactory2.valueOf(gc3);
    assertTrue(dt3 instanceof UTCDateTime);
    assertEquals(78796801, ((UTCDateTime) dt3).getUTCSeconds());
    assertEquals((Integer) 0, ((UTCDateTime) dt3).getNanoseconds());
    assertEquals("1972-07-01T00:00:00.0+0000", dt3.toDateTimeString(TimeZone.getTimeZone("UTC")));

    assertEquals(2, ((UTCDateTime) dt3).getUTCSeconds() - ((UTCDateTime) dt2).getUTCSeconds());
  }

  @Test
  public void testUTCDateTimeParsingWithLeapSeconds() {
    DateTimeInterface dt1 = DateTimeFactory2.valueOf("1970-01-01T00:00:00UTC");
    assertTrue(dt1 instanceof UTCDateTime);
    assertEquals("1970-01-01T00:00:00+0000", dt1.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(
        "1969-12-31T21:00:00-0300",
        dt1.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.NEGATIVE, 3, 00)));
    assertEquals(
        "1970-01-01T01:00:00+0100", dt1.toDateTimeString(TimeZone.getTimeZone("Europe/Berlin")));
    assertEquals(0, ((UTCDateTime) dt1).getUTCSeconds());
    assertNull(((UTCDateTime) dt1).getNanoseconds());
    assertEquals("0UTC", dt1.toDatabaseString());

    dt1 = DateTimeFactory2.valueOf(dt1.toDatabaseString());
    assertTrue(dt1 instanceof UTCDateTime);
    assertEquals("1970-01-01T00:00:00+0000", dt1.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertTrue(dt1 instanceof UTCDateTime);
    assertEquals("1970-01-01T00:00:00+0000", dt1.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(
        "1969-12-31T21:00:00-0300",
        dt1.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.NEGATIVE, 3, 00)));
    assertEquals(
        "1970-01-01T01:00:00+0100", dt1.toDateTimeString(TimeZone.getTimeZone("Europe/Berlin")));
    assertEquals(0, ((UTCDateTime) dt1).getUTCSeconds());
    assertNull(((UTCDateTime) dt1).getNanoseconds());

    assertEquals("0UTC", dt1.toDatabaseString());

    GregorianCalendar gc = new GregorianCalendar();
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1970, 0, 1, 00, 00, 00);
    assertEquals(0L, ((UTCDateTime) dt1).getUTCSeconds() - gc.getTimeInMillis() / 1000);
    assertNull(((UTCDateTime) dt1).getNanoseconds());

    final DateTimeInterface dt2 = DateTimeFactory2.valueOf("1972-06-30T23:59:59UTC");
    assertTrue(dt2 instanceof UTCDateTime);
    assertEquals("1972-06-30T23:59:59+0000", dt2.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(78796799, ((UTCDateTime) dt2).getUTCSeconds());
    assertNull(((UTCDateTime) dt2).getNanoseconds());

    final DateTimeInterface dt3 = DateTimeFactory2.valueOf("1972-06-30T23:59:60UTC");
    assertTrue(dt3 instanceof UTCDateTime);
    assertEquals("1972-06-30T23:59:60+0000", dt3.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(78796800, ((UTCDateTime) dt3).getUTCSeconds());
    assertNull(((UTCDateTime) dt3).getNanoseconds());
    assertEquals("78796800UTC", dt3.toDatabaseString());

    final DateTimeInterface dt4 = DateTimeFactory2.valueOf("1972-07-01T00:00:00UTC");
    assertTrue(dt4 instanceof UTCDateTime);
    assertEquals("1972-07-01T00:00:00+0000", dt4.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(78796801, ((UTCDateTime) dt4).getUTCSeconds());
    assertNull(((UTCDateTime) dt4).getNanoseconds());
    assertEquals("78796801UTC", dt4.toDatabaseString());

    final DateTimeInterface dt5 = DateTimeFactory2.valueOf("2015-07-01 20:15:03UTC");
    assertTrue(dt5 instanceof UTCDateTime);
    assertEquals("2015-07-01T20:15:03+0000", dt5.toDateTimeString(TimeZone.getTimeZone("UTC")));
    gc = new GregorianCalendar();
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(2015, 6, 1, 20, 15, 03);
    assertEquals(26L, ((UTCDateTime) dt5).getUTCSeconds() - gc.getTimeInMillis() / 1000);
    assertNull(((UTCDateTime) dt5).getNanoseconds());

    assertEquals("1435781729UTC", dt5.toDatabaseString());

    DateTimeInterface dt6 = DateTimeFactory2.valueOf("2015-07-01 20:15:03.123456UTC");
    assertTrue(dt6 instanceof UTCDateTime);
    assertEquals(
        "2015-07-01T20:15:03.123456+0000", dt6.toDateTimeString(TimeZone.getTimeZone("UTC")));
    GregorianCalendar gc2 = new GregorianCalendar();
    gc2.clear();
    gc2.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc2.set(2015, 6, 1, 20, 15, 03);
    assertEquals(26L, ((UTCDateTime) dt6).getUTCSeconds() - gc2.getTimeInMillis() / 1000);
    assertEquals((Integer) 123456000, ((UTCDateTime) dt6).getNanoseconds());

    assertEquals("1435781729UTC123456000", dt6.toDatabaseString());
    dt6 = DateTimeFactory2.valueOf(dt6.toDatabaseString());
    assertTrue(dt6 instanceof UTCDateTime);
    assertEquals(
        "2015-07-01T20:15:03.123456+0000", dt6.toDateTimeString(TimeZone.getTimeZone("UTC")));
    gc2 = new GregorianCalendar();
    gc2.clear();
    gc2.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc2.set(2015, 6, 1, 20, 15, 03);
    assertEquals(26L, ((UTCDateTime) dt6).getUTCSeconds() - gc2.getTimeInMillis() / 1000);
    assertEquals((Integer) 123456000, ((UTCDateTime) dt6).getNanoseconds());

    DateTimeInterface dt7 = DateTimeFactory2.valueOf("1997-06-30T23:59:59UTC");
    assertTrue(dt7 instanceof UTCDateTime);
    assertEquals("1997-06-30T23:59:59+0000", dt7.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(867715219, ((UTCDateTime) dt7).getUTCSeconds());
    assertNull(((UTCDateTime) dt7).getNanoseconds());
    assertEquals("867715219UTC", dt7.toDatabaseString());
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1997, 5, 30, 23, 59, 59);
    assertEquals(20L, ((UTCDateTime) dt7).getUTCSeconds() - gc.getTimeInMillis() / 1000);
    dt7 = DateTimeFactory2.valueOf(dt7.toDatabaseString());
    assertTrue(dt7 instanceof UTCDateTime);
    assertEquals("1997-06-30T23:59:59+0000", dt7.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(867715219, ((UTCDateTime) dt7).getUTCSeconds());
    assertNull(((UTCDateTime) dt7).getNanoseconds());
    assertEquals("867715219UTC", dt7.toDatabaseString());
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1997, 5, 30, 23, 59, 59);
    assertEquals(20L, ((UTCDateTime) dt7).getUTCSeconds() - gc.getTimeInMillis() / 1000);

    DateTimeInterface dt8 = DateTimeFactory2.valueOf("1997-06-30T23:59:60UTC");
    assertTrue(dt8 instanceof UTCDateTime);
    assertEquals("1997-06-30T23:59:60+0000", dt8.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(867715220, ((UTCDateTime) dt8).getUTCSeconds());
    assertNull(((UTCDateTime) dt8).getNanoseconds());
    assertEquals("867715220UTC", dt8.toDatabaseString());
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1997, 5, 30, 23, 59, 59);
    assertEquals(21L, ((UTCDateTime) dt8).getUTCSeconds() - gc.getTimeInMillis() / 1000);
    dt8 = DateTimeFactory2.valueOf(dt8.toDatabaseString());
    assertTrue(dt8 instanceof UTCDateTime);
    assertEquals("1997-06-30T23:59:60+0000", dt8.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(867715220, ((UTCDateTime) dt8).getUTCSeconds());
    assertNull(((UTCDateTime) dt8).getNanoseconds());
    assertEquals("867715220UTC", dt8.toDatabaseString());
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1997, 5, 30, 23, 59, 59);
    assertEquals(21L, ((UTCDateTime) dt8).getUTCSeconds() - gc.getTimeInMillis() / 1000);

    DateTimeInterface dt9 = DateTimeFactory2.valueOf("1997-07-01T00:00:00UTC");
    assertTrue(dt9 instanceof UTCDateTime);
    assertEquals("1997-07-01T00:00:00+0000", dt9.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(867715221, ((UTCDateTime) dt9).getUTCSeconds());
    assertNull(((UTCDateTime) dt9).getNanoseconds());
    assertEquals("867715221UTC", dt9.toDatabaseString());
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1997, 6, 1, 0, 0, 0);
    assertEquals(21L, ((UTCDateTime) dt9).getUTCSeconds() - gc.getTimeInMillis() / 1000);
    dt9 = DateTimeFactory2.valueOf(dt9.toDatabaseString());
    assertTrue(dt9 instanceof UTCDateTime);
    assertEquals(867715221, ((UTCDateTime) dt9).getUTCSeconds());
    assertEquals("1997-07-01T00:00:00+0000", dt9.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertNull(((UTCDateTime) dt9).getNanoseconds());
    assertEquals("867715221UTC", dt9.toDatabaseString());
    gc.clear();
    gc.setTimeZone(TimeZone.getTimeZone("UTC"));
    gc.set(1997, 6, 1, 0, 0, 0);
    assertEquals(21L, ((UTCDateTime) dt9).getUTCSeconds() - gc.getTimeInMillis() / 1000);
  }

  @Test
  public void testIsLeapSupported() throws SQLException, ConnectionException {
    // no leap java second support
    final Timestamp ts1 = Timestamp.valueOf("1972-06-30 23:59:59");
    final Timestamp ts2 = Timestamp.valueOf("1972-06-30 23:59:60");
    final Timestamp ts3 = Timestamp.valueOf("1972-07-01 00:00:00");

    assertEquals(1000, ts2.getTime() - ts1.getTime());

    assertEquals(ts2, ts3);

    final GregorianCalendar gc1 = new GregorianCalendar();
    gc1.clear();
    gc1.set(1972, 05, 30, 23, 59, 59);
    gc1.setTimeZone(TimeZone.getDefault());
    System.out.println(gc1.toString());

    assertEquals(ts1.getTime(), gc1.getTimeInMillis());

    gc1.clear();
    gc1.set(1972, 05, 30, 23, 59, 60);
    gc1.setTimeZone(TimeZone.getDefault());
    System.out.println(gc1.toString());

    assertEquals(ts2.getTime(), gc1.getTimeInMillis());
  }

  @Test
  public void testPartialDateTime() {
    assertTrue(DateTimeFactory2.valueOf("2015") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015") instanceof Date);
    assertTrue(DateTimeFactory2.valueOf("2015-01") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01") instanceof Date);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01") instanceof Date);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01T20") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01T20:15") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015+0100") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015+0100") instanceof Date);
    assertTrue(DateTimeFactory2.valueOf("2015-01+0100") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01+0100") instanceof Date);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01+0100") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01+0100") instanceof Date);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01T20+0100") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01T20:15+0100") instanceof SemiCompleteDateTime);

    final UTCDateTime lb = (UTCDateTime) DateTimeFactory2.valueOf("2015-01-01T00:00:00+0100");
    final UTCDateTime ub = (UTCDateTime) DateTimeFactory2.valueOf("2015-01-02T00:00:00+0100");
    final SemiCompleteDateTime dt =
        (SemiCompleteDateTime) DateTimeFactory2.valueOf("2015-01-01+0100");
    assertEquals(dt.getInclusiveLowerBound().getUTCSeconds(), lb.getUTCSeconds());
    assertEquals(dt.getExclusiveUpperBound().getUTCSeconds(), ub.getUTCSeconds());

    assertTrue(DateTimeFactory2.valueOf("2015") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01T20") instanceof SemiCompleteDateTime);
    assertTrue(DateTimeFactory2.valueOf("2015-01-01T20:15") instanceof SemiCompleteDateTime);

    final UTCDateTime lb2 = (UTCDateTime) DateTimeFactory2.valueOf("2015-01-01T00:00:00UTC");
    final UTCDateTime ub2 = (UTCDateTime) DateTimeFactory2.valueOf("2015-01-02T00:00:00UTC");
    final SemiCompleteDateTime dt2 =
        (SemiCompleteDateTime) DateTimeFactory2.valueOf("2015-01-01UTC");
    assertEquals(dt2.getInclusiveLowerBound().getUTCSeconds(), lb2.getUTCSeconds());
    assertEquals(dt2.getExclusiveUpperBound().getUTCSeconds(), ub2.getUTCSeconds());
  }

  @Test
  public void testDateToString() {
    assertEquals("2015", DateTimeFactory2.valueOf("2015").toDateTimeString(null));
    assertEquals("2015-01", DateTimeFactory2.valueOf("2015-01").toDateTimeString(null));
    assertEquals("2015-01-03", DateTimeFactory2.valueOf("2015-01-03").toDateTimeString(null));
    assertEquals("2015", DateTimeFactory2.valueOf("2015+0100").toDateTimeString(null));
    assertEquals("2015-01", DateTimeFactory2.valueOf("2015-01+0100").toDateTimeString(null));
    assertEquals("2015-01-03", DateTimeFactory2.valueOf("2015-01-03+0100").toDateTimeString(null));
  }

  @Test
  public void testDateFromDatabase() {
    String db_str = "20160101";
    DateTimeFactory2 fac = new DateTimeFactory2();
    fac.setDate(db_str);

    assertTrue(fac.getDateTime() instanceof Date);
    assertEquals("2016-01-01", fac.getDateTime().toDateTimeString(null));
    assertEquals("20160101.NULL.NULL", fac.getDateTime().toDatabaseString());

    db_str = "20160100";

    fac = new DateTimeFactory2();
    fac.setDate(db_str);

    assertTrue(fac.getDateTime() instanceof Date);
    assertEquals("2016-01", fac.getDateTime().toDateTimeString(null));
    assertEquals("20160100.NULL.NULL", fac.getDateTime().toDatabaseString());

    db_str = "20160000";

    fac = new DateTimeFactory2();
    fac.setDate(db_str);

    assertTrue(fac.getDateTime() instanceof Date);
    assertEquals("2016", fac.getDateTime().toDateTimeString(null));
    assertEquals("20160000.NULL.NULL", fac.getDateTime().toDatabaseString());

    db_str = "20160000.NULL.NULL";
    DateTimeInterface dt = DateTimeFactory2.valueOf(db_str);
    assertTrue(dt instanceof Date);
    assertEquals("2016", dt.toDateTimeString(null));
    assertEquals("20160000.NULL.NULL", dt.toDatabaseString());

    db_str = "20160100.NULL.NULL";
    dt = DateTimeFactory2.valueOf(db_str);
    assertTrue(dt instanceof Date);
    assertEquals("2016-01", dt.toDateTimeString(null));
    assertEquals("20160100.NULL.NULL", dt.toDatabaseString());

    db_str = "20160101.NULL.NULL";
    dt = DateTimeFactory2.valueOf(db_str);
    assertTrue(dt instanceof Date);
    assertEquals("2016-01-01", dt.toDateTimeString(null));
    assertEquals("20160101.NULL.NULL", dt.toDatabaseString());
  }

  @Test
  public void testBC() {
    DateTimeFactory2.valueOf("2015-01-01T00:00:00");
    DateTimeFactory2.valueOf("-2015-01-01T00:00:00");
    DateTimeFactory2.valueOf("753-01-01T00:00:00");
    DateTimeFactory2.valueOf("-753-01-01T00:00:00");

    Interval d = (Interval) DateTimeFactory2.valueOf("1-01-01T01:00:00+0200");
    assertEquals("-0001-12-31T23:00:00+0000", d.toDateTimeString(TimeZone.getTimeZone("UTC")));

    d = (Interval) DateTimeFactory2.valueOf("-1-12-31T23:30:00+0000");
    assertEquals("-0001-12-31T23:30:00+0000", d.toDateTimeString(TimeZone.getTimeZone("UTC")));
    assertEquals(
        "0001-01-01T01:30:00+0200",
        d.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.POSITIVE, 02, 00)));

    d = (Interval) DateTimeFactory2.valueOf("-9999-01-01");
    assertTrue(d instanceof Date);
    assertEquals("-9999-01-01", d.toDateTimeString(null));
    assertEquals(
        "-9999-01-01", d.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.POSITIVE, 4, 30)));
    assertEquals("-99989899.NULL.NULL", d.getILB_NF2());

    Interval d2 = (Interval) DateTimeFactory2.valueOf(d.getILB_NF2());
    assertTrue(d2 instanceof Date);
    assertEquals("-9999-01-01", d2.toDateTimeString(null));
    assertEquals(
        "-9999-01-01", d2.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.POSITIVE, 4, 30)));
    assertEquals("-99989899.NULL.NULL", d2.getILB_NF2());

    d = (Interval) DateTimeFactory2.valueOf("-9999-01");
    assertTrue(d instanceof Date);
    assertEquals("-9999-01", d.toDateTimeString(null));
    assertEquals(
        "-9999-01", d.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.POSITIVE, 4, 30)));
    assertEquals("-99989900.NULL.NULL", d.getILB_NF2());

    d2 = (Interval) DateTimeFactory2.valueOf(d.getILB_NF2());
    assertTrue(d2 instanceof Date);
    assertEquals("-9999-01", d2.toDateTimeString(null));
    assertEquals(
        "-9999-01", d2.toDateTimeString(new UTCTimeZoneShift(UTCTimeZoneShift.POSITIVE, 4, 30)));
    assertEquals("-99989900.NULL.NULL", d2.getILB_NF2());
  }

  @Test
  public void testUTCRange() {
    Interval d = (Interval) DateTimeFactory2.valueOf("9999-01-01T23:59:59UTC");
    assertTrue(d instanceof UTCDateTime);
    assertEquals("253370851226UTC", d.getILB_NF1());
    Interval d2 = UTCDateTime.UTCSeconds(253370851225L, null);
    assertTrue(d instanceof UTCDateTime);
    assertEquals("253370851225UTC", d2.getILB_NF1());
    assertEquals(
        d.toDateTimeString(TimeZone.getDefault()), d.toDateTimeString(TimeZone.getDefault()));
    assertEquals("9999-01-01T23:59:59+0000", d.toDateTimeString(TimeZone.getTimeZone("UTC")));

    d = (Interval) DateTimeFactory2.valueOf("9999-01-01T23:59:59.999999999UTC");
    assertTrue(d instanceof UTCDateTime);
    assertEquals("253370851226UTC999999999", d.getILB_NF1());
    d2 = UTCDateTime.UTCSeconds(253370851225L, 999999999);
    assertTrue(d instanceof UTCDateTime);
    assertEquals("253370851225UTC999999999", d2.getILB_NF1());
    assertEquals(
        d.toDateTimeString(TimeZone.getDefault()), d.toDateTimeString(TimeZone.getDefault()));
    assertEquals(
        "9999-01-01T23:59:59.999999999+0000", d.toDateTimeString(TimeZone.getTimeZone("UTC")));

    d = (Interval) DateTimeFactory2.valueOf("-9999-01-01T00:00:00UTC");
    assertTrue(d instanceof UTCDateTime);
    assertEquals("-377680233600UTC", d.getILB_NF1());
    d2 = UTCDateTime.UTCSeconds(-377680233600L, null);
    assertTrue(d instanceof UTCDateTime);
    assertEquals("-377680233600UTC", d2.getILB_NF1());
    assertEquals(
        d.toDateTimeString(TimeZone.getDefault()), d.toDateTimeString(TimeZone.getDefault()));
    assertEquals("-9999-01-01T00:00:00+0000", d.toDateTimeString(TimeZone.getTimeZone("UTC")));

    d = (Interval) DateTimeFactory2.valueOf("-9999-01-01T00:00:00.0UTC");
    assertTrue(d instanceof UTCDateTime);
    assertEquals("-377680233600UTC0", d.getILB_NF1());
    d2 = UTCDateTime.UTCSeconds(-377680233600L, 0);
    assertTrue(d instanceof UTCDateTime);
    assertEquals("-377680233600UTC0", d2.getILB_NF1());
    assertEquals(
        d.toDateTimeString(TimeZone.getDefault()), d.toDateTimeString(TimeZone.getDefault()));
  }
}
