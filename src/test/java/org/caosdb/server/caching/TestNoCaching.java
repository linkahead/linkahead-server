/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.caching;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import org.apache.commons.jcs.JCS;
import org.apache.commons.jcs.access.CacheAccess;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.database.backend.transaction.RetrieveProperties;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestNoCaching {

  @BeforeAll
  public static void init() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.setProperty(ServerProperties.KEY_CACHE_DISABLE, "TRUE");
    CaosDBServer.initCaching();
    JCSCacheHelper.init();
  }

  @Test
  public void testCacheConfig() {
    CacheAccess<String, String> retrieve_properties_cache =
        JCS.getInstance(RetrieveProperties.CACHE_REGION);
    assertEquals(0, retrieve_properties_cache.getCacheAttributes().getMaxObjects());
  }
}
