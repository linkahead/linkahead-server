package org.caosdb.server.jobs.core;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestInsertFilesInDir {

  @BeforeAll
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testExclude() throws IOException {
    final InsertFilesInDir job = new InsertFilesInDir();
    job.init(null, null, null);
    job.parseValue("-e ^.*test.*$ test");
    final File testFile = new File("test.dat");
    assertTrue(job.isExcluded(testFile));
  }
}
