/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.jobs;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.entity.DeleteEntity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.UpdateEntity;
import org.jdom2.Element;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class JobConfigTest {

  @BeforeAll
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testGetTransactionType() {
    final JobConfig jobConfig = JobConfig.getInstance();
    assertEquals("Retrieve", jobConfig.getTransactionType(new RetrieveEntity("test")));
    assertEquals("Insert", jobConfig.getTransactionType(new InsertEntity("test", Role.Record)));
    assertEquals("Delete", jobConfig.getTransactionType(new DeleteEntity(new EntityID("1234"))));
    assertEquals("Update", jobConfig.getTransactionType(new UpdateEntity(new Element("Record"))));
  }
}
