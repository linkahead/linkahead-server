/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 */

package org.caosdb.server.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.CredentialsValidator;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.accessControl.UserSources;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrievePasswordValidatorImpl;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.entity.Message;
import org.caosdb.server.permissions.PermissionRule;
import org.caosdb.server.scripting.ScriptingPermissions;
import org.caosdb.server.scripting.ServerSideScriptingCaller;
import org.jdom2.Element;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;

public class TestScriptingResource {

  public static class RetrieveRoleMockup implements RetrieveRoleImpl {

    public RetrieveRoleMockup(Access a) {}

    @Override
    public Role retrieve(String role) throws TransactionException {
      Role ret = new Role();
      ret.name = "anonymous";
      ret.description = "bla";
      return ret;
    }

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}
  }

  public static class RetrievePermissionRules implements RetrievePermissionRulesImpl {

    public RetrievePermissionRules(Access a) {}

    @Override
    public HashSet<PermissionRule> retrievePermissionRule(String role) throws TransactionException {
      HashSet<PermissionRule> result = new HashSet<>();
      result.add(
          new PermissionRule(
              true, false, ScriptingPermissions.PERMISSION_EXECUTION("anonymous_ok")));
      return result;
    }

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}
  }

  public static class RetrieveUserMockUp implements RetrieveUserImpl {

    public RetrieveUserMockUp(Access a) {}

    @Override
    public ProtoUser execute(Principal principal) throws TransactionException {
      return new ProtoUser();
    }

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}
  }

  public static class RetrievePasswordValidator implements RetrievePasswordValidatorImpl {

    public RetrievePasswordValidator(Access a) {}

    @Override
    public CredentialsValidator<String> execute(String name) throws TransactionException {
      return new CredentialsValidator<String>() {

        @Override
        public boolean isValid(String credential) {
          return true;
        }
      };
    }

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }
  }

  @BeforeAll
  public static void setupShiro() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.initShiro();

    BackendTransaction.setImpl(RetrieveRoleImpl.class, RetrieveRoleMockup.class);
    BackendTransaction.setImpl(RetrievePermissionRulesImpl.class, RetrievePermissionRules.class);
    BackendTransaction.setImpl(RetrieveUserImpl.class, RetrieveUserMockUp.class);
    BackendTransaction.setImpl(
        RetrievePasswordValidatorImpl.class, RetrievePasswordValidator.class);

    UserSources.getDefaultRealm();
  }

  ScriptingResource resource =
      new ScriptingResource() {
        @Override
        public int callScript(
            java.util.List<String> invokation,
            Integer timeout_ms,
            java.util.List<org.caosdb.server.entity.FileProperties> files,
            Object authToken)
            throws Message {
          if (invokation.get(0).equals("anonymous_ok")) {
            return 0;
          }
          return -1;
        }

        @Override
        public Element generateRootElement(ServerSideScriptingCaller caller) {
          return new Element("OK");
        }

        @Override
        public Object generateAuthToken(String purpose) {
          return "";
        }
      };

  @Test
  public void testUnsupportedMediaType() {
    Representation entity = new StringRepresentation("asdf");
    entity.setMediaType(MediaType.TEXT_ALL);
    Request request = new Request(Method.POST, "../test", entity);
    request.getAttributes().put("SRID", "asdf1234");
    request.setDate(new Date());
    resource.init(null, request, new Response(null));
    resource.handle();
    assertEquals(Status.CLIENT_ERROR_UNSUPPORTED_MEDIA_TYPE, resource.getResponse().getStatus());
  }

  @Test
  public void testAnonymousWithOutPermission() {
    Subject user = SecurityUtils.getSubject();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    user.login(AnonymousAuthenticationToken.getInstance());
    Form form = new Form("call=anonymous_no_permission");
    Representation entity = form.getWebRepresentation();
    Request request = new Request(Method.POST, "../test", entity);
    request.setRootRef(new Reference("bla"));
    request.getAttributes().put("SRID", "asdf1234");
    request.setDate(new Date());
    request.setHostRef("bla");
    resource.init(null, request, new Response(null));

    resource.handle();
    assertEquals(Status.CLIENT_ERROR_FORBIDDEN, resource.getResponse().getStatus());
  }

  @Test
  public void testAnonymousWithPermission() {
    Subject user = SecurityUtils.getSubject();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    user.login(AnonymousAuthenticationToken.getInstance());
    Form form = new Form("call=anonymous_ok");
    Representation entity = form.getWebRepresentation();
    Request request = new Request(Method.POST, "../test", entity);
    request.setRootRef(new Reference("bla"));
    request.getAttributes().put("SRID", "asdf1234");
    request.setDate(new Date());
    request.setHostRef("bla");
    resource.init(null, request, new Response(null));

    resource.handle();
    assertEquals(Status.SUCCESS_OK, resource.getResponse().getStatus());
  }

  @Test
  public void testForm2invocation() throws Message {
    Form form =
        new Form(
            "-Ooption=OPTION&call=CA%20LL&-Ooption2=OPTION2&-p=POS1&-p4=POS3&-p2=POS2&IGNORED");
    List<String> list = resource.form2CommandLine(form);
    assertEquals("CA LL", list.get(0));
    assertEquals("--option=OPTION", list.get(1));
    assertEquals("--option2=OPTION2", list.get(2));
    assertEquals("POS1", list.get(3));
    assertEquals("POS2", list.get(4));
    assertEquals("POS3", list.get(5));
    assertEquals(6, list.size());
  }

  @Test
  public void testHandleForm() throws Message, IOException {
    Subject user = SecurityUtils.getSubject();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    user.login(AnonymousAuthenticationToken.getInstance());
    Form form = new Form("call=anonymous_ok");
    assertEquals(0, resource.handleForm(form));
  }
}
