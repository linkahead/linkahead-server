/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.accessControl;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import org.junit.jupiter.api.Test;

public class TestDefaultPamScriptCaller {
  @Test
  public void testDefaultPamScript() throws IOException, InterruptedException {
    final File f = new File(Pam.DEFAULT_PAM_SCRIPT);
    assertTrue(f.exists());

    final ProcessBuilder pb = new ProcessBuilder(f.getCanonicalPath(), "asdf", "asdf");
    final Process p = pb.start();
    assertFalse(p.waitFor() == 0);

    final Pam.DefaultPamScriptCaller c = new Pam.DefaultPamScriptCaller(null);
    assertFalse(c.isValid("asdf", "sdf2"));
  }

  @Test
  public void testTestPamScript() throws IOException, InterruptedException {
    final File f = new File("misc/pam_authentication/test_authentication.sh");
    assertTrue(f.exists());

    final ProcessBuilder pb = new ProcessBuilder(f.getCanonicalPath(), "asdf", "2345");
    final Process p = pb.start();
    assertTrue(p.waitFor() == 0);

    final Pam.DefaultPamScriptCaller c = new Pam.DefaultPamScriptCaller(null);
    assertFalse(c.isValid("asfs", "73457"));
  }
}
