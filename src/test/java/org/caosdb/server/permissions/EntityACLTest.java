/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.permissions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.BitSet;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.AuthenticationUtils;
import org.caosdb.server.accessControl.Config;
import org.caosdb.server.accessControl.CredentialsValidator;
import org.caosdb.server.accessControl.OneTimeAuthenticationToken;
import org.caosdb.server.accessControl.Principal;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrievePasswordValidatorImpl;
import org.caosdb.server.database.backend.interfaces.RetrievePermissionRulesImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.backend.interfaces.RetrieveUserImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.database.proto.ProtoUser;
import org.caosdb.server.resource.AbstractCaosDBServerResource;
import org.caosdb.server.resource.AbstractCaosDBServerResource.XMLParser;
import org.caosdb.server.utils.Utils;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class EntityACLTest {

  public static long convert(final BitSet bits) {
    long value = 0L;
    for (int i = 0; i < bits.length(); ++i) {
      value += bits.get(i) ? (1L << i) : 0L;
    }
    return value;
  }

  /** a no-op mock-up which resolved all rules to an empty set of permissions. */
  public static class RetrievePermissionRulesMockup implements RetrievePermissionRulesImpl {

    public RetrievePermissionRulesMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public HashSet<PermissionRule> retrievePermissionRule(String role) throws TransactionException {
      return new HashSet<>();
    }
  }

  /** a mock-up which returns null */
  public static class RetrieveRoleMockup implements RetrieveRoleImpl {

    public RetrieveRoleMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public Role retrieve(String role) throws TransactionException {
      return null;
    }
  }

  public static class RetrievePasswordValidatorMockup implements RetrievePasswordValidatorImpl {

    public RetrievePasswordValidatorMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public CredentialsValidator<String> execute(String name) throws TransactionException {
      if (name.equals("anonymous")) {
        return new CredentialsValidator<String>() {

          @Override
          public boolean isValid(String credential) {
            return false;
          }
        };
      }
      return null;
    }
  }

  public static class RetrieveUserMockup implements RetrieveUserImpl {

    public RetrieveUserMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public ProtoUser execute(Principal principal) throws TransactionException {
      if (principal.getUsername().equals("anonymous")) {
        return new ProtoUser();
      }
      return null;
    }
  }

  @BeforeAll
  public static void init() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.initShiro();
    assertNotNull(EntityACL.GLOBAL_PERMISSIONS);

    BackendTransaction.setImpl(
        RetrievePermissionRulesImpl.class, RetrievePermissionRulesMockup.class);
    BackendTransaction.setImpl(RetrieveRoleImpl.class, RetrieveRoleMockup.class);
    BackendTransaction.setImpl(
        RetrievePasswordValidatorImpl.class, RetrievePasswordValidatorMockup.class);
    BackendTransaction.setImpl(RetrieveUserImpl.class, RetrieveUserMockup.class);
  }

  @Test
  public void testConvert() {
    long l = Integer.MAX_VALUE;
    System.out.println(EntityACL.convert(l));
    l = Integer.MIN_VALUE;
    System.out.println(EntityACL.convert(l));
    l = Long.MAX_VALUE;
    System.out.println(EntityACL.convert(l));
    l = Long.MIN_VALUE;
    System.out.println(EntityACL.convert(l));
    final BitSet bitSet = new BitSet(64);
    assertEquals(0, convert(bitSet));

    bitSet.set(0);
    assertEquals(1, convert(bitSet));

    bitSet.clear();
    bitSet.set(63);
    assertEquals(Long.MIN_VALUE, convert(bitSet));

    bitSet.clear();
    bitSet.set(0, 63, true);
    assertEquals(Long.MAX_VALUE, convert(bitSet));

    bitSet.clear();
    bitSet.set(62);
    assertEquals(EntityACL.MIN_PRIORITY_BITSET, convert(bitSet));

    bitSet.set(63);
    assertEquals(-EntityACL.MIN_PRIORITY_BITSET, convert(bitSet));

    bitSet.clear();
    bitSet.set(0, 64, true);
    assertEquals(-1L, convert(bitSet));

    assertTrue(EntityACL.convert(-12523).get(63));
    assertFalse(EntityACL.convert(12523).get(63));

    bitSet.clear();
    bitSet.set(0, 63, true);
    assertEquals(Long.MAX_VALUE + 1L, Long.MIN_VALUE);
    assertEquals(Long.MAX_VALUE, convert(bitSet));
  }

  @Test
  public void testIsDenial() {
    final BitSet bitSet = new BitSet(64);
    bitSet.set(63);

    assertTrue(EntityACL.isDenial(convert(bitSet)));
    assertTrue(EntityACL.isDenial(-12523));
    assertFalse(EntityACL.isDenial(0));
    assertFalse(EntityACL.isDenial(246345));
    assertTrue(EntityACL.isDenial(-EntityACL.MIN_PRIORITY_BITSET));
  }

  @Test
  public void testIsAllowance() {
    final BitSet bitSet = new BitSet(32);
    bitSet.set(27);

    assertTrue(EntityACL.isAllowance(convert(bitSet)));
    assertTrue(EntityACL.isAllowance(12523));
    assertTrue(EntityACL.isAllowance(0));
    assertFalse(EntityACL.isAllowance(-246345));
    assertTrue(EntityACL.isAllowance(EntityACL.MIN_PRIORITY_BITSET));
  }

  @Test
  public void testGetResultingACL() {
    final LinkedList<Long> acl = new LinkedList<Long>();
    acl.add(1L); // +P1
    assertEquals(1L, EntityACL.getResultingACL(acl)); // P1

    acl.add(2L); // +P2
    assertEquals(3L, EntityACL.getResultingACL(acl)); // P1,P2

    acl.add(4L); // +P3
    assertEquals(7L, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(Long.MIN_VALUE + 1); // -P1
    assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(Long.MIN_VALUE + 1); // -P1Allow
    assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(Long.MIN_VALUE + 8); // -P4
    assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(8L); // +P4
    assertEquals(6L, EntityACL.getResultingACL(acl)); // P2,P3

    acl.add(EntityACL.MIN_PRIORITY_BITSET + 1); // ++P1
    assertEquals(7L, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(EntityACL.MIN_PRIORITY_BITSET + 2); // ++P2
    assertEquals(7L, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(Long.MIN_VALUE + 1); // -P1
    assertEquals(7l, EntityACL.getResultingACL(acl)); // P1,P2,P3

    acl.add(Long.MIN_VALUE + 4); // -P1
    assertEquals(3l, EntityACL.getResultingACL(acl)); // P1,P2

    acl.add(EntityACL.MIN_PRIORITY_BITSET + 8); // ++P4
    assertEquals(11L, EntityACL.getResultingACL(acl)); // P1,P2,P4

    acl.add(Long.MIN_VALUE + EntityACL.MIN_PRIORITY_BITSET + 2); // --P2
    assertEquals(9L, EntityACL.getResultingACL(acl)); // P1,P4

    acl.add(Long.MIN_VALUE + EntityACL.MIN_PRIORITY_BITSET + 1); // --P1Allow
    assertEquals(8L, EntityACL.getResultingACL(acl)); // P4
  }

  @Test
  public void testOwnerBitSet() {
    assertEquals(convert(EntityACL.convert(EntityACL.OWNER_BITSET).get(1, 32)), 0);
  }

  @Test
  public void testDeserialize() {
    assertTrue(EntityACL.deserialize("{}") instanceof EntityACL);
    assertTrue(EntityACL.deserialize("{\"tf\":134}") instanceof EntityACL);
    assertTrue(EntityACL.deserialize("{\"tf\":6343,\"bla\":884}") instanceof EntityACL);
    assertTrue(EntityACL.deserialize("{\"tf\":-2835,\"bla\":884}") instanceof EntityACL);
    assertTrue(EntityACL.deserialize("{\"?OWNER?\":526,\"tahsdh   \": -235}") instanceof EntityACL);
    assertTrue(EntityACL.deserialize("{\"asdf\":2345}") instanceof EntityACL);
    assertTrue(raisesIllegalStateException("{"));
    assertTrue(raisesIllegalStateException("}"));
    assertTrue(raisesIllegalStateException("{tf:}"));
    assertTrue(raisesIllegalStateException("{tf:;}"));
    assertTrue(raisesIllegalStateException("{:234}"));
    assertTrue(raisesIllegalStateException("{:234;}"));
    assertTrue(raisesIllegalStateException("{tf:tf;}"));
    assertTrue(raisesIllegalStateException("{tf: +5259;}"));
    assertTrue(raisesIllegalStateException("{tf;}"));
    assertTrue(raisesIllegalStateException("{tf:123223727356235782735235;}"));
  }

  public boolean raisesIllegalStateException(final String input) {
    try {
      EntityACL.deserialize(input);
    } catch (final IllegalStateException e) {
      return true;
    }
    return false;
  }

  public static AbstractCaosDBServerResource.XMLParser parser = new XMLParser();

  public Element stringToJdom(final String s) throws JDOMException, IOException {
    return parser.parse(Utils.String2InputStream(s)).getRootElement();
  }

  @Test
  public void testEntityACLForAnonymous() {
    Subject anonymous = SecurityUtils.getSubject();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    anonymous.login(AnonymousAuthenticationToken.getInstance());
    assertTrue(AuthenticationUtils.isAnonymous(anonymous));
    EntityACL acl = EntityACL.getOwnerACLFor(anonymous);
    assertNotNull(acl);
    assertTrue(acl.getOwners().isEmpty());
  }

  @Test
  public void testFactory() {
    final AbstractEntityACLFactory<EntityACL> f = new EntityACLFactory();

    org.caosdb.server.permissions.Role role1 = org.caosdb.server.permissions.Role.create("role1");
    Config config1 = new Config();
    config1.setRoles(new String[] {role1.toString()});
    OneTimeAuthenticationToken token1 = OneTimeAuthenticationToken.generate(config1);
    Subject user1 = SecurityUtils.getSecurityManager().createSubject(null);
    user1.login(token1);

    org.caosdb.server.permissions.Role role2 = org.caosdb.server.permissions.Role.create("role2");
    Config config2 = new Config();
    config2.setRoles(new String[] {role2.toString()});
    OneTimeAuthenticationToken token2 = OneTimeAuthenticationToken.generate(config2);
    Subject user2 = SecurityUtils.getSecurityManager().createSubject(null);
    user2.login(token2);

    f.grant(role1, "UPDATE:NAME");
    assertTrue((f.create().isPermitted(user1, EntityPermission.UPDATE_NAME)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.UPDATE_NAME)));
    f.grant(role2, "DELETE");
    assertFalse((f.create().isPermitted(user1, EntityPermission.DELETE)));
    assertTrue((f.create().isPermitted(user2, EntityPermission.DELETE)));
    f.deny(role2, 1);
    f.deny(role1, 1);
    assertFalse((f.create().isPermitted(user1, EntityPermission.DELETE)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.DELETE)));
    f.grant(role1, true, 1);
    assertTrue((f.create().isPermitted(user1, EntityPermission.DELETE)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.DELETE)));
    f.deny(role2, true, 1);
    assertTrue((f.create().isPermitted(user1, EntityPermission.DELETE)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.DELETE)));
    f.grant(role2, true, 1);
    assertTrue((f.create().isPermitted(user1, EntityPermission.DELETE)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.DELETE)));
    f.deny(role1, true, 1);
    assertFalse((f.create().isPermitted(user1, EntityPermission.DELETE)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.DELETE)));
    assertTrue((f.create().isPermitted(user1, EntityPermission.UPDATE_NAME)));
    assertFalse((f.create().isPermitted(user2, EntityPermission.UPDATE_NAME)));
  }

  @Test
  public void testRemove() {
    EntityACLFactory f = new EntityACLFactory();
    f.grant(org.caosdb.server.permissions.Role.create("role1"), false, EntityPermission.DELETE);
    f.deny(org.caosdb.server.permissions.Role.create("role2"), false, EntityPermission.EDIT_ACL);
    f.grant(
        org.caosdb.server.permissions.Role.create("role3"), true, EntityPermission.RETRIEVE_ACL);
    f.deny(
        org.caosdb.server.permissions.Role.create("role4"), true, EntityPermission.RETRIEVE_ENTITY);

    EntityACL other = f.create();

    f.grant(org.caosdb.server.permissions.Role.create("role2"), false, EntityPermission.EDIT_ACL);
    f.grant(
        org.caosdb.server.permissions.Role.create("role5"), false, EntityPermission.RETRIEVE_FILE);

    f.remove(other); // normalize and remove "other"

    EntityACL tester = f.create();
    assertEquals(1, tester.getRules().size()); // Permissions array has been written and read
    for (EntityACI aci : tester.getRules()) {
      assertEquals(aci.getResponsibleAgent(), org.caosdb.server.permissions.Role.create("role5"));
    }
  }

  @Test
  public void testNormalize() {
    EntityACLFactory f = new EntityACLFactory();
    f.grant(org.caosdb.server.permissions.Role.create("role1"), false, EntityPermission.DELETE);
    f.deny(org.caosdb.server.permissions.Role.create("role1"), false, EntityPermission.DELETE);
    f.grant(org.caosdb.server.permissions.Role.create("role1"), true, EntityPermission.DELETE);
    f.deny(org.caosdb.server.permissions.Role.create("role1"), true, EntityPermission.DELETE);

    // priority denail overrides everything else
    EntityACL denyDelete = f.create();
    assertEquals(1, denyDelete.getRules().size());
    for (EntityACI aci : denyDelete.getRules()) {
      assertEquals(org.caosdb.server.permissions.Role.create("role1"), aci.getResponsibleAgent());
      assertTrue(EntityACL.isDenial(aci.getBitSet()));
      assertTrue(EntityACL.isPriorityBitSet(aci.getBitSet()));
    }
  }

  @Test
  public void testOwnership() {
    EntityACLFactory f = new EntityACLFactory();
    f.grant(
        org.caosdb.server.permissions.Role.create("the_owner"), false, EntityPermission.EDIT_ACL);
    f.deny(
        org.caosdb.server.permissions.Role.create("someone_else"),
        false,
        EntityPermission.EDIT_ACL);
    EntityACL acl = f.create();
    assertEquals(1, acl.getOwners().size());
    assertEquals("the_owner", acl.getOwners().get(0).toString());
  }

  @Test
  public void testPermissionsFor() {
    EntityACLFactory f = new EntityACLFactory();
    f.deny(org.caosdb.server.permissions.Role.ANONYMOUS_ROLE, false, EntityPermission.EDIT_ACL);
    f.grant(org.caosdb.server.permissions.Role.OWNER_ROLE, false, "*");
    EntityACL acl = f.create();

    Subject anonymous = SecurityUtils.getSubject();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    anonymous.login(AnonymousAuthenticationToken.getInstance());
    assertTrue(AuthenticationUtils.isAnonymous(anonymous));

    assertNotNull(acl);
    assertTrue(acl.getOwners().isEmpty());
    final Set<EntityPermission> permissionsFor =
        EntityACL.getPermissionsFor(anonymous, acl.getRules());

    assertFalse(permissionsFor.contains(EntityPermission.RETRIEVE_ENTITY));
  }
}
