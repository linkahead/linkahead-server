/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2021 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.query;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.database.access.InitAccess;
import org.caosdb.server.transaction.WriteTransaction;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class QueryTest {

  @BeforeAll
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.initShiro();
  }

  String getCacheKey(String query) {
    Query q = new Query(query);
    q.parse();
    return q.getCacheKey(true);
  }

  String getCacheKeyWithUser(String query) {
    Subject anonymous = SecurityUtils.getSubject();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "true");
    anonymous.login(AnonymousAuthenticationToken.getInstance());
    Query q = new Query(query, anonymous);
    q.parse();
    return q.getCacheKey(true);
  }

  @Test
  public void testGetKey() {
    assertEquals("R_RECORDE_enameF_POV(pname,=,val1)", getCacheKey("FIND ename WITH pname = val1"));
    assertEquals(
        "R_RECORDE_enameF_POV(pname,=,val1)", getCacheKey("COUNT ename WITH pname = val1"));
    assertEquals(
        "R_RECORDE_enameF_POV(pname,=,val1)",
        getCacheKey("SELECT bla FROM ename WITH pname = val1"));
    assertEquals(
        "R_RECORDE_enameF_POV(pname,null,null)", getCacheKey("SELECT bla FROM ename WITH pname"));
    assertEquals(
        "R_RECORDE_enameF_maxPOV(pname,null,null)",
        getCacheKey("SELECT bla FROM ename WITH THE GREATEST pname"));

    assertEquals(
        "R_RECORDE_enameF_POV(pname,=,val1)", getCacheKey("FIND RECORD ename WITH pname = val1"));
    assertEquals("R_ENTITYF_POV(pname,=,val1)", getCacheKey("COUNT ENTITY WITH pname = val1"));
    assertEquals(
        "R_RECORDE_enameF_Conj(POV(pname,=,val1)POV(ename2,=,val2))",
        getCacheKey("SELECT bla FROM ename WITH pname = val1 AND ename2 = val2"));

    assertEquals("V_R_ENTITYF_ID(,>,2)", getCacheKey("FIND ANY VERSION OF ENTITY WITH ID > 2"));
    assertEquals("R_ENTITYF_ID(min,,)", getCacheKey("FIND ENTITY WITH THE SMALLEST ID"));
    assertEquals("R_ENTITYF_SAT(asdf/%%)", getCacheKey("FIND ENTITY WHICH IS STORED AT /asdf/*"));
    assertEquals(
        "R_ENTITYF_SAT(asdf/asdf)", getCacheKey("FIND ENTITY WHICH IS STORED AT asdf/asdf"));
    assertEquals(
        "R_RECORDE_enameF_POV(ref1,null,null)SUB(POV(pname,>,val1)",
        getCacheKey("FIND ename WITH ref1 WITH pname > val1 "));
    assertEquals(
        "R_RECORDE_enameF_@(ref1,null)SUB(POV(pname,>,val1)",
        getCacheKey("FIND ename WHICH IS REFERENCED BY ref1 WITH pname > val1 "));

    assertEquals(
        "U_anonymous@anonymousR_RECORDE_enameF_POV(pname,=,val1)",
        getCacheKeyWithUser("FIND ename WITH pname = val1"));
  }

  @Test
  public void testOptimizationOfFindStar() {
    Query q = new Query("FIND *");
    q.parse(false);
    assertEquals("*", q.getEntity().str);
    assertNull(q.getRole());

    q.optimize();
    assertNull(q.getEntity());
    assertEquals(Query.Role.RECORD, q.getRole());
  }

  /**
   * Assure that {@link WriteTransaction#commit()} calls {@link Query#clearCache()}.
   *
   * <p>Since currently the cache shall be cleared whenever there is a commit.
   */
  @Test
  public void testEtagChangesAfterWrite() {
    String old = Query.getETag();
    assertNotNull(old);

    WriteTransaction w =
        new WriteTransaction(null) {

          @Override
          public boolean useCache() {
            // this function is being overriden purely for the purpose of calling
            // commit() (which is protected)
            try {
              // otherwise the test fails because getAccess() return null;
              setAccess(new InitAccess(null));

              commit();
            } catch (Exception e) {
              fail("this should not happen");
            }
            return false;
          }
        };

    // trigger commit();
    w.useCache();

    String neu = Query.getETag();
    assertNotEquals(old, neu, "old and new tag should not be equal");
  }
}
