/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2019 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class UtilsTest {

  /** Test utility functions. */

  /** The log function from utils contained a typo. So here we test it for correct functionality. */
  @Test
  public void testFunctionLog() {
    assert Utils.log(1024, 1024L) == 1.0;
    assert Utils.log(1024, 1028L) > 1.0;
    assert Utils.log(1024, (long) Math.pow(1024, 2)) == 2.0;
  }

  /** Test some cases of human readable file sizes. */
  @Test
  public void testConvertFileSizes() {
    assertEquals(Utils.getReadableByteSize(1022L), "1022B");
    assertEquals(Utils.getReadableByteSize(0L), "0B");
    assertEquals(Utils.getReadableByteSize(-1L), "-1B");
    assertEquals(Utils.getReadableByteSize(1024L), "1KiB");
    assertEquals(Utils.getReadableByteSize(2048L), "2KiB");
    assertEquals(Utils.getReadableByteSize(1024 * 1024L), "1MiB");
    assertEquals(Utils.getReadableByteSize(1024 * 2048L), "2MiB");
  }
}
