/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.regex.Pattern;
import net.jcip.annotations.NotThreadSafe;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.FileSystem;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemCheckHash;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemCheckSize;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemFileExists;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemFileWasModifiedAfter;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemGetFileIterator;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemGetFileIterator.FileNameIterator;
import org.caosdb.server.database.backend.implementation.UnixFileSystem.UnixFileSystemHelper;
import org.caosdb.server.database.backend.interfaces.FileCheckHash;
import org.caosdb.server.database.backend.interfaces.FileCheckSize;
import org.caosdb.server.database.backend.interfaces.FileExists;
import org.caosdb.server.database.backend.interfaces.FileWasModifiedAfter;
import org.caosdb.server.database.backend.interfaces.GetFileIteratorImpl;
import org.caosdb.server.database.backend.transaction.FileConsistencyCheck;
import org.caosdb.server.database.backend.transaction.GetFileIterator;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.Message;
import org.eclipse.jetty.io.RuntimeIOException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

@NotThreadSafe
public class FileUtilsTest {
  public static Path tempDirectory;
  public static File testRoot;
  public static File someDir;
  public static File linkToSomeDir;
  public static File someFile;
  public static File someDir_someFile;
  public static File linkToSomeFile;
  public static File tmpFolderCaosDB;

  private static File newFolder(String name) throws IOException {
    Path ret = Files.createTempDirectory(tempDirectory, name);
    return ret.toFile();
  }

  @BeforeAll
  public static void setup() throws Message, IOException, CaosDBException, InterruptedException {
    CaosDBServer.initServerProperties();
    BackendTransaction.setImpl(FileExists.class, UnixFileSystemFileExists.class);
    BackendTransaction.setImpl(GetFileIteratorImpl.class, UnixFileSystemGetFileIterator.class);
    BackendTransaction.setImpl(
        FileWasModifiedAfter.class, UnixFileSystemFileWasModifiedAfter.class);
    BackendTransaction.setImpl(FileCheckSize.class, UnixFileSystemCheckSize.class);
    BackendTransaction.setImpl(FileCheckHash.class, UnixFileSystemCheckHash.class);

    tempDirectory = Files.createTempDirectory(null);
    testRoot = newFolder("fileutils_testfolder");
    File basePath = newFolder("caosdbRoot");
    File dropOffBox = newFolder("dropOffBox");
    someDir = testRoot.toPath().resolve("some_dir").toFile();
    linkToSomeDir = testRoot.toPath().resolve("link_to_some_dir").toFile();
    someFile = testRoot.toPath().resolve("some_file").toFile();
    someDir_someFile = someDir.toPath().resolve("someFile").toFile();
    linkToSomeFile = testRoot.toPath().resolve("link_to_some_file").toFile();

    tmpFolderCaosDB = newFolder("tmpFolderCaosDB");
    CaosDBServer.setProperty(ServerProperties.KEY_FILE_SYSTEM_ROOT, basePath.toString());
    CaosDBServer.setProperty(ServerProperties.KEY_DROP_OFF_BOX, dropOffBox.toString());
    CaosDBServer.setProperty(ServerProperties.KEY_TMP_FILES, tmpFolderCaosDB.toString());
    FileSystem.init();

    basePath = new File(FileSystem.getBasepath());
    assertTrue(basePath.canWrite());
    assertTrue(basePath.canRead());
    assertTrue(basePath.canExecute());
    assertTrue(new File(FileSystem.getTmp()).canWrite());
    assertTrue(new File(FileSystem.getTmp()).canRead());
    assertTrue(new File(FileSystem.getTmp()).canExecute());
    assertTrue(new File(FileSystem.getDropOffBox()).canWrite());
    assertTrue(new File(FileSystem.getDropOffBox()).canRead());
    assertTrue(new File(FileSystem.getDropOffBox()).canExecute());

    deleteTmp();
    // FileUtils.createFolders(testRoot);
    FileUtils.createFolders(someDir);
    someFile.createNewFile();
    someDir_someFile.createNewFile();
    FileUtils.createSymlink(linkToSomeDir, someDir);
    FileUtils.createSymlink(linkToSomeFile, someDir_someFile);
  }

  @AfterAll
  public static void teardown() throws IOException, CaosDBException, InterruptedException {
    FileUtils.delete(testRoot, true);
    deleteTmp();
    assertFalse(testRoot.exists());
    if (tempDirectory.toFile().exists()) {
      tempDirectory.toFile().delete();
    }
  }

  /**
   * @throws InterruptedException
   * @throws CaosDBException
   */
  @BeforeEach
  public void testTmpEmpty() throws IOException, CaosDBException, InterruptedException {
    if (new File(FileSystem.getTmp()).exists()) {
      if (0 != new File(FileSystem.getTmp()).list().length) {
        System.err.println("TMP not empty, aborting test!");
        System.err.println(FileSystem.getTmp());
        for (String f : new File(FileSystem.getTmp()).list()) {
          System.err.println(f);
        }
        teardown();
        fail("TMP not empty, aborting test");
      }
      assertEquals(0, new File(FileSystem.getTmp()).list().length);
    }
  }

  public static void deleteTmp() throws IOException, CaosDBException, InterruptedException {
    File tmpDir = new File(FileSystem.getTmp());
    if (tmpDir.exists())
      for (File f : tmpDir.listFiles()) {
        System.err.println(f.getAbsolutePath());
        FileUtils.delete(f, true).cleanUp();
      }
  }

  @AfterEach
  public void callDeleteTmp() throws IOException, CaosDBException, InterruptedException {
    deleteTmp();
  }

  @Test
  public void testGetMimeTypeNull() {

    assertThrows(NullPointerException.class, () -> FileUtils.getMimeType(null));
  }

  @Test
  public void testGetMimeTypeNonExisting() {

    final File f = new File("nonexisting");
    assertFalse(f.exists());

    assertThrows(RuntimeIOException.class, () -> FileUtils.getMimeType(f));
  }

  @Test
  public void testGetMimeTypeTextFile() {
    final File f = someFile;
    assertTrue(f.exists());
    assertEquals("inode/x-empty", FileUtils.getMimeType(f));
  }

  @Test
  public void testGetMimeTypeSymlink() throws IOException, InterruptedException, CaosDBException {
    final File symlink = testRoot.toPath().resolve("simlink").toFile();

    try {
      FileUtils.createSymlink(symlink, someFile);
      assertEquals("inode/x-empty", FileUtils.getMimeType(symlink));
    } catch (final Exception e) {
      throw e;
    } finally {
      FileUtils.unlink(symlink);
    }
  }

  @Test
  public void testIsSubDirNullChild() {

    assertThrows(NullPointerException.class, () -> FileUtils.isSubDirSymlinkSave(null, someDir));
  }

  @Test
  public void testIsSubDirNullPar() {

    assertThrows(NullPointerException.class, () -> FileUtils.isSubDirSymlinkSave(someFile, null));
  }

  @Test
  public void testIsSubDirNull() {
    assertThrows(NullPointerException.class, () -> FileUtils.isSubDirSymlinkSave(null, null));
  }

  @Test
  public void directChild1() {
    final File child = someFile;
    final File parent = testRoot;
    assertTrue(FileUtils.isSubDirSymlinkSave(child, parent));
  }

  @Test
  public void deepChild() {
    final File child = someDir_someFile;
    final File parent = testRoot;
    assertTrue(FileUtils.isSubDirSymlinkSave(child, parent));
  }

  @Test
  public void testSymlinkRoot() throws IOException, InterruptedException, CaosDBException {
    final File symlink_root = linkToSomeDir;
    assertTrue(symlink_root.exists());
    assertTrue(symlink_root.isDirectory());
    assertTrue(FileUtils.isSymlink(symlink_root));
    try {
      FileUtils.isSubDirSymlinkSave(someFile, symlink_root);
    } catch (final RuntimeException e) {
      assertEquals("Illegal symlink found!", e.getMessage());
    }
  }

  @Test
  public void testSymlinkChildFile() {
    final File child = linkToSomeFile;
    assertTrue(child.exists());
    assertTrue(child.isFile());
    assertTrue(FileUtils.isSymlink(child));
    assertTrue(FileUtils.isSubDirSymlinkSave(child, testRoot));
  }

  @Test
  public void testSymlinkChildDir() {
    final File root = testRoot;
    final File symlink_child = linkToSomeDir;
    assertTrue(symlink_child.exists());
    assertTrue(symlink_child.isDirectory());
    assertTrue(FileUtils.isSymlink(symlink_child));
    try {
      FileUtils.isSubDirSymlinkSave(symlink_child, root);
    } catch (final RuntimeException e) {
      assertEquals("Illegal symlink found!", e.getMessage());
    }
  }

  @Test
  public void testIllegalDoubleDot() {
    final File child = new File(linkToSomeDir.getAbsolutePath() + "../");
    assertFalse(child.exists());
  }

  @Test
  public void TestChownScript() throws IOException, InterruptedException, CaosDBException {
    FileUtils.testChownScript();

    final File f = new File(FileSystem.getDropOffBox() + "chown_test_file");
    f.createNewFile();
    try {
      FileUtils.runChownScript(f);
    } finally {
      if (f.exists()) {
        f.delete();
      }
    }
  }

  @Test
  public void testCreateFolders() throws Message {
    final File f = new File(testRoot.getAbsoluteFile() + "/testfolder/testsubfolder/testfile");
    assertFalse(f.getAbsoluteFile().getParentFile().exists());
    final Undoable u = FileUtils.createFolders(f.getAbsoluteFile().getParentFile());
    assertTrue(f.getAbsoluteFile().getParentFile().exists());
    u.undo();
    assertFalse(f.getAbsoluteFile().getParentFile().exists());
  }

  @Test
  public void testRenameUndoable1() throws IOException, Message {
    final String newFileTmpName = FileSystem.getTmp() + "renameTestFile" + Utils.getUID();
    final File newFile = new File(newFileTmpName);
    assertTrue(newFile.createNewFile());
    newFile.deleteOnExit();
    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    assertFalse(target.exists());
    target.deleteOnExit();

    final Undoable u = FileUtils.rename(newFile, target);

    // file is moved to file system
    assertFalse(new File(newFileTmpName).exists());
    assertTrue(new File(targetName).exists());
    assertEquals(target.getAbsolutePath(), targetName);
    assertFalse(newFile.exists());

    u.undo();

    // file is moved back to tmp dir
    assertTrue(new File(newFileTmpName).exists());
    assertFalse(new File(targetName).exists());

    newFile.delete();
    target.delete();
  }

  @Test
  public void testRenameUndoable2() throws IOException, Message, NoSuchAlgorithmException {
    final String newFileTmpName = FileSystem.getTmp() + "renameTestFile" + Utils.getUID();
    final File newFile = new File(newFileTmpName);
    assertFalse(newFile.exists());

    final PrintWriter outNew = new PrintWriter(newFileTmpName);
    outNew.println("newFile");
    outNew.close();

    assertTrue(newFile.exists());
    newFile.deleteOnExit();

    final String newHash = FileUtils.getChecksum(newFile);

    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    assertFalse(target.exists());
    final PrintWriter outOld = new PrintWriter(targetName);
    outOld.println("oldFile");
    outOld.close();

    assertTrue(target.exists());
    target.deleteOnExit();

    final String oldHash = FileUtils.getChecksum(target);

    final Undoable u = FileUtils.rename(newFile, target);

    // file is moved to file system, old file is moved to tmp
    assertFalse(new File(newFileTmpName).exists());
    assertTrue(new File(targetName).exists());
    assertEquals(target.getAbsolutePath(), targetName);
    assertFalse(newFile.exists());

    u.undo();

    // file is moved back to tmp dir, old file is moved back to filesystem.
    assertTrue(new File(newFileTmpName).exists());
    assertTrue(new File(targetName).exists());

    assertEquals(newHash, FileUtils.getChecksum(new File(newFileTmpName)));
    assertEquals(oldHash, FileUtils.getChecksum(new File(targetName)));

    target.delete();
    newFile.delete();
  }

  @Test
  public void testRenameUndoable3()
      throws IOException, Message, NoSuchAlgorithmException, InterruptedException {
    assertEquals(0, new File(FileSystem.getTmp()).listFiles().length);

    final String newFileTmpName = FileSystem.getTmp() + "renameTestFile" + Utils.getUID();
    final File newFile = new File(newFileTmpName);
    assertFalse(newFile.exists());

    final PrintWriter outNew = new PrintWriter(newFileTmpName);
    outNew.println("newFile");
    outNew.close();

    assertTrue(newFile.exists());
    newFile.deleteOnExit();

    final String newHash = FileUtils.getChecksum(newFile);

    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    assertFalse(target.exists());
    final PrintWriter outOld = new PrintWriter(targetName);
    outOld.println("oldFile");
    outOld.close();

    assertTrue(target.exists());
    target.deleteOnExit();

    final Undoable u = FileUtils.rename(newFile, target);

    // file is moved to file system, old file is moved to tmp
    assertFalse(new File(newFileTmpName).exists());
    assertTrue(new File(targetName).exists());
    assertEquals(target.getAbsolutePath(), targetName);
    assertFalse(newFile.exists());

    u.cleanUp();

    // tmp file has been deleted and new file exists in the file system.
    assertFalse(new File(newFileTmpName).exists());
    assertTrue(new File(targetName).exists());

    // tmp folder is empty again.
    assertEquals(0, new File(FileSystem.getTmp()).listFiles().length);

    assertEquals(newHash, FileUtils.getChecksum(new File(targetName)));

    newFile.delete();
    target.delete();
  }

  @Test
  public void testRenameUndoable4() throws IOException, Message, NoSuchAlgorithmException {

    final String targetName = FileSystem.getPath("renameTestFile.dat");
    final File target = new File(targetName);
    assertFalse(target.exists());
    final PrintWriter outOld = new PrintWriter(targetName);
    outOld.println("file");
    outOld.close();

    assertTrue(target.exists());
    target.deleteOnExit();

    final String hash = FileUtils.getChecksum(target);

    final File newFile = new File(targetName);
    assertEquals(newFile.getAbsolutePath(), target.getAbsolutePath());

    // rename file to itself
    final Undoable u = FileUtils.rename(newFile, target);

    u.cleanUp();

    assertTrue(new File(targetName).exists());
    assertEquals(hash, FileUtils.getChecksum(new File(targetName)));

    target.delete();
  }

  @Test
  public void testSymlinkBehavior() throws IOException, InterruptedException, CaosDBException {

    // root dir for this test
    final String rootPath = "./testSymlinkBehavior/";
    final File root = new File(rootPath);
    root.deleteOnExit();
    root.mkdir();

    assertTrue(root.isDirectory());
    assertFalse(FileUtils.isSymlink(root));

    // one file, ...
    final File dir1 = new File(root + "/dir1/");
    dir1.deleteOnExit();

    // ... one symlink
    final File dir1Link = new File(root + "/dir1Link/");
    dir1Link.deleteOnExit();

    dir1.mkdir();
    FileUtils.createSymlink(dir1Link, dir1);

    assertTrue(dir1.isDirectory());
    assertFalse(FileUtils.isSymlink(dir1));
    assertTrue(dir1Link.isDirectory());
    assertTrue(FileUtils.isSymlink(dir1Link));

    // delete symlink (do not delete dir1!)
    dir1Link.delete();
    assertFalse(dir1Link.exists());
    assertTrue(dir1.exists());

    // recreate symlink
    FileUtils.createSymlink(dir1Link, dir1);
    assertTrue(dir1.isDirectory());
    assertFalse(FileUtils.isSymlink(dir1));
    assertTrue(dir1Link.isDirectory());
    assertTrue(FileUtils.isSymlink(dir1Link));

    // delete dir1
    dir1.delete();
    // ... dir1 does not exist
    assertFalse(dir1.exists());
    // ... and dir1Link does also not exists!!!
    assertFalse(dir1Link.exists());

    // recreate dir1
    dir1.mkdir();
    assertTrue(dir1.exists());
    assertTrue(dir1.isDirectory());
    assertFalse(FileUtils.isSymlink(dir1));
    assertTrue(dir1Link.exists());
    assertTrue(dir1Link.isDirectory());
    assertTrue(FileUtils.isSymlink(dir1Link));
  }

  @Test
  public void testFileTimestamp() throws IOException, InterruptedException {

    // root dir for this test
    final String rootPath = "./testFileTimestamp/";
    final File root = new File(rootPath);
    root.deleteOnExit();

    root.mkdir();
    assertTrue(root.isDirectory());
    assertTrue(root.exists());

    // root timestamp
    final long t1 = root.lastModified();
    assertFalse(t1 == 0);

    // timestamp has 1 second accuracy
    Thread.sleep(1000);

    // createFile
    final File file = new File(rootPath + "file");
    file.deleteOnExit();
    file.createNewFile();
    assertTrue(file.exists());
    assertTrue(file.isFile());

    // file timestampm
    final long t2 = file.lastModified();
    assertFalse(t2 == 0);

    // file was created -> root dir changed
    assertFalse(t1 == t2);

    // timestamp has 1 second accuracy
    Thread.sleep(1000);

    // write something to file
    final PrintStream out = new PrintStream(file);
    out.println("blablabla");
    out.flush();
    out.close();

    final long t3 = file.lastModified();
    assertFalse(t3 == 0);

    assertFalse(t2 == t3);
  }

  static Access access = Initialization.setUp().getAccess();

  @Test
  public void testFileSystemConsistencyCheck()
      throws NoSuchAlgorithmException,
          IOException,
          TransactionException,
          InterruptedException,
          Message {
    final String rootPath = "./testFileSystemConsistencyCheck/";
    final File root = new File(rootPath);
    root.deleteOnExit();
    root.mkdir();

    final UnixFileSystemHelper h = new UnixFileSystemHelper(rootPath);
    access.setHelper("UnixFileSystemHelper", h);

    FileConsistencyCheck check =
        execute(new FileConsistencyCheck("test1.dat", 0, null, 0L, null), access);
    assertEquals(FileConsistencyCheck.FILE_DOES_NOT_EXIST, check.getResult());

    final File f = new File(rootPath + "test1.dat");
    f.deleteOnExit();
    f.createNewFile();

    PrintStream out = new PrintStream(f);
    out.println("blablabla");
    out.flush();
    out.close();
    final SHA512 hasher = new SHA512();
    final String hash = hasher.hash(f);
    final long size = f.length();
    final long timestamp = f.lastModified();

    check = execute(new FileConsistencyCheck("test1.dat", size, hash, timestamp, hasher), access);
    assertEquals(FileConsistencyCheck.OK, check.getResult());

    // timestamp has 1 second accuracy
    Thread.sleep(1000);

    out = new PrintStream(f);
    out.println("blablabla");
    out.flush();
    out.close();
    final String hash2 = hasher.hash(f);
    final long timestamp2 = f.lastModified();
    final long size2 = f.length();
    assertEquals(hash, hash2);
    assertTrue(timestamp2 > timestamp);

    assertTrue(f.exists());
    check = execute(new FileConsistencyCheck("test1.dat", size2, hash, timestamp, hasher), access);
    assertEquals(FileConsistencyCheck.OK, check.getResult());

    out = new PrintStream(f);
    out.println("asdfasdfasdf");
    out.flush();
    out.close();

    check = execute(new FileConsistencyCheck("test1.dat", size, hash, timestamp, hasher), access);
    assertEquals(FileConsistencyCheck.FILE_MODIFIED, check.getResult());
  }

  private <K extends BackendTransaction> K execute(K t, Access access2) {
    t.setAccess(access2);
    t.executeTransaction();
    return t;
  }

  @Test
  public void testFileIterator() throws IOException {
    final String rootPath = "./testFileIterator/";
    final File root = new File(rootPath);
    root.deleteOnExit();
    root.mkdir();

    final File dat1 = new File(rootPath + "dat1");
    dat1.deleteOnExit();
    dat1.createNewFile();

    final File dat2 = new File(rootPath + "dat2");
    dat2.deleteOnExit();
    dat2.createNewFile();

    final File subdir = new File(rootPath + "subdir/");
    subdir.deleteOnExit();
    subdir.mkdir();

    final File empty = new File(rootPath + "empty/");
    empty.deleteOnExit();
    empty.mkdir();

    final File dat3 = new File(rootPath + "subdir/dat3");
    dat3.deleteOnExit();
    dat3.createNewFile();

    final FileNameIterator iterator = new FileNameIterator(root, rootPath);

    assertEquals(rootPath + "dat1", iterator.next());
    assertEquals(rootPath + "dat2", iterator.next());
    assertEquals(rootPath + "empty/", iterator.next());
    assertEquals(rootPath + "subdir/dat3", iterator.next());
  }

  @Test
  public void testGetFileIterator() throws TransactionException, IOException, Message {
    final String rootPath = "/testGetFileIterator/";

    final File root = new File(FileSystem.getPath(rootPath));
    root.deleteOnExit();
    root.mkdir();

    final UnixFileSystemHelper h = new UnixFileSystemHelper(rootPath);
    access.setHelper("UnixFileSystemHelper", h);

    final File dat1 = new File(FileSystem.getPath(rootPath + "dat1"));
    dat1.deleteOnExit();
    dat1.createNewFile();

    final File dat2 = new File(FileSystem.getPath(rootPath + "dat2"));
    dat2.deleteOnExit();
    dat2.createNewFile();

    final File subdir = new File(FileSystem.getPath(rootPath + "subdir/"));
    subdir.deleteOnExit();
    subdir.mkdir();

    final File empty = new File(FileSystem.getPath(rootPath + "empty/"));
    empty.deleteOnExit();
    empty.mkdir();

    final File dat3 = new File(FileSystem.getPath(rootPath + "subdir/dat3"));
    dat3.deleteOnExit();
    dat3.createNewFile();

    final Iterator<String> iterator = execute(new GetFileIterator(rootPath), access).getIterator();

    assertEquals(rootPath + "dat1", iterator.next());
    assertEquals(rootPath + "dat2", iterator.next());
    assertEquals(rootPath + "empty/", iterator.next());
    assertEquals(rootPath + "subdir/dat3", iterator.next());
  }

  /**
   * Tests if assertDir
   * <li>Recognizes an existing directory
   * <li>Recognizes a non-directory file
   * <li>Creates a given directory
   * <li>Creates a random directory
   */
  @Test
  public void testir() throws IOException {
    String existingDirStr = "existingDir";
    String existingFileStr = "existingFile";
    String newDirStr = "newDir/";
    File existingDir = new File(tmpFolderCaosDB, existingDirStr);
    existingDir.mkdir();
    File existingFile = new File(tmpFolderCaosDB, existingFileStr);
    existingFile.createNewFile();
    File newDir = new File(tmpFolderCaosDB, newDirStr);

    // Existing directory
    assertEquals(existingDir.toString(), FileSystem.assertDir(existingDirStr));

    // Existing file
    boolean thrown = false;
    try {
      FileSystem.assertDir(existingFileStr);
    } catch (IOException e) {
      assertEquals(e.getMessage(), "File " + existingFile.toString() + " is not a directory.");
      thrown = true;
    } finally {
      if (!thrown) {
        fail(
            "FileSystem.assertDir("
                + existingFileStr
                + ") did not throw an exception, although the existing file was not a directory.");
      }
    }

    // New dir with fixed name
    assertFalse(newDir.exists());
    assertEquals(newDir.toString(), FileSystem.assertDir(newDirStr));
    assertTrue(newDir.exists());

    // New dir with random name
    String randomDirStr = FileSystem.assertDir(null);
    File randomDir = new File(randomDirStr);
    assertTrue(randomDir.exists());
    assertTrue(randomDir.isDirectory());

    // Example: WFUWVHTP-U4KLDIEQ-JGKSTJPM
    String randName = randomDir.getName();
    assertTrue(randName.length() >= 26);
    assertTrue(Pattern.matches("^[-A-Z2-7]+$", randName));
  }
}
