/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.scripting;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.entity.FileProperties;
import org.caosdb.server.entity.Message;
import org.caosdb.server.utils.ServerMessages;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

public class TestServerSideScriptingCaller {

  public static File testFolder = null;
  public static File testFile = null;
  public static File testExecutable = null;
  final Map<String, String> emptyEnv = new HashMap<String, String>();

  @BeforeAll
  public static void setupTestFolder() throws IOException {
    testFolder = Files.createTempDirectory(".TestDir_ServerSideScripting").toFile();
    CaosDBServer.initServerProperties();
    CaosDBServer.getServerProperties()
        .setProperty(
            ServerProperties.KEY_SERVER_SIDE_SCRIPTING_BIN_DIRS, testFolder.getAbsolutePath());

    testFile = testFolder.toPath().resolve("TestFile").toFile();
    testExecutable = testFolder.toPath().resolve("TestScript").toFile();
    FileUtils.forceDeleteOnExit(testFolder);
    FileUtils.forceDeleteOnExit(testFile);
    FileUtils.forceDeleteOnExit(testExecutable);

    assertFalse(testFile.exists());
    assertFalse(testExecutable.exists());
    testFolder.mkdirs();
    testFile.createNewFile();
    FileUtils.write(testFile, "asdfhaskdjfhaskjdf", "UTF-8");

    testExecutable.createNewFile();
    testExecutable.setExecutable(true);
  }

  @AfterAll
  public static void deleteTestFolder() throws IOException {
    FileUtils.forceDelete(testFile);
    FileUtils.forceDelete(testFolder);
    assertFalse(testFolder.exists());
    assertFalse(testFile.exists());
  }

  public void testExeExit(final int code) throws IOException {
    FileUtils.write(testExecutable, "exit " + code, "UTF-8");
  }

  public void testSleep(final int seconds) throws IOException {
    FileUtils.write(testExecutable, "sleep " + seconds, "UTF-8");
  }

  public void testPrintArgsToStdErr() throws IOException {
    FileUtils.write(testExecutable, "(>&2 echo \"$@\")", "UTF-8");
  }

  public void testPrintArgsToStdOut() throws IOException {
    FileUtils.write(testExecutable, "echo \"$@\"", "UTF-8");
  }

  /**
   * The test executable will write the content of the given environment variable
   *
   * <p>Output goes to stdout, between hyphens, like so: `-my value-`.
   */
  private void preparePrintEnv(final String variable) throws IOException {
    FileUtils.write(testExecutable, "echo \"-$" + variable + "-\"", "UTF-8");
  }

  private File pwd;

  @BeforeEach
  public void setupPWD() throws IOException {
    this.pwd = testFolder.toPath().resolve("testPWD").toFile();
    FileUtils.forceDeleteOnExit(this.pwd);
    assertFalse(this.pwd.exists());
  }

  @AfterEach
  public void deletePWD() throws IOException {
    if (this.pwd.exists()) {
      FileUtils.forceDelete(this.pwd);
    }
    assertFalse(this.pwd.exists());
  }

  @Test
  public void testCreateWorkingDirFailure() throws Exception {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);

    this.pwd.mkdirs();
    Exception exc = assertThrows(Exception.class, () -> caller.createWorkingDir());
    assertEquals(
        exc.getMessage(), "The working directory must be non-existing when the caller is invoked.");
  }

  @Test
  public void testCreateWorkingDirOk() throws Exception {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();

    // exists
    assertTrue(this.pwd.exists());
    assertTrue(this.pwd.isDirectory());
  }

  /**
   * Throw {@link FileNotFoundException} because PWD does not exist.
   *
   * @throws IOException
   * @throws CaosDBException
   */
  @Test()
  public void testPutFilesInWorkingDirFailure1() throws IOException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);

    Exception exc =
        assertThrows(Exception.class, () -> caller.putFilesInWorkingDir(new ArrayList<>()));
    assertEquals(exc.getMessage(), "Could not create the FILE_UPLOAD_DIR.");
  }

  /**
   * Throw {@link CaosDBException} because tmpIdentifier is null or empty.
   *
   * @throws FileNotFoundException
   * @throws CaosDBException
   */
  @Test
  public void testPutFilesInWorkingDirFailure2() throws FileNotFoundException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    files.add(new FileProperties(null, null, null));

    Exception exc = assertThrows(Exception.class, () -> caller.putFilesInWorkingDir(files));
    assertEquals(exc.getMessage(), "The path must not be null or empty!");
  }

  /**
   * Throw {@link NullPointerException} because file is null.
   *
   * @throws FileNotFoundException
   * @throws CaosDBException
   */
  @Test
  public void testPutFilesInWorkingDirFailure3() throws FileNotFoundException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    final FileProperties f = new FileProperties(null, null, null);
    f.setTmpIdentifier("a2s3d4f5");
    f.setPath("testfile");
    files.add(f);

    Exception exc = assertThrows(Exception.class, () -> caller.putFilesInWorkingDir(files));
    assertEquals(exc.getMessage(), "target was null.");
  }

  /**
   * Throw {@link TransactionException} because file does not exist.
   *
   * @throws FileNotFoundException
   * @throws CaosDBException
   */
  @Test
  public void testPutFilesInWorkingDirFailure4() throws FileNotFoundException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    final FileProperties f = new FileProperties(null, null, null);
    f.setTmpIdentifier("a2s3d4f5");
    f.setFile(new File("blablabla_non_existing"));
    f.setPath("bla");
    files.add(f);

    Exception exc = assertThrows(Exception.class, () -> caller.putFilesInWorkingDir(files));
    assertEquals(exc.getMessage(), "target does not exist.");
  }

  /**
   * putFilesInWorkingDir returns silently because files is null.
   *
   * @throws IOException
   * @throws CaosDBException
   */
  @Test()
  public void testPutFilesInWorkingDirReturnSilently() throws IOException, CaosDBException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);

    caller.putFilesInWorkingDir(null);
  }

  @Test
  public void testPutFilesInWorkingDirOk() throws CaosDBException, IOException {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);
    this.pwd.mkdirs();

    final ArrayList<FileProperties> files = new ArrayList<>();
    final FileProperties f = new FileProperties(null, null, null);
    f.setTmpIdentifier("a2s3d4f5");
    f.setFile(testFile);
    f.setPath("testfile");
    files.add(f);

    caller.putFilesInWorkingDir(files);

    assertEquals(1, caller.getUploadFilesDir().listFiles().length);
    final File file = caller.getUploadFilesDir().listFiles()[0];
    FileUtils.contentEquals(file, testFile);
  }

  /**
   * Throw Exception because Script returns with error code 1.
   *
   * @throws Exception
   */
  @Test
  public void testCallScriptFailure1() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "", emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    testExeExit(1);

    assertEquals(1, caller.callScript(cmd[0], cmd, null, emptyEnv));
  }

  @Test
  public void testCallScriptOkSimple() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "", emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    testExeExit(0);

    assertEquals(0, caller.callScript(cmd[0], cmd, null, emptyEnv));
  }

  @Test
  public void testWatchTimeout() throws InterruptedException {
    final TimeoutProcess p =
        new TimeoutProcess(
            new Process() {

              int exit = 0;

              @Override
              public int waitFor() throws InterruptedException {
                return this.exit;
              }

              @Override
              public OutputStream getOutputStream() {
                return null;
              }

              @Override
              public InputStream getInputStream() {
                return null;
              }

              @Override
              public InputStream getErrorStream() {
                return null;
              }

              @Override
              public int exitValue() {
                return this.exit;
              }

              @Override
              public void destroy() {
                this.exit = 1;
              }

              @Override
              public boolean isAlive() {
                return this.exit == 0;
              }
            },
            -1);

    assertFalse(p.wasTimeouted());
    final Thread watchTimeout = TimeoutProcess.watchTimeout(p, 100);
    watchTimeout.start();
    watchTimeout.join();
    assertEquals(1, p.exitValue());
    assertTrue(p.wasTimeouted());
  }

  @Timeout(value = 1, unit = TimeUnit.SECONDS)
  @Test
  public void testTimeout() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, 500, null, "", emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    testSleep(10);

    assertThrows(TimeoutException.class, () -> caller.callScript(cmd[0], cmd, null, emptyEnv));
  }

  @Test
  public void testCleanup() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, 500, null, null, emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());
    caller.cleanup();
    assertFalse(caller.getTmpWorkingDir().exists());
  }

  @Test
  public void testInvokeWithErrorInCreateWorkingDir() throws Message {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            new String[] {""}, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {
            throw new Exception();
          }

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {}

          @Override
          public int callScript(
              String cmd, String[] cmdLine, Object authToken, Map<String, String> env) {
            return 0;
          }

          @Override
          public void cleanup() {}
        };

    Message exc = assertThrows(Message.class, () -> caller.invoke());
    assertEquals(exc, ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR);
  }

  @Test
  public void testInvokeWithErrorInPutFilesInWorkingDir() throws Message {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            new String[] {""}, -1, null, "", emptyEnv, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {}

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {
            throw new CaosDBException("");
          }

          @Override
          public int callScript(
              String cmd, String[] cmdLine, Object authToken, Map<String, String> env) {
            return 0;
          }

          @Override
          public void cleanup() {}
        };
    Message exc = assertThrows(Message.class, () -> caller.invoke());
    assertEquals(exc, ServerMessages.SERVER_SIDE_SCRIPT_SETUP_ERROR);
  }

  @Test
  public void testInvokeWithErrorInCallScript() throws Message {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    assertTrue(new File(cmd[0]).exists());
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {}

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {}

          @Override
          public void createTmpHomeDir() throws Exception {}

          @Override
          public int callScript(
              String cmd, String[] cmdLine, Object authToken, Map<String, String> env)
              throws IOException {
            throw new IOException();
          }

          @Override
          public void cleanup() {}
        };

    Message exc = assertThrows(Message.class, () -> caller.invoke());
    assertEquals(exc, ServerMessages.SERVER_SIDE_SCRIPT_ERROR);
  }

  @Test
  public void testInvokeWithErrorInCleanup() throws Message {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd) {
          @Override
          public void createWorkingDir() throws Exception {}

          @Override
          public void putFilesInWorkingDir(final Collection<FileProperties> files)
              throws FileNotFoundException, CaosDBException {}

          @Override
          public int callScript(
              String cmd, String[] cmdLine, Object authToken, Map<String, String> env) {
            return 0;
          }

          @Override
          public void cleanup() {
            super.cleanup();
          }

          @Override
          public void deleteWorkingDir(File pwd) throws IOException {
            throw new IOException();
          }
        };

    Exception exc = assertThrows(RuntimeException.class, () -> caller.invoke());
    assertEquals(exc.getMessage(), "Cleanup failed.");
  }

  @Test
  public void testPrintStdErr() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath(), "opt1", "opt2"};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, -1, null, "authToken", emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();

    testPrintArgsToStdErr();

    caller.callScript(cmd[0], cmd, "authToken", emptyEnv);

    assertEquals(
        "--auth-token=authToken opt1 opt2\n", FileUtils.readFileToString(caller.getStdErrFile()));
  }

  @Test
  public void testPrintStdOut() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath(), "opt1", "opt2"};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, -1, null, "authToken", emptyEnv, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();

    testPrintArgsToStdOut();

    caller.callScript(cmd[0], cmd, "authToken", emptyEnv);

    assertEquals(
        "--auth-token=authToken opt1 opt2\n", FileUtils.readFileToString(caller.getStdOutFile()));
  }

  /** Test if environment variables are passed correctly and can be read by the script. */
  @Test
  public void testCallScriptEnvironment() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    Map<String, String> env = new HashMap<String, String>();
    env.put("TEST", "testcontent");
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(cmd, -1, null, "", env, new ScriptingUtils(), this.pwd);

    caller.createWorkingDir();
    assertTrue(caller.getTmpWorkingDir().exists());

    preparePrintEnv("TEST");

    caller.callScript(cmd[0], cmd, null, env);

    assertEquals("-testcontent-\n", caller.getStdOut());
    caller.cleanup();
  }

  @Test
  public void testCleanupNonExistingWorkingDir() {
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            null, -1, null, "authToken", emptyEnv, new ScriptingUtils(), this.pwd);
    caller.cleanup();
  }

  /** Does the order of directory creation matter? */
  @Test
  public void testDirectoriesInWrongOrder() throws Message {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);

    assertThrows(
        Exception.class,
        () -> {
          caller.createTmpHomeDir();
          caller.createWorkingDir();
        });
    caller.cleanup();
  }

  /** Is the new home directory created correctly? */
  @Test
  public void testWorkingDirCreation() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);
    caller.createWorkingDir();
    caller.createTmpHomeDir();
    assertTrue(caller.getTmpHomeDir().exists());
    caller.cleanup();
  }

  /** Does copying files over to the new home directory work? */
  @Test
  public void testWorkingDirCopying() throws Exception {
    final String[] cmd = {testExecutable.getAbsolutePath()};
    final ServerSideScriptingCaller caller =
        new ServerSideScriptingCaller(
            cmd, -1, null, null, emptyEnv, new ScriptingUtils(), this.pwd);
    caller.createWorkingDir();
    // Create source folder in working directory
    String scriptingHomeDir = CaosDBServer.getServerProperty("SERVER_SIDE_SCRIPTING_HOME_DIR");
    File tempHomeDirSrc = new File(caller.getTmpWorkingDir(), "home_source");
    CaosDBServer.setProperty("SERVER_SIDE_SCRIPTING_HOME_DIR", tempHomeDirSrc.toString());
    tempHomeDirSrc.mkdir();
    String testFileName = "testfile.txt";
    String testContent = "sloikdfgu89w4t78930tvgyum9q238456t27ht2";
    File testFile = new File(tempHomeDirSrc, testFileName);
    // testFile.createNewFile();
    FileUtils.write(testFile, testContent, "UTF-8");
    // Attempt to copy the source to the new home
    caller.createTmpHomeDir();
    // Compare source and new home
    File expectedFile = new File(caller.getTmpHomeDir(), testFileName);
    assertTrue(expectedFile.exists());
    String expectedFileContent = FileUtils.readFileToString(expectedFile, "UTF-8");
    assertEquals(expectedFileContent, testContent);
    caller.cleanup();
    CaosDBServer.setProperty("SERVER_SIDE_SCRIPTING_HOME_DIR", scriptingHomeDir);
  }
}
