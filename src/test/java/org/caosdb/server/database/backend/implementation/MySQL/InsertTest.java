/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.database.backend.implementation.MySQL;

import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.deriveStage1Inserts;
import static org.caosdb.server.database.backend.implementation.MySQL.DatabaseUtils.deriveStage2Inserts;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.datatype.SingleValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.InsertEntity;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.wrapper.Property;
import org.junit.jupiter.api.Test;

public class InsertTest {

  private Deque<EntityID> registerReplacementIds(int count) {
    Deque<EntityID> replacementIds = new ArrayDeque<>();
    for (int i = 1; i < count + 1; i++) {
      replacementIds.add(new EntityID(Integer.toString(-i)));
    }
    return replacementIds;
  }

  /**
   * Record with very deep property tree.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation1() throws Exception {
    final Entity r = new InsertEntity("Test", Role.Record);
    final Property p1 = new Property(new RetrieveEntity(new EntityID("1")));
    p1.setRole("Property");
    p1.setValue(new GenericValue("V1"));
    p1.setDatatype("TEXT");
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(new RetrieveEntity(new EntityID("2")));
    p2.setRole("Property");
    p2.setValue(new GenericValue("V2"));
    p2.setDatatype("TEXT");
    p2.setStatementStatus(StatementStatus.RECOMMENDED);
    r.addProperty(p2);

    final Property p3 = new Property(new RetrieveEntity(new EntityID("3")));
    p3.setRole("Property");
    p3.setValue(new GenericValue("V3"));
    p3.setDatatype("TEXT");
    p3.setStatementStatus(StatementStatus.OBLIGATORY);
    p2.addProperty(p3);

    final Property p4 = new Property(new RetrieveEntity(new EntityID("4")));
    p4.setRole("Property");
    p4.setValue(new GenericValue("V4"));
    p4.setDatatype("TEXT");
    p4.setStatementStatus(StatementStatus.OBLIGATORY);
    r.addProperty(p4);

    final Property p5 = new Property(new RetrieveEntity(new EntityID("5")));
    p5.setRole("Property");
    p5.setValue(new GenericValue("V5"));
    p5.setDatatype("TEXT");
    p5.setStatementStatus(StatementStatus.OBLIGATORY);
    p4.addProperty(p5);

    final Property p6 = new Property(new RetrieveEntity(new EntityID("6")));
    p6.setRole("Property");
    p6.setValue(new GenericValue("V6"));
    p6.setDatatype("TEXT");
    p6.setStatementStatus(StatementStatus.OBLIGATORY);
    p5.addProperty(p6);

    final Property p7 = new Property(new RetrieveEntity(new EntityID("7")));
    p7.setRole("Property");
    p7.setValue(new GenericValue("V7"));
    p7.setDatatype("TEXT");
    p7.setStatementStatus(StatementStatus.OBLIGATORY);
    r.addProperty(p7);

    final Property p8 = new Property(new RetrieveEntity(new EntityID("8")));
    p8.setRole("Property");
    p8.setValue(new GenericValue("V8"));
    p8.setDatatype("TEXT");
    p8.setStatementStatus(StatementStatus.OBLIGATORY);
    p7.addProperty(p8);

    final Property p9 = new Property(new RetrieveEntity(new EntityID("9")));
    p9.setRole("Property");
    p9.setValue(new GenericValue("V9"));
    p9.setDatatype("TEXT");
    p9.setStatementStatus(StatementStatus.OBLIGATORY);
    p8.addProperty(p9);

    final Property p10 = new Property(new RetrieveEntity(new EntityID("10")));
    p10.setRole("Property");
    p10.setValue(new GenericValue("V10"));
    p10.setDatatype("TEXT");
    p10.setStatementStatus(StatementStatus.OBLIGATORY);
    p9.addProperty(p10);

    final LinkedList<Property> stage1Inserts = new LinkedList<>();
    final LinkedList<Property> stage2Inserts = new LinkedList<>();

    int replacementCount = deriveStage1Inserts(stage1Inserts, r);
    assertEquals(3, replacementCount);

    deriveStage2Inserts(stage2Inserts, stage1Inserts, registerReplacementIds(replacementCount), r);

    assertEquals(7, stage1Inserts.size());
    assertFalse(stage1Inserts.get(0) instanceof Replacement);
    assertEquals("1", stage1Inserts.get(0).getId().toString());
    assertEquals("V1", ((SingleValue) stage1Inserts.get(0).getValue()).toDatabaseString());

    assertFalse(stage1Inserts.get(1) instanceof Replacement);
    assertEquals("2", stage1Inserts.get(1).getId().toString());
    assertEquals("V2", ((SingleValue) stage1Inserts.get(1).getValue()).toDatabaseString());

    assertFalse(stage1Inserts.get(2) instanceof Replacement);
    assertEquals("4", stage1Inserts.get(2).getId().toString());
    assertEquals("V4", ((SingleValue) stage1Inserts.get(2).getValue()).toDatabaseString());

    assertTrue(stage1Inserts.get(3) instanceof Replacement);
    assertEquals("-1", stage1Inserts.get(3).getId().toString());
    assertEquals("V5", ((SingleValue) stage1Inserts.get(3).getValue()).toDatabaseString());

    assertFalse(stage1Inserts.get(4) instanceof Replacement);
    assertEquals("7", stage1Inserts.get(4).getId().toString());
    assertEquals("V7", ((SingleValue) stage1Inserts.get(4).getValue()).toDatabaseString());

    assertTrue(stage1Inserts.get(5) instanceof Replacement);
    assertEquals("-2", stage1Inserts.get(5).getId().toString());
    assertEquals("V8", ((SingleValue) stage1Inserts.get(5).getValue()).toDatabaseString());

    assertTrue(stage1Inserts.get(6) instanceof Replacement);
    assertEquals("-3", stage1Inserts.get(6).getId().toString());
    assertEquals("V9", ((SingleValue) stage1Inserts.get(6).getValue()).toDatabaseString());

    assertEquals(6, stage2Inserts.size());
    assertEquals(new EntityID("3"), stage2Inserts.get(0).getId());
    assertEquals("V3", ((SingleValue) stage2Inserts.get(0).getValue()).toDatabaseString());
    assertEquals(new EntityID("2"), stage2Inserts.get(0).getDomain());

    assertEquals(new EntityID("-1"), stage2Inserts.get(1).getId());
    assertEquals(ReplacementStatus.REPLACEMENT, stage2Inserts.get(1).getStatementStatus());
    assertEquals(new EntityID("5"), ((ReferenceValue) stage2Inserts.get(1).getValue()).getId());
    assertEquals(new EntityID("4"), stage2Inserts.get(1).getDomain());

    assertEquals(new EntityID("6"), stage2Inserts.get(2).getId());
    assertEquals("V6", ((SingleValue) stage2Inserts.get(2).getValue()).toDatabaseString());
    assertEquals(new EntityID("-1"), stage2Inserts.get(2).getDomain());

    assertEquals(new EntityID("-2"), stage2Inserts.get(3).getId());
    assertEquals(ReplacementStatus.REPLACEMENT, stage2Inserts.get(3).getStatementStatus());
    assertEquals(new EntityID("8"), ((ReferenceValue) stage2Inserts.get(3).getValue()).getId());
    assertEquals(new EntityID("7"), stage2Inserts.get(3).getDomain());

    assertEquals(new EntityID("-3"), stage2Inserts.get(4).getId());
    assertEquals(ReplacementStatus.REPLACEMENT, stage2Inserts.get(4).getStatementStatus());
    assertEquals(new EntityID("9"), ((ReferenceValue) stage2Inserts.get(4).getValue()).getId());
    assertEquals(new EntityID("-2"), stage2Inserts.get(4).getDomain());

    assertEquals(new EntityID("10"), stage2Inserts.get(5).getId());
    assertEquals("V10", ((SingleValue) stage2Inserts.get(5).getValue()).toDatabaseString());
    assertEquals(new EntityID("-3"), stage2Inserts.get(5).getDomain());
  }

  /**
   * Record with two properties of the same id and different values. One has a sub property, the
   * other does not.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation2() throws Exception {
    final Entity r = new InsertEntity("Test", Role.Record);
    final Property p1 = new Property(new RetrieveEntity(new EntityID("1")));
    p1.setRole("Property");
    p1.setValue(new GenericValue("V1-1"));
    p1.setDatatype("TEXT");
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(new RetrieveEntity(new EntityID("1")));
    p2.setRole("Property");
    p2.setValue(new GenericValue("V1-2"));
    p2.setDatatype("TEXT");
    p2.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p2);

    final Property subp = new Property(new RetrieveEntity(new EntityID("2")));
    subp.setRole("Property");
    subp.setValue(new GenericValue("V2"));
    subp.setDatatype("TEXT");
    subp.setStatementStatus(StatementStatus.FIX);
    p2.addProperty(subp);

    final LinkedList<Property> stage1Inserts = new LinkedList<>();
    final LinkedList<Property> stage2Inserts = new LinkedList<>();

    int replacementCount = deriveStage1Inserts(stage1Inserts, r);
    deriveStage2Inserts(stage2Inserts, stage1Inserts, registerReplacementIds(replacementCount), r);

    assertEquals(3, stage1Inserts.size());

    assertFalse(stage1Inserts.get(0) instanceof Replacement);
    assertEquals(new EntityID("1"), stage1Inserts.get(0).getId());
    assertEquals("V1-1", ((SingleValue) stage1Inserts.get(0).getValue()).toDatabaseString());

    assertTrue(stage1Inserts.get(1) instanceof Replacement);
    assertEquals("V1-2", ((SingleValue) stage1Inserts.get(1).getValue()).toDatabaseString());
    assertEquals(new EntityID("-1"), stage1Inserts.get(1).getId());

    assertFalse(stage1Inserts.get(2) instanceof Replacement);
    assertEquals(new EntityID("-1"), stage1Inserts.get(2).getId());
    assertEquals("1", ((SingleValue) stage1Inserts.get(2).getValue()).toDatabaseString());

    assertEquals(1, stage2Inserts.size());

    assertFalse(stage2Inserts.get(0) instanceof Replacement);
    assertEquals(new EntityID("2"), stage2Inserts.get(0).getId());
    assertEquals("V2", ((SingleValue) stage2Inserts.get(0).getValue()).toDatabaseString());
  }

  /**
   * Record with three properties of the same id without any value. The first has a sub property,
   * the others do not.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation3() throws Exception {
    final Entity r = new InsertEntity("Test", Role.Record);
    final Property p1 = new Property(new RetrieveEntity(new EntityID("1")));
    p1.setRole("Property");
    p1.setDatatype("TEXT");
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(new RetrieveEntity(new EntityID("1")));
    p2.setRole("Property");
    p2.setDatatype("TEXT");
    p2.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p2);

    final Property p3 = new Property(new RetrieveEntity(new EntityID("1")));
    p3.setRole("Property");
    p3.setDatatype("TEXT");
    p3.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p3);

    final Property sub1 = new Property(new RetrieveEntity(new EntityID("2")));
    sub1.setRole("Property");
    sub1.setDatatype("TEXT");
    sub1.setValue(new GenericValue("V1"));
    sub1.setStatementStatus(StatementStatus.FIX);
    p1.addProperty(sub1);

    final LinkedList<Property> stage1Inserts = new LinkedList<>();
    final LinkedList<Property> stage2Inserts = new LinkedList<>();

    int count = deriveStage1Inserts(stage1Inserts, r);
    deriveStage2Inserts(stage2Inserts, stage1Inserts, registerReplacementIds(count), r);

    assertEquals(4, stage1Inserts.size());
    assertTrue(stage1Inserts.get(0) instanceof Replacement);
    assertEquals(new EntityID("-1"), stage1Inserts.get(0).getId());
    assertEquals(null, stage1Inserts.get(0).getValue());

    assertFalse(stage1Inserts.get(1) instanceof Replacement);
    assertEquals(new EntityID("-1"), stage1Inserts.get(1).getId());
    assertEquals("1", ((ReferenceValue) stage1Inserts.get(1).getValue()).getId().toString());

    assertFalse(stage1Inserts.get(2) instanceof Replacement);
    assertEquals(new EntityID("1"), stage1Inserts.get(2).getId());
    assertEquals(null, stage1Inserts.get(2).getValue());

    assertFalse(stage1Inserts.get(3) instanceof Replacement);
    assertEquals(new EntityID("1"), stage1Inserts.get(3).getId());
    assertEquals(null, stage1Inserts.get(3).getValue());

    assertEquals(1, stage2Inserts.size());
    assertEquals(new EntityID("2"), stage2Inserts.get(0).getId());
    assertEquals("V1", ((SingleValue) stage2Inserts.get(0).getValue()).toDatabaseString());
    assertEquals(new EntityID("-1"), stage2Inserts.get(0).getDomain());
  }

  /**
   * Record with a list property.
   *
   * @throws Exception
   */
  @Test
  public void testTransformation4() throws Exception {

    final Entity r = new InsertEntity("Test", Role.Record);
    final Property p1 = new Property(new RetrieveEntity(new EntityID("1")));
    p1.setRole("Property");
    p1.setDatatype("TEXT");
    p1.setValue(new GenericValue("V1"));
    p1.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p1);

    final Property p2 = new Property(new RetrieveEntity(new EntityID("2")));
    p2.setRole("Property");
    p2.setDatatype("LIST<TEXT>");
    p2.setStatementStatus(StatementStatus.FIX);
    final CollectionValue vals = new CollectionValue();
    vals.add(new GenericValue("L1"));
    vals.add(new GenericValue("L2"));
    vals.add(new GenericValue("L3"));
    p2.setValue(vals);
    r.addProperty(p2);

    final Property p3 = new Property(new RetrieveEntity(new EntityID("3")));
    p3.setRole("Property");
    p3.setDatatype("TEXT");
    p3.setValue(new GenericValue("V3"));
    p3.setStatementStatus(StatementStatus.FIX);
    r.addProperty(p3);

    final LinkedList<Property> stage1Inserts = new LinkedList<>();
    final LinkedList<Property> stage2Inserts = new LinkedList<>();

    int count = deriveStage1Inserts(stage1Inserts, r);
    deriveStage2Inserts(stage2Inserts, stage1Inserts, registerReplacementIds(count), r);

    assertEquals(4, stage1Inserts.size());

    assertFalse(stage1Inserts.get(0) instanceof Replacement);
    assertEquals(new EntityID("1"), stage1Inserts.get(0).getId());
    assertEquals("V1", ((SingleValue) stage1Inserts.get(0).getValue()).toDatabaseString());

    assertTrue(stage1Inserts.get(1) instanceof Replacement);
    assertEquals(new EntityID("-1"), stage1Inserts.get(1).getId());
    assertTrue(stage1Inserts.get(1).getValue() instanceof CollectionValue);

    assertFalse(stage1Inserts.get(2) instanceof Replacement);
    assertEquals(new EntityID("-1"), stage1Inserts.get(2).getId());
    assertEquals("2", ((ReferenceValue) stage1Inserts.get(2).getValue()).getId().toString());

    assertFalse(stage1Inserts.get(3) instanceof Replacement);
    assertEquals(new EntityID("3"), stage1Inserts.get(3).getId());
    assertEquals("V3", ((SingleValue) stage1Inserts.get(3).getValue()).toDatabaseString());

    assertEquals(0, stage2Inserts.size());
  }

  /** Deeply nested properties without any values, with overridden name and description */
  @Test
  public void testTransformation5() {
    final Entity r = new InsertEntity("Test", Role.RecordType);
    final Property p1 = new Property(new RetrieveEntity(new EntityID("1")));
    p1.setRole("Property");
    p1.setDatatype("TEXT");
    p1.setDescription("desc1");
    p1.setDescOverride(true);
    p1.setName("P1");
    p1.setNameOverride(true);
    p1.setStatementStatus(StatementStatus.RECOMMENDED);
    r.addProperty(p1);

    final Property p2 = new Property(new RetrieveEntity(new EntityID("2")));
    p2.setRole("Property");
    p2.setDatatype("TEXT");
    p2.setDescription("desc2");
    p2.setDescOverride(true);
    p2.setName("P2");
    p2.setNameOverride(true);
    p2.setStatementStatus(StatementStatus.RECOMMENDED);
    r.addProperty(p2);

    final Property p21 = new Property(new RetrieveEntity(new EntityID("1")));
    p21.setRole("Property");
    p21.setDatatype("TEXT");
    p21.setDescription("desc21");
    p21.setDescOverride(true);
    p21.setName("P21");
    p21.setNameOverride(true);
    p21.setStatementStatus(StatementStatus.FIX);
    p2.addProperty(p21);

    final Property p22 = new Property(new RetrieveEntity(new EntityID("2")));
    p22.setRole("Property");
    p22.setDatatype("TEXT");
    p22.setDescription("desc22");
    p22.setDescOverride(true);
    p22.setName("P22");
    p22.setNameOverride(true);
    p22.setStatementStatus(StatementStatus.FIX);
    p2.addProperty(p22);

    final Property p3 = new Property(new RetrieveEntity(new EntityID("3")));
    p3.setRole("Property");
    p3.setDatatype("TEXT");
    p3.setDescription("desc3");
    p3.setDescOverride(true);
    p3.setName("P3");
    p3.setNameOverride(true);
    p3.setStatementStatus(StatementStatus.RECOMMENDED);
    r.addProperty(p3);

    final Property p31 = new Property(new RetrieveEntity(new EntityID("1")));
    p31.setRole("Property");
    p31.setDatatype("TEXT");
    p31.setDescription("desc31");
    p31.setDescOverride(true);
    p31.setName("P31");
    p31.setNameOverride(true);
    p31.setStatementStatus(StatementStatus.FIX);
    p3.addProperty(p31);

    final Property p32 = new Property(new RetrieveEntity(new EntityID("2")));
    p32.setRole("Property");
    p32.setDatatype("TEXT");
    p32.setDescription("desc32");
    p32.setDescOverride(true);
    p32.setName("P32");
    p32.setNameOverride(true);
    p32.setStatementStatus(StatementStatus.FIX);
    p3.addProperty(p32);

    final Property p321 = new Property(new RetrieveEntity(new EntityID("1")));
    p321.setRole("Property");
    p321.setDatatype("TEXT");
    p321.setDescription("desc321");
    p321.setDescOverride(true);
    p321.setName("P321");
    p321.setNameOverride(true);
    p321.setStatementStatus(StatementStatus.FIX);
    p32.addProperty(p321);

    final Property p322 = new Property(new RetrieveEntity(new EntityID("2")));
    p322.setRole("Property");
    p322.setDatatype("TEXT");
    p322.setDescription("desc322");
    p322.setDescOverride(true);
    p322.setName("P322");
    p322.setNameOverride(true);
    p322.setStatementStatus(StatementStatus.FIX);
    p32.addProperty(p322);

    final Property p323 = new Property(new RetrieveEntity(new EntityID("3")));
    p323.setRole("Property");
    p323.setDatatype("TEXT");
    p323.setDescription("desc323");
    p323.setDescOverride(true);
    p323.setName("P323");
    p323.setNameOverride(true);
    p323.setStatementStatus(StatementStatus.FIX);
    p32.addProperty(p323);

    final Property p33 = new Property(new RetrieveEntity(new EntityID("3")));
    p33.setRole("Property");
    p33.setDatatype("TEXT");
    p33.setDescription("desc33");
    p33.setDescOverride(true);
    p33.setName("P33");
    p33.setNameOverride(true);
    p33.setStatementStatus(StatementStatus.FIX);
    p3.addProperty(p33);

    List<Property> stage1Inserts = new LinkedList<>();
    List<Property> stage2Inserts = new LinkedList<>();
    int c = DatabaseUtils.deriveStage1Inserts(stage1Inserts, r);
    DatabaseUtils.deriveStage2Inserts(stage2Inserts, stage1Inserts, registerReplacementIds(c), r);

    assertEquals(4, c);
    assertEquals(7, stage1Inserts.size());
    assertEquals(8, stage2Inserts.size());
  }
}
