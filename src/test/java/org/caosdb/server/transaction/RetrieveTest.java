/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.transaction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.ServerProperties;
import org.caosdb.server.accessControl.AnonymousAuthenticationToken;
import org.caosdb.server.accessControl.Role;
import org.caosdb.server.database.BackendTransaction;
import org.caosdb.server.database.access.Access;
import org.caosdb.server.database.backend.interfaces.RetrieveRoleImpl;
import org.caosdb.server.database.exceptions.TransactionException;
import org.caosdb.server.database.misc.TransactionBenchmark;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.container.RetrieveContainer;
import org.caosdb.server.entity.xml.IdAndServerMessagesOnlyStrategy;
import org.caosdb.server.permissions.EntityACLFactory;
import org.caosdb.server.utils.EntityStatus;
import org.caosdb.server.utils.ServerMessages;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class RetrieveTest {

  // @review Florian Spreckelsen 2022-03-22
  @BeforeAll
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
    CaosDBServer.setProperty(ServerProperties.KEY_AUTH_OPTIONAL, "TRUE");
    CaosDBServer.initShiro();

    BackendTransaction.setImpl(RetrieveRoleImpl.class, RetrieveRoleMockup.class);
  }

  /** a mock-up which returns null */
  public static class RetrieveRoleMockup implements RetrieveRoleImpl {

    public RetrieveRoleMockup(Access a) {}

    @Override
    public void setTransactionBenchmark(TransactionBenchmark b) {}

    @Override
    public TransactionBenchmark getBenchmark() {
      return null;
    }

    @Override
    public Role retrieve(String role) throws TransactionException {
      return null;
    }
  }

  @Test
  public void testMissingRetrievePermission() {
    Subject subject = SecurityUtils.getSubject();
    subject.login(AnonymousAuthenticationToken.getInstance());
    EntityInterface entity = new RetrieveEntity(new EntityID("1234"));
    EntityACLFactory fac = new EntityACLFactory();
    fac.deny(AnonymousAuthenticationToken.PRINCIPAL, "RETRIEVE:ENTITY");
    entity.setEntityACL(fac.create());
    RetrieveContainer container = new RetrieveContainer(null, null, null, null);
    assertTrue(entity.getMessages().isEmpty());
    assertEquals(entity.getEntityStatus(), EntityStatus.QUALIFIED);
    container.add(entity);
    Retrieve retrieve = new Retrieve(container);
    retrieve.postTransaction();
    assertFalse(entity.getMessages().isEmpty());
    assertEquals(entity.getMessages("error").get(0), ServerMessages.AUTHORIZATION_ERROR);
    assertEquals(entity.getEntityStatus(), EntityStatus.UNQUALIFIED);
    assertTrue(entity.getSerializeFieldStrategy() instanceof IdAndServerMessagesOnlyStrategy);
  }
}
