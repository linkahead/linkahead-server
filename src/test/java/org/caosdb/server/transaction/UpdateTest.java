/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2022,2023 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2022 Daniel Hornung <d.hornung@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.transaction;

import static org.caosdb.server.utils.EntityStatus.QUALIFIED;
import static org.caosdb.server.utils.EntityStatus.VALID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import org.caosdb.server.CaosDBException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.datatype.CollectionValue;
import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.StatementStatus;
import org.caosdb.server.entity.UpdateEntity;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.permissions.EntityPermission;
import org.caosdb.server.permissions.Permission;
import org.caosdb.server.utils.EntityStatus;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UpdateTest {

  @BeforeAll
  public static void setup() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testDeriveUpdate_SameName()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new RetrieveEntity("Name");
    final Entity oldEntity = new RetrieveEntity("Name");
    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), EntityStatus.VALID);
  }

  @Test
  public void testDeriveUpdate_DifferentName()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new RetrieveEntity("NewName");
    final Entity oldEntity = new RetrieveEntity("OldName");
    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), EntityStatus.QUALIFIED);
  }

  @Test
  public void testDeriveUpdate_SameProperty()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new RetrieveEntity(new EntityID("1234"));
    final Property newProperty = new Property(new RetrieveEntity(new EntityID("1")));
    newEntity.addProperty(newProperty);
    final Property oldProperty = new Property(new RetrieveEntity(new EntityID("1")));
    final Entity oldEntity = new RetrieveEntity(new EntityID("1234"));
    oldEntity.addProperty(oldProperty);

    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), VALID);
  }

  @Test
  public void testDeriveUpdate_AnotherProperty()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final Entity newEntity = new RetrieveEntity(new EntityID("1234"));
    final Property newProperty = new Property(new RetrieveEntity(new EntityID("1")));
    final Property newProperty2 = new Property(new RetrieveEntity(new EntityID("2")));
    newEntity.addProperty(newProperty);
    newEntity.addProperty(newProperty2);
    final Property oldProperty = new Property(new RetrieveEntity(new EntityID("1")));
    final Entity oldEntity = new RetrieveEntity(new EntityID("1234"));
    oldEntity.addProperty(oldProperty);

    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), QUALIFIED);
    assertEquals(newProperty.getEntityStatus(), VALID);
    assertEquals(newProperty2.getEntityStatus(), QUALIFIED);
  }

  @Test
  public void testDeriveUpdate_SameUnit()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final EntityInterface magicUnit = new RetrieveEntity(new EntityID("1234"));
    magicUnit.setName("Unit");
    magicUnit.setId(new EntityID("24"));
    magicUnit.setDatatype("TEXT");

    final Entity newEntity = new RetrieveEntity(new EntityID("1234"));
    final Property newProperty = new Property(new RetrieveEntity(new EntityID("1")));

    final Property newUnit = new Property(new RetrieveEntity(new EntityID("5")));
    newUnit.setName(magicUnit.getName());
    newUnit.setId(magicUnit.getId());
    newUnit.setDatatype(magicUnit.getDatatype());
    newUnit.setStatementStatus(StatementStatus.FIX);
    newUnit.setValue(new GenericValue("m"));
    newUnit.setEntityStatus(EntityStatus.QUALIFIED);
    newProperty.addProperty(newUnit);

    newEntity.addProperty(newProperty);

    final Entity oldEntity = new RetrieveEntity(new EntityID("1234"));
    final Property oldProperty = new Property(new RetrieveEntity(new EntityID("1")));

    final Property oldUnit = new Property(new RetrieveEntity(new EntityID("5")));
    oldUnit.setName(magicUnit.getName());
    oldUnit.setId(magicUnit.getId());
    oldUnit.setDatatype(magicUnit.getDatatype());
    oldUnit.setStatementStatus(StatementStatus.FIX);
    oldUnit.setValue(new GenericValue("m"));
    oldUnit.setEntityStatus(EntityStatus.QUALIFIED);
    oldProperty.addProperty(oldUnit);

    oldEntity.addProperty(oldProperty);

    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(newUnit.getEntityStatus(), VALID);
    assertEquals(newProperty.getEntityStatus(), VALID);
    assertEquals(newEntity.getEntityStatus(), VALID);
  }

  @Test
  public void testDeriveUpdate_DifferentUnit()
      throws NoSuchAlgorithmException, IOException, CaosDBException {
    final EntityInterface magicUnit = new RetrieveEntity(new EntityID("1234"));
    magicUnit.setName("Unit");
    magicUnit.setId(new EntityID("24"));
    magicUnit.setDatatype("TEXT");

    final Entity newEntity = new RetrieveEntity(new EntityID("1234"));
    final Property newProperty = new Property(new RetrieveEntity(new EntityID("1")));

    final Property newUnit = new Property(new RetrieveEntity(new EntityID("5")));
    newUnit.setName(magicUnit.getName());
    newUnit.setId(magicUnit.getId());
    newUnit.setDatatype(magicUnit.getDatatype());
    newUnit.setStatementStatus(StatementStatus.FIX);
    newUnit.setValue(new GenericValue("m"));
    newUnit.setEntityStatus(EntityStatus.QUALIFIED);
    newProperty.addProperty(newUnit);

    newEntity.addProperty(newProperty);

    final Entity oldEntity = new RetrieveEntity(new EntityID("1234"));
    final Property oldProperty = new Property(new RetrieveEntity(new EntityID("1")));

    final Property oldUnit = new Property(new RetrieveEntity(new EntityID("5")));
    oldUnit.setName(magicUnit.getName());
    oldUnit.setId(magicUnit.getId());
    oldUnit.setDatatype(magicUnit.getDatatype());
    oldUnit.setStatementStatus(StatementStatus.FIX);
    oldUnit.setValue(new GenericValue("s"));
    oldUnit.setEntityStatus(EntityStatus.QUALIFIED);
    oldProperty.addProperty(oldUnit);

    oldEntity.addProperty(oldProperty);

    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(newEntity.getEntityStatus(), QUALIFIED);
    assertEquals(newProperty.getEntityStatus(), QUALIFIED);
  }

  @Test
  public void testDeriveUpdate_Collections()
      throws NoSuchAlgorithmException, CaosDBException, IOException {

    final Entity newEntity = new UpdateEntity();
    final Property newProperty = new Property(new UpdateEntity(new EntityID("1")));
    newProperty.setDatatype("List<Person>");
    CollectionValue newValue = new CollectionValue();
    newValue.add(new ReferenceValue("1234"));
    newValue.add(null);
    newValue.add(new GenericValue(2345));
    newValue.add(new GenericValue(3465));
    newProperty.setValue(newValue);
    newEntity.addProperty(newProperty);
    newEntity.setEntityStatus(QUALIFIED);
    newProperty.setEntityStatus(QUALIFIED);

    // old entity represents the stored entity.
    final Entity oldEntity = new UpdateEntity();
    final Property oldProperty = new Property(new UpdateEntity(new EntityID("1")));
    oldProperty.setDatatype("List<Person>");
    CollectionValue oldValue = new CollectionValue();
    // Values are shuffled but have the correct index
    oldValue.add(1, null);
    oldValue.add(3, new GenericValue(3465));
    oldValue.add(2, new ReferenceValue("2345"));
    oldValue.add(0, new ReferenceValue("1234"));
    oldProperty.setValue(oldValue);
    oldEntity.addProperty(oldProperty);

    HashSet<Permission> permissions = new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    // Both have been identified as equals
    assertTrue(permissions.isEmpty());
    assertEquals(newEntity.getEntityStatus(), VALID);
    assertEquals(newProperty.getEntityStatus(), VALID);

    // NEW TEST CASE
    newValue.add(null); // newValue has another null
    newProperty.setValue(newValue);
    oldEntity.addProperty(oldProperty); // Add again, because deriveUpdate throws it away
    newEntity.setEntityStatus(QUALIFIED);
    newProperty.setEntityStatus(QUALIFIED);

    HashSet<Permission> permissions2 =
        new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    HashSet<Permission> expected = new HashSet<Permission>();
    expected.add(EntityPermission.UPDATE_ADD_PROPERTY);
    expected.add(EntityPermission.UPDATE_REMOVE_PROPERTY);
    assertEquals(expected, permissions2);
    assertEquals(newEntity.getEntityStatus(), QUALIFIED);
    assertEquals(newProperty.getEntityStatus(), QUALIFIED);

    // NEW TEST CASE
    // now change the order of oldValue
    CollectionValue oldValue2 = new CollectionValue();
    // Values are shuffled but have the correct index
    oldValue2.add(0, null);
    oldValue2.add(1, new GenericValue(3465));
    oldValue2.add(2, new ReferenceValue("2345"));
    oldValue2.add(3, new ReferenceValue("1234"));
    oldValue2.add(4, null);
    oldProperty.setValue(oldValue2);

    oldEntity.addProperty(oldProperty); // Add again, because deriveUpdate throws it away
    newEntity.setEntityStatus(QUALIFIED);
    newProperty.setEntityStatus(QUALIFIED);

    HashSet<Permission> permissions3 =
        new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    assertEquals(expected, permissions3);
    assertEquals(newEntity.getEntityStatus(), QUALIFIED);
    assertEquals(newProperty.getEntityStatus(), QUALIFIED);
  }

  /** For issue #217: Server gets list property datatype wrong if description is updated. */
  @Test
  public void testDeriveUpdate_UpdateList()
      throws NoSuchAlgorithmException, CaosDBException, IOException {
    // @review Florian Spreckelsen 2022-03-15
    final Property oldProperty = new Property(new UpdateEntity(new EntityID("1")));
    final Property newProperty = new Property(new UpdateEntity(new EntityID("1")));
    oldProperty.setDatatype("List<Person>");
    newProperty.setDatatype("List<Person>");
    final Entity oldEntity = new UpdateEntity();
    final Entity newEntity = new UpdateEntity();

    oldProperty.setRole("Record");
    newProperty.setRole("Property");
    oldEntity.addProperty(oldProperty);
    newEntity.addProperty(newProperty);

    // The only difference between old and new
    newEntity.setDescription("New description.");

    new WriteTransaction(null).deriveUpdate(newEntity, oldEntity);
    // check if newEntity's Property VALID.
    assertEquals(VALID, newEntity.getProperties().get(0).getEntityStatus());
  }
}
