/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
package org.caosdb.server.entity.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.caosdb.server.datatype.GenericValue;
import org.caosdb.server.datatype.ReferenceValue;
import org.caosdb.server.entity.Entity;
import org.caosdb.server.entity.EntityID;
import org.caosdb.server.entity.EntityInterface;
import org.caosdb.server.entity.RetrieveEntity;
import org.caosdb.server.entity.Role;
import org.caosdb.server.entity.wrapper.Property;
import org.caosdb.server.query.Query.Selection;
import org.jdom2.Element;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PropertyToElementStrategyTest {

  public static Entity house = null;
  public static Property houseHeight = null;
  public static Entity window = null;
  public static Property windowHeight = null;
  public static Entity houseOwner = null;
  public static Property windowProperty = null;

  /**
   * Create a nested selection out of the dot-separated parts of <code>select</code>.
   *
   * <p>The returned Selection has nested subselections, so that each subselection corresponds to
   * the next part and the remainder of the initial <code>select</code> String.
   */
  public static Selection parse(final String select) {
    final String[] split = select.split("\\.");
    final Selection result = new Selection(split[0]);
    Selection next = result;

    for (int i = 1; i < split.length; i++) {
      next.setSubSelection(new Selection(split[i]));
      next = next.getSubselection();
    }
    return result;
  }

  @BeforeEach
  public void setup() {
    window = new RetrieveEntity(new EntityID("1234"), Role.Record);
    windowHeight = new Property(new RetrieveEntity("height", Role.Property));
    window.addProperty(windowHeight);
    windowHeight.setValue(new GenericValue("windowHeight"));

    houseOwner = new RetrieveEntity("The Queen", Role.Record);

    house = new RetrieveEntity("Buckingham Palace", Role.Record);
    houseHeight = new Property(new RetrieveEntity("height", Role.Property));
    houseHeight.setValue(new GenericValue("houseHeight"));
    house.addProperty(houseHeight);
    windowProperty = new Property(new RetrieveEntity(new EntityID("2345")));
    windowProperty.setName("window");
    windowProperty.setDatatype("window");
    windowProperty.setValue(new ReferenceValue(window.getId()));
    house.addProperty(windowProperty);

    house.addProperty(new Property(new RetrieveEntity()));
    house.addProperty(new Property(houseHeight));
  }

  @Test
  public void test() {
    final PropertyToElementStrategy strategy = new PropertyToElementStrategy();
    final SerializeFieldStrategy setFieldStrategy =
        new SerializeFieldStrategy().addSelection(parse("height"));
    final EntityInterface property = windowProperty;
    ((ReferenceValue) property.getValue()).setEntity(window, true);
    final Element element = strategy.toElement(property, setFieldStrategy);
    assertEquals("Property", element.getName());
    assertEquals("2345", element.getAttributeValue("id"));
    assertEquals("window", element.getAttributeValue("name"));
    assertEquals(1, element.getChildren().size());
    assertEquals("Record", element.getChildren().get(0).getName());

    final Element recordElement = element.getChild("Record");
    assertEquals("1234", recordElement.getAttributeValue("id"));
    assertEquals(1, recordElement.getChildren().size());
    assertEquals("windowHeight", recordElement.getChild("Property").getText());
  }
}
