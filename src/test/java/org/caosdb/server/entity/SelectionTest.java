/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
package org.caosdb.server.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import org.caosdb.server.CaosDBServer;
import org.caosdb.server.entity.xml.SerializeFieldStrategy;
import org.caosdb.server.query.Query;
import org.caosdb.server.query.Query.Selection;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class SelectionTest {

  @BeforeAll
  public static void initServerProperties() throws IOException {
    CaosDBServer.initServerProperties();
  }

  @Test
  public void testEmpty1() {
    final SerializeFieldStrategy setFieldStrategy = new SerializeFieldStrategy();

    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
    assertTrue(setFieldStrategy.isToBeSet("description"));
    assertTrue(setFieldStrategy.isToBeSet("blabla"));

    setFieldStrategy.addSelection(null);

    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
    assertTrue(setFieldStrategy.isToBeSet("description"));
    assertTrue(setFieldStrategy.isToBeSet("blabla"));
  }

  @Test
  public void testName1() {
    final Selection selection = new Selection("name");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName2() {
    final Selection selection = new Selection("id");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName3() {
    final Selection selection = new Selection("value");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName4() {
    final Selection selection = new Selection("description");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName5() {
    final Selection selection = new Selection("datatype");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName6() {
    final Selection selection = new Selection("datatype");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testName7() {
    final Selection selection = new Selection("blabla");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testId1() {
    final Selection selection = new Selection("id");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testId2() {
    final Selection selection = new Selection("name");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testId3() {
    final Selection selection = new Selection("description");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testId4() {
    final Selection selection = new Selection("blablabla");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("id"));
  }

  @Test
  public void testDesc1() {
    final Selection selection = new Selection("description");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertTrue(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testDesc2() {
    final Selection selection = new Selection("name");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertFalse(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testDesc3() {
    final Selection selection = new Selection("id");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertFalse(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testDesc4() {
    final Selection selection = new Selection("blablaba");
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(selection);

    assertFalse(setFieldStrategy.isToBeSet("description"));
  }

  @Test
  public void testMulti1() {
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy())
            .addSelection(new Selection("id"))
            .addSelection(new Selection("name"));

    assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    assertFalse(setFieldStrategy.isToBeSet("description"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testMulti2() {
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy())
            .addSelection(new Selection("id"))
            .addSelection(new Selection("description"));

    assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    assertFalse(setFieldStrategy.isToBeSet("datatype"));
    assertTrue(setFieldStrategy.isToBeSet("description"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testMulti3() {
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy())
            .addSelection(new Selection("datatype"))
            .addSelection(new Selection("description"));

    assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    assertTrue(setFieldStrategy.isToBeSet("datatype"));
    assertTrue(setFieldStrategy.isToBeSet("description"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testMulti4() {
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy())
            .addSelection(new Selection("datatype"))
            .addSelection(new Selection("value"));

    assertFalse(setFieldStrategy.isToBeSet("blablabla"));
    assertTrue(setFieldStrategy.isToBeSet("datatype"));
    assertTrue(setFieldStrategy.isToBeSet("value"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
  }

  @Test
  public void testComposition1() {
    final SerializeFieldStrategy setFieldStrategy =
        (new SerializeFieldStrategy()).addSelection(new Selection("blabla"));

    assertTrue(setFieldStrategy.isToBeSet("blabla"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
    assertFalse(setFieldStrategy.isToBeSet("bleb"));

    final SerializeFieldStrategy forProperty = setFieldStrategy.forProperty("blabla");
    assertTrue(forProperty.isToBeSet("id"));
    assertTrue(forProperty.isToBeSet("name"));
    assertTrue(forProperty.isToBeSet("blub"));
  }

  @Test
  public void testComposition2() {
    final SerializeFieldStrategy setFieldStrategy =
        new SerializeFieldStrategy()
            .addSelection(new Selection("blabla"))
            .addSelection(new Selection("blabla.name"));

    assertTrue(setFieldStrategy.isToBeSet("blabla"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
    assertFalse(setFieldStrategy.isToBeSet("bleb"));

    final SerializeFieldStrategy forProperty = setFieldStrategy.forProperty("blabla");
    assertTrue(forProperty.isToBeSet("id"));
    assertTrue(forProperty.isToBeSet("name"));
    assertTrue(forProperty.isToBeSet("blub"));
  }

  @Test
  public void testComposition3() {
    final SerializeFieldStrategy setFieldStrategy =
        new SerializeFieldStrategy()
            .addSelection(new Selection("blabla").setSubSelection(new Selection("value")))
            .addSelection(new Selection("blabla").setSubSelection(new Selection("description")));

    assertTrue(setFieldStrategy.isToBeSet("blabla"));
    assertTrue(setFieldStrategy.isToBeSet("id"));
    assertTrue(setFieldStrategy.isToBeSet("name"));
    assertFalse(setFieldStrategy.isToBeSet("bleb"));

    final SerializeFieldStrategy forProperty = setFieldStrategy.forProperty("blabla");
    assertTrue(forProperty.isToBeSet("id"));
    assertTrue(forProperty.isToBeSet("name"));
    assertTrue(forProperty.isToBeSet("description"));
    assertTrue(forProperty.isToBeSet("value"));
    assertFalse(forProperty.isToBeSet("blub"));
  }

  @Test
  public void testSubSubSelection() {
    String query_str = "SELECT property.subproperty.subsubproperty FROM ENTITY";
    Query query = new Query(query_str);
    query.parse();
    assertEquals(query.getSelections().size(), 1);

    Selection s = query.getSelections().get(0);
    assertEquals(s.toString(), "property.subproperty.subsubproperty");
    assertEquals(s.getSubselection().toString(), "subproperty.subsubproperty");
    assertEquals(s.getSubselection().getSubselection().toString(), "subsubproperty");
  }

  @Test
  public void testSubSubProperty() {
    Selection s =
        new Selection("property")
            .setSubSelection(
                new Selection("subproperty").setSubSelection(new Selection("subsubproperty")));

    assertEquals(s.toString(), "property.subproperty.subsubproperty");

    SerializeFieldStrategy setFieldStrategy = new SerializeFieldStrategy().addSelection(s);
    assertTrue(setFieldStrategy.isToBeSet("property"));
    assertFalse(setFieldStrategy.forProperty("property").isToBeSet("sadf"));
    //    assertFalse(setFieldStrategy.forProperty("property").isToBeSet("name"));
    assertTrue(setFieldStrategy.forProperty("property").isToBeSet("subproperty"));
    assertTrue(
        setFieldStrategy
            .forProperty("property")
            .forProperty("subproperty")
            .isToBeSet("subsubproperty"));
  }
}
