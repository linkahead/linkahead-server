Server Configuration
====================

Main Configuration File
-----------------------

The server is configured through configuration files. The main file is the
`server.conf`.

The `server.conf` is a list of key-value pairs. A configuration file may
contain empty lines, comment lines, and key-value lines.  Comment lines begin
with a hash (`#`). Key-value lines must have the format `KEY_NAME=VALUE` or
`KEY_NAME = VALUE`.


The server default configuration is located at `./conf/core/server.conf`.
Upstream defaults are stored here. The possible configuration options are
documented inside the
`default file <https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/blob/dev/conf/core/server.conf>`__.

User specific configuration should be in `./conf/ext/` and  override settings
in `./conf/core/`.

The default configuration can be overriden by

    1. the file ./conf/ext/server.conf

    2. any file in ./conf/ext/server.conf.d/ in (approximately?) alphabetical order

    3. environment variables with the prefix `CAOSDB_CONFIG_`

in this order.

Further Configuration Files
---------------------------

Further settings are to be set in files which are by default stored in `./conf/core/`:

``global_entity_permissions.xml``
   :doc:`Permissions<../permissions>` which are automatically set, based on user roles.  See the
   `default file
   <https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/blob/dev/conf/core/global_entity_permissions.xml>`__.

``usersources.ini``
   This file defines possible sources which are checked when a user tries to authenticate.  Each
   defined source has a special section, the possible options are defined separately for each user
   source.  At the moment the best place to look for this specific documentation is at the API
   documentation of :java:type:`UserSource` and its implementing classes.  The provided `template
   file
   <https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/blob/dev/conf/core/usersources.ini.template>`__
   also has some information.  The general concept about authentication realms is described in
   :java:type:`UserSources`.

``authtoken.yaml``
   Configuration for dispensed authentication tokens, which can be used to authenticate to CaosDB
   without the need of a user/password combination. One-time Authentication Tokens can be configure
   to be issued for special purposes (e.g. a call of a server-side script or initial setup after the server start)
   or to be written to a file on a regular basis. An example of a configuration is located at `./conf/core/authtoken.example.yaml`.
   There is more documentation inside the `template file
   <https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/blob/dev/conf/core/authtoken.example.yaml>`__.

``cache.ccf``
   Configuration for the Java Caching System (JCS) which can be used by the server.  More
   documentation is `upstream
   <http://commons.apache.org/proper/commons-jcs/getting_started/intro.html>`_ and inside `the file
   <https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/blob/dev/conf/core/cache.ccf>`_.

``log4j2-default.properties``, ``log4j2-debug.properties``
   Configuration for logging, following the standard described by the `log4j library
   <https://logging.apache.org/log4j/2.x/>`_.  The ``default`` file is always loaded, in debug mode
   the ``debug`` file iss added as well.

The administrator may set the corresponding options in the main configuration
file (`./conf/core/server.conf` and it's friends) to replace these special
files with custom configuration.

Changing the configuration at runtime
-------------------------------------

Remark:
   Only when the server is in debug mode, the general configuration can be changed at runtime.

In the debug case, the server provides the ``_server_properties`` resource which allows the ``GET``
and ``POST`` methods to access the server's properties.  The Python client library conveniently
wraps this in the :any:`caosdb-pylib:caosdb.common.administration` module.
