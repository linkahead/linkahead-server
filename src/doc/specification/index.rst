Specifications
==============

Specifications of assorted topics

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   AbstractProperty
   Fileserver
   Record
   Authentication
   Datatype
   Paging
   RecordType
   Query syntax <../query-syntax>
   Server side scripting <Server-side-scripting>
   Message API <Message-API>
   Entity API <entity_api>
