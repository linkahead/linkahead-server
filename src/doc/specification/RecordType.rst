RecordType
==========

Overview
--------

RecordTypes act as templates for :doc:`Records <Record>`. They
provide a description for a type of Record and define which
:doc:`Properties <AbstractProperty>` could, should, or must be
present. Properties come with an `importance` attribute which tells
the user or client program how strongly necessary the Property is. (As
all other entities,) RecordTypes can inherit from other
RecordTypes (or any Entities). When RecordTypes inherit from other
RecordTypes, the `inheritance` flag tells which properties shall be
inherited from the parents to the children.

Importance
~~~~~~~~~~

The `importance` value of a Property states whether it may, should, or
must be present in all children of this Entity. The table below
lists the possible importances, together with the default behavior of
the server when the property is not present in a child.

.. list-table::
   :widths: 25 25 50
   :width: 100%
   :header-rows: 1

   * - Importance
     - Meaning
     - Consequence when omitted
   * - ``OBLIGATORY``
     - Property must be present
     - Can be forced to be committed the server, but usually
       an error will be returned.
   * - ``RECOMMENDED``
     - Property should be present (makes sense for most users)
     - No consequence right now, but in future versions a warning will
       be returned.
   * - ``SUGGESTED`` (default)
     - Property may be present (may make sense for some users)
     - No negative consequence
   * - ``FIX``
     - Property is not present in any child
     - ---

The default behavior can be overridden for individual transaction by setting the
`force-missing-obligatory` flag to ``ERROR`` (the default), ``WARN``, or ``IGNORE``.  These flags
cause a transaction that would result in an entity with a missing `obligatory` property to return an
error, a warning, or to execute without a warning, respectively. The importance value ``FIX`` is
used for properties that are specific to the Entity itself and is explained below.
       
.. note::

   Note that in contrast to RecordTypes, all properties of a
   :doc:`Record` have the importance ``FIX`` since in general, even in the
   case of a Record having children, its properties are not
   inherited.

Inheritance
~~~~~~~~~~~

The `inheritance` flag specifies which :doc:`Properties
<AbstractProperty>` are inherited from the parent RecordType(s) to the
child.

.. list-table::
   :widths: 40 60
   :header-rows: 1

   * - Inheritance
     - Meaning
   * - `None` (default)
     - Nothing is inherited
   * - ``OBLIGATORY``
     - Properties of importance ``OBLIGATORY`` and above
   * - ``RECOMMENDED``
     - Properties of importance ``RECOMMENDED`` and above
   * - ``SUGGESTED``
     - Properties of importance ``SUGGESTED`` and above
   * - ``ALL``
     - Copy everything from the parent.

Note that inheritance ``ALL`` is essentially the same as ``SUGGESTED``
right now, but this behavior might change in the future. Also note
that properties of importance ``FIX`` are never inherited this
way. This is precisely what this importance level is meant for:
properties that are specific to the parent entity and are not
inherited by default. You can still add this property to the child
manually if you wish, though, but this is not covered by the
automatic inheritance.

.. note::

   The behavior of `inheritance` is currently not well-defined if the
   child is a :doc:`Record`, not a RecordType.

Examples
--------

.. warning::

   The following examples have not been updated in a while and may
   serve as a starting point, but are not guaranteed to be fully
   applicable to the most recent versions of CaosDB.

GET Requests
~~~~~~~~~~~~

Single-Get
^^^^^^^^^^

*Request:*

.. code-block::

   GET http://localhost:8122/RecordType/1

*Response:*

.. code-block:: XML
		
   <?xml version="1.0" encoding="UTF-8"?>
     <Response>
       <RecordType id="1" name="Experiment" description="Description Experiment">
         <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory" />
	 <Property id="4" name="id" type="integer" description="identifier" exponent="0" importance="recommended" />
	 <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory" />
	 <Property id="7" name="stopDate" type="datetime" description="stop" importance="recommended" />
	 <Property id="12" name="file" type="file" description="file" importance="suggested" />
       </RecordType>
     </Response>

Multi-Get
^^^^^^^^^

*Request:*

.. code-block::

   GET http://localhost:8122/RecordType/1&2&3

*Response:*

.. code-block:: XML

   <?xml version="1.0" encoding="UTF-8"?>
     <Response>
       <RecordType id="1" name="Experiment" description="Description Experiment">
         <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory" />
	 <Property id="4" name="id" type="integer" description="identifier" exponent="0" importance="recommended" />
         <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory" />
         <Property id="7" name="stopDate" type="datetime" description="stop" importance="recommended" />
         <Property id="12" name="file" type="file" description="file" importance="suggested" />
       </RecordType>
       <RecordType id="2" name="Measurement" description="Description Measurement">
         <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory" />
         <Property id="4" name="id" type="integer" description="identifier" exponent="0" importance="recommended" />
         <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory" />
         <Property id="7" name="stopDate" type="datetime" description="stop" importance="recommended" />
         <Property id="12" name="file" type="file" description="file" importance="suggested" />
       </RecordType>
       <RecordType id="3" name="Video" description="Description Video">
         <Property id="1" name="explanation" type="text" description="explains the thing" importance="recommended" />
         <Property id="6" name="startDate" type="datetime" description="start" importance="recommended" />
         <Property id="12" name="file" type="file" description="file" importance="obligatory" />
       </RecordType>
     </Response>

	
Get all
^^^^^^^

*Request:*

.. code-block::

   GET http://localhost:8122/RecordType/
   GET http://localhost:8122/RecordType

*Response:*

.. note::

   Outdated? At least the bug tracker doesn't exist anymore.

.. code-block:: XML

   <?xml version="1.0" encoding="UTF-8"?>
     <Response />


POST Requests
~~~~~~~~~~~~~

*Request is to be send to:*

.. code-block::

   POST http://localhost:8122/RecordType/
   POST http://localhost:8122/RecordType/

Single-Post
^^^^^^^^^^^

*HTTP Body:*

.. code-block:: XML

   <Post>  
     <RecordType name="TimmsRecordType11" >
       <Property id="3" importance="recommended"/>
       <Property id="4" importance="obligatory"/>
       <Property name="age" />
     </RecordType>
   </Post>

*Response:*

.. code-block:: XML

   <?xml version="1.0" encoding="UTF-8"?>
     <Response>
       <RecordType id="40" name="TimmsRecordType11">
         <Property id="3" name="name" type="text" description="Name" importance="recommended" />
         <Property id="4" name="id" type="integer" description="identifier" importance="suggested" />
       </RecordType>
     </Response>

Multi-Post
^^^^^^^^^^

*HTTP Body:*

.. code-block:: XML

   <Post>  
     <RecordType name="TimmsRecordType11" >
       <Property id="3" importance="recommended"/>
       <Property id="4" importance="obligatory"/>
       <Property name="age" />
     </RecordType>
     <RecordType name="TimmsRecordType12" >
       <Property id="6" importance="recommended"/>
       <Property id="7" importance="obligatory"/>
       <Property id="15" />
     </RecordType>
   </Post>

*Response:*

.. code-block:: XML

   <?xml version="1.0" encoding="UTF-8"?>
     <Response>
       <RecordType id="40" name="TimmsRecordType11" description="null">
         <Property id="3" name="name" type="text" description="Name" importance="recommended" />
         <Property id="4" name="id" type="integer" description="identifier" importance="suggested" />
         <Property id="3" name="name" type="text" description="Name" importance="recommended" />
         <Property id="4" name="id" type="integer" description="identifier" exponent="0" importance="suggested" />
       </RecordType>
       <RecordType id="41" name="TimmsRecordType12">
         <Property id="6" name="startDate" type="datetime" description="start" importance="recommended" />
         <Property id="7" name="stopDate" type="datetime" description="stop" importance="suggested" />
         <Property id="15" name="TimmsIntProperty" type="integer" unit="kg" description="This is TimmsIntProperty" importance="suggested" />
       </RecordType>
     </Response>

DELETE RecordTypes
~~~~~~~~~~~~~~~~~~

HTTP-DELETE-requests are to be send to ``http://${host}:${port}/RecordType/...``. Default port is 8123.

*Example*

.. code-block:: 

   DELETE http://${host}:${port}/RecordType/1&2&3&4


