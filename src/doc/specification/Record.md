# Record

**Warning:** This specification is outdated. It is included to serve as a starting point for a more up-to-date description of the `Record` entity.

= GET records = 

HTTP-GET-requests are to be send to `http://${host}:${port}/mpidsserver/Record/...`. Default port is 8123.

### Get single record

{{{ 
GET http://${host}:${port}/mpidsserver/Record/1
}}}

returns in case of success:


        <Response id="1353783822304" generated="2012-11-23 11:10:50.507" items="1">
          <Record id="1" generator="X23" timestamp="2012-09-22 00:56:03.0" recordtypename="Experiment" recordtypeid="1">
            <Property id="4" name="id" type="integer" description="identifier" exponent="0" importance="recommended">100111327</Property>  
            <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory">Here comes the explanation</Property>
            <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory">2010-01-20 20:15:00.0</Property>
          </Record>
        </Response>


If a requested record didn't exist, it returns:


        <Response id="1354256823059" generated="2012-11-23 11:17:06.754" items="1">
          <Record id="1">
            <Error code="101" description="Record does not exist. "/>
          </Record>
        </Response>

### Get multiple records

{{{ 
GET http://${host}:${port}/mpidsserver/Record/1&2
}}}

returns in case of success:


        <Response id="1354570488723" generated="2012-11-23 11:12:33.666" items="2">
          <Record id="1" generator="X23" timestamp="2012-09-22 00:56:03.0" recordtypename="Experiment" recordtypeid="1">
            <Property id="4" name="id" type="integer" description="identifier" exponent="0" importance="recommended">100111327</Property>
            <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory">Here comes the explanation</Property>
            <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory">2010-01-20 20:15:00.0</Property>
          </Record>
          <Record id="2" generator="G22" timestamp="2012-09-22 00:56:03.0" recordtypename="Heart" recordtypeid="6">
            <Property id="9" name="weightg" type="double" unit="g" description="weight in g" exponent="0" importance="obligatory">1000.0</Property>
          </Record>
        </Response>

Same behavior as above if one of these didn't exist.

## POST records

The POST-requests are to be send to `http://${host}:${port}/mpidsserver/Record`. Default port is 8123.

### Post single record

*HTTP body:*


        <Post>
          <Record generator="Timm" recordtypeid="1">
            <Property id="6">2012-12-24 18:00:00</Property>
            <Property id="1">We investigate if Santa Clause does fit down the chimney</Property>
          </Record>
        </Post>

| Remarks |
|---------|
* The obligatory root tag is `<Post>`
* A record has to have a `generator="..."` attribute and a _recordtype_ (i.e. a `recordtypeid="..."` or `recordtypename="..."` attribute).
* Depending on the _recordtype_ of the posted _record_ there are several _concrete properties_ that must be definded (they are _obligatory_).

In case of success the response will look like:


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1353708657159" generated="2012-11-23 11:55:41.223" items="1">
          <Record id="272" generator="Timm" timestamp="2012-11-23 10:55:43.0" recordtypename="Experiment" recordtypeid="1">
            <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory">2012-12-24 18:00:00.0</Property>
            <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory">We investigate if Santa Clause does fit down the chimney</Property>
          </Record>
        </Response>

| Remarks |
|---------|
* The responces contains the whole record as it lies in the database now. That entails it's final `id`, a timestamp (`generated` attribute) and a full description of the properties with name, type, descpription and so on.
* For further specifications of the _concrete properties_ see [wiki:ConcreteProperty].

#### Erroneous properies

Now, there are a couple of errors that can occur (cf. [wiki:errorcodes]). The errors that occur due to corrupt concrete properies are described in [wiki:ConcreteProperty]. If any Errors occur on the level of properties, the whole record is handled as unqualified to be posted. Suppose the post body reads


        <Post>
          <Record generator="Timm" recordtypeid="1">
            <Property id="6">Christmas Eve</Property>
            <Property id="1">Does Santa Clause fit down the chimney</Property>
          </Record>
        </Post>

The first property cannot be parsed to a datetime format. Therefore, the response will be


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1354575364020" generated="2012-11-23 12:37:20.045" items="1">
          <Record generator="Timm" recordtypename="Experiment" recordtypeid="1">
            <Error code="103" description="Record has unqualified Properties. " />
            ...
          </Record>
        </Response>

| Remarks |
|---------|
* The error "Record has unqualified Properties" occures whenever the posted concrete properties of the record are corrupt (i.e. any `<Error>` tag is child of a `<Property>` tag).
* Erroneous records are not stored to the database.

#### Erroneous record tags

1) If the recordtype hasn't been indicated, the server returns


        ...
        <Error code="105" description="Record has no RecordType. " />
        ...

2) The indicated recordtype does not exist:


        ...
        <Error code="105" description="Record's RecordType does not exist. " />
        ...
view Tickets #16 #17

3) The generator has not been indicated:


        ...
        <Error code="105" description="Record has no Generator. " />
        ...

4) The record's _recordtype_ demands for a property that isn't present:


        <Post>
          <Record generator="Timm" recordtypeid="1">
            <Property id="6">2012-12-24 18:00:00</Property>
          </Record>
        </Post>

returns:


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1354701618532" generated="2012-11-23 13:25:41.59" items="1">
          <Record generator="Timm" recordtypename="Experiment" recordtypeid="1">
            <Error code="102" description="Obligatory Property 1 (explanation) is missing. " />
            <Property id="6" name="startDate" type="datetime" description="start" importance="obligatory">2012-12-24 18:00:00.0</Property>
          </Record></Response>


5) Record does'nt entail any properties:


        ...
        <Error code="105" description="Record has no Properties. " />
        ...

6) The indicated ID and name of a _recordtype_ are mismatching (at least one of them exists but they don't correspond to each other).


        ...
        <Error code="105" description="Record has invalid RecordType (Name/ID mismatch). " />
        ...


### Post multiple records

It is possible to post several records in one go. Here we go through a few scenarios with independent sets of records and closely intertwined ones. 

#### Independent records

*HTTP body:*


        <Post>
          <Record generator="Timm" recordtypeid="1">
            ... (properties) ...
          </Record>
          <Record generator="Timm" recordtypeid="1">
            ... (properties) ...
          </Record>
          ... (more records) ...
        </Post>

returns:


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1356209136586" generated="2012-11-30 11:33:39.897" items="2">
          <Record id="274" generator="Timm" timestamp="2012-11-30 10:33:41.0" recordtypename="Experiment" recordtypeid="1">
            ... (properties) ...
          </Record>
          <Record id="275" generator="Timm" timestamp="2012-11-30 10:33:41.0" recordtypename="Experiment" recordtypeid="1">
            ... (properties) ...
          </Record>
          ... (more records) ...
        </Response>

#### Non-circular interdependent records:

*HTTP body:*


        <Post>
          <Record generator="Timm" recordtypeid="1">
            <Property id="10">-1</Property>
            ... (more properties) ...
          </Record>
          <Record id="-1" generator="Timm" recordtypeid="1">
            ... (properties) ...
          </Record>
          ... (more records) ...
        </Post>

returns:


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1354608915619" generated="2012-11-30 11:40:35.369" items="2">
          <Record id="277" generator="Timm" timestamp="2012-11-30 10:40:37.0" recordtypename="Experiment" recordtypeid="1">
            <Property id="10" name="experiment" type="reference" reference="1" description="reference to experiment" importance="suggested">276</Property>
            ... (more properties) ...
          </Record>
          <Record id="276" generator="Timm" timestamp="2012-11-30 10:40:37.0" recordtypename="Experiment" recordtypeid="1">
            ... (properties) ...
          </Record>
          ... (more records) ...
        </Response>

| Remarks |
|---------|
* The record that is to be referenced by another gets an id less than zero (id="-1"). This id is provisional and will be replaced by the final id, which is the one the record is stored with in the database, in the response (id="276"). 

#### Circular dependent records

*HTTP body:*


        <Post>
          <Record id="-2" generator="Timm" recordtypeid="1">
            <Property id="10">-1</Property>
            ... (more properties) ...
          </Record>
          <Record id="-1" generator="Timm" recordtypeid="1">
            <Property id="10">-2</Property>
            ... (more properties) ...
          </Record>
          ... (more records) ...
        </Post>

returns:


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1354698071005" generated="2012-11-30 11:52:28.789" items="2">
          <Record id="278" generator="Timm" timestamp="2012-11-30 10:52:30.0" recordtypename="Experiment" recordtypeid="1">
            <Property id="10" name="experiment" type="reference" reference="1" description="reference to experiment" importance="suggested">279</Property>
            ... (more properties) ...
          </Record><Record id="279" generator="Timm" timestamp="2012-11-30 10:52:30.0" recordtypename="Experiment" recordtypeid="1">
            <Property id="10" name="experiment" type="reference" reference="1" description="reference to experiment" importance="suggested">278</Property>
            ... (more properties) ...
          </Record>
          ... (more records) ...
        </Response>

| Remarks |
|---------|
* Likewise,records that are to be referenced by each other get ids less than zero (id="-1" and id="-2"). These are provisional and will be replaced by the final ids in the response (id="278" and id="279"). 


#### Erroneous multi-posts

1) If any error occurs in a single record, all correct records are nevertheless stored (In future this behavior should be controlled better by an _atomic flag_. See #18). 

2) If a referenced record is corrupt, the referencing record is marked with an error tag equally.

*HTTP body:*


        <Post>
          <Record generator="Timm" recordtypeid="1">
            <Property id="10">-1</Property>
             ... (more properties) ...
          </Record>
          <Record id="-1" generator="Timm" recordtypeid="1">
            <Property id="1">Explanation2</Property>
            <!-- obligatory property 6 is missing -->
          </Record>
          ... (more records) ...
        </Post>

returns:


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1356334296695" generated="2012-11-30 12:14:37.246" items="2">
          <Record id="-2" generator="Timm" recordtypename="Experiment" recordtypeid="1">
            <Error code="103" description="Record has unqualified Properties. " />
            <Property id="10" name="experiment" type="reference" reference="1" description="reference to experiment" importance="suggested">
              <Error code="204" description="Referenced Record is unqualified. " />
              -1
            </Property>
            ... (more properties) ...
          </Record>
          <Record id="-1" generator="Timm" recordtypename="Experiment" recordtypeid="1">
            <Error code="102" description="Obligatory Property 6 (startDate) is missing. " />
            <Property id="1" name="explanation" type="text" description="explains the thing" importance="obligatory">Explanation2</Property>
          </Record>
          ... (more records) ...
        </Response>

3) Self-referencing is not allowed. A self-referencing record will return


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1355236745833" generated="2012-11-30 12:18:45.066" items="1">
          <Record id="-2" generator="Timm" recordtypename="Experiment" recordtypeid="1">
            <Error code="104" description="Self-referencing is not allowed. " />
            <Property id="10" name="experiment" type="reference" reference="1" description="reference to experiment" importance="suggested">-2</Property>
            ... (more properties) ...
          </Record>
          ... (more records) ...
        </Response>

4) Referencing a non-existing provisional id will return (cf. #19):


        <?xml version="1.0" encoding="UTF-8"?>
        <Response id="1354990914009" generated="2012-11-30 13:07:00.45" items="1">
          <Record generator="Timm" recordtypename="Experiment" recordtypeid="1">
            <Error code="104" description="Referenced record does not exist. " />
            <Property id="10" name="experiment" type="reference" reference="1" description="reference to experiment" importance="suggested">-2</Property>
            ... (more properties) ...
          </Record>
          ... (more records) ...
        </Response>

= DELETE records = 

HTTP-DELETE-requests are to be send to `http://${host}:${port}/mpidsserver/Record/...`. Default port is 8123.

*Example*
{{{ 
DELETE http://${host}:${port}/mpidsserver/Record/1&2&3&4
}}}






