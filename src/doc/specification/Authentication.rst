==============
Authentication
==============

Some features of CaosDB are available to registered users only. Making any changes to the data stock
via HTTP requires authentication.

Sessions
========

Login
-----

Authentication is done by ``username`` and ``password``. They must be sent as form data with a POST
request to the `/login/` resource:

username:
  The user name, for example ``admin`` (on demo.indiscale.com).

password:
  The password, for example ``caosdb`` (on demo.indiscale.com).

Logout
------

The server does not invalidate AuthTokens. They invalidate after they expire or
when the server is being restartet. Client should just delete their AuthToken
to 'logout'.

However, in order to remove the AuthToken cookie from the browsers there is a
convenient resource which will invalidate the cookie (not the AuthToken).

Send

``GET http://host:port/logout``

and the server will return an empty AuthToken cookie which immediately expires.

Example using ``curl``
----------------------

.. _curl-login:

Login
~~~~~

To use curl for talking with the server, first save your password into a
variable: ``PW=$(cat)``

The create a cookie in ``cookie.txt`` like this (note that this makes your
password visible for a short time to everyone on your system:

.. code-block:: sh

   curl -X POST -c cookie.txt -D head.txt -d username=<USERNAME> -d password="$PW" --insecure "https://<SERVER>/login

Now ``cookie.txt`` contains the required authentication token information in the ``SessionToken``
cookie (url-encoded json).

.. rubric:: Example token content

.. code-block:: json

   ["S","PAM","admin",[],[],1682509668825,3600000,"Z6J4B[...]-OQ","31d3a[...]ab2c10"]

Using the token
~~~~~~~~~~~~~~~

To use the cookie, pass it on with later requests:

.. code-block:: sh

   curl -X GET -b cookie.txt --insecure "https://<SERVER>/Entity/123"
