# Datatype

## TEXT
* Description: TEXT stores stores any text values.
* Range: Any [utf-8](https://en.wikipedia.org/wiki/UTF-8) encodable sequence of characters with maximal 65,535 bytes. (Simply put: In most cases, any text with less than 65,535 letters and spaces will work. But if you use special characters like `à`, `€` or non-latin letters then the number of bytes, which are needed to store it, increases. Then the effective maximal length is smaller than 65,535. A bad case scenario would be a text in Chinese. Chinese characters need about three times the space of letters from the latin alphabet. Therefore, only 21845 Chinese characters can be stored within this datatype. Which is still quite a lot I guess :D)
* Examples: 
  * `Am Faßberg 17, D-37077 Göttingen, Germany`
  * `Experiment went well until the problem with the voltmeter occured. Don't use the results after that.`
  * `someone@email.org`
  * `Abstract: bla bla bla ...`
  * `Head of Group`
  * `http://www.bmp.ds.mpg.de`
  * 

        A. Schlemmer, S. Berg, TK Shajahan, S. Luther, U. Parlitz,
           Quantifying Spatiotemporal Complexity of Cardiac Dynamics using Ordinal Patterns,
           37th Annual International Conference of the IEEE Engineering in Medicine and Biology Society (EMBC), 2015, doi: 10.1109/EMBC.2015.7319283

----

## BOOLEAN
* Description: BOOLEAN stores boolean `TRUE` or `FALSE`. It is therefore suitable for any variable that represents that something is the case or not.
* Accepted Values: `TRUE` or `FALSE`, case insensitive (i.e. it doesn't matter if you use capitals or small letters).
* Note: You could also use a TEXT datatype to represent booleans (or even INTEGER or DOUBLE). But it makes a lot of sense to use this special datatype as it ensures that only the two possible values, `TRUE` or `FALSE` are inserted into the database. Every other input would be rejected. This helps to keep the database understandable and to avoid mistakes.

----

## INTEGER
* Description: INTEGER stores integer numbers. If you need floating point variables, take a look at DOUBLE.
* Range: `-2147483648` to `2147483647`, `-0` is interpreted and stored as `0`.
* Note: This rather limited range is just provisional. It can be extended with low effort as soon as requested.

----

## DOUBLE
* Description: DOUBLE stores floating point numbers with a double precision as defined by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point).
* Range: 
  * From `2.2250738585072014E-308` to `1.7976931348623157E308` (negative and positive) with a precision of 15 decimals. 
  * Any other decimal number _might work_ but it is not guaranteed.
  * `-0`, `0`, `NaN`, `-inf` and `inf`
* Note: The server generates a warning when the precision of the submitted DOUBLE value is to high to be preserved.

----

## DATETIME
The DateTime data type exists in (currently) three flavors which are dynamically chosen during parsing on the serverside. The flavors have different ranges, support of time zones and intended use cases. Only the first two flavors are actually implemented for storage and queries. The third one is implemented for queries exclusively.

### UTCDateTime
* Description: This DATETIME flavor stores values which represent a single point of time according to [UTC](https://en.wikipedia.org/wiki/Coordinated_Universal_Time) with the format specified by [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) (Combined date and time). It does support [UTC Leap Seconds](https://en.wikipedia.org/wiki/Leap_second) and time zones.
* Range: From `-9999-01-01T00:00:00.0UTC` to `9999-12-31T23:59:59.999999999UTC` with nanosecond precision.
* Examples:
  * `2016-01-01T13:23:00.0CEST` which means _January 1, 2016,  1:23 PM, Central European Summer Time_.
  * `-800-01-01T13:23:00.0` which means _January 1, 800 BC,  1:23 PM, UTC_.
* Note:
  * It is allowed to ommit the nanosecond part of a UTCDateTime (`2016-01-01T13:23:00CEST`). This indicates a precision of seconds for a UTCDateTime value.

### Date
 Description:: This DATETIME flavor stores values which represent a single date, month or year according to the [gregorian calendar](https://en.wikipedia.org/wiki/Gregorian_Calendar). A month/year is conceived as a single date with the presion of a month/year. This concept is useful if you try to understand the query semantics which are explained [elsewhere](../CaosDB-Query-Language.html#pov-property-operator-value).
 Format:: `Y[YYY][-MM[-dd]]` (where square brackets mean that the expression is optional).
 Range:: Any valid date according to the gregorian calendar from `-9999-01-01` to `9999-12-31` (and respective dates with lower precision. E.g. the year `-9999`). There is no year `0`.
* Note: Date is a specialization of [#SemiCompleteDateTime]. 

### SemiCompleteDateTime
* Description: A generalization of the _Date_ and _UTCDateTime_ flavors. In general, there is no time zone support. Although this flavor is not yet storable in general, it is implemented for search queries yet. I.e. you might search for `FIND ... date>2015-04-03T20:15` yet.
* Format: `Y[YYY]['-MM[-dd[Thh:[mm[:ss[.ns]]]]]]]`. 
* Special Properties: For every SemiCompleteDateTime _d_ there exists a _Inclusive Lower Bound_ (`d.ILB`) and a _Exclusive Upper Bound_ (`d.EUB`). That means, a SemiCompleteDateTime can be interpreted as an interval of time. E.g. `2015-01` is the half-open interval `[2015-01-01T00:00:00.0, 2016-01-01T00:00:00.0)`. ILB and EUB are UTCDateTimes respectively. These properties are important for the semantics of the the query language, especialy the [operators](./QueryLanguage#POVDateTime). 

### Future Flavors
Please file a new feature request as soon as you need them.
* Time:: For a time of the day (without the date). Supports time zones.
* FragmentaryDateTime:: For any fragmentary DateTime. That is an arbitrary combination of year, month, day of week, day of month, day of year, hour of day, minute, seconds (and nanoseconds). This flavor is useful for recurrent events like a bus schedule (_Saturday, 7:30_) or the time of a standing order for money transfer (_third day of the month_).

----

## REFERENCE
* Description: REFERENCE values store the [Valid ID](../Glossary.html#valid-id) of an existing entity. The are useful to establish links between two entities. 
* Accepted Values: Any [Valid ID](./Glossary#valid-id) or [Valid Unique Existing Name](./Glossary#valid-unique-existing-name) or [Valid Unique Temporary ID](./Glossary#valid-unique-temporary-id) or [Valid Unique Prospective Name](./Glossary#valid-unique-prospective-pame).
* Note:
  * After beeing processed successfully by the server the REFERENCE value is normalized to a [Valid ID](./Glossary#valid-id). I.e. it is guaranteed that a REFERENCE value of a valid property is a positive integer.

### FILE
* Description: A FILE is a special REFERENCE. It only allows entity IDS which belong to a File.

### RecordType as a data type
* Furthermore, any RecordType can be used as a data type. This is a variant of the REFERENCE data type where any entity is a valid value which is a child of the RecordType in question.
* Example:
  * Let `Person` be a RecordType, `Bertrand Russel` be a child of `Person`. Then `Bertrand Russel` is a valid value for a property with a `Person` data type.

## LIST
* Description: A LIST is always a list of something which has another data type. E.g. A LIST of TEXT values, a LIST of REFERENCES value, etc. Here we call TEXT resp. REFERENCE the **Element Data Type**. The LIST data type allows you to store an arbitrary empty or non-empty ordered set (with duplicates) of values of the *same* data type into one property. Each value must be a valid value of the Element Data Type.
* Example:
  * LIST of INTEGER: ```[0, 2, 4, 5, 8, 2, 3, 6, 7]```
  * LIST of Person, while `Person` is a RecordType: ```['Bertrand Russel', 'Mahatma Ghandi', 'Mother Therese']```


