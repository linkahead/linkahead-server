.. _Entity Permissions Table:

Entity Permissions
==================

Permission to the following actions can be granted or denied in CaosDB:

.. list-table:: Entity permission actions
   :header-rows: 1
   :widths: 33 67

   * - Action
     - Description
   * - ``RETRIEVE:ENTITY``
     - Permission to retrieve the full entity (name, description, data type,
       ...) with all parents and properties (unless prohibited by another rule
       on the property level).
   * - ``RETRIEVE:ACL``
     - Permission to retrieve the full and final ACL of this entity.
   * - ``RETRIEVE:HISTORY``
     - Permission to retrieve the history of this entity.
   * - ``RETRIEVE:OWNER``
     - Permission to retrieve the owner(s) of this entity.
   * - ``RETRIEVE:FILE``
     - Permission to download the file belonging to this entity.
   * - ``DELETE``
     - Permission to delete this entity.
   * - ``EDIT:ACL``
     - Permission to change the user-specified part of this entity's ACL. Roles
       with this Permission are called ``Owners``.
   * - ``UPDATE:DESCRIPTION``
     - Permission to change the value of this entity.
   * - ``UPDATE:VALUE``
     - Permission to change the value of this entity.
   * - ``UPDATE:ROLE``
     - Permission to change the role of this entity.
   * - ``UPDATE:PARENT:REMOVE``
     - Permission  to remove parents from this entity.
   * - ``UPDATE:PROPERTY:REMOVE``
     - Permission to remove properties from this entity.
   * - ``UPDATE:PROPERTY:ADD``
     - Permission to add a property to this entity.
   * - ``UPDATE:NAME``
     - Permission to change the name of this entity.
   * - ``UPDATE:DATA_TYPE``
     - Permission to change the data type of this entity.
   * - ``UPDATE:FILE:REMOVE``
     - Permission to delete the file of this entity.
   * - ``UPDATE:FILE:ADD``
     - Permission to set a file for this entity.
   * - ``UPDATE:FILE:MOVE``
     - Permission to move an existing file to a new location.
   * - ``USE:AS_REFERENCE``
     - Permission to refer to this entity via a reference property.
   * - ``USE:AS_PROPERTY``
     - Permission to implement this entity as a property.
   * - ``USE:AS_PARENT``
     - Permission to use this entity as a super type for other entities.
   * - ``USE:AS_DATA_TYPE``
     - Permission to use this entity as a data type for reference properties.
   * - ``UPDATE:QUERY_TEMPLATE_DEFINITION``
     - Permission to update the query template definition of this QueryTemplate
