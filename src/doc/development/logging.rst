Logging
=======

Framework
---------

We use the SLF4J API with a log4j2 backend for all of our Code. Please
do not use log4j2 directly or any other logging API.

Note that some libraries on the classpath use the ``java.util.logging``
API and log4j1 logging framework instead. These loggers cannot be
configurated with the help of this README by now.

Configuration
-------------

The configuration of the log4j2 backend is done via ``properties`` files which comply with the
`log4j2 specifications
<https://logging.apache.org/log4j/2.x/manual/configuration.html#Properties>`__.
XML, YAML, or JSON files are not supported. The usual mechanisms for
automatic configuration with such files is disabled. Instead, files have
to be placed into the ``conf`` subdirs, as follows:

Default and Debug Logging
~~~~~~~~~~~~~~~~~~~~~~~~~

The default configuration is located at
``conf/core/log4j2-default.properties``. For the debug mode, the
configuration from ``conf/core/log4j2-debug.properties`` is merged with
the default configuration. These files should not be changed by the
user.

User Defined Logging
~~~~~~~~~~~~~~~~~~~~

The default and debug configuration can be overridden by the user with
``conf/ext/log4j2.properties`` and any file in the directory
``conf/ext/log4j2.properties.d/`` which is suffixed by ``.properties``.
All logging configuration files are merged using the standard merge
strategy of log4:

   .. rubric:: Composite Configuration
      :name: composite-configuration

   Log4j allows multiple configuration files to be used by specifying
   them as a list of comma separated file paths on
   log4j.configurationFile. The merge logic can be controlled by
   specifying a class that implements the MergeStrategy interface on the
   log4j.mergeStrategy property. The default merge strategy will merge
   the files using the following rules:

   1. The global configuration attributes are aggregated with those in later configurations
      replacing those in previous configurations, with the exception that the highest status level
      and the lowest monitorInterval greater than 0 will be used.
   
   2. Properties from all configurations are aggregated.  Duplicate properties replace those in
      previous configurations.

   3. Filters are aggregated under a CompositeFilter if more than one Filter is defined. Since
      Filters are not named duplicates may be present.

   4. Scripts and ScriptFile references are aggregated.  Duplicate definiations replace those in
      previous configurations.

   5. Appenders are aggregated. Appenders with the same name are replaced by those in later
      configurations, including all of the Appender’s subcomponents.

   6. Loggers are all aggregated. Logger attributes are individually merged with duplicates being
      replaced by those in later configurations. Appender references on a Logger are aggregated with
      duplicates being replaced by those in later configurations. Filters on a Logger are aggregated
      under a CompositeFilter if more than one Filter is defined. Since Filters are not named
      duplicates may be present. Filters under Appender references included or discarded depending
      on whether their parent Appender reference is kept or discarded.

`Source <https://logging.apache.org/log4j/2.x/manual/configuration.html#CompositeConfiguration>`__

Some Details and Examples
-------------------------

Make Verbose
~~~~~~~~~~~~

To make the server logs on the console more verbose, insert
``rootLogger.level = DEBUG`` or even ``rootLogger.level = TRACE`` into a
properties file in the ``conf/ext/log4j2.properties.d/`` directory or
the ``conf/ext/log4j2.properties`` file.

Log Directory
~~~~~~~~~~~~~

By default, log files go to ``./log/``,
e.g. ``./log/request_errors/current.log``. The log directory in
``DEBUG_MODE`` is located at ``./testlog/``.

To change that, insert ``property.LOG_DIR = /path/to/my/logs`` into a
properties file in the ``conf/ext/log4j2.properties.d/`` directory or
the ``conf/ext/log4j2.properties`` file

Special loggers
~~~~~~~~~~~~~~~

-  ``REQUEST_ERRORS_LOGGER`` for logging server errors with SRID, full
   request and full response. WARNING: This logger stores unencrypted
   content of request with possibly confidential content.
-  ``REQUEST_TIME_LOGGER`` for timing the requests.

These loggers are defined in the ``conf/core/log4j2-default.properties``
file.

Enable Request Time Logger
^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``REQUEST_TIME_LOGGER`` is disabled by default, its log level is set
to ``OFF``. To enable it and write logs to the directory denoted by
``property.LOG_DIR``, create a ``properties`` file under
``conf/ext/log4j2.properties.d/`` which contains at least

.. code:: properties

   property.REQUEST_TIME_LOGGER_LEVEL = TRACE

debug.log
~~~~~~~~~

When in ``DEBUG_MODE``, e.g. when started with ``make run-debug``, the
server also writes all logs to ``debug.log`` in the log directory.
