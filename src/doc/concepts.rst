
Basic concepts of the CaosDB server
===================================

.. toctree::
   :hidden:
   :glob:

   Data Model <Data-Model>
   permissions
   roles

The CaosDB server provides the HTTP API resources to users and client libraries.  It uses a plain
MariaDB/MySQL database as backend for data storage, raw files are stored separately on the file
system.
