Data model tutorial
===================

Data models are a central concept of research data management. The
open-source framework LinkAhead makes it easy to create an `agile
data model <https://docs.indiscale.com/caosdb-server/Data-Model.html>`__
which suits your needs, so you can profit from your collected data fast.

.. contents::
   :local:

Frequently asked questions about data models
--------------------------------------------

What is a data model?
~~~~~~~~~~~~~~~~~~~~~

In LinkAhead, the `data model <https://docs.indiscale.com/caosdb-server/Data-Model.html>`__
describes in a generic way how the data is structured. This document will help you understand this
concept and how you can adjust the data model to your needs.

Let’s give one example what we mean by the data model describing the
data structure: In experimental research, the data model may say that
each experiment has one or more experimenters and also some lab notes.

.. figure:: img/very_simple_data_model.png
   :alt: The simplest data model consists of just one RecordType

   A very simple data model, consisting of only one RecordType

The data model may also say that there are cell culture experiments,
which *inherit* all properties of generic experiments, but in addition
have the number of dishes, the time the cell cultures were started and a
*reference* to the cell line that was used. When we talk about
references, we mean that there are other data sets (in this case cell
lines) which are not embedded directly, but only pointed at. For
example, a number of cell culture experiments may reference the same
cell line, and when someone adds a note to that cell line, this note
will be available to all cell culture experiments.

.. figure:: img/simple_data_model.png
   :alt: The first data model extended by inheritance and references

   A simple data model, with four RecordTypes

In LinkAhead, the data model consists of *RecordTypes* and *Properties*. Each `*RecordType*
<https://docs.indiscale.com/caosdb-server/Data-Model.html#record-types>`__ corresponds to one type
of data set, for example an experiment, a cell culture experiment or a cell line. And each data set
can have `*Properties* <https://docs.indiscale.com/caosdb-server/Data-Model.html#properties>`__
which define it further: for example
the number of dishes in a cell culture experiment, a publication
describing a cell line or the cell line which was used for an
experiment.

And what about the data?
~~~~~~~~~~~~~~~~~~~~~~~~

The data model describes the generic structure of the data, but what
does this mean for your actual data? When you want to store data in
LinkAhead, you typically look for a matching *RecordType* and
then create *Record*\ s of that type. This means for example, that a
*Record* of type “Cell Culture” should have the *Properties* that the
*RecordType* provides: experimenters, lab notes, number of dishes, time
of experiment start and the used cell lines.

.. figure:: img/data_model_and_data.png
   :alt: Records are always based upon (at least) one RecordType, this defines the relationship between data model and data

   A diagram showing the relationship between RecordTypes (the data
   model) and Records (the data)

Following a data model greatly simplifies your scientific research data
management, and this is what LinkAhead is for: You know that all
(or most) of your cell culture experiments will have the same set of
properties, so you can search for all experiments where the properties
match your desired criteria, for example those with cell lines which
were described in publications of a certain author.

What if I need to change my data model?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Science moves fast, and your infrastructure should be able to follow
just as swiftly. The data model in LinkAhead was designed to be
very flexible, so it can adapt to your needs.

There are multiple ways to modify and enhance your data model, for
example you could use the web application or program the changes with
the Python client. This document describes how to change the data model
with the LinkAhead web application.

What happens to my data if the data model changes?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With most systems, changing the data model is not possible without
migrating the existing data. With LinkAhead however, your
“legacy” data can simply stay where it is, it will not be modified. New
data, that you enter after changing the data model, will of course
follow your changes and adhere to your “new” data model.

Modifying the data model in LinkAhead
-------------------------------------

The easiest way to change the data model is to `use the LinkAhead
web
application <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html>`__.
But the first step is to look at the current data model (*RecordTypes*
and *Properties*) and your current situation, to find out what you need
changed. It may help to ask yourself these questions:

- Do you want to describe data of existing types in more detail? -> :ref:`Create
  *Properties*<new_property>` if necessary, then `add
  <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html#add-properties-to-an-existing-recordtype>`__
  these *Properties* to the existing *RecordTypes*.
- Do you need to add a property more than once? -> Make that property `a list property
  <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html#make-a-property-into-a-list>`__\
  .
-  Are there completely new types of data? -> :ref:`Create a new
   *RecordType* <new_recordtype>` .
-  Are there new types of data which would nearly fit into an existing
   RecordType, but not quite? Do you still want to keep the existing
   RecordType for some purposes, but require changes or additions for
   other purposes? -> :ref:`Create a new
   *RecordType* <new_recordtype>` which inherits from the
   existing RecordType as a parent.
-  Do you have a new setup, new devices, new protocols, new software? ->
   :ref:`Create a new
   *RecordType* <new_recordtype>` .
- Are there *Properties* or *RecordTypes* which were created in the past, but will never be used? ->
   `Delete them
   <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html#delete-an-entity>`__.

Once you know what you want to do, open the LinkAhead web application, log in and click on the “Edit
Mode” button in the menu.  Then follow one of the instructions in the next sections or the general `Edit
Mode documentation <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html>`__..

.. figure:: img/edit_mode_button.png
   :alt: The Edit Mode button appears when a user is logged in.

   A screen shot of the menu, with the Edit Mode button highlighted

.. _new_property:

Create a new Property
~~~~~~~~~~~~~~~~~~~~~

Creating the required Properties often is the first step for changes to
the data model.

1. Check that there is no Property yet which does what you want:

   -  Search for existing Properties: ``FIND PROPERTY *qualit*`` to
      search for Properties with “qualit” in the name, ``FIND PROPERTY``
      for all Properties.
   -  Look at related RecordTypes, maybe they already use a Property
      which does the thing you need.
   -  Ask around. If possible at least two persons. Your team mates may
      have had the same thoughts already, or may know about someone who
      implemented your desired Property.

2. Activate the *Edit Mode* in the top menu. Now the edit mode toolbox
   on the right hand side of the screen should be visible.
3. Click on the “Create Property” button in the toolbox. A dialog for
   the new Property shows up.
4. Follow the `WebUI documentation
   <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html#new-property>`__ for Property
   creation.

.. _new_recordtype:

Create a new RecordType
~~~~~~~~~~~~~~~~~~~~~~~

1. Check that there is no RecordType yet which does what you want:

   -  Search for existing RecordTypes: ``FIND RECORDTYPE *experi*`` to
      search for RecordTypes with “experi” in the name,
      ``FIND RECORDTYPE`` for all RecordTypes.
   -  Look at related RecordTypes, maybe you can inherit from one?
   -  Ask around. If possible at least two persons. You team mates may
      have had the same thoughts already, or may know about someone who
      implemented your desired RecordType.

2. Activate the *Edit Mode* in the top menu. Now the edit mode toolbox
   on the right hand side of the screen should be visible.

3. Click on the “Create RecordType” button in the toolbox. A dialog for
   the new Property shows up.

4. Follow the `WebUI documentation
   <https://docs.indiscale.com/caosdb-webui/tutorials/edit_mode.html#new-recordtype>`__ for
   RecordType creation.

What about letting users only choose from a fixed set of values (a.k.a. enums)?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You may ask yourself how you can restrict users (or yourself) to choose
property values from a fixed set. For example, entering the
experimenters in an “Experiment” record may be cumbersome and
error-prone. Or entering a chemical formula again and again may become
tedious. This is where data management tools like LinkAhead can
help you by referencing between entities.

In this example, instead of having a TEXT Property “experimenter”, where
users need to enter the name by hand, you could create a new RecordType
“Person”. Then create a Record for each experimenter in your team. And
finally, remove the “experimenter” TEXT Property and create a new
REFERENCE Property which references to the “Person” records. Now all the
users have to do is to choose the experimenter from a drop-down menu in
LinkAhead.

.. figure:: img/screenshot_dropdown.png
   :alt: Users may choose one of multiple Persons when enums are implemented via REFERENCE Properties

   A screenshot of a drop down menu where one of multiple “Person” records may be chosen

