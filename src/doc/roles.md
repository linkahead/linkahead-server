# Roles #

## Users and roles ##

Interaction with CaosDB happens either as an authenticated *user* or without
authentication.  In CaosDB, users can have zero, one or more *roles*, several
users may have the same role, and there may be roles without any users.

## What are users and roles good for? ##

The user and their roles are always returned by the server in answers to requests
and can thus be interpreted and used by clients.  The most important use though
is [permission](permissions) checking in the server: Access and
modification of
entities can be controlled via roles, so that users of a given role are allowed
or denied certain actions.  Incidentally, the permission to edit the permissions
of an entity is seen as defining the ownership of an object: Being able to
change the permissions is equivalent to being the owner.

## Special roles ##

There are some special roles, which are automatically assigned to users:

- `anonymous` :: If requests are sent to the server without authentication, so
  that no user is defined, the request always has the role `anonymous`.
- *User names* :: An authenticated user implicitly has a role with the same name
  as the user name.
- `?OWNER?` :: If a user has the permission to edit the permissions of an
  entity, the user automatically has the `?OWNER?` roler for that entity.
- `?OTHER?` :: The `?OTHER?` role is the contrary to the `?OWNER?` role: A user
  is either the owner of an entity, or has the role `?OTHER?`.

Except for the `anonymous` role, these special roles are not returned by the
server, but can nevertheless be used to define
[permissions](permissions.html#permissions).
