Related Projects
++++++++++++++++

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

.. container:: projects

   For in-depth documentation for users, administrators  and developers, you may want to visit the subproject-specific documentation pages for:

   :`MySQL backend <https://docs.indiscale.com/caosdb-mysqlbackend>`_: The MySQL/MariaDB components of the LinkAhead server.

   :`WebUI <https://docs.indiscale.com/caosdb-webui>`_: The default web frontend for the LinkAhead server.

   :`PyLinkAhead <https://docs.indiscale.com/caosdb-pylib>`_: The LinkAhead Python library.

   :`Advanced user tools <https://docs.indiscale.com/caosdb-advanced-user-tools>`_: The advanced Python tools for LinkAhead.

   :`LinkAhead Crawler <https://docs.indiscale.com/caosdb-crawler/>`_: The crawler is the main tool for automatic data integration in LinkAhead.

   :`LinkAhead <https://docs.indiscale.com/caosdb-deploy>`_: Your all inclusive LinkAhead software package.

   :`Back to Overview <https://docs.indiscale.com/>`_: LinkAhead Documentation.
