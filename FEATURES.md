# Features

* The CaosDB Server implements a CaosDB GRPC API Endpoint (v0.3.0)
  Authentication is supported via a Basic scheme, using the well-known
  "authentication" header.
  Notable limitations of the current implementation of the API:
  * It is currently not possible to mix retrievals
    (caosdb.entity.v1.RetrieveRequest) with any other transaction type - so
    transaction are either read-only or write-only. The server throws an error
    if it finds mixed read/write transactions.
  * It is currently not possible to have more that one query
    (caosdb.entity.v1.Query) in a single transaction. The server throws an
    error if it finds more than one query.
* Legacy XML/HTTP API (Deprecated)
* Deployment of caosdb-webui (>=0.8.0)
* Server-side Scripting API (v0.1)
* CaosDB Query Language Processor
* CaosDB FileSystem
* User Management, Authentication and Authorization
  * Internal authentication service
  * Authentication via an external service (PAM, LDAP)
