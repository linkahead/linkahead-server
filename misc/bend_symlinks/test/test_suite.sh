#!/bin/bash

source ./src/utils.sh
set +o errexit


BEND=./bend_symlinks.sh
FILE_SYSTEM_ROOT=test_dir/links
DATA_DIR=test_dir/original

oneTimeSetUp () {
    mkdir -p $FILE_SYSTEM_ROOT $DATA_DIR
}

oneTimeTearDown () {
    rm -rf test_dir
}

tearDown () {
    rm -rf $FILE_SYSTEM_ROOT/*
    rm -rf $DATA_DIR/*
}

_make_test_file () {
    touch "$DATA_DIR/$1"
    TARGET=$(realpath "$DATA_DIR/$1")
    LINK=$FILE_SYSTEM_ROOT/$1
    ln -s "$TARGET" "$LINK"
    LINKED=$(realpath "$LINK")
    assertEquals "initial target $1" "$LINKED" "$TARGET"
}

_break_link_move_file () {
    set -o noglob
    OLD_PATH="$DATA_DIR/$1"
    OLD_PATH_REAL=$(realpath "$OLD_PATH")
    NEW_PATH="$DATA_DIR/$2"
    NEW_PATH_REAL=$(realpath "$NEW_PATH")
    LINK="$FILE_SYSTEM_ROOT/$1"
    mv "$OLD_PATH_REAL" "$NEW_PATH_REAL"
    LINKED=$(realpath "$LINK")
    assertEquals "still target $OLD_PATH_REAL" "$LINKED" "$OLD_PATH_REAL"
    assertFalse "$LINK link is broken" "[ -f '$LINK' ]"
    assertFalse "$OLD_PATH_REAL was moved" "[ -f '$OLD_PATH_REAL' ]"
    assertTrue "$NEW_PATH_REAL is there" "[ -f '$NEW_PATH_REAL' ]"
    set +o noglob
}

testVersion () {
    assertEquals "version 0.1" "0.1" "$($BEND -v)"
}

assertLinkOk () {
    set -o noglob
    LINK=$(realpath "$FILE_SYSTEM_ROOT/$1")
    TARGET=$(realpath "$DATA_DIR/$2")
    assertTrue "target exists $LINK" "[ -f '$LINK' ]"
    assertEquals "target matches $TARGET" "$TARGET" "$LINK"
    set +o noglob
}

testIgnoreUnbroken () {
    _make_test_file "fileA"
    RESULTS=$($BEND $FILE_SYSTEM_ROOT "fileA" "fileA.new" 2>&1) # attempt to rename

    assertEquals "ignoring not broken" "#IGNORING (not broken): $(realpath "$PWD")/test_dir/links/fileA" "$RESULTS"

    assertLinkOk "fileA" "fileA"
}

testIgnoreMissingNew () {
    _make_test_file "fileA"

    _break_link_move_file "fileA" "fileA.new"

    RESULTS=$($BEND $FILE_SYSTEM_ROOT "fileA" "fileA.non" 2>&1)

    assertEquals "ignoring broken new" "#IGNORING (broken new): $(realpath $DATA_DIR)/fileA.non" "$RESULTS"

    TARGET=$(realpath -m "$DATA_DIR/fileA")
    LINK=$(realpath -m "$FILE_SYSTEM_ROOT/fileA")
    assertFalse "symlink still broken" "[ -e '$LINK' ]"
    assertEquals "target still old" "$TARGET" "$LINK"
}


testFileName () {
    _make_test_file "fileA"
    _make_test_file "fileB"

    _break_link_move_file "fileA" "fileA.new"

    RESULTS=$($BEND $FILE_SYSTEM_ROOT "fileA" "fileA.new" 2>&1) # rename all fileA to fileA.new

    assertLinkOk "fileA" "fileA.new"
    assertLinkOk "fileB" "fileB"
}

testFullPath () {
    _make_test_file "fileA"
    _make_test_file "fileB"

    _break_link_move_file "fileA" "fileA.new"

    REGEX_OLD=$(escape_simple_path "$DATA_DIR/fileA")
    REPLACEMENT=$(escape_slash "$DATA_DIR/fileA.new")
    RESULTS=$($BEND $FILE_SYSTEM_ROOT "$REGEX_OLD" "$REPLACEMENT" 2>&1)

    assertLinkOk "fileA" "fileA.new"
    assertLinkOk "fileB" "fileB"
}

_testFullPathWithStrageChars () {
    file_name="fileA$1bla"
    _make_test_file "$file_name"

    _break_link_move_file "$file_name" "$file_name.new"

    REGEX_OLD=$(escape_simple_path "$DATA_DIR/$file_name")
    REPLACEMENT=$(escape_slash "$DATA_DIR/$file_name.new")
    RESULTS=$($BEND "$FILE_SYSTEM_ROOT" "$REGEX_OLD" "$REPLACEMENT" 2>&1)

    assertLinkOk "$file_name" "$file_name.new"
    assertLinkOk "fileB" "fileB"
}

testFullPathWithStrangeChars () {
    _make_test_file "fileB"
    _testFullPathWithStrageChars "A"
    _testFullPathWithStrageChars "#"
    _testFullPathWithStrageChars "0"
    _testFullPathWithStrageChars "!"
    _testFullPathWithStrageChars "."
    _testFullPathWithStrageChars ";"
    _testFullPathWithStrageChars ","
    _testFullPathWithStrageChars "$"
    _testFullPathWithStrageChars "["
    _testFullPathWithStrageChars "("
    _testFullPathWithStrageChars "{"
    _testFullPathWithStrageChars "]"
    _testFullPathWithStrageChars "[.]"
    _testFullPathWithStrageChars " "
}

testRegex () {
    _make_test_file "fileA.0"
    _make_test_file "dataA.1"
    _make_test_file "fileA.0ok"
    _make_test_file "fileB"

    _break_link_move_file "fileA.0" "file0-A"
    _break_link_move_file "dataA.1" "data1-A"

    RESULTS=$($BEND "$FILE_SYSTEM_ROOT" "([a-z]+)([A-Z])+\.([01])$" "\1\3-\2" 2>&1)

    assertLinkOk "fileA.0" "file0-A"
    assertLinkOk "dataA.1" "data1-A"
    assertLinkOk "fileB" "fileB"
    assertLinkOk "fileA.0ok" "fileA.0ok"
}


testDryRun () {
    _make_test_file "fileA"

    OLD_TARGET=$(realpath "$DATA_DIR/fileA")
    NEW_TARGET=$(realpath "$DATA_DIR/fileA.new")

    SYMLINK=$(realpath "$FILE_SYSTEM_ROOT/fileA")
    assertTrue "1 target exists $SYMLINK" "[ -f '$SYMLINK' ]"
    assertEquals "1 target matches $OLD_TARGET" "$OLD_TARGET" "$SYMLINK"

    _break_link_move_file "fileA" "fileA.new"

    SYMLINK=$(realpath "$FILE_SYSTEM_ROOT/fileA")
    assertFalse "2 target does not exist $SYMLINK" "[ -f '$SYMLINK' ]"
    assertEquals "2 target matches $OLD_TARGET" "$OLD_TARGET" "$SYMLINK"

    RESULTS=$($BEND -D "$FILE_SYSTEM_ROOT" "fileA" "fileA.new" 2>&1)

    SYMLINK=$(realpath "$FILE_SYSTEM_ROOT/fileA")
    assertFalse "3 target does not exist $SYMLINK" "[ -f '$SYMLINK' ]"
    assertEquals "3 target matches $OLD_TARGET" $OLD_TARGET "$SYMLINK"

    RESULTS=$($BEND "$FILE_SYSTEM_ROOT" "fileA" "fileA.new")

    SYMLINK=$(realpath "$FILE_SYSTEM_ROOT/fileA")
    assertTrue "4 target exists $SYMLINK" "[ -f '$SYMLINK' ]"
    assertEquals "4 target matches $NEW_TARGET" $NEW_TARGET "$SYMLINK"

}

testUtilsOldAndNewDir () {
    OLD_DIR="/root/to/old/"
    assertTrue "old root does not exist" "[ ! -e '$OLD_DIR' ]"
    NEW_DIR=$(realpath -m "$FILE_SYSTEM_ROOT/root/to/new/")
    # new_dir must exist
    mkdir -p "$NEW_DIR"

    OLD_DIR=$(old_dir "$OLD_DIR")
    NEW_DIR=$(new_dir "$NEW_DIR")
    PWD_ESC=$(escape_simple_path $(realpath "$FILE_SYSTEM_ROOT"))

    assertEquals "OLD_DIR correct" "^\/root\/to\/old\/(.*)$" "$OLD_DIR"
    assertEquals "NEW_DIR correct" "$PWD_ESC\/root\/to\/new\/\1" "$NEW_DIR"

    RESULT=$(echo "/root/to/old/subdir/fileA" | sed -r "s/$OLD_DIR/$NEW_DIR/g")
    assertEquals "result" $(realpath -m "$FILE_SYSTEM_ROOT/root/to/new/subdir/fileA") "$RESULT"
}


testSymlinkToSymlink () {
    touch "$DATA_DIR/fileA"
    ln -s $(realpath "$DATA_DIR/fileA") "$DATA_DIR/symlinkA"
    ln -s $(realpath -s "$DATA_DIR/symlinkA") "$FILE_SYSTEM_ROOT/symlinkA"
    assertLinkOk "symlinkA" "symlinkA"
    assertLinkOk "symlinkA" "fileA"


    # move only the symlink in data_dir
    mv $DATA_DIR/symlinkA $DATA_DIR/symlinkA.new
    assertFalse "symlink in fs broken" "[ -e '$FILE_SYSTEM_ROOT/symlinkA' ]"
    assertTrue "symlink in data ok ($TARGET)" "[ -e '$DATA_DIR/symlinkA.new' ]"

    RESULT=$($BEND "$FILE_SYSTEM_ROOT" "symlinkA" "symlinkA.new" 2>&1)

    assertTrue "simlink is fixed" "[ -e '$FILE_SYSTEM_ROOT/symlinkA' ]"


    assertLinkOk "symlinkA" "symlinkA.new"
    assertLinkOk "symlinkA" "fileA"

}

source shunit2
