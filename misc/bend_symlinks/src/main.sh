# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Timm Fitschen (t.fitschen@indiscale.com)
# Copyright (C) 2019 IndiScale (info@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

source "src/utils.sh"

### PARSE COMMAND LINE ARGUMENTS ###

# OPTIONS -v -h
IS_DRY_RUN=0
IS_MOVE=0
while getopts ":hvdD" opt; do
  case ${opt} in
    h )
      echo "$HELP"
      exit 0
      ;;
    v )
      echo "$VERSION"
      exit 0
      ;;
    d )
      IS_MOVE=1
      ;;
    D )
      IS_DRY_RUN=1
      ;;
    \? )
      echo "Invalid option. See '$0 -h' for more information" 1>&2
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))


# POSTIONAL ARGUMENTS
if [ $# -ne 3 ] ; then
    echo "Illegal number of positional parameters. See '$0 -h' for more information" 1>&2
    exit 1
fi
FILE_SYSTEM_ROOT=$1
REGEX_OLD=$2
REPLACEMENT=$3


if [ $IS_MOVE -eq 1 ] ; then
    REGEX_OLD=$(old_dir "$REGEX_OLD")
    REPLACEMENT=$(new_dir "$REPLACEMENT")
fi


set -o noglob
find -P $(realpath $FILE_SYSTEM_ROOT) -type l -print0 |
        while ISF= read -r -d '' syml; do
  OLD_TARGET=$(realpath -m "$syml" | sed -n -r "/$REGEX_OLD/p")
  if [ -z "$OLD_TARGET" ] ; then
    # filter non matching
    continue
  fi

  if [ -e "$syml" ] ; then
    echo "#IGNORING (not broken): $syml" 1>&2
    continue
  fi

  NEW_TARGET=$(echo "$OLD_TARGET" | sed -r "s/$REGEX_OLD/$REPLACEMENT/g")
  if [ ! -e "$NEW_TARGET" ] ; then
    echo "#IGNORING (broken new): $NEW_TARGET" 1>&2
    continue
  fi

  echo -e "$OLD_TARGET\t$NEW_TARGET"
  if [ $IS_DRY_RUN -eq 1 ] ; then
    continue
  fi

  # -f means force overwriting
  ln -fs "$NEW_TARGET" "$syml" ;

done
set +o noglob

