#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019-2020 Timm Fitschen (t.fitschen@indiscale.com)
# Copyright (C) 2019-2020 IndiScale GmbH (info@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
set -o errexit -o noclobber -o nounset -o pipefail

function escape_simple_path () {
    SPATH=$(escape_slash "$1")
    # ?
    SPATH=$(echo "$SPATH" | sed "s/\?/\\?/g")
    # .
    SPATH=$(echo "$SPATH" | sed "s/\./\\\\./g")
    # $
    SPATH=$(echo "$SPATH" | sed "s/\\$/\\\\$/g")
    # [
    SPATH=$(echo "$SPATH" | sed -r "s/\[/\\\\[/g")
    # (
    SPATH=$(echo "$SPATH" | sed -r "s/\(/\\\\(/g")
    # {
    SPATH=$(echo "$SPATH" | sed -r "s/\{/\\\\{/g")
    # white space
    SPATH=$(echo "$SPATH" | sed -r "s/ /\\ /g")
    echo "$SPATH"
}

function escape_slash () {
    echo "${1//\//\\\/}"
}


function old_dir () {
    OLD_DIR=$(realpath -m "$1")
    OLD_DIR=$(escape_simple_path "$OLD_DIR")
    echo "^$OLD_DIR\/(.*)$"
}

function new_dir () {
    NEW_DIR=$(realpath -m "$1")
    NEW_DIR=$(escape_slash "$NEW_DIR")
    echo "$NEW_DIR\/\1"
}
