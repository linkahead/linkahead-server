/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */
// Pam Authentication
// A. Schlemmer, 07/2018

/*
Note: This program needs sufficient right to authenticate against anyone but
oneself.  This can be done for example by changing the effective group id:

```
$ ls -l bin
-rwxrwsrwx 1 root shadow 16992 Apr 28 07:45 pam_authentication
```
*/

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>

#include <security/pam_appl.h>
#include <security/pam_misc.h>

char *password; // needs to be global

int supply_password(int num_msg, const struct pam_message **msgm,
                    struct pam_response **responsep, void *appdata_ptr) {
  struct pam_response *response =
    (struct pam_response*)calloc(sizeof(struct pam_response), num_msg);
  int i;
  for (i=0; i<num_msg; i++) {
    if (msgm[i]->msg_style == PAM_PROMPT_ECHO_OFF) {
      response[i].resp = strdup(password);
      response[i].resp_retcode = 0;
    }
  }
  *responsep = response;
  return PAM_SUCCESS;
}

static struct pam_conv conv =
  {
   supply_password,
   NULL
  };

/**
 * @brief      Obtain the password from the given file.
 *
 * @param      filename: If "-", read from stdin.
 *
 * @return     0 if successful, else 1.
 */
bool get_password(char *filename) {
  // With code from https://stackoverflow.com/a/1196696/232888
  // by user https://stackoverflow.com/users/89266/dfa
  struct termios backup, secret_setting;
  bool is_tty = isatty(fileno(stdin));
  /* disable echo */
  if (is_tty) {
    tcgetattr(fileno(stdin), &backup);
    secret_setting = backup;
    secret_setting.c_lflag &= ~ECHO;
    secret_setting.c_lflag |= ECHONL;

    if (tcsetattr(fileno(stdin), TCSANOW, &secret_setting) != 0) {
      perror("Setting echo-less output flags failed.");
      return EXIT_FAILURE;
    }
  }

  FILE *pwfile;
  if (strcmp(filename, "-")) {
    pwfile = fopen(filename, "r");
    if (pwfile == NULL) {
      perror(filename);
      if (is_tty) {
        tcsetattr(fileno(stdin), TCSANOW, &backup);
      }
      return false;
    }
  } else {
    pwfile = stdin;
    printf("Enter the password: ");
  }
  password = NULL;
  size_t n = 0;
  ssize_t pwlen = getline(&password, &n, pwfile);
  if (strcmp(filename, "-")) {
    fclose(pwfile);
  }
  password[pwlen - 1] = 0;

  /* restore terminal settings */
  if (is_tty) {
    if (tcsetattr(fileno(stdin), TCSANOW, &backup) != 0) {
      perror("Resetting output flags failed.");
    }
  }

  return true;
}

int main(int argc, char **argv) {
  if (argc < 2 || argc > 3) {
    fprintf(stderr, "Usage: pam_authentication <username> [<password_file>]\n");
    return 2;
  }

  pam_handle_t *pamh;
  char *username = argv[1];
  char *pw_file = "-";
  if (argc == 3) {
    pw_file = argv[2];
  }

  if (! get_password(pw_file)) {
    fprintf(stderr, "Error while reading password.\n");
    return 3;
  }

  int res = pam_start("login", username, &conv, &pamh);

  if (!res == PAM_SUCCESS) {
    fprintf(stderr, "Error in starting pam authentication.\n");
    return 2;
  }
  /* printf("\n>%s<\n", password);  // Warning: this prints the password! */
  res = pam_authenticate(pamh, 0);

/* printf("PAM_AUTH_ERR: %i\n\ */
/* PAM_CRED_INSUFFICIENT: %i\n\ */
/* PAM_AUTHINFO_UNAVAIL: %i\n", PAM_AUTH_ERR, PAM_CRED_INSUFFICIENT, PAM_AUTHINFO_UNAVAIL); */
/*   printf("Return code (success=%i) %i: %s\n", PAM_SUCCESS, res, pam_strerror(pamh, res)); */
  free(password);
  
  return res;
}
